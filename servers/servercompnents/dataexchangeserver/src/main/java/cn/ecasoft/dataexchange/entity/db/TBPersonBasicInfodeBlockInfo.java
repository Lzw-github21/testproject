package cn.ecasoft.dataexchange.entity.db;

import org.nutz.dao.entity.annotation.Table;

import java.sql.Timestamp;

@Table("TBPersonBasicInfodeBlockInfo")
public class TBPersonBasicInfodeBlockInfo {
    public String rowGuid;
    public Integer id;
    public String idCard;
    public String sgxkGuid;
    public Integer deBlockManageDepNum;
    public String deBlockUserName;
    public Timestamp deBlockDate;
    public String deBlockMark;
    public Integer lockType;
    public String cityNum;
    public String countyNum;
    public String corpCode;
    public String typeState;
    public String auditName;
    public String auditDateTime;
    public String auditMark;
    public Integer unlockType;
    public String recordGuid;

}
