package cn.ecasoft.dataexchange.entity.dto;

import cn.ecasoft.dataexchange.entity.db.TBProjectInfoChange;
import lombok.Getter;
import lombok.Setter;

/**
 * @author XuLiuKai
 */
@Setter
@Getter
public class TBProjectInfoChangeDto extends TBProjectInfoChange {

    private String PRINTNUMBER;
    private String USERPWD2;
    private String ZJLYGJZFCZZJTZ;
    private String ZJLYGYQYZJTZ;
    private String ZJLYGJRZ;
    private String ZJLYSYGJZZHZWGZFZJ;
    private String ZJLYJTJJZZTZ;
    private String ZJLYWSTZ;
    private String ZJLYGGTTZ;
    private String ZJLYSYTZ;
    private String ZJLYQTZJLY;
    private String ecoPropertyNum;
    private String STATUSNUM1;
    private String rowguid;


}
