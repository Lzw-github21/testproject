package cn.ecasoft.dataexchange.common.utils;

import java.io.InputStream;
import java.util.Date;

/**
 * @author: mhd
 * @date: 2021/6/12 15:05
 * Description: 文件操作方法
 */
public interface FileStorage {

    // 上传

    Boolean upload(String bucketName, String objectName, byte[] data);

    Boolean upload(String bucketName, String objectName, InputStream stream);


    // 下载

    InputStream download(String bucketName, String objectName);

    byte[] downloadByte(String bucketName, String objectName);

    // 删除

    Boolean delete(String bucketName, String objectName);

    // 存在

    Boolean exist(String bucketName, String objectName);

    // 获取url下载

    String getOnlineAddress(String bucketName, String objectName, Date expiration);

//    Boolean copy(String bucketName, String sourceObj, String targetObj);
//
//    Boolean copy(String bucketName, String sourceObj, String targetBucket, String targetObj);

    // 移动

    Boolean move(String bucketName, String sourceObj, String targetObj);

//    Boolean move(String bucketName, String sourceObj, String targetBucket, String targetObj);
}
