package cn.ecasoft.dataexchange.common.mms.util;

import org.apache.commons.lang.builder.ToStringStyle;

/**
 * @description: 移动短信
 * @author: 杨攀  2020/8/12 16:35
 */
public class  SDKToStringStyle extends ToStringStyle {
    private static final long serialVersionUID = -5006999832743996942L;

    public static SDKToStringStyle getInstance() {
        return new SDKToStringStyle();
    }

    private SDKToStringStyle() {
        this.setContentStart("\t");
        this.setContentEnd("");
        this.setFieldNameValueSeparator(":");
        this.setArrayStart("[");
        this.setArrayEnd("]");
        this.setArraySeparator(",");
        this.setNullText("");
        this.setFieldSeparator("\t");
        this.setUseClassName(true);
        this.setUseShortClassName(true);
        this.setUseIdentityHashCode(false);
    }
}