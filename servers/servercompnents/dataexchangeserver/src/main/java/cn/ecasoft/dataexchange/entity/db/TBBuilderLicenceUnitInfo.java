package cn.ecasoft.dataexchange.entity.db;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Table;

import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@Table("TBBuilderLicenceUnitInfo")
public class TBBuilderLicenceUnitInfo {

    private String row_Guid;
    private Integer id;
    private String unitCode;
    private String subPrjName;
    private BigDecimal invest;
    private BigDecimal buildArea;
    private String floorCount;
    private String bottomFloorCount;
    private BigDecimal buildHeight;
    private String prjLevelNum;
    private String pjrSize;
    private String memo;
    private BigDecimal subProjectLength;
    private BigDecimal subProjectSpan;
    private String structureTypeNum;
    private BigDecimal floorBuildArea;
    private BigDecimal bottomFloorBuildArea;
    private String prjGuid;
    private String isSelf;
    private String sgxkGuid;
    private BigDecimal rfBottomArea;
    private String zlNum;
    private Integer isShockisolationBuilding;
    private Integer isGreenBuilding;
    private String greenBuidingLevel;
    private String seismicIntensityScale;
    private Integer isSuperHightBuilding;
    private Integer suiteCount;
    private BigDecimal structureHeight;
    private BigDecimal singleSpanRC;
    private BigDecimal singleSpanHS;
    private Date createTime;
    private Date upDateTime;
}