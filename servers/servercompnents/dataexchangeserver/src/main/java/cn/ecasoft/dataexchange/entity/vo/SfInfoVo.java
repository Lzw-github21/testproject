package cn.ecasoft.dataexchange.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Dee
 * @date 2023/7/17
 * <p>Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SfInfoVo {
    // 勘察
    private List<SfInfoVo> econCorpInfo;
    // 设计
    private List<SfInfoVo> designCorpInfo;
    // 施工
    private List<SfInfoVo> consCorpInfo;
    // 监理
    private List<SfInfoVo> superCorpName;
    // 总承包
    private List<SfInfoVo> zcbCorpInfo;

    private String uploadDate;
    private String rowGuid;

}
