package cn.ecasoft.dataexchange.services.impl;

import cn.ecasoft.basic.JsonData;
import cn.ecasoft.basic.datatable.DataRow;
import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.configproperties.EcaSqlsConfig;
import cn.ecasoft.dataexchange.services.StaffExtendService;
import cn.ecasoft.utils.DBhelper;
import cn.ecasoft.utils.EcaConfig;
import cn.ecasoft.utils.SecurityHelper;
import cn.ecasoft.utils.SessionHelper;
import cn.ecasoft.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author 李志威
 * @Description
 * @date 2023/8/24
 */
@Service
@Slf4j
public class StaffExtendServiceImpl implements StaffExtendService {
    @Autowired
    private DBhelper dBhelper;

    @Autowired
    private SessionHelper sessionHelper;

    @Autowired
    private SecurityHelper securityHelper;

    @Autowired
    private EcaSqlsConfig ecaSqlsConfig;

    @Autowired
    private EcaConfig ecaConfig;
    /**
     * SQL 根据创建人 查询 子用户(SYS_STAFFEXTEND)
     */
    private static final String SQLKEY_MODEL_SELECTEXTENDSTAFF_GETALLINFOBYCREATEDBY = "model-selectextendstaff-getallinfobycreatedby";
    /**
     * SQL 根据 *** 查询 子用户(SYS_STAFFEXTEND)
     */
    private static final String SQLKEY_MODEL_SELECTSTAFFLISTBYUSERID = "model-selectstafflistbyuserid";

    /**
     * SQL 根据 st_id和创建人查询 子用户(SYS_STAFFEXTEND)
     */
    private static final String SQLKEY_MODEL_SELECTEXTENDSTAFF = "model-selectextendstaff";

    /**
     * SQL 根据登录名 查询 人员定义(sys_staffdef) -> 重复校验
     */
    private static final String SQLKEY_MODEL_SELECTSTAFF = "model-selectstaff";


    /**
     * SQL 更新 子用户(SYS_STAFFEXTEND)
     */
    private static final String SQLKEY_MODEL_UPDATEEXTENDSTAFF = "model-updateextendstaff";

    /**
     * SQL 添加 子用户_标签
     */
    private static final String SQLKEY_MODEL_INSERTEXTENDSTAFFDEF_CREATELABEL = "model-insertextendstaffdef-createlabel";

    /**
     * SQL 查询 子用户_标签
     */
    private static final String SQLKEY_MODEL_SELECTEXTENDSTAFFDEF_SELECTLABEL = "model-selectextendstaffdef-selectlabel";

    /**
     * SQL 修改 子用户_标签
     */
    private static final String SQLKEY_MODEL_MODIFYTEXTENDSTAFFDEF_MODIFYLABEL = "model-modifytextendstaffdef-modifylabel";

    /**
     * SQL 删除  子用户_标签
     */
    private static final String SQLKEY_MODEL_DELETETEXTENDSTAFFDEF_DELETELABEL = "model-deletetextendstaffdef-deletelabel";

    private static final String SQLKEY_MODEL_UPDATESTAFFDEFEXTEND = "UPDATE SYS_STAFFEXTEND SET LABELNAME=@LABELNAME, DESCRIPTION=@DESCRIPTION, ABLEMOUDLES=@ABLEMOUDLES WHERE isDelete is null and LABELNAME = @LABELNAMEOLD AND CREATED_BY = @CREATED_BY AND ISUSER = 1";


    @Override
    public JsonData getAccountList(HttpServletRequest request) throws Exception {
        //响应对象 responseResult
        JsonData responseResult = new JsonData();
        //查询 selectCondition
        HashMap<String, Object> selectCondition = new HashMap<>(16);
        //根据创建人 查询 子用户
        Integer userId = Integer.valueOf(this.sessionHelper.getUserInfo(request.getHeader("sessionId")).getUserId());
        selectCondition.put("CREATED_BY", userId);
        String MODEL_SELECTEXTENDSTAFF_SQL = "SELECT a.isAreaAdmin,a.DEPTNAME,a.DEPARTNAME,a.ID,a.ST_ID,a.ISUSER,a.LABELNAME,a.DESCRIPTION,a.ABLEMOUDLES,a.ROW_GUID,a.IDCARD,a.PROVINCENUM,a.CITYNUM,a.AREANUM,b.adminareaname FROM SYS_STAFFEXTEND a left join tbxzqdmdic b on a.areanum = b.class4ID WHERE a.CREATED_BY = @CREATED_BY and a.isDelete is null";
        if (userId.toString().equals("1003") || userId.toString().equals("40460")) {
            MODEL_SELECTEXTENDSTAFF_SQL = "SELECT a.isAreaAdmin,a.DEPTNAME,a.DEPARTNAME,a.ID,a.ST_ID,a.ISUSER,a.LABELNAME,a.DESCRIPTION,a.ABLEMOUDLES,a.ROW_GUID,a.IDCARD,a.PROVINCENUM,a.CITYNUM,a.AREANUM,b.adminareaname FROM SYS_STAFFEXTEND a left join tbxzqdmdic b on a.areanum = b.class4ID WHERE (a.CREATED_BY = '1003' or a.CREATED_BY = '40460') and a.isDelete is null";
        }
        ecaSqlsConfig.getMap().put("SQLKEY_MODEL_SELECTEXTENDSTAFF_GETALLINFOBYCREATEDBY", MODEL_SELECTEXTENDSTAFF_SQL);
        DataTable dt = this.dBhelper.QueryDataTable("", "SQLKEY_MODEL_SELECTEXTENDSTAFF_GETALLINFOBYCREATEDBY", selectCondition);
        ecaSqlsConfig.getMap().remove("SQLKEY_MODEL_SELECTEXTENDSTAFF_GETALLINFOBYCREATEDBY");
        //过滤条件
        DataRow[] childAccountRows = dt.Select("ISUSER = 1");
        responseResult.put("childAccountData", childAccountRows);
        responseResult.put("userData", null);
        if (childAccountRows.length > 0) {
            List<String> filter = new ArrayList<>();
            for (DataRow row : childAccountRows) {
                //添加 关联人员ID外键
                filter.add(row.getString("ST_ID"));
            }
            selectCondition.put("USERIDLIST", "'" + String.join("','", filter) + "'");
            responseResult.put("userData", this.dBhelper.QueryDataTable("", SQLKEY_MODEL_SELECTSTAFFLISTBYUSERID, selectCondition, selectCondition));
        }
        responseResult.put("labelData", dt.Select("ISUSER = 0 "));

        String selectAllLabelDataSqlKey = UUID.randomUUID().toString();
        String selectAllLabelDataSql = "SELECT a.isAreaAdmin,a.DEPTNAME,a.DEPARTNAME,a.ID,a.ST_ID,a.ISUSER,a.LABELNAME,a.DESCRIPTION,a.ABLEMOUDLES,a.ROW_GUID,a.IDCARD,a.PROVINCENUM,a.CITYNUM,a.AREANUM,b.adminareaname FROM SYS_STAFFEXTEND a left join tbxzqdmdic b on a.areanum = b.class4ID WHERE a.isDelete is null and a.ISUSER = 0";
        ecaSqlsConfig.getMap().put(selectAllLabelDataSqlKey, selectAllLabelDataSql);
        DataTable allLabelData = dBhelper.QueryDataTable("", selectAllLabelDataSqlKey, selectCondition);
        ecaSqlsConfig.getMap().remove(selectAllLabelDataSqlKey);

        responseResult.put("allLabelData", allLabelData);
        return responseResult;
    }

    @Override
    public JsonData getAccountNameList(String countyNum, HttpServletRequest request) throws Exception {
        HashMap<String, Object> queryMap = new HashMap<>();
        //构造查询人员列表sql
        StringBuilder selectUserListSqlBuilder = new StringBuilder();
        selectUserListSqlBuilder.append("select a.st_id,a.st_name,a.mobiletel,b.deptname,b.userType from sys_staffdef a inner join sys_staffextend b on a.st_id = b.st_id where b.isDelete is null and (a.nflag <> 1 or a.nflag is null)  and (a.IsRetirement <> 1 or a.IsRetirement is null) and  a.COUNTYNUM = @countyNum");
        queryMap.put("countyNum", countyNum);
        String selectUserListSql = selectUserListSqlBuilder.toString();
        ecaSqlsConfig.getMap().put("selectUserListSqlKey", selectUserListSql);
        DataTable userList = dBhelper.QueryDataTable("", "selectUserListSqlKey", queryMap);
        ecaSqlsConfig.getMap().remove("selectUserListSqlKey");
        JsonData jsonData = new JsonData();
        jsonData.put("msg", "成功");
        jsonData.put("code", 200);
        jsonData.put("data", userList);
        return jsonData;
    }

    @Override
    public JsonData getAccountByUserId(String userId) throws Exception {
        HashMap<String, Object> queryMap = new HashMap<>();
        queryMap.put("userId", userId);
        String selectUserSql = "select c.cityname,c.countyName,a.roleTypeNum,a.mobile,a.st_id,a.IPADDRESS,a.ST_LOGINNAME,a.st_name,b.row_guid,b.created_by,b.labelname,b.labelGuid,b.description,b.ablemoudles,a.IDCARD,a.countyNum,a.MOBILETEL,b.DEPTNAME,b.DEPTNAME as organName,b.DEPARTNAME,b.userType,IFNULL(b.isAreaAdmin,0) as isAreaAdmin from sys_staffdef a inner join sys_staffextend b on a.st_id = b.st_id inner join sys_deptdef_extend c on a.countyNum = c.countyNum where b.isDelete is null and b.isuser = 1 and a.st_id = @userId";
        ecaSqlsConfig.getMap().put("selectUserSqlKey", selectUserSql);
        DataTable userList = dBhelper.QueryDataTable("", "selectUserSqlKey", queryMap);
        ecaSqlsConfig.getMap().remove("selectUserSqlKey");
        JsonData jsonData = new JsonData();
        jsonData.put("msg", "成功");
        jsonData.put("code", 200);
        jsonData.put("data", userList);
        return jsonData;
    }

    @Override
    public JsonData createChildAccount(String stName, String stLoginName, String ableMoudles, String provinceNum, String cityNum, String password,
                                       String areaNum, String idCard, String mobileTel, String userType, String isAreaAdmin, String organName, String telephone, String roleTypeNum, HttpServletRequest request) throws Exception {
        JsonData responseResult = new JsonData();
        HashMap<String, Object> insertCondition = new HashMap<>();
        //1.根据登录名 查询 人员定义 -> 重复校验
        //登录名
        String loginName = null;
        String loginArea = "";
        if (StringUtils.isBlank(stLoginName)) {
            switch (roleTypeNum) {
                case "1":
                    loginArea = provinceNum;
                    break;
                case "2":
                    loginArea = cityNum;
                    break;
                case "3":
                    loginArea = areaNum;
                    break;
                default:
                    loginArea = "330100";
            }
            loginName = this.getLoginName(loginArea).get("loginName").toString();
            insertCondition.put("ST_LOGINNAME", loginName);
        } else {
            insertCondition.put("ST_LOGINNAME", stLoginName);
        }
        while (this.dBhelper.QueryDataTable("", SQLKEY_MODEL_SELECTSTAFF, insertCondition).HasRow()) {
            loginName = this.getLoginName(areaNum).get("loginName").toString();
            insertCondition.put("ST_LOGINNAME", loginName);
        }
        //根据子账号手机号判断重复
        insertCondition.put("MOBILETEL", mobileTel);
        String judgeSql = "select * from sys_staffextend where MOBILETEL = @MOBILETEL and isDelete is null";
        ecaSqlsConfig.getMap().put("judgeSqlKey", judgeSql);
        DataTable userList = dBhelper.QueryDataTable("", "judgeSqlKey", insertCondition);
        ecaSqlsConfig.getMap().remove("judgeSqlKey");
        if (userList.HasRow()) {
            responseResult.put("success", false);
            responseResult.put("message", "已存在相同手机号的子账号，请勿重复添加!");
            return responseResult;
        }
        //判断添加子账号人员类型
        Integer userId = Integer.valueOf(this.sessionHelper.getUserInfo(request.getHeader("sessionId")).getUserId());

        int userStId = dBhelper.GetPkValue("", "SYS_STAFFDEF", "ST_ID");
        //2.1 st_id
        insertCondition.put("ST_ID", userStId);
        //2.2 登录名
        insertCondition.put("ST_NAME", stName);
        //2.3 密码
        insertCondition.put("ST_PASSWD", password);
        //2.4  手机号
        insertCondition.put("MOBILETEL", mobileTel);
        //2.4  手机号
        insertCondition.put("telephone", telephone);
        //2.5 row_guid
        insertCondition.put("ROW_GUID", UUID.randomUUID().toString());
        //2.6 身份证
        insertCondition.put("IDCARD", idCard);
        //2.7 系统可用: 1：勾选禁用系统
        insertCondition.put("ISECAUSER", 0);
        //2.9 市区划代码
        insertCondition.put("CITYNUM", cityNum);
        //2.10 县区划代码
        insertCondition.put("AREANUM", areaNum);
        insertCondition.put("roleTypeNum", roleTypeNum);

        AtomicReference<Boolean> success = new AtomicReference<>(false);
        //开启事务
        String finalUserType = userType;
        dBhelper.StartTrans(() -> {
            try {
                String SQL_INSERTSTAFFDEF = "INSERT INTO SYS_STAFFDEF " +
                        "( IDCARD, ROW_GUID, ST_ID, ST_LOGINNAME, MOBILETEL, ST_PASSWD, ST_NAME, ISECAUSER,IPADDRESS,CITYNUM,COUNTYNUM,MOBILE,roleTypeNum) VALUES " +
                        "( @IDCARD, @ROW_GUID, @ST_ID, @ST_LOGINNAME, @MOBILETEL, @ST_PASSWD, @ST_NAME, 0,@ipAddress,@CITYNUM,@AREANUM,@telephone,@roleTypeNum)";
                ecaSqlsConfig.getMap().put("SQL_INSERTSTAFFDEF", SQL_INSERTSTAFFDEF);
                success.set(this.dBhelper.QueryInt("", "SQL_INSERTSTAFFDEF", insertCondition) > 0);
                ecaSqlsConfig.getMap().remove("SQL_INSERTSTAFFDEF");
                // 3-添加子账户
                if (success.get()) {
                    insertCondition.put("ID", this.dBhelper.GetPkValue("", "SYS_STAFFEXTEND", "ID"));
                    insertCondition.put("CREATED_BY", Integer.valueOf(this.sessionHelper.getUserInfo(request.getHeader("sessionId")).getUserId()));
                    insertCondition.put("ABLEMOUDLES", ableMoudles);
                    insertCondition.put("ISUSER", 1);
                    insertCondition.put("ROW_GUID", UUID.randomUUID().toString());
                    insertCondition.put("PROVINCENUM", provinceNum);
                    insertCondition.put("CITYNUM", cityNum);
                    insertCondition.put("AREANUM", areaNum);
                    insertCondition.put("DEPTNAME", organName);
                    String SQLKEY_MODEL_INSERTEXTENDSTAFFDEF_CREATEACCOUNT = "INSERT INTO SYS_STAFFEXTEND (DEPTNAME,DEPARTNAME,ID, CREATED_BY, ISUSER, LABELNAME, ABLEMOUDLES, ST_ID, ROW_GUID, PROVINCENUM, CITYNUM, AREANUM, IDCARD, MOBILETEL,isAreaAdmin,LABELGUID,USERTYPE) VALUES(@DEPTNAME,@DEPARTNAME,@ID, @CREATED_BY, @ISUSER, @LABELNAME, @ABLEMOUDLES, @ST_ID, @ROW_GUID, @PROVINCENUM, @CITYNUM, @AREANUM, @IDCARD, @MOBILETEL,@isAreaAdmin,@LABELGUID,@userType)";
                    ecaSqlsConfig.getMap().put("SQLKEY_MODEL_INSERTEXTENDSTAFFDEF_CREATEACCOUNT", SQLKEY_MODEL_INSERTEXTENDSTAFFDEF_CREATEACCOUNT);
                    success.set(this.dBhelper.QueryInt("", "SQLKEY_MODEL_INSERTEXTENDSTAFFDEF_CREATEACCOUNT", insertCondition) > 0);
                    ecaSqlsConfig.getMap().remove("SQLKEY_MODEL_INSERTEXTENDSTAFFDEF_CREATEACCOUNT");
                }
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }
        });
        //记录用户操作日志 @userId,@roleDesc,@dataType,@result,@msg,@exceptionInfo
        HashMap<String, Object> logMap = new HashMap<>();
        logMap.put("userId", userId);
        logMap.put("dataType", "账号创建");
        logMap.put("msg", userId + "用户创建了账号" + loginName + "详细信息" + insertCondition);
        this.migrationRecordLog(logMap, null);
        responseResult.put("success", success.get());
        return responseResult;
    }

    @Override
    public JsonData modifyChildAccount(String stName, String userId, String password, String ableMoudles, String mobileTel, String idCard, String cityNum, String areaNum, String deptName, String isAreaAdmin, String telephone, String roleTypeNum, HttpServletRequest request) throws Exception {
        JsonData responseResult = new JsonData();
        AtomicBoolean success = new AtomicBoolean(false);
        HashMap<String, Object> updateCondition = new HashMap<>(16);
        String createdBy = String.valueOf(this.sessionHelper.getUserInfo(request.getHeader("sessionId")).getUserId());
        //管理员
        String filterMoudles = ecaConfig.GetGroupConfig("ZZHFP", "ZZHFP_FILTERMOUDLES_CONFIG");
        if (StringUtils.isNotBlank(isAreaAdmin) && isAreaAdmin.equals("1")) {
            //如果是管理员账号，自动给子账号分配模块权限。
            String newAbleMoudle = ableMoudles + "," + filterMoudles;
            String[] strs = newAbleMoudle.split(",");
            Set<String> set = new HashSet<>();
            for (String str : strs) {
                set.add(str);
            }
            String[] modleMerge = set.toArray(new String[set.size()]);
            ableMoudles = String.join(",", modleMerge);
        }
        updateCondition.put("isAreaAdmin", isAreaAdmin);
        updateCondition.put("ST_ID", userId);
        updateCondition.put("ST_NAME", stName);
        updateCondition.put("MOBILETEL", mobileTel);
        updateCondition.put("IDCARD", idCard);
        updateCondition.put("password", password);
        updateCondition.put("deptName", deptName);
        updateCondition.put("AREANUM", areaNum);
        updateCondition.put("CITYNUM", cityNum);
        updateCondition.put("telephone", telephone);
        updateCondition.put("roleTypeNum", roleTypeNum);
        updateCondition.put("ABLEMOUDLES", ableMoudles);
        updateCondition.put("password", password);
        //开启事务
        dBhelper.StartTrans(() -> {
            try {
                //更新人员定义表
                String SQLKEY_MODEL_UPDATESTAFF = UUID.randomUUID().toString();
                ecaSqlsConfig.getMap().put(SQLKEY_MODEL_UPDATESTAFF, "UPDATE SYS_STAFFDEF SET st_passwd = @password,mobile = @telephone,ST_NAME=@ST_NAME, MOBILETEL=@MOBILETEL, IDCARD=@IDCARD,CITYNUM=@CITYNUM,COUNTYNUM=@AREANUM,roleTypeNum = @roleTypeNum WHERE ST_ID = @ST_ID");
                this.dBhelper.QueryInt("", SQLKEY_MODEL_UPDATESTAFF, updateCondition);
                ecaSqlsConfig.getMap().remove(SQLKEY_MODEL_UPDATESTAFF);

                String updateUserExtend = UUID.randomUUID().toString();
                ecaSqlsConfig.getMap().put(updateUserExtend, "UPDATE SYS_STAFFEXTEND SET deptname = @deptName,MOBILETEL=@MOBILETEL, IDCARD=@IDCARD, CITYNUM=@CITYNUM,AREANUM=@AREANUM, ABLEMOUDLES=@ABLEMOUDLES WHERE ST_ID = @ST_ID and isuser = 1");
                this.dBhelper.QueryInt("", updateUserExtend, updateCondition);
                ecaSqlsConfig.getMap().remove(updateUserExtend);
                success.set(true);
            } catch (Exception e) {
                success.set(false);
                responseResult.put("msg", e.getMessage());
            }
        });
        if (success.get()) {
            //记录用户操作日志 @userId,@roleDesc,@dataType,@result,@msg,@exceptionInfo
            HashMap<String, Object> logMap = new HashMap<>();
            logMap.put("userId", userId);
            logMap.put("dataType", "账号修改");
            logMap.put("msg", createdBy + "用户修改了账号" + userId + "详细信息" + updateCondition);
            this.migrationRecordLog(logMap, null);
        }
        responseResult.put("success", Boolean.valueOf(success.get()));
        return responseResult;
    }

    @Override
    public JsonData deleteChildAccount(String userId, HttpServletRequest request) throws Exception {
        JsonData responseResult = new JsonData();
        HashMap<String, Object> deleteCondition = new HashMap<>();
        String createdBy = String.valueOf(this.sessionHelper.getUserInfo(request.getHeader("sessionId")).getUserId());
        deleteCondition.put("ST_ID", userId);
        deleteCondition.put("CREATED_BY", createdBy);
        //删除子用户
        boolean success = this.dBhelper.QueryInt("", "model-deleteextendstaffdef-deleteAccount", deleteCondition) > 0;
        if (success) {
            //删除用户定义
            success = this.dBhelper.QueryInt("", "model-deletestaffdef", deleteCondition) > 0;
        }
        //记录用户操作日志 @userId,@roleDesc,@dataType,@result,@msg,@exceptionInfo
        HashMap<String, Object> logMap = new HashMap<>();
        logMap.put("userId", userId);
        logMap.put("dataType", "账号删除");
        logMap.put("msg", createdBy + "用户删除了用户" + userId);
        this.migrationRecordLog(logMap, null);
        responseResult.put("success", success);
        return responseResult;
    }

    @Override
    public JsonData getLoginName(String classID) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        params.put("classID", classID);
        String selectSqlString = "select * from tbxzqdmdic where AdminAreaClassID = @classID";
        String updateSqlString = "update tbxzqdmdic set CountNum = @CountNum where AdminAreaClassID = @classID";
        //获取当前id
        ecaSqlsConfig.getMap().put("SQLKEY_SELECT_SELECTCOUNTNUM", selectSqlString);
        DataTable dataRows = dBhelper.QueryDataTable("", "SQLKEY_SELECT_SELECTCOUNTNUM", params);
        ecaSqlsConfig.getMap().remove("SQLKEY_SELECT_SELECTCOUNTNUM");
        Integer CountNum = (dataRows.getFirst().get("CountNum") == null ? 0 : dataRows.getFirst().getInt("CountNum")) + 1;
        String loginName = classID + String.format("%04d", CountNum);
        //获取后更新区划计数表数据
        params.put("CountNum", CountNum);
        ecaSqlsConfig.getMap().put("SQLKEY_UPDATE_UPDATECOUNTNUM", updateSqlString);
        dBhelper.QueryInt("", "SQLKEY_UPDATE_UPDATECOUNTNUM", params);
        ecaSqlsConfig.getMap().remove("SQLKEY_UPDATE_UPDATECOUNTNUM");
        JsonData responseResult = new JsonData();
        responseResult.put("loginName", loginName);
        return responseResult;
    }


    @Override
    public JsonData getDepExtendtList(String roleNum, HttpServletRequest request) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        HashMap<String, Object> deptListMap = new HashMap<>();
        Boolean flag = false;
        params.put("roleNum", roleNum);
        String selectSqlString;
        if (roleNum.equals("330000")) {
            selectSqlString = "select * from sys_deptdef_extend where parentNum = @roleNum or countyNum = @roleNum";
        } else {
            selectSqlString = "select b.* from sys_deptdef_extend a inner join sys_deptdef_extend b on a.cityNum = b.parentNum where a.countyNum = @roleNum and a.layered = 2 order by b.CountyNum";
        }
        //获取部门列表
        ecaSqlsConfig.getMap().put("SQLKEY_SELECT_SELECTDEPTLIST", selectSqlString);
        DataTable dataRows = dBhelper.QueryDataTable("", "SQLKEY_SELECT_SELECTDEPTLIST", params);
        ecaSqlsConfig.getMap().remove("SQLKEY_SELECT_SELECTDEPTLIST");
        deptListMap.put(dataRows.getFirst().getString("DEPTNAME"), dataRows);
        JsonData responseResult = new JsonData();
        responseResult.put("code", 200);
        responseResult.put("msg", "成功");
        responseResult.put("data", deptListMap);
        return responseResult;
    }

    @Override
    public JsonData disableAccount(String userId, String status, HttpServletRequest request) throws Exception {
        HashMap<String, Object> dataMap = new HashMap<>();
        JsonData jsonData = new JsonData();
        dataMap.put("userId", userId);
        dataMap.put("nflag", status);
        String createdBy = String.valueOf(this.sessionHelper.getUserInfo(request.getHeader("sessionId")).getUserId());
        dataMap.put("CREATED_BY", createdBy);
        //判断修改用户是不是该账号的创建人
        String updateStaffdefSql = "update sys_staffdef set nflag = @nflag where ST_ID = @userId";
        //获取部门列表
        ecaSqlsConfig.getMap().put("updateStaffdefSqlKey", updateStaffdefSql);
        int total = dBhelper.QueryInt("", "updateStaffdefSqlKey", dataMap);
        ecaSqlsConfig.getMap().remove("updateStaffdefSqlKey");
        //记录用户操作日志 @userId,@roleDesc,@dataType,@result,@msg,@exceptionInfo
        HashMap<String, Object> logMap = new HashMap<>();
        logMap.put("userId", userId);
        logMap.put("dataType", "账号禁用");
        logMap.put("msg", createdBy + "用户禁用了账号" + userId);
        this.migrationRecordLog(logMap, null);
        jsonData.put("success", true);
        jsonData.put("code", 200);
        jsonData.put("msg", "成功");
        jsonData.put("total", total);
        return jsonData;
    }

    @Override
    public JsonData accountFilter(String condition, HttpServletRequest request) throws Exception {

        HashMap<String, Object> queryMap = new HashMap<>();
        //构造查询人员列表sql
        StringBuilder selectUserListSqlBuilder = new StringBuilder();
        selectUserListSqlBuilder.append("select a.st_id,a.st_name,a.mobiletel,a.citynum,a.COUNTYNUM,c.id as deptId,b.deptname,c.parentNum,c.layered from sys_staffdef a inner join sys_staffextend b on a.st_id = b.st_id inner join sys_deptdef_extend c on a.COUNTYNUM = c.countyNum where b.isDelete is null and (a.nflag <> 1 or a.nflag is null) and a.COUNTYNUM is not null and (a.IsRetirement <> 1 or a.IsRetirement is null) and (a.st_name like CONCAT('%',@condition,'%') or a.st_loginname like CONCAT('%',@condition,'%'))");
        queryMap.put("condition", condition);
        String selectUserListSql = selectUserListSqlBuilder.toString();
        ecaSqlsConfig.getMap().put("selectUserListSqlKey", selectUserListSql);
        DataTable userList = dBhelper.QueryDataTable("", "selectUserListSqlKey", queryMap);
        ecaSqlsConfig.getMap().remove("selectUserListSqlKey");
        JsonData jsonData = new JsonData();
        jsonData.put("msg", "成功");
        jsonData.put("code", 200);
        jsonData.put("data", userList);
        return jsonData;
    }

    @Override
    public JsonData selectDisable(String userId) throws Exception {
        HashMap<String, Object> dataMap = new HashMap<>();
        dataMap.put("userId", userId);
        String selectStaffdefSql = "select IFNULL(ISECAUSER,0) as ISECAUSER from sys_staffdef  where ST_ID = @userId";
        ecaSqlsConfig.getMap().put("selectStaffdefSqlKey", selectStaffdefSql);
        DataTable dataRows = dBhelper.QueryDataTable("", "selectStaffdefSqlKey", dataMap);
        ecaSqlsConfig.getMap().remove("selectStaffdefSqlKey");
        String ISECAUSER = null;
        if (dataRows.HasRow()) {
            DataRow dataRow = dataRows.getFirst();
            ISECAUSER = dataRow.getString("ISECAUSER");
        }
        JsonData jsonData = new JsonData();
        jsonData.put("status", ISECAUSER);
        jsonData.put("code", 200);
        jsonData.put("msg", "成功");
        return jsonData;
    }


    // 随机生成 8 位密码
    public static String generatePassword() {
        int length = 10; // 密码长度
        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; // 密码字符集

        Random random = new Random();
        StringBuilder password = new StringBuilder();

        for (int i = 0; i < length; i++) {
            int index = random.nextInt(chars.length());
            password.append(chars.charAt(index));
        }
        return password.toString();
    }

    /**
     * 记录子账号操作日志
     *
     * @return
     * @userId,@roleDesc,@dataType,@result,@msg,@exceptionInfo
     */
    @GetMapping("/migrationRecordLog")
    public void migrationRecordLog(HashMap<String, Object> logMap, Exception e) {
        try {
            String exception = null;
            if (e != null) {
                exception = e.getMessage();
            }
            if (!logMap.containsKey("exceptionInfo")) {
                logMap.put("exceptionInfo", exception);
            }
            logMap.put("rowGuid", UUID.randomUUID().toString());
            String insertMigrationRecordLogSql = "insert into sys_staffextend_log (userId,dataType,resultType,msg,exceptionInfo) values (@userId,@dataType,@result,@msg,@exceptionInfo)";
            //获取部门列表
            ecaSqlsConfig.getMap().put("insertMigrationRecordLogSqlKey", insertMigrationRecordLogSql);
            dBhelper.QueryDataTable("", "insertMigrationRecordLogSqlKey", logMap);
            ecaSqlsConfig.getMap().remove("insertMigrationRecordLogSqlKey");
            return;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 获取用户信息
     */
    @GetMapping("/getUserInfo")
    private DataTable getUserInfo(@RequestParam("userId") String userId) throws Exception {
        HashMap<String, Object> queryMap = new HashMap<>();
        queryMap.put("userId", userId);
        String selectUserSql = "select * from sys_staffdef a left join sys_staffextend b on a.st_id = b.st_id where b.isDelete is null and b.isuser = 1 and (a.nflag <> 1 or a.nflag is null) and a.isecauser = 0 and (a.IsRetirement <> 1 or a.IsRetirement is null) and a.st_id = @userId";
        ecaSqlsConfig.getMap().put("selectUserSqlKey", selectUserSql);
        DataTable userList = dBhelper.QueryDataTable("", "selectUserSqlKey", queryMap);
        ecaSqlsConfig.getMap().remove("selectUserSqlKey");
        return userList;
    }
}
