package cn.ecasoft.dataexchange.entity.db;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Table;

@Getter
@Setter
@Table("TBSFCorpInfo_Change")
public class TBSFCorpInfoChange {

    private int id;
    private String corpTypeNum;
    private String corpName;
    private String corpCode;
    private String corpLeader;
    private int corpLeaderCardType;
    private String corpLeaderCardNum;
    private String sgxkGuid;
    private String row_Guid;
    private String corpLeaderPhone;
    private String changeGuid;
    private String toRowGuid;
    private int changeID;

}
