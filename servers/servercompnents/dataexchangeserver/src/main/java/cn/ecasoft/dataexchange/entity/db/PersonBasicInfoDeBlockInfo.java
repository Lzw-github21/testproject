package cn.ecasoft.dataexchange.entity.db;

import org.nutz.dao.entity.annotation.Table;

import java.util.Date;


@Table("TBPersonBasicInfodeBlockInfo")
public class PersonBasicInfoDeBlockInfo {

    public Integer id;
    public String row_Guid;
    public String idCard;
    public String sgxkGuid;
    public String deBlockManageDepNum;
    public String deBlockUserName;
    public Date deBlockDate;
    public String deBlockMark;
    public Integer lockType;
    public String cityNum;
    public String countyNum;
    public String corpCode;
    public int typeState;
    public String auditName;
    public String auditDateTime;
    public String auditMark;
    public Integer unlockType;
    public String recordGuid;

}