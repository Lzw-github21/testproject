package cn.ecasoft.dataexchange.entity.enums;

public enum MaterialMapping {
    MATERIAL_376("376", "101", "依法办理用地批准手续的证明文件（根据项目实际情况，可以是建设用地划拨决定书、建设用地批准书、建设用地使用权证、不动产权证中的一种）"),
    MATERIAL_377("377", "102", "建设工程规划许可证或者乡村建设规划许可证（依法不需要取得规划许可的装饰装修工程等无需提供）"),
    MATERIAL_378("378", "103", "施工图设计文件审查合格书"),
    MATERIAL_233("233", "106", "施工场地已经基本具备施工条件的承诺书"),
    MATERIAL_234("234", "104", "建设工程施工合同（依法必须进行招标的项目同时提供中标通知书）"),
    MATERIAL_235("235", "107", "建设资金已经落实的承诺书"),
    MATERIAL_236("236", "105", "有保证工程质量和安全的具体措施（包括危险性较大分部分项工程清单及其安全管理措施）的承诺书");

    private final String oldUuid;
    private final String newUuid;
    private final String materialName;

    MaterialMapping(String oldUuid, String newUuid, String materialName) {
        this.oldUuid = oldUuid;
        this.newUuid = newUuid;
        this.materialName = materialName;
    }

    public static MaterialMapping getByOldUuid(String oldUuid) {
        for (MaterialMapping mapping : MaterialMapping.values()) {
            if (mapping.oldUuid.equals(oldUuid)) {
                return mapping;
            }
        }
        return null;
    }

    public String getNewUuid() {
        return newUuid;
    }

    public String getMaterialName() {
        return materialName;
    }
}