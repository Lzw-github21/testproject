package cn.ecasoft.dataexchange.services.impl;

import cn.ecasoft.basic.datatable.DataRow;
import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.configproperties.EcaSqlsConfig;
import cn.ecasoft.dataexchange.mapper.PushToZLJMapper;
import cn.ecasoft.dataexchange.services.PushToZLJService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * @author XuLiuKai
 * @desc 施工许可推送浙里建
 */
@Service
@Slf4j
public class PushToZLJServiceImpl implements PushToZLJService {

    @Resource
    private PushToZLJMapper pushToZLJMapper;
    @Resource
    private EcaSqlsConfig ecaSqlsConfig;

    //todo 这里还有一个步骤是放在 施工许可的mysql 内的定时事件做的
    public void pushToZljFunction() throws Exception {
        DataTable dataSet = new DataTable();
        String[] strArray1 = {"tbbuilderlicencemanage", "tbsfcorpinfo", "tbprojectfinishunitinfo"};

        for (String s : strArray1) {
            switch (s) {
                case "tbbuilderlicencemanage":
                    dataSet = pushToZLJMapper.getUnPushTbBuilderLicenceManageData();
                    break;
                case "tbsfcorpinfo":
                    dataSet = pushToZLJMapper.getUnPushTbsfcorpinfoData();
                    break;
                case "tbprojectfinishunitinfo":
                    dataSet = pushToZLJMapper.getUnPushTbprojectfinishunitinfoData();
                    break;
            }
            if (dataSet != null && !dataSet.isEmpty()) {
                SaveTable(dataSet, s);
            } else {
                log.info(s + "_new 表没有需要推送的数据");
            }
        }
    }


    private void SaveTable(DataTable dt, String strTableName) throws Exception {
        //获取表结构，这里获取的表结构是浙里建的表结构
        DataTable dataTable = pushToZLJMapper.getTableStructure(strTableName);

        StringBuilder row = new StringBuilder("INSERT INTO " + strTableName + " (");
        StringBuilder row1 = new StringBuilder(" values( ");
        HashSet<String> columnsSet = new HashSet<>();
        //所有列名的hashSet
        dataTable.getColumns().forEach((e, k) -> columnsSet.add(e.toLowerCase()));
        dt.getColumns().forEach((e, k) -> {
            //忽略大小写进行匹配
            if (columnsSet.contains(e.toLowerCase())) {
                row.append(e).append(", ");
                row1.append("@").append(e).append(", ");
            }
        });

        StringBuilder sql = row.deleteCharAt(row.length() - 2).append(") ").append(row1.deleteCharAt(row1.length() - 2)).append(" )");
        String sqlKey = "i5bDrCwOjIBDWPILh6peQA";
        ecaSqlsConfig.getMap().put(sqlKey, sql.toString());

        List<String> guids = new ArrayList<>();
        for (DataRow dataRow : dt) {
            guids.add(dataRow.getString("guid"));
            HashMap<String, Object> paramsMap = new HashMap<>();
            dataRow.forEach((e, k) -> {
                if (columnsSet.contains(e.toLowerCase())) {
                    paramsMap.put(e, k);
                }
            });
            //往浙里建数据库内插入数据
            pushToZLJMapper.insertData(sqlKey, paramsMap);
        }

        //修改推送记录，这里需要注意的是：修改的是施工许可数据库，而不是浙里建的数据库
        int i = pushToZLJMapper.updateTSRecord(strTableName + "_new", guids);
        log.info("修改" + i + "条" + strTableName + "的推送记录");
    }
}
