package cn.ecasoft.dataexchange.entity.db;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Table;

@Setter
@Getter
@Table("TBBuilderLicenceManage_ChangeDetails")
public class ChangeDetail {

    private int id;
    private String toRowGuid;
    private String dataFilled;
    private String fieldName;
    private String oldValue;
    private String newValue;
    private String rowGuid;
}
