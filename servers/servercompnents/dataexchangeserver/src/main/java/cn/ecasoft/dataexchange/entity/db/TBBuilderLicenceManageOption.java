package cn.ecasoft.dataexchange.entity.db;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class TBBuilderLicenceManageOption {
    private String row_Guid;
    private int id;
    private String sgxkGuid;
    private String optionType;
    private Integer cancelType;
    private String createUser;
    private Date createDate;
    private String optionCase;
    private Date uploadDate;
    private String barCode;
    private Integer auditStatus;
    private Date auditDate;
    private String auditMark;
    private String manageUserName;
    private String manageDeptName;
    private String consCorpCode;
    private String uploadTmHz;
    private String optionGuidHz;
}
