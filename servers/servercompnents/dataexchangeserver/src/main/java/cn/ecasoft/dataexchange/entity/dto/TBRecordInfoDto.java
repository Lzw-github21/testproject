package cn.ecasoft.dataexchange.entity.dto;

import cn.ecasoft.dataexchange.entity.db.TBRecordInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author XuLiuKai
 */
@Setter
@Getter
public class TBRecordInfoDto extends TBRecordInfo {

    private String rowGuid;
    private String UniqueNum;
    private String PrjGuid;
    private String PrjName;
    private String PrjTypeNum;
    private String Status;
    private String ShenQNum;
    private String ManageAdminAreaNum;
    private String ISZLPG;
    private Date ZLPGdate;
    private String ISAQPG;
    private Date AQPGdate;
    private String isdzzz_zl;
    private String isdzzz_zldate;
    private String isdzzz_aq;
    private String isdzzz_aqdate;
    private String isdzzz_sgxk;
    private String isdzzz_sgxkdate;
    private String isdzzz_dzzz;
    private String Is_TSSK;
    private String IsAddFj;
    private String FirstRowGuid;
    private String SFXF;
    private String SFRF;
    private String CensorTypeNum;
}
