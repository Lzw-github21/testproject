package cn.ecasoft.dataexchange.controller;

import cn.ecasoft.basic.JsonData;
import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.configproperties.EcaSqlsConfig;
import cn.ecasoft.dataexchange.services.StaffExtendService;
import cn.ecasoft.utils.DBhelper;
import cn.ecasoft.utils.SessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * 系统子账号权限管理相关接口
 *
 * @author 李志威
 * @date 2023/5/17
 */
@RestController
@RequestMapping("/creatAccount")
public class StaffExtendController {

    @Autowired
    private StaffExtendService staffService;

    @Autowired
    private SessionHelper sessionHelper;

    @Autowired
    private DBhelper dBhelper;

    @Autowired
    private EcaSqlsConfig ecaSqlsConfig;

    /**
     * 查询子账户列表
     *
     * @param request HttpServletRequest
     * @return - childAccountData 子账户信息
     * - userData 用户数据
     * - labelData 标签数据
     * @throws Exception
     */
    @GetMapping("/getAccountList")
    public JsonData getAccountList(HttpServletRequest request) throws Exception {
        return staffService.getAccountList(request);
    }

    /**
     * 通过区划编码获取区划下人员列表
     *
     * @param countyNum 部门名称
     * @return
     * @throws Exception
     */
    @GetMapping("/getAccountNameList")
    public JsonData getAccountNameList(@RequestParam(value = "countyNum") String countyNum, HttpServletRequest request) throws Exception {
        return staffService.getAccountNameList(countyNum, request);
    }

    /**
     * 通过人员id查询子账号详情
     *
     * @param userId 部门名称
     * @return
     * @throws Exception
     */
    @GetMapping("/getAccountByUserId")
    public JsonData getAccountByUserId(@RequestParam(value = "userId") String userId) throws Exception {
        return staffService.getAccountByUserId(userId);
    }

    /**
     * 创建子账户
     *
     * @param stName      使用人
     * @param stLoginName 登录账号
     * @param labelName   标签名称（根据是否是用户判断，是用户：以逗号分隔（多个），是标签时：单个值）
     * @param ableMoudles 可用的模块编号们（逗号隔开）
     * @param provinceNum 省区划代码
     * @param cityNum     市区划代码
     * @param areaNum     县区划代码
     * @param idCard      身份证
     * @param mobileTel   手机号
     * @param telephone   联系电话
     * @param organName   机构名称
     * @return
     * @throws Exception
     */
    @PostMapping("/createChildAccount")
    public JsonData createChildAccount(@RequestParam("ST_NAME") String stName,
                                       @RequestParam(value = "ST_LOGINNAME", required = false) String stLoginName,
                                       @RequestParam(value = "IPADDRESS", required = false) String ipAddress,
                                       @RequestParam(value = "LABELNAME", required = false) String labelName,
                                       @RequestParam(value = "LABELGUID", required = false) String LABELGUID,
                                       @RequestParam(value = "ABLEMOUDLES") String ableMoudles,
                                       @RequestParam("PROVINCENUM") String provinceNum,
                                       @RequestParam("CITYNUM") String cityNum,
                                       @RequestParam(value = "ST_PASSWD", required = false) String password,
                                       @RequestParam("AREANUM") String areaNum,
                                       @RequestParam("IDCARD") String idCard,
                                       @RequestParam("MOBILETEL") String mobileTel,
                                       @RequestParam(value = "userType", required = false) String userType,
                                       @RequestParam(value = "isAreaAdmin", defaultValue = "0") String isAreaAdmin,
                                       @RequestParam(value = "organName") String organName,
                                       @RequestParam(value = "telephone", defaultValue = "") String telephone,
                                       @RequestParam(value = "roleTypeNum") String roleTypeNum,
                                       HttpServletRequest request) throws Exception {

        return staffService.createChildAccount(stName, stLoginName, ableMoudles, provinceNum, cityNum, password,
                areaNum, idCard, mobileTel, userType, isAreaAdmin, organName, telephone, roleTypeNum, request);
    }

    /**
     * 修改子账户
     *
     * @param stName      使用人
     * @param ableMoudles 可用的模块编号们（逗号隔开）
     * @param cityNum     市区划代码
     * @param areaNum     县区划代码
     * @param idCard      身份证
     * @param mobileTel   手机号
     * @param telephone   联系电话
     * @param deptName    机构名称
     * @param roleTypeNum 账号所属级别
     * @return
     * @throws Exception
     */
    @PostMapping("/modifyChildAccount")
    public JsonData modifyChildAccount(@RequestParam("ST_NAME") String stName,
                                       @RequestParam(value = "ST_ID", defaultValue = "") String userId,
                                       @RequestParam(value = "ST_PASSWD", required = false) String password,
                                       @RequestParam(value = "ABLEMOUDLES", defaultValue = "") String ableMoudles,
                                       @RequestParam(value = "MOBILETEL", defaultValue = "") String mobileTel,
                                       @RequestParam(value = "IDCARD", required = false) String idCard,
                                       @RequestParam("CITYNUM") String cityNum,
                                       @RequestParam("AREANUM") String areaNum,
                                       @RequestParam("organName") String deptName,
                                       @RequestParam(value = "isAreaAdmin", required = false) String isAreaAdmin,
                                       @RequestParam(value = "telephone", required = false) String telephone,
                                       @RequestParam(value = "roleTypeNum") String roleTypeNum,
                                       HttpServletRequest request) throws Exception {
        return staffService.modifyChildAccount(stName, userId,password, ableMoudles, mobileTel, idCard, cityNum, areaNum, deptName, isAreaAdmin, telephone, roleTypeNum, request);
    }

    /**
     * 删除子用户
     *
     * @param userId  关联人员ID外键
     * @param request HttpServletRequest
     * @return
     * @throws Exception
     */
    @PostMapping("/deleteChildAccount")
    public JsonData deleteChildAccount(@RequestParam("ST_ID") String userId,
                                       HttpServletRequest request) throws Exception {
        return staffService.deleteChildAccount(userId, request);
    }


    /**
     * 生成子账号登录用户名
     *
     * @param
     * @param classID 所属区划编码
     * @return
     * @throws Exception
     */
    //@PostMapping("/getLoginName")
    public JsonData getLoginName(@RequestParam(value = "classID", defaultValue = "") String classID) throws Exception {
        return staffService.getLoginName(classID);
    }


    /**
     * 获取系统部门列表
     *
     * @return
     * @throws Exception
     */
    @GetMapping("/getDepExtendtList")
    public JsonData getDepExtendtList(@RequestParam(value = "roleNum", defaultValue = "330000") String roleNum,
                                      HttpServletRequest request) throws Exception {
        return staffService.getDepExtendtList(roleNum, request);
    }


    /**
     * 通过userId禁用账号登录
     *
     * @param userId 用户ID
     * @param status 1 禁用  0 启用
     * @return
     * @throws Exception
     */
    @GetMapping("/disableAccount")
    public JsonData disableAccount(@RequestParam("userId") String userId,
                                   @RequestParam("status") String status,
                                   HttpServletRequest request) throws Exception {
        return staffService.disableAccount(userId, status, request);
    }

    /**
     * n
     * 账号全局搜索
     *
     * @param condition
     * @param request
     * @return
     * @throws Exception
     */
    @GetMapping("/accountFilter")
    public JsonData accountFilter(@RequestParam(value = "condition") String condition,
                                  HttpServletRequest request) throws Exception {
        return staffService.accountFilter(condition, request);
    }

    /**
     * 通过userId查看账号的禁用状态
     *
     * @param userId 用户ID
     * @return
     * @throws Exception
     */
    @GetMapping("/selectDisable")
    public JsonData selectDisable(@RequestParam("userId") String userId) throws Exception {
        return staffService.selectDisable(userId);
    }

    /**
     * 获取接口报错信息
     *
     * @return
     */
    @GetMapping("/getLogServiceInfo")
    public DataTable getLogServiceInfo(@RequestParam(value = "pkid", required = false) String pkid) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        params.put("pkid", pkid);
        ecaSqlsConfig.getMap().put("SQLKEY_SELECT_LOGSERVICEINFO", "select * from sys_log_service where pkid = @pkid");
        DataTable dataRows = dBhelper.QueryDataTable("", "SQLKEY_SELECT_LOGSERVICEINFO", params);
        ecaSqlsConfig.getMap().remove("SQLKEY_SELECT_LOGSERVICEINFO");
        return dataRows;
    }
}
