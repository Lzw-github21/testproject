package cn.ecasoft.dataexchange.entity.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author xuliukai
 * @since 2023/10/7 11:48
 */
@Getter
@Setter
public class WebServiceItemRecordInVO implements Serializable {
    private String PROJECT_CODE;
    private String UNIQUE_NUM;
    private String OUTER_KEY;
    private String OUTER_TYPE;
    private String DEAL_USERID;
    private String DEAL_USERNAME;
    private String DEAL_DEPTID;
    private String DEAL_DEPTNAME;
    private String DEAL_OPINION;
    private String DEAL_STATE;
    private String DEAL_TIME;
    private String REPLY_RESULT;
    private String REPLY_NUMBER;
    private String REPLY_FILE_NAME;
    private String REPLY_FILE;
    private String UPLOAD_DATE;

    public WebServiceItemRecordInVO() {
    }

    public WebServiceItemRecordInVO(String PROJECT_CODE, String UNIQUE_NUM,
                                    String OUTER_KEY, String OUTER_TYPE, String DEAL_USERID,
                                    String DEAL_USERNAME, String DEAL_DEPTID, String DEAL_DEPTNAME,
                                    String DEAL_OPINION, String DEAL_STATE, String DEAL_TIME,
                                    String REPLY_RESULT, String REPLY_NUMBER, String REPLY_FILE_NAME,
                                    String REPLY_FILE, String UPLOAD_DATE) {
        this.PROJECT_CODE = PROJECT_CODE;
        this.UNIQUE_NUM = UNIQUE_NUM;
        this.OUTER_KEY = OUTER_KEY;
        this.OUTER_TYPE = OUTER_TYPE;
        this.DEAL_USERID = DEAL_USERID;
        this.DEAL_USERNAME = DEAL_USERNAME;
        this.DEAL_DEPTID = DEAL_DEPTID;
        this.DEAL_DEPTNAME = DEAL_DEPTNAME;
        this.DEAL_OPINION = DEAL_OPINION;
        this.DEAL_STATE = DEAL_STATE;
        this.DEAL_TIME = DEAL_TIME;
        this.REPLY_RESULT = REPLY_RESULT;
        this.REPLY_NUMBER = REPLY_NUMBER;
        this.REPLY_FILE_NAME = REPLY_FILE_NAME;
        this.REPLY_FILE = REPLY_FILE;
        this.UPLOAD_DATE = UPLOAD_DATE;
    }
}
