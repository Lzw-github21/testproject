package cn.ecasoft.dataexchange.common.utils;

import cn.ecasoft.dataexchange.entity.model.FileInfo;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author xuliukai
 * @since 2023/10/27 9:33
 */
public class FileUtils {

    public static FileInfo getFileInfoExcludeByte(String url) throws Exception {
        return getFileInfo(url, false);
    }

    /**
     * 获取文件的信息
     *
     * @param url    文件路径
     * @param isByte 是否设置byte数组，如果设置为false，就不去读取文件的二进制数据，如果设置为true就去读取
     *               怎么做的目的是减少内存开销
     * @return
     * @throws Exception
     */
    public static FileInfo getFileInfo(String url, Boolean isByte) throws Exception {
        URL downloadUrl = new URL(url);
        URLConnection connection = downloadUrl.openConnection();
        // 获取文件名
        String fileName = downloadUrl.getFile();
        // 获取文件后缀
        String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
        // 获取文件大小
        long fileSize = connection.getContentLength();
        InputStream inputStream = connection.getInputStream();
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        byte[] bufferBytes = new byte[1024];
        int bytesRead;
        while ((bytesRead = inputStream.read(bufferBytes)) != -1) {
            buffer.write(bufferBytes, 0, bytesRead);
        }

        FileInfo fileInfo = new FileInfo();
        fileInfo.setFileName(fileName);
        fileInfo.setFileExtension(fileExtension);
        fileInfo.setSize(fileSize);
        if (isByte) {
            // 获取文件字节数组
            byte[] fileBytes = buffer.toByteArray();
            fileInfo.setFileByte(fileBytes);
        }

        return fileInfo;
    }

}
