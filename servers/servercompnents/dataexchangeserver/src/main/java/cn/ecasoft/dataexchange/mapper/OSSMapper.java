package cn.ecasoft.dataexchange.mapper;

import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.common.utils.Constants;
import cn.ecasoft.dataexchange.entity.db.SysEcafiles;
import cn.ecasoft.dataexchange.entity.model.EntitySql;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.UUID;

/**
 * @author xuliukai
 * @since 2023/10/26 16:38
 */
@Service
public class OSSMapper extends CommonMapperByDBHelp {

    public int insertOss(SysEcafiles sysEcafiles) {
        EntitySql entitySql = super.entityToInsertSql(sysEcafiles, "SYS_ECAFILES");
        return super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, entitySql.getSql(),
                UUID.randomUUID().toString().replace("-", ""), entitySql.getParams());
    }


    public DataTable getDataFromSysEcaFiles(String to_row_guid) {
        String sql = "select * from sys_ecafiles where to_row_guid = @to_row_guid order by CREATEDATE desc limit 1";
        HashMap<String, Object> params = new HashMap<>();
        params.put("to_row_guid", to_row_guid);
        return executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, UUID.randomUUID().toString().replace("-", ""), params);
    }

}
