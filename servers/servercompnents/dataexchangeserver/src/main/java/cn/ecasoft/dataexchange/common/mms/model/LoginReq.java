package cn.ecasoft.dataexchange.common.mms.model;

import cn.ecasoft.dataexchange.common.mms.util.SDKToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @description: 移动短信
 * @author: 杨攀  2020/8/12 16:33
 */
public class LoginReq {
    private String apId;
    private String secretKey;
    private String ecName;
    private String version = "2.0.0";

    public LoginReq() {
    }

    public String getApId() {
        return this.apId;
    }

    public void setApId(String apId) {
        this.apId = apId;
    }

    public String getSecretKey() {
        return this.secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getEcName() {
        return this.ecName;
    }

    public void setEcName(String ecName) {
        this.ecName = ecName;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String toString() {
        ToStringBuilder tsb = new ToStringBuilder(this, SDKToStringStyle.getInstance());
        tsb.append("apId", this.apId);
        tsb.append("secretKey", this.secretKey);
        tsb.append("ecName", this.ecName);
        tsb.append("version", this.version);
        return tsb.toString();
    }
}
