package cn.ecasoft.dataexchange.entity.enums;

import java.util.HashMap;
import java.util.HashSet;

/**
 * @author xuliukai
 */
public class AnnexMapping {
    //旧的
    public static HashMap<String, String> titleMap = new HashMap<>();
    //新申请 附件 material_name
    public static HashMap<String, String> FJ_MAP_XSQ = new HashMap<>();
    //变更  附件 material_name
    public static HashMap<String, String> FJ_MAP_BG = new HashMap<>();

    //变更记录之前数据 查询sql对照表
    public static HashMap<String, String> CHANGE_SQL_MAP_AFTER = new HashMap<>();
    //变更记录之后数据 查询sql对照表
    public static HashMap<String, String> CHANGE_SQL_MAP_BEFORE = new HashMap<>();
    //变更对照忽略字段
    public static HashSet<String> IGNORE_FIELD = new HashSet<>();
    //字典表
    public static HashMap<String, String> SGXK_DIC_SQL = new HashMap<>();
    //消防人防特殊字段对照表
    public static HashSet<String> XFRF_SET = new HashSet<>();

    //备案表字段映射
    public static HashMap<String, String> BEI_AN_BIAO = new HashMap<>();
    public static HashMap<String, String> SGXK_BIAO = new HashMap<>();
    //单体字段映射
    public static HashMap<String, String> DT_BIAO = new HashMap<>();

    public static HashMap<String, String> FG_TABLE_NAME_MAP = new HashMap<>();

    //发改推送我们数据不是所有的数据都要保存的，只要保存3.0相关的
    public static HashSet<String> FG_TABLE_NEED_SAVE = new HashSet<>();

    static {
        // 添加映射关系
        titleMap.put("依法办理用地批准手续的证明文件(根据项目实际情况，可以是建设用地划拨决定书、建设用地批准书、建设用地使用权证、不动产权证中的一种)", "d62ba866-603e-48a7-a92e-423bcac3a1b5");
        titleMap.put("建设工程规划许可证或者乡村建设规划许可证(依法不需要取得规划许可的装饰装修工程等无需提供)", "ed7ccf14-026a-42c7-abf4-7fe87657c5b4");
        titleMap.put("施工图设计文件审查合格书(含消防审查、人防审查意见)和全套施工图设计文件", "c3e84239-ecc4-41b2-ad1d-f41f01fe58d2");
        titleMap.put("建设工程施工合同(依法必须进行招标的项目同时提供中标通知书)", "66424d05-611b-4c9f-a50d-391f4966101b");
        titleMap.put("有保证工程质量和安全的具体措施(包括危险性较大分部分项工程清单及其安全管理措施)的承诺书", "a96673d4-4cf5-43b4-9556-9ff37e5eb645");
        titleMap.put("施工场地已经基本具备施工条件的承诺书", "69d7967e-6da0-4579-9955-f7defe54a808");
        titleMap.put("建设资金已经落实的承诺书", "7c207c5f-08bf-42e0-bea0-0f034ecea43d");
        titleMap.put("建筑工程施工许可证", "6c780c47-318e-46fb-a75b-ddce15284d49");
        titleMap.put("其他", "1845b573-3105-44fd-a16b-3cf655403d08");

        FJ_MAP_XSQ.put("依法办理用地批准手续的证明文件","4ec07701-3a02-405e-8886-1c4cd2a65883");
        FJ_MAP_XSQ.put("建设工程规划许可证或者乡村建设规划许可证","e0bf11c9-4056-4e5d-b4b7-1a78af19d585");
        FJ_MAP_XSQ.put("施工图设计文件审查合格书或施工图上传确认书","9f5455b6-f8e3-46d4-8734-24d6fdd0e4ce");
        FJ_MAP_XSQ.put("施工图设计文件审查合格书","9f5455b6-f8e3-46d4-8734-24d6fdd0e4ce");
        FJ_MAP_XSQ.put("建设工程施工合同","e40381f5-22ef-4f4f-b833-50563303b068");
        FJ_MAP_XSQ.put("有保证工程质量和安全的具体措施","1178720e-e57b-4357-913e-79b7fa92e0f7");
        FJ_MAP_XSQ.put("施工场地已经基本具备施工条件的承诺书","9054005b-ccc8-453f-8cd2-0547c09b2ebb");
        FJ_MAP_XSQ.put("建设资金已落实的承诺书","fd953cdc-8c14-479e-b61f-ddfa47a44a4e");
        FJ_MAP_XSQ.put("中标通知书","865d8549-2e52-4e19-8e54-37c0e548e1a9");

        FJ_MAP_BG.put("中标通知书","160726b8-8823-4f16-81cd-83f72f663624");
        FJ_MAP_BG.put("依法办理用地批准手续的证明文件","82eedd65-3e87-48cc-ae16-53c4ba2a6fd5");
        FJ_MAP_BG.put("建设工程规划许可证或者乡村建设规划许可证","af2e3032-b30d-4bcc-9df2-44667dae6c8d");
        FJ_MAP_BG.put("施工图设计文件审查合格书或施工图上传确认书","b159de7a-36e2-4da0-acc7-65f8c5cc926e");
        FJ_MAP_BG.put("建设工程施工合同","b657d7c1-c3ad-4db6-be2d-e4ccbcfb428d");
        FJ_MAP_BG.put("有保证工程质量和安全的具体措施","a79f2bde-2838-41ef-8ff7-04f8549a0a42");
        FJ_MAP_BG.put("施工场地已经基本具备施工条件的承诺书","ab8f761c-991b-4810-b198-2b1d752a0294");
        FJ_MAP_BG.put("建设资金已落实的承诺书","d67c7079-f844-4f03-9ab5-e24ab699ddf3");

        //变更前查询sql
        CHANGE_SQL_MAP_AFTER.put("tbbuilderlicencemanage_New", "select * from tbbuilderlicencemanage_New where RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_AFTER.put("tbprojectfinishunitinfo_New", "select * from tbprojectfinishunitinfo_New where RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_AFTER.put("tbeconcorpinfo_New", "select * from tbSfCorpInfo_New where CorpTypeNum = 1 and RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_AFTER.put("tbdesigncorpinfo_New", "select * from tbSfCorpInfo_New where CorpTypeNum = 2 and RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_AFTER.put("tbconscorpinfo_New", "select * from tbSfCorpInfo_New where CorpTypeNum = 3 and RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_AFTER.put("tbsupercorpinfo_New", "select * from tbSfCorpInfo_New where CorpTypeNum = 4 and RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_AFTER.put("tbzcbcorpinfo_New", "select * from tbSfCorpInfo_New where CorpTypeNum = 5 and RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_AFTER.put("tbxfmanageinfo_New", "select * from tbxfmanageinfo where RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_AFTER.put("tbrfmanageinfo_New", "select * from tbrfmanageinfo where RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_AFTER.put("tbrecordinfo_new", "select * from tbrecordinfo_new where rowguid = @rowguid;");

        //变更后查询sql
        CHANGE_SQL_MAP_BEFORE.put("tbbuilderlicencemanage_New_Change", "select * from tbbuilderlicencemanage_New_Change where RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_BEFORE.put("tbprojectfinishunitinfo_New_Change", "select * from tbprojectfinishunitinfo_New_Change where RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_BEFORE.put("tbeconcorpinfo_New_Change", "select * from tbSfCorpInfo_New_Change where CorpTypeNum = 1 and RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_BEFORE.put("tbdesigncorpinfo_New_Change", "select * from tbSfCorpInfo_New_Change where CorpTypeNum = 2 and RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_BEFORE.put("tbconscorpinfo_New_Change", "select * from tbSfCorpInfo_New_Change where CorpTypeNum = 3 and RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_BEFORE.put("tbsupercorpinfo_New_Change", "select * from tbSfCorpInfo_New_Change where CorpTypeNum = 4 and RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_BEFORE.put("tbzcbcorpinfo_New_Change", "select * from tbSfCorpInfo_New_Change where CorpTypeNum = 5 and RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_BEFORE.put("tbxfmanageinfo_New_Change", "select * from tbxfmanageinfo_New_Change where RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_BEFORE.put("tbrfmanageinfo_New_Change", "select * from tbrfmanageinfo_New_Change where RecordGuid = @rowguid;");
        CHANGE_SQL_MAP_BEFORE.put("tbrecordinfo_new_Change", "select * from tbrecordinfo_new where rowguid = @rowguid;");

        IGNORE_FIELD.add("sgxkguid");
        IGNORE_FIELD.add("rowguid");
        IGNORE_FIELD.add("guid");
        IGNORE_FIELD.add("RecordGuid");
        IGNORE_FIELD.add("row_guid");

        //结构体系字典表
        SGXK_DIC_SQL.put("TBPRJSTRUCTURETYPEDIC", "select * from TBPRJSTRUCTURETYPEDIC");
        //产生单体类型字典表
        SGXK_DIC_SQL.put("TBUNITCREATETYPEDIC", "select * from TBUNITCREATETYPEDIC");
        //工程等级字典表
        SGXK_DIC_SQL.put("TBPRJLEVELDIC", "select * from TBPRJLEVELDIC WHERE prjlevelnum LIKE '32%'");
        //资质序列
        SGXK_DIC_SQL.put("tbtradetypedic", "SELECT * from tbtradetypedic where isnew=1");
        //现场管理人员角色
        SGXK_DIC_SQL.put("tb_sgxkselepsersontypedetail", "SELECT * FROM tb_sgxkselepsersontypedetail");
        //结构类型
        SGXK_DIC_SQL.put("tbStructureTypedic", "SELECT * FROM tbStructureTypedic");
        //材料类别
        SGXK_DIC_SQL.put("TBBWCLTYPEDIC", "SELECT * FROM TBBWCLTYPEDIC");
        //储存性质
        SGXK_DIC_SQL.put("TBCCTYPEDIC", "SELECT * FROM TBCCTYPEDIC");
        //设置类型
        SGXK_DIC_SQL.put("TBSETTYPEDIC", "SELECT * FROM TBSETTYPEDIC");
        //使用性质
        SGXK_DIC_SQL.put("TBUSETYPEDIC", "SELECT * FROM TBUSETYPEDIC");
        //消防设施
        SGXK_DIC_SQL.put("TBXFFACILITIESDIC", "SELECT * FROM TBXFFACILITIESDIC");
        //装修部位
        SGXK_DIC_SQL.put("TBZXPLACEDIC", "SELECT * FROM TBZXPLACEDIC");

        XFRF_SET.add("zxplace");
        XFRF_SET.add("bigintsites");
        XFRF_SET.add("qtspecproject");
        XFRF_SET.add("xffacilities");

        BEI_AN_BIAO.put("rowguid", "备案主键");
        BEI_AN_BIAO.put("prjGuid", "项目主键");
        BEI_AN_BIAO.put("PrjName", "项目名称");
        BEI_AN_BIAO.put("ProvinceNum", "项目所在省");
        BEI_AN_BIAO.put("CityNum", "项目所在地市");
        BEI_AN_BIAO.put("CountyNum", "项目所在区县");
        BEI_AN_BIAO.put("UPDATETIME", "上报时间");
        BEI_AN_BIAO.put("Status", "审核状态");
        BEI_AN_BIAO.put("ShenQNum", "申请类型");
        BEI_AN_BIAO.put("ManageAdminAreaNum", "上报部门编码");
        BEI_AN_BIAO.put("SFXF", "是否消防");
        BEI_AN_BIAO.put("SFRF", "是否人防");


        SGXK_BIAO.put("BuildCorpCodeType", "建设单位代码类型");
        SGXK_BIAO.put("PrjCode", "项目代码");
        SGXK_BIAO.put("PrjName", "工程名称");
        SGXK_BIAO.put("PrjTypeNum", "项目类型");
        SGXK_BIAO.put("ContractMoney", "合同金额");
        SGXK_BIAO.put("ProvinceNum", "工程所在省份");
        SGXK_BIAO.put("CityNum", "工程所在地市");
        SGXK_BIAO.put("CountyNum", "工程所在区县");
        SGXK_BIAO.put("Address", "建设地点");
        SGXK_BIAO.put("PrjSize", "建设规模");
        SGXK_BIAO.put("BARGAINDAYS", "合同工期");
        SGXK_BIAO.put("BuildCorpName", "建设单位名称");
        SGXK_BIAO.put("BuildCorpCode", "建设单位统一社会新用代码");
        SGXK_BIAO.put("BUILDCORPADDRESS", "建设单位地址");
        SGXK_BIAO.put("econtypeNum", "经济性质");
        SGXK_BIAO.put("legalName", "法人");
        SGXK_BIAO.put("LEGALMANIDCARD", "法人身份证");
        SGXK_BIAO.put("BUILDERCORPLEADER", "建设单位项目负责人");
        SGXK_BIAO.put("BUILDERCORPLEADERIDCARD", "建设单位项目负责人身份证");
        SGXK_BIAO.put("BUILDERCORPLEADERPHONE", "建设单位项目负责人联系电话");
        SGXK_BIAO.put("BUILDCORPPERSON", "建设单位联系人");
        SGXK_BIAO.put("BUILDCORPPERSONPHONE", "建设单位联系人电话");

        DT_BIAO.put("buildArea", "建筑面积");
        DT_BIAO.put("invest", "工程总造价(万元)");
        DT_BIAO.put("buildHeight", "建筑高度(米)");
        DT_BIAO.put("prjLevelNum", "工程等级");
        DT_BIAO.put("STRUCTURETYPENUM", "结构体系");

        FG_TABLE_NAME_MAP.put("builderLicenceManageVO", "tbbuilderlicencemanage_new");//3.0要保存
        FG_TABLE_NAME_MAP.put("projectFinishUnitInfoVOList", "tbprojectfinishunitinfo_new");//3.0要保存
        FG_TABLE_NAME_MAP.put("econcorpInfoVOList", "tbeconcorpinfo");//2.0不保存
        FG_TABLE_NAME_MAP.put("designcorpInfoVOList", "tbdesigncorpinfo");//2.0不保存
        FG_TABLE_NAME_MAP.put("conscorpInfoVOList", "tbconscorpinfo");//2.0不保存
        FG_TABLE_NAME_MAP.put("supercorpInfoVOList", "tbsupercorpinfo");//2.0不保存
        FG_TABLE_NAME_MAP.put("zcbcorpInfoVOList", "tbzcbcorpinfo");//3.0要保存
        FG_TABLE_NAME_MAP.put("projectXFVO", "TBXFManageInfo");//要保存
        FG_TABLE_NAME_MAP.put("projectRFVO", "TBRFManageInfo");//要保存
        FG_TABLE_NAME_MAP.put("recordinfoVO", "tbrecordinfo_new");//要保存
        FG_TABLE_NAME_MAP.put("itemMaterialVOList", "TBFileInfo");//不保存


        FG_TABLE_NEED_SAVE.add("tbbuilderlicencemanage_new");
        FG_TABLE_NEED_SAVE.add("tbprojectfinishunitinfo_new");
        FG_TABLE_NEED_SAVE.add("TBXFManageInfo");
        FG_TABLE_NEED_SAVE.add("TBRFManageInfo");
        FG_TABLE_NEED_SAVE.add("tbrecordinfo_new");
        FG_TABLE_NEED_SAVE.add("tbsfcorpinfo_new");
    }

}
