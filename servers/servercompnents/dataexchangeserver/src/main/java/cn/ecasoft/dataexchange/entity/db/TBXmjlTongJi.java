package cn.ecasoft.dataexchange.entity.db;

import org.nutz.dao.entity.annotation.Table;

@Table("TBXmjlTongJi")
public class TBXmjlTongJi {
    public String row_Guid;
    public Integer id;
    public String corpName;
    public String corpCode;
    public String personName;
    public String idCard;
    public Integer isLock;
    public String cityNum;
    public String countyNum;
    public String manageAdminAreaNum;
    public Integer personType;
    public String sgxkGuid;
    public String recordGuid;

}
