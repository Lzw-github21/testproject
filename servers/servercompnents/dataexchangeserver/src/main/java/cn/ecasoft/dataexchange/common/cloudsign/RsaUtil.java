package cn.ecasoft.dataexchange.common.cloudsign;

import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * 与政务网前后台加解密工具类
 * @author:   2020/8/14 15:53
 */
public class RsaUtil {
    private final static String KEY_RSA = "RSA";//定义加密方式
    private static final int MAX_ENCRYPT_BLOCK = 117;//RSA最大加密明文大小
    private static final int MAX_DECRYPT_BLOCK = 128;//RSA最大解密密文大小

    private final static String KEY_RSA_SIGNATURE = "MD5withRSA";//定义签名算法
    public RsaUtil() {    }

    /**
     * Description:公钥加密
     * param dataStr    待加密数据
     * param publicKeyStr 私钥
     * return
     * @throws Exception
     */
    public static String encryptByPublicKey(String publicKeyStr, String dataStr) throws Exception {
        byte[] data = dataStr.getBytes("UTF-8");
        // 获取公钥字符串时,进行了encryptBase64操作,因此此处需对公钥钥解密
        byte[] bytes = decryptBase64(publicKeyStr);
        // 取得公钥
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
        KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
        PublicKey publicKey = factory.generatePublic(keySpec);
        // 对数据加密
        Cipher cipher = Cipher.getInstance(factory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        int inputLen = data.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段加密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(data, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_ENCRYPT_BLOCK;
        }
        byte[] encryptedData = out.toByteArray();
        out.close();
        return encryptBase64(encryptedData);
    }

    /**
     * Description:私钥解密
     * param dataStr    待解密数据
     * param privateKeyStr 私钥
     * return
     * @throws Exception
     */
    public static String decryptByPrivateKey(String privateKeyStr, String dataStr) throws Exception {
        byte[] data =decryptBase64(dataStr);
        // 获取私钥字符串时,进行了encryptBase64操作,因此此处需对私钥解密
        byte[] bytes = decryptBase64(privateKeyStr);
        // 取得私钥
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
        KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
        PrivateKey privateKey = factory.generatePrivate(keySpec);
        // 对数据解密
        Cipher cipher = Cipher.getInstance(factory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, privateKey);

        int inputLen = data.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段解密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                cache = cipher
                        .doFinal(data, offSet, MAX_DECRYPT_BLOCK);
            } else {
                cache = cipher
                        .doFinal(data, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_DECRYPT_BLOCK;
        }
        byte[] decryptedData = out.toByteArray();
        out.close();

        return new String(decryptedData,"UTF-8");
    }


    /**
     * Description:用私钥对信息生成数字签名
     * param dataStr    待解密数据
     * param privateKeyStr 私钥
     * return
     * @throws Exception
     */
    public static String signByPrivateKey(String privateKey, String dataStr) throws Exception {
        String str = "";
        byte[] data = dataStr.getBytes("UTF-8");
        // 解密由base64编码的私钥
        byte[] bytes = decryptBase64(privateKey);
        // 构造PKCS8EncodedKeySpec对象
        PKCS8EncodedKeySpec pkcs = new PKCS8EncodedKeySpec(bytes);
        // 指定的加密算法
        KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
        // 取私钥对象
        PrivateKey key = factory.generatePrivate(pkcs);
        // 用私钥对信息生成数字签名
        Signature signature = Signature.getInstance(KEY_RSA_SIGNATURE);
        signature.initSign(key);
        signature.update(data);
        str = encryptBase64(signature.sign());
        return str;
    }

    /**
     * Description:用公钥校验数字签名
     * param dataStr    加密数据
     * param publicKeyStr 私钥
     * param sign 数字签名
     * return  校验成功返回true，失败返回false
     * @throws Exception
     * @throws Exception
     */
    public static boolean verifyByPublicKey(String publicKeyStr, String dataStr, String sign) throws Exception {
        boolean flag = false;
        byte[] data = dataStr.getBytes("UTF-8");
        // 解密由base64编码的公钥
        byte[] bytes = decryptBase64(publicKeyStr);
        // 构造X509EncodedKeySpec对象
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
        // 指定的加密算法
        KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
        // 取公钥对象
        PublicKey key = factory.generatePublic(keySpec);
        // 用公钥验证数字签名
        Signature signature = Signature.getInstance(KEY_RSA_SIGNATURE);
        signature.initVerify(key);
        signature.update(data);
        flag = signature.verify(decryptBase64(sign));
        return flag;
    }


    /**
     * @throws Exception
     */
    public static void generateKey() throws Exception {
        RsaUtil encrypt = new RsaUtil();
        KeyPairGenerator generator = KeyPairGenerator.getInstance(KEY_RSA);
        generator.initialize(1024);
        KeyPair keyPair = generator.generateKeyPair();
        // 公钥
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        // 私钥
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        System.out.println("publicKey:"+encryptBase64(publicKey.getEncoded()));
        System.out.println("privateKey:"+encryptBase64(privateKey.getEncoded()));
    }


    /**
     * @throws Exception
     */
    private static byte[] decryptBase64(String key) {
        return javax.xml.bind.DatatypeConverter.parseBase64Binary(key);
    }

    /**
     * @throws Exception
     */
    private static String encryptBase64(byte[] key) {
        return javax.xml.bind.DatatypeConverter.printBase64Binary(key);
    }


}

