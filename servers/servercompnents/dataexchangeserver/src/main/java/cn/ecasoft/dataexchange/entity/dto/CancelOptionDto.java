package cn.ecasoft.dataexchange.entity.dto;

import cn.ecasoft.dataexchange.entity.db.TBBuilderLicenceManageOption;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * @author XuLiuKai
 */
@Setter
@Getter
public class CancelOptionDto {

    private String CityNum;
    private List<TBBuilderLicenceManageOption> tbbuilderlicencemanage_option;

    public boolean paramsCheck() {
        if (StringUtils.isBlank(this.CityNum)) {
            return false;
        }

        if (this.tbbuilderlicencemanage_option == null || this.tbbuilderlicencemanage_option.size() == 0) {
            return false;
        }

        return true;
    }
}
