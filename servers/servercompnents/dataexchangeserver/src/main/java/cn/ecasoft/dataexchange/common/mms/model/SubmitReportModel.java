package cn.ecasoft.dataexchange.common.mms.model;

/**
 * @description: 移动短信
 * @author: 杨攀  2020/8/12 17:16
 */
public class SubmitReportModel {
    private String reportStatus;
    private String[] mobiles;
    private String submitDate;
    private String receiveDate;
    private String errorCode;
    private String msgGroup;

    public SubmitReportModel() {
    }

    public String getReportStatus() {
        return this.reportStatus;
    }

    public void setReportStatus(String reportStatus) {
        this.reportStatus = reportStatus;
    }

    public String[] getMobiles() {
        return this.mobiles;
    }

    public void setMobiles(String[] mobiles) {
        this.mobiles = mobiles;
    }

    public String getSubmitDate() {
        return this.submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getReceiveDate() {
        return this.receiveDate;
    }

    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMsgGroup() {
        return this.msgGroup;
    }

    public void setMsgGroup(String msgGroup) {
        this.msgGroup = msgGroup;
    }
}
