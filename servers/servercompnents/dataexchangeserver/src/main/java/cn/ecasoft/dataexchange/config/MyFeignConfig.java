package cn.ecasoft.dataexchange.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Client;
import feign.Feign;
import feign.Logger;
import feign.Request;
import feign.Retryer;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

/**
 * feign配置类
 *
 * @author yangsen
 * @date 2021/4/26 10:04
 */
@Slf4j
@Configuration
public class MyFeignConfig<T> {

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
//    @Autowired
//    private HttpClient httpClient;
    @Bean
    public Logger logger() {
        return new Logger() {
            @Override
            protected void log(String configKey, String format, Object... args) {
                log.info(String.format(methodTag(configKey) + format, args));
            }
        };
    }
    /**
     * 注入新的Decoder Feign将自动 替换
     */
    @Bean
    public Decoder feignDecoder() {
        MessageConverter messageConverter = new MessageConverter();
        ObjectFactory<HttpMessageConverters> objectFactory = () -> new HttpMessageConverters(messageConverter);
        return new SpringDecoder(objectFactory);
    }

    @Bean
    public Client feignClient() {
        return new Client.Default(getTrustingClientSSLSocketFactory(), new NoopHostnameVerifier());
    }
    //绕过https证书验证
    private SSLSocketFactory getTrustingClientSSLSocketFactory() {
        try {
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, getTrustingClientTrustManager(), new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (Exception e) {
            throw new RuntimeException("Failed to create trusting client SSL socket factory", e);
        }
    }

    private TrustManager[] getTrustingClientTrustManager() {
        return new TrustManager[] {
                new X509TrustManager() {
                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    @Override
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }
                    @Override
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
        };
    }


    /**
     * 构建动态URL
     * @param t 接口泛型
     * @param url 请求的url地址
     */
    public T target(Class<T> t, String url) {
        MyFeignConfig<T> tMyFeignConfig = new MyFeignConfig<>();
        HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter(new ObjectMapper());
        ObjectFactory<HttpMessageConverters> converter = ()-> new HttpMessageConverters(jsonConverter);
        return Feign.builder()
                .decoder(tMyFeignConfig.feignDecoder())
                .encoder(new SpringEncoder(converter))
                .logLevel(tMyFeignConfig.feignLoggerLevel())
                .logger(tMyFeignConfig.logger())
                .target(t, url);
    }

    @Bean
    Request.Options feignOptions() {
        return new Request.Options(/**connectTimeoutMillis**/60 * 1000, /** readTimeoutMillis **/60 * 1000);
    }
    @Bean
    Retryer feignRetryer(){
        return Retryer.NEVER_RETRY;
    }
    @Bean
    ErrorDecoder feignErrorDecoder(){
        return new CustomErrorDecoder();
    }

}
