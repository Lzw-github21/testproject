package cn.ecasoft.dataexchange.entity.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author XuLiuKai
 */
@Getter
@Setter
public class LockBack {
    private Boolean result;
    private String strErrMsg;
}
