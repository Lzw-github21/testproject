package cn.ecasoft.dataexchange.config;

import com.alibaba.gov.api.domain.AtgBusSecretKey;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName AtgConfiguration
 * @Author CZG
 * @Date 2020/10/22 15:35
 * @Description TODO
 **/
@Configuration
@ConfigurationProperties(prefix = "atgconfig")
public class AtgConfiguration {
    private String gatewayUrl;
    private String appId;
    private List<Map<String, String>> secretKeys = new ArrayList<>();

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public List<AtgBusSecretKey> getSecretKeys() {
        ArrayList<AtgBusSecretKey> secretKeyList = new ArrayList<>();
        for (Map<String, String> secretKey : secretKeys) {
            AtgBusSecretKey key = new AtgBusSecretKey(secretKey.get("key_id"), secretKey.get("secret_key"));
            secretKeyList.add(key);
        }

        return secretKeyList;
    }

    public void setSecretKeys(List<Map<String, String>> secretKeys) {
        this.secretKeys = secretKeys;
    }
}
