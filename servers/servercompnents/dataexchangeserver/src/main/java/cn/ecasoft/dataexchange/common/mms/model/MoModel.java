package cn.ecasoft.dataexchange.common.mms.model;

/**
 * @description: 移动短信
 * @author: 杨攀  2020/8/12 16:31
 */
public class MoModel {
    private String mobile;
    private String smsContent;
    private String sendTime;
    private String addSerial;

    public MoModel() {
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSmsContent() {
        return this.smsContent;
    }

    public void setSmsContent(String smsContent) {
        this.smsContent = smsContent;
    }

    public String getSendTime() {
        return this.sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getAddSerial() {
        return this.addSerial;
    }

    public void setAddSerial(String addSerial) {
        this.addSerial = addSerial;
    }
}
