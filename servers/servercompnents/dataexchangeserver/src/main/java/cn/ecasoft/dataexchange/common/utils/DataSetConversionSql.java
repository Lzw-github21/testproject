package cn.ecasoft.dataexchange.common.utils;

import cn.ecasoft.basic.datatable.DataRow;
import cn.ecasoft.dataexchange.entity.model.DataSetSqlParams;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.HashSet;

/**
 * @author xuliukai
 * @since 2023/9/7 10:36
 */
public class DataSetConversionSql {

    /**
     * dataSet 是多张表的集合，其中包含多张 dateTable
     * --> dateTable 是一张表，其中包含多条 dataRow
     * -->dataRow 就是单行数据
     * <p>
     * 将 dataTable 转化为 sql
     * DataTable 是一条数据，他的本质是一个Map集合
     * {
     * A:a
     * B:b
     * C:c
     * }
     * 其中 A 是字段名，a是字段值
     * 等价于下面的表
     * A----B----C
     * a |  b |  c
     *
     * @param dataRow   表数据单行
     * @param tableName 表名
     */
    public static DataSetSqlParams getInsertSql(DataRow dataRow, String tableName) {
        StringBuilder sb = new StringBuilder("insert into " + tableName);
        return montageSqlForInsert(sb, dataRow);
    }

    /**
     * 这里这个是 columnSet 是字段的集合，用来筛选的
     * 什么时候使用：当传入参数为 dataRow ，其中的字段和需要插入到数据库表中的字段可能会不一致的时候使用
     * 这个使用是用 columnSet 作为筛选条件，哪些字段需要insert，哪些不需要
     *
     * @param dataRow
     * @param tableName
     * @param columnsSet
     * @return
     */
    public static DataSetSqlParams getInsertSqlAfterFilterate(DataRow dataRow, String tableName, HashSet<String> columnsSet) {
        StringBuilder sb = new StringBuilder("insert into " + tableName);
        return montageSqlForInsert(sb, dataRow, columnsSet);
    }

    //这个只能用来生成发改相关的sql，因为里面对字段有些处理
    public static DataSetSqlParams getInsertSqlForFG(DataRow dataRow, String tableName, HashSet<String> columnsSet) {
        StringBuilder sb = new StringBuilder("insert into " + tableName);

        //这里需要将字段进行处理
        if (!"tbrecordinfo_new".equals(tableName)) {
            String row_guid = dataRow.getString("guid");
            if (!StringUtils.isBlank(row_guid)){
                dataRow.put("row_guid",row_guid);
            }
            String recordGuid = dataRow.getString("rowguid");
            if (!StringUtils.isBlank(recordGuid)){
                dataRow.put("recordGuid",recordGuid);
            }
            dataRow.remove("guid");
            dataRow.remove("rowguid");
        }else {
            String row_guid = dataRow.getString("rowguid");
            dataRow.put("row_guid",row_guid);
            dataRow.remove("rowguid");
        }

        return montageSqlForInsert(sb, dataRow, columnsSet);
    }

    public static DataSetSqlParams updateSql(DataRow dataRow, String tableName) {
        StringBuilder sb = new StringBuilder("update " + tableName + " set ");
        return montageSqlForUpdate(sb, dataRow);
    }

    /**
     * 这里需要注意的是，columnsSet 内的字段改成全小写，不然可能会匹配不上
     *
     * @param sb
     * @param dataRow
     * @param columnsSet
     * @return
     */
    private static DataSetSqlParams montageSqlForInsert(StringBuilder sb, DataRow dataRow, HashSet<String> columnsSet) {
        boolean type = columnsSet != null;

        StringBuilder fieldSb = new StringBuilder();
        StringBuilder valueSb = new StringBuilder();
        HashMap<String, Object> params = new HashMap<>();
        dataRow.forEach((k, v) -> {
            if (v != null) {
                if (type) {
                    if (!columnsSet.contains(k.toLowerCase())) {
                        return;
                    }
                }
                fieldSb.append(k).append(",");
                valueSb.append("@").append(k.toLowerCase()).append(",");
                params.put(k.toLowerCase(), v);
            }
        });
        fieldSb.deleteCharAt(fieldSb.length() - 1);
        valueSb.deleteCharAt(valueSb.length() - 1);

        sb.append(" (").append(fieldSb).append(" ) values (").append(valueSb).append(")");
        DataSetSqlParams pa = new DataSetSqlParams();
        pa.setParams(params);
        pa.setSql(sb.toString());
        return pa;
    }

    private static DataSetSqlParams montageSqlForInsert(StringBuilder sb, DataRow dataRow) {
        return montageSqlForInsert(sb, dataRow, null);
    }

    private static DataSetSqlParams montageSqlForUpdate(StringBuilder sb, DataRow dataRow) {
        DataSetSqlParams pa = new DataSetSqlParams();
        StringBuilder fieldSb = new StringBuilder();
        HashMap<String, Object> params = new HashMap<>();
        dataRow.forEach((k, v) -> {
            if (v != null) {
                fieldSb.append(k).append("=").append("@").append(k.toLowerCase()).append(",");
                params.put(k.toLowerCase(), v);
            }
        });
        fieldSb.deleteCharAt(fieldSb.length() - 1);
        sb.append(fieldSb);
        pa.setSql(sb.toString());
        pa.setParams(params);
        return pa;
    }
}
