package cn.ecasoft.dataexchange.entity.dto;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author XuLiuKai
 * tbsfcorpinfo_new
 */
@Setter
@Getter
public class ISKBQZMNDTO {

    // 1：已经推送成功，2：推送，3：推送失败
    private String IsTsFG;
    // 区县编码
    private String CityType;

    private List<TBProjectInfoDto> tbprojectinfo;
    private List<TBBuilderLicenceManageDto> tbbuilderlicencemanage;
    private List<TBBuilderLicenceUnitInfoDto> tbbuilderlicenceunitinfo;
    private List<TBSFCorpInfoNewDto> tbsfcorpinfo_new;

    public boolean checkParams() {
        if (StringUtils.isAnyBlank(this.CityType, this.IsTsFG)) {
            return false;
        }


        if (this.tbbuilderlicencemanage.isEmpty() || this.tbprojectinfo.isEmpty() ||
                this.tbbuilderlicenceunitinfo.isEmpty() || this.tbsfcorpinfo_new.isEmpty()) {
            return false;
        }

        //打证日期
        String ReleaseDate = this.getTbbuilderlicencemanage().get(0).getReleaseDate();
        if (StringUtils.isAnyBlank(ReleaseDate)) {
            throw new RuntimeException("施工许可的打证日期不能为空");
        }

        // 获取当前日期
        LocalDate currentDate = LocalDate.now();
        // 将 ReleaseDate 字符串的前 10 个字符解析为日期
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate update = LocalDate.parse(ReleaseDate.substring(0, 10), dateFormatter);
        // 比较发证日期和当前日期
        if (update.isAfter(currentDate)) {
            throw new RuntimeException("发证日期不能大于当前日期");
        }

        return true;
    }


}
