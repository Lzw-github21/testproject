package cn.ecasoft.dataexchange.mapper;

import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.common.utils.Constants;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @author xuliukai
 * @since 2023/11/4 14:53
 */
@Service
public class OFDWebMapper extends CommonMapperByDBHelp {

    public DataTable getInfoByRecordGuid(String recordGuid) {
        String sql = "SELECT DISTINCT tbbuilderlicencemanage_new.PrjCode, ECertID, UniqueNum, " +
                "tbbuilderlicencemanage_new.ROW_GUID AS sgxkguid " +
                "FROM tbrecordinfo_new " +
                "LEFT JOIN tbbuilderlicencemanage_new ON tbrecordinfo_new.ROW_GUID = tbbuilderlicencemanage_new.RecordGuid " +
                "WHERE tbrecordinfo_new.ROW_GUID = @recordGuid " +
                "order by tbrecordinfo_new.createtime desc limit 1";

        HashMap<String, Object> params = new HashMap<>();
        params.put("recordGuid", recordGuid);
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, params);
    }
}
