package cn.ecasoft.dataexchange.mapper;

import cn.ecasoft.dataexchange.common.utils.Constants;
import cn.ecasoft.dataexchange.config.DbHelperExtend;
import cn.ecasoft.dataexchange.entity.db.PersonBasicInfoDeBlockInfo;
import cn.ecasoft.dataexchange.entity.db.TBPersonBasicInfoLockInfo;
import cn.ecasoft.dataexchange.entity.db.TBXmjlTongJi;
import org.mapstruct.Mapper;
import org.nutz.dao.Sqls;
import org.nutz.dao.entity.Record;
import org.nutz.dao.sql.Sql;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author XuLiuKai
 */
@Service
public class LockHelperMapper extends CommonMapper {

    @Resource
    private DbHelperExtend dbHelperExtend;

    public List<Record> getCountTbuserinfoByRowGuid(String IDCard) {
        String sql = "select IDCard from dmstand.tbuserinfo where IDCard = @IDCard and IsDelete = 0";
        Sql confirmSql = Sqls.create(sql).setParam("IDCard", IDCard);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer updateTbuserinfoLockBy(String IDCard, int isLock) {
        String sql = "update dmstand.tbuserinfo set isLock = @isLock where IDCard = @IDCard and IsDelete = 0";
        Sql confirmSql = Sqls.create(sql).setParam("IDCard", IDCard).setParam("isLock", isLock);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer updateTbuserinfoLock(String IDCard) {
        String sql = "update DMSTAND.tbuserinfo a INNER JOIN DMSTAND.tbrecordinfo b on a.RecordGuid = b.Row_Guid set a.isLock = 1 " +
                "where a.IDCard = @IDCard and a.IsDelete = 0 and b.RecordStatus in (2, 5)";
        Sql confirmSql = Sqls.create(sql).setParam("IDCard", IDCard);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer getCountTbuserinfo(String IDCard) {
        String sql = "select a.IDCARDTYPENUM from  DMSTAND.tbuserinfo a INNER JOIN DMSTAND.tbrecordinfo b on a.RecordGuid = b.Row_Guid " +
                "where a.idcard = @IDCard and a.IsDelete = 0 and b.RecordStatus in (2, 5)";
        Sql confirmSql = Sqls.create(sql).setParam("IDCard", IDCard);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer insertDataToTBPersonBasicInfoLockInfo(TBPersonBasicInfoLockInfo entity) {
        Sql confirmSql = getInsertSqlByEntity(entity, "DMSTAND.TBPersonBasicInfoLockInfo");
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer insertDataToTbxmjltongji(TBXmjlTongJi entity) {
        Sql confirmSql = getInsertSqlByEntity(entity, "DMSTAND.tbxmjltongji");
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer updateTbxmjltongjiLockByIdcard(String IDCard, int isLock) {
        String sql = "update DMSTAND.tbxmjltongji set Islock = @isLock where IDCard = @IDCard ";
        Sql confirmSql = Sqls.create(sql).setParam("IDCard", IDCard).setParam("isLock", isLock);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer getProjectDirectorCount(String Idcard) {
        String sql = "select count(distinct tmp.prjcode) from DMSTAND.tbbuilderlicencemanage tmp " +
                "left join DMSTAND.TBSFCorpInfo tmp1 on tmp.row_guid = tmp1.sgxkguid where 1 = 1 and tmp1.CorpLeaderCardNum = @Idcard " +
                "and tmp1.corptypenum = 4 and (tmp.IsFilish = 0 or tmp.IsFilish is null) and tmp.CENSORSTATUSNUM = 128 " +
                "and tmp.OptionType in (0, 4)";
        Sql confirmSql = Sqls.create(sql).setParam("Idcard", Idcard);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer getCountTbbuilderlicencemanage(String Idcard, int corptypenum) {
        String sql = "select count(1) count from DMSTAND.tbbuilderlicencemanage tmp left join DMSTAND.TBSFCorpInfo tmp1 " +
                "on tmp.row_guid = tmp1.sgxkguid where 1 = 1 and tmp1.CorpLeaderCardNum = @Idcard and tmp1.corptypenum = @corptypenum " +
                "and(tmp.IsFilish = 0 or tmp.IsFilish is null) and tmp.CENSORSTATUSNUM = 128 " +
                "and tmp.OptionType in (0, 4)";
        Sql confirmSql = Sqls.create(sql).setParam("Idcard", Idcard).setParam("corptypenum", corptypenum);
        return dbHelperExtend.queryCount(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer getTbuserinfoCountByIdCard(String Idcard) {
        String sql = "select isLock from DMSTAND.tbuserinfo where idcard = @Idcard";
        Sql confirmSql = Sqls.create(sql).setParam("Idcard", Idcard);
        return dbHelperExtend.queryCount(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer insertDataToTBPERSONBASICINFODEBLOCKINFO(PersonBasicInfoDeBlockInfo entity) {
        Sql confirmSql = getInsertSqlByEntity(entity, "DMSTAND.TBPersonBasicInfodeBlockInfo");
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer getPKValue(String tableName, String fieldName) {
        return dbHelperExtend.GetPkValue(Constants.DM_MASTER_GUID, tableName, fieldName);
    }

    public List<Record> getDataFromTbuserinfoByIdCard(String IdCard) {
        String sql = "select IDCard from DMSTAND.tbuserinfo where IDCard = @IdCard and IsDelete = 0";
        Sql confirmSql = Sqls.create(sql).setParam("IdCard", IdCard);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer deleteTbxmjltongjiByIdCard(String IdCard, String sgxkguid) {
        String sql = "DELETE FROM DMSTAND.tbxmjltongji  where IDCard = @IdCard AND sgxkguid = @sgxkguid";
        Sql confirmSql = Sqls.create(sql).setParam("IdCard", IdCard).setParam("sgxkguid", sgxkguid);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

}
