package cn.ecasoft.dataexchange.services.impl;

import cn.ecasoft.basic.datatable.DataRow;
import cn.ecasoft.basic.datatable.DataSet;
import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.common.utils.Constants;
import cn.ecasoft.dataexchange.common.utils.DataSetConversionSql;
import cn.ecasoft.dataexchange.entity.model.DataSetSqlParams;
import cn.ecasoft.dataexchange.mapper.PushSgxkToSjcInfoMapper;
import cn.ecasoft.dataexchange.services.PushSgxkToSjcInfoService;
import org.nutz.trans.Trans;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author xuliukai
 * @since 2023/10/25 16:32
 * @Desc 这个是推送数据到数据仓，但是由于数据量实在是太大了，而且修改频繁，所有定时推送原逻辑是全量推送
 * 但是后来在翻新施工许可的时候，改为增量推送
 * 现在改为在navicat上面推送
 */
@Service
public class PushSgxkToSjcInfoServiceImpl implements PushSgxkToSjcInfoService {

    @Resource
    private PushSgxkToSjcInfoMapper pushSgxkToSjcInfoMapper;

    private DataSet getPushToSJCData(String row_guid) {
        DataSet ds = new DataSet();
        //施工许可表
        ds.put("biz_031_TBBuilderLicenceManage_DHY", pushSgxkToSjcInfoMapper.SGXK(row_guid));
        //单体表
        ds.put("biz_031_tbprojectfinishunitinfo_DHY", pushSgxkToSjcInfoMapper.DT(row_guid));
        //四方主体表
        ds.put("biz_031_TBSFCorpInfo_DHY", pushSgxkToSjcInfoMapper.SF(row_guid));
        //人防表
        ds.put("biz_031_TBRFManageInfo_DHY", pushSgxkToSjcInfoMapper.RF(row_guid));
        //消防表
        ds.put("biz_031_TBXFManageInfo_DHY", pushSgxkToSjcInfoMapper.XF(row_guid));
        //审核意见表
        ds.put("biz_031_tbrecordopinioninfo_DHY", pushSgxkToSjcInfoMapper.SHYJ(row_guid));
        return ds;
    }


    @Override
    public RetMsgUtil<Object> PushSgxkToSjcInfo(String row_guid) {
        AtomicReference<Integer> total = new AtomicReference<>(0);
        try {
            //通过row_guid 在数据库中获取数据
            DataSet ds = this.getPushToSJCData(row_guid);
            if (ds.isEmpty()) {
                return RetMsgUtil.fail("查询不到该row_guid相关信息");
            }

            Trans.exec(() -> {
                ds.forEach((k, v) -> {
                    //如果是空的直接跳过，可能会出现其中一张表或者多张表是空的可能性
                    if (v.isEmpty()) {
                        return;
                    }
                    StringBuilder sb = new StringBuilder();
                    for (DataRow dataRow : v) {
                        sb.append(dataRow.getString("rowguid")).append(",");
                    }
                    //去掉最后一位的逗号
                    sb.deleteCharAt(sb.length() - 1);
                    //通过row_guid删除原有数据
                    pushSgxkToSjcInfoMapper.deleteByRowGuids(k, sb.toString());

                    for (DataRow dataRow : v) {
                        //生成sql语句
                        DataSetSqlParams insertSql = DataSetConversionSql.getInsertSql(dataRow, k);
                        //执行插入sql语句
                        total.updateAndGet(v1 -> v1 + pushSgxkToSjcInfoMapper.executeSqlForCount(Constants.DM_SJC_GUID, insertSql.getSql(),
                                UUID.randomUUID().toString().replace("-", ""), insertSql.getParams()));

                    }

                });
            });
        } catch (Exception e) {
            e.printStackTrace();
            //记录推送失败的row_guid
            pushSgxkToSjcInfoMapper.recordError(row_guid);
            return RetMsgUtil.fail("推送过程出现异常");
        }

        return RetMsgUtil.ok("执行完成，insert " + total + "条数据");
    }

}
