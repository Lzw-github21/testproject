package cn.ecasoft.dataexchange.entity.dto;

import cn.ecasoft.dataexchange.entity.db.TBProjectInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * @author XuLiuKai
 */
@Setter
@Getter
public class TBProjectInfoDto extends TBProjectInfo {
    private String guid;
    private String securityText;
    private String basicType;
    private String floor;
    private String corpMan;
    private String corpManPhone;
    private String certNum;
    private String titleLevel;
    private String builderCorpLeaderIdCardTypeNum;
    private String buildCorpPersonName;
    private String buildCorpPersonPhone;
    private String isNestingZone;
    private String isIndustry;
    private String planArea;
    private String userId;
    private String oldBuilderName;
    private String Is_HY;
    private String Is_JT;
    private String Is_LH;
    private String Is_QLC;
    private String SFXW;
}
