package cn.ecasoft.dataexchange.entity.dto;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

/**
 * @author XuLiuKai
 */
@Setter
@Getter
public class GetOFDFileDto {
    //单体信息
    private String zb;
    //施工许可信息
    private String fb;
    //二维码信息
    private String qrCodeBase64Content;
    //签章编码
    private String sealCode;

    private String row_guid;

    public boolean paramsCheck() {
        return !StringUtils.isAnyBlank(this.zb, this.fb, this.qrCodeBase64Content, this.sealCode, this.row_guid);
    }
}
