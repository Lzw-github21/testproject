package cn.ecasoft.dataexchange.mapper;

import cn.ecasoft.basic.datatable.DataRow;
import cn.ecasoft.basic.datatable.DataSet;
import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.common.utils.Constants;
import cn.ecasoft.dataexchange.common.utils.RedisConstants;
import cn.ecasoft.dataexchange.entity.db.SGXKBGDetails;
import cn.ecasoft.dataexchange.entity.enums.AnnexMapping;
import cn.ecasoft.dataexchange.entity.model.DataSetSqlParams;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

/**
 * @author XuLiuKai
 * @Desc 发改的业务是从发改拉取数据保存到诚信平台内，所以这里访问的数据源是施工许可的数据源
 */
@Service
public class FGMapper extends CommonMapperByDBHelp {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    public DataTable getStatus(String uniqueNum, String rowGuid, String ShenQNum) {
        String sql = "select status from tbrecordinfo_new where (UniqueNum = @uniqueNum or row_guid = @rowGuid) and ShenQNum = @ShenQNum";
        String sqlKey = "uLxoC9DpNsiM6NAv";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("uniqueNum", uniqueNum);
        paramsMap.put("rowGuid", rowGuid);
        paramsMap.put("ShenQNum", ShenQNum);
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public String getStatusByShenQNumIn(String uniqueNum, String rowGuid, String ShenQNum) {
        String sql = "select status from tbrecordinfo_new where (UniqueNum = @uniqueNum or row_guid = @rowGuid) and ShenQNum in (@ShenQNum)";
        String sqlKey = "uLxoC9DpNsgYN3NcyaoTACd";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("uniqueNum", uniqueNum);
        paramsMap.put("rowGuid", rowGuid);
        paramsMap.put("ShenQNum", ShenQNum);
        DataTable dataTable = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
        return dataTable.get(0).getString("status");
    }

    public String getRowGuid(String projectCode) {
        String sql = "select row_guid from tbbuilderlicencemanage_new where prjcode = @projectCode and ROW_guid is not null ";
        String sqlKey = "1CSY4jTSYQ5jb6j9zOCH2A";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("projectCode", projectCode);
        DataTable dataRows = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
        if (dataRows.isEmpty()) {
            return "";
        }
        return dataRows.get(0).getString("row_guid");
    }

    public int getCountByYeWuId(String yewuid) {
        String sql = "select count(*) count from tbhuanjiematerialinfo where RecordGuid = @yewuid ";
        String sqlKey = "Vwgi3CsciQ";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("yewuid", yewuid);
        return super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public int getCountByMaterialnum(String yewuid, String materialnum) {
        String sql = "select count(*) count from tbhuanjiematerialinfo where RecordGuid = @yewuid and materialnum = @materialnum";
        String sqlKey = "aqd2g9FxipCP9hBFjz";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("yewuid", yewuid);
        paramsMap.put("materialnum", materialnum);
        return super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public DataTable getTbmaterialmodelinfo_newData() {
        String sql = "select * from tbmaterialmodelinfo_new where buildlicense = 1";
        String sqlKey = "UNnaCR9tlr3EXIdB5F";
        HashMap<String, Object> paramsMap = new HashMap<>();
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public DataTable getTbmaterialmodelinfo_newDataByBuildlicenseChange() {
        String sql = "select * from tbmaterialmodelinfo_new where buildlicenseChange = 1";
        String sqlKey = "UNnaCR9tlr3EXIdB5FcyqUfhh7";
        HashMap<String, Object> paramsMap = new HashMap<>();
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public int insertDataToTbhuanjiematerialinfo(String yewutype, String RowGuid, String materialnum, String materialname) {
        String sql = "insert into tbhuanjiematerialinfo(Row_Guid,AnnexTypee,RecordGuid,materialnum,title,filecount) values " +
                "(@newGuid, @yewutype, @RowGuid,@materialnum,@materialname, 0)";
        String sqlKey = "LXjkCCV2SI3O";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("newGuid", UUID.randomUUID().toString().replace("-", ""));
        paramsMap.put("RowGuid", RowGuid);
        paramsMap.put("materialnum", materialnum);
        paramsMap.put("materialname", materialname);
        paramsMap.put("yewutype", yewutype);
        return super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public String getIdFromTbhuanjiematerialinfo(String RowGuid) {
        String sql = "select Row_Guid from tbhuanjiematerialinfo where RecordGuid = @RowGuid";
        String sqlKey = "onYULmTyQjmaSd9S3l";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("RowGuid", RowGuid);
        DataTable dataRows = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
        StringBuilder sb = new StringBuilder();
        for (DataRow dataRow : dataRows) {
            sb.append(dataRow.getString("id")).append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public DataTable getDataByYewuid(String yewuid, String materialnum) {
        String sql = "select * from tbhuanjiematerialinfo where RecordGuid = @yewuid and materialnum = @materialnum";
        String sqlKey = "UNnaCR9tlr3EXIdB5F";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("yewuid", yewuid);
        paramsMap.put("materialnum", materialnum);
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public int deleteTbhuanjiematerialinfodetailsByIds(String ids) {
        String sql = "delete from tbhuanjiematerialinfodetails where RecordGuid in (@ids)";
        String sqlKey = "hv9wPVSETgPFbpTTpCZV9MdQ";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("ids", ids);
        return super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public DataTable getSRFromTbrecordInfoNew(String UniqueNum, String rowguid, String ShenQNum) {
        String sql = "select status,rowguid from tbrecordinfo_new where (UniqueNum = @UniqueNum or row_guid = @rowguid) and ShenQNum = @ShenQNum";
        String sqlKey = "1MEyImBXHWlS6xyzARcNGU7q";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("UniqueNum", UniqueNum);
        paramsMap.put("rowguid", rowguid);
        paramsMap.put("ShenQNum", ShenQNum);
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public DataTable getMaxSortNum(String RowGuid, String SORT_UUID) {
        String sql = "SELECT MAX(sortnum) as nIndex from tbhuanjiematerialinfodetails where RecordGuid in ( " +
                "select row_guid from tbhuanjiematerialinfo where RecordGuid = @RowGuid and materialnum = @SORT_UUID )";
        String sqlKey = "t890g39o9PP0yzc9NjIQ";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("RowGuid", RowGuid);
        paramsMap.put("SORT_UUID", SORT_UUID);
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public void updateIspromise(String IS_CNZ, String RowGuid, String SORT_UUID) {
        String sql = "update tbhuanjiematerialinfo set Ispromise = @IS_CNZ where Row_Guid in " +
                "(select Row_Guid from tbhuanjiematerialinfo where RecordGuid = @RowGuid and materialnum = @SORT_UUID AND ROW_GUID IS NOT NULL ))";
        String sqlKey = "OssJHmUkNpff9i5SPjt";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("RowGuid", RowGuid);
        paramsMap.put("SORT_UUID", SORT_UUID);
        paramsMap.put("IS_CNZ", IS_CNZ);
        super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public void insertTbhuanjiematerialinfodetails(HashMap<String, Object> paramsMap) {
        String sql = "insert into tbhuanjiematerialinfodetails(" +
                "Row_Guid,RecordGuid,fileext,filename,serverfilename,sortnum,IsTQ,Ispromise) " +
                "values(@id,@RecordGuid,@fileext,@filename,@serverfilename,@sortnum,@IsTQ,@Ispromise)";
        String sqlKey = "9z73vdpbcA";
        super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    // RowGuid, file.MATE_SORT_UUID
    public String getHjgroupguid(String yewuid, String materialnum) {
        String sql = "select Row_Guid from tbhuanjiematerialinfo where RecordGuid = @yewuid and materialnum = @materialnum";
        String sqlKey = "9MYrySOhKunY4YYA";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("yewuid", yewuid);
        paramsMap.put("materialnum", materialnum);
        DataTable dataRows = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
        return dataRows.get(0).getString("id");
    }

    /**
     * 获取表结构
     *
     * @param tableName
     * @return
     */
    public DataTable getTableStructure(String tableName) {
        String sql = "select * from " + tableName + " where 1 = 2";
        String sqlKey = "ASDDS6FHFNIIUY56SDF";
        HashMap<String, Object> paramsMap = new HashMap<>();
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public HashSet<String> getTableStructureToHashSet(String tableName) {
        DataTable tableStructure = this.getTableStructure(tableName);
        HashSet<String> hashSet = new HashSet<>();
        tableStructure.getColumns().forEach((k, v) -> hashSet.add(k));
        return hashSet;
    }


    //执行sql
    public int executeSql(String sql, HashMap<String, Object> paramsMap) {
        //这里需要注意的是，这里的sqlKey不能是固定的，如果是固定的，会出现顶撞之类的事情出现
        String sqlKey = UUID.randomUUID().toString();
        return super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public DataTable getDataFromTbrecordinfo_new(String rowguid) {
        String sql = "select * from tbrecordinfo_new where row_guid = @rowguid";
        String sqlKey = "QWYEOOIXS972132UY56SDF";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("rowguid", rowguid);
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public DataTable getDataFromTbhuanjiematerialinfo(String rowguid) {
        String sql = "select * from tbhuanjiematerialinfo where RecordGuid = @rowguid";
        String sqlKey = "hDTx8a4kJJGWghFZAw7r";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("rowguid", rowguid);
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    //获取变更前数据
    public DataSet getBGAfter(String rowguid) {
        DataSet dataSet = new DataSet();
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("rowguid", rowguid);
        AnnexMapping.CHANGE_SQL_MAP_AFTER.forEach((k, v) -> {
            DataTable t = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, v, UUID.randomUUID().toString(), paramsMap);
            dataSet.setName(k);
            dataSet.add(t);
        });
        return dataSet;
    }

    //获取变更后数据
    public DataSet getBGBefore(String rowguid) {
        DataSet dataSet = new DataSet();
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("rowguid", rowguid);
        AnnexMapping.CHANGE_SQL_MAP_BEFORE.forEach((k, v) -> {
            DataTable t = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, v, UUID.randomUUID().toString(), paramsMap);
            dataSet.setName(k);
            dataSet.add(t);
        });
        return dataSet;
    }

    public DataSet getSGXKDic() {
        String dicStr = stringRedisTemplate.opsForValue().get(RedisConstants.SGXK_DIC_KEY);

        if (StringUtils.isNotBlank(dicStr)) {
            return JSONObject.parseObject(dicStr, DataSet.class);
        }

        DataSet dataSet = new DataSet();
        AnnexMapping.SGXK_DIC_SQL.forEach((k, v) -> {
            HashMap<String, Object> paramsMap = new HashMap<>();
            DataTable t = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, v, UUID.randomUUID().toString(), paramsMap);
            dataSet.setName(k);
            dataSet.add(t);
        });
        stringRedisTemplate.opsForValue().set(RedisConstants.SGXK_DIC_KEY, JSONObject.toJSONString(dataSet));

        return dataSet;
    }


    public int updateTbbuilderlicencemanage_new_change(HashMap<String, Object> paramsMap) {
        String sql = "UPDATE tbbuilderlicencemanage_new_change set changeContent = @changeContent,ZSchangeContent = @ZSchangeContent," +
                "UnitChangeContent = @UnitChangeContent,BuilderLicenceNum = @BuilderLicenceNum,PrjNum = @PrjNum,mark='{6}' where RowGuid = @RowGuid;";
        String sqlKey = "9HuOqe3aS89CdYSjLUJqPU";
        return super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public void deleteChangedetails(String RowGuid) {
        String sql = "delete from tbbuilderlicencemanage_new_changedetails where RecordGuid = @RowGuid";
        String sqlKey = "1x74FwvoUy1CB988ahr";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("RowGuid", RowGuid);
        super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }


    public String getECertIDFromTbbuilderlicencemanage_new(String RowGuid) {
        String sql = "select eCertID from tbbuilderlicencemanage_new  where RecordGuid = @RowGuid";
        String sqlKey = "vKgjuLy8fqlkcfE1LWHg9EJ1hKMtQ";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("RowGuid", RowGuid);
        DataTable dataTable = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
        if (dataTable.isEmpty()) {
            return "";
        }
        return dataTable.get(0).getString("eCertID");
    }

    private DataSetSqlParams convertToInsertSQL(Object obj, String tableName) throws IllegalAccessException {
        StringBuilder sql = new StringBuilder("INSERT INTO ").append(tableName).append(" (");
        StringBuilder sqlValue = new StringBuilder(") VALUES (");
        HashMap<String, Object> params = new HashMap<>();
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Object value = field.get(obj);
            if (value != null && !value.toString().isEmpty()) {
                sql.append(field.getName()).append(", ");
                sqlValue.append("@").append(field.getName()).append(", ");
                params.put(field.getName(), value);
            }
        }

        sql.setLength(sql.length() - 2); // 移除最后的逗号和空格
        sqlValue.setLength(sqlValue.length() - 2); // 移除最后的逗号和空格
        sqlValue.append(")");

        sql.append(sqlValue);
        DataSetSqlParams p = new DataSetSqlParams();
        p.setParams(params);
        p.setSql(sql.toString());
        return p;
    }


    //tbbuilderlicencemanage_new_changedetails
    public void saveTable(List<SGXKBGDetails> detailsTable, String tableName) {
        for (SGXKBGDetails details : detailsTable) {
            DataSetSqlParams dsp = new DataSetSqlParams();
            try {
                dsp = this.convertToInsertSQL(details, tableName);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (dsp.getParams() == null || dsp.getParams().isEmpty()) {
                continue;
            }
            super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, dsp.getSql(), UUID.randomUUID().toString(), dsp.getParams());
        }
    }
}
