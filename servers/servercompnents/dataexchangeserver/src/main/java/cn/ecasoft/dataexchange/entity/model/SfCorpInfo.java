package cn.ecasoft.dataexchange.entity.model;

import org.nutz.dao.entity.annotation.Table;

/**
 * @author Dee
 * @date 2023/7/17
 * <p>Description:
 */
@Table("")
public class SfCorpInfo {
    private String corpName;
    private String corpCode;
    private String corpLeader;
    private String corpLeaderCardNum;
    private String corpLeaderPhone;
}
