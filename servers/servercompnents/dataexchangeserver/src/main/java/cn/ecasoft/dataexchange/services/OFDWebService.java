package cn.ecasoft.dataexchange.services;

import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;

/**
 * @author XuLiuKai
 * @Desc 获取施工许可证的接口
 */
public interface OFDWebService {

    RetMsgUtil<Object> getOFDFile(String zb, String fb, String qrCodeBase64Content,
                                  String sealCode, String row_guid) throws Exception;

    RetMsgUtil<Object> getOSSParams();
}
