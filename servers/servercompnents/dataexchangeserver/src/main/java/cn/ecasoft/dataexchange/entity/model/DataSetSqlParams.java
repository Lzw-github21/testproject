package cn.ecasoft.dataexchange.entity.model;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

/**
 * @author xuliukai
 * @since 2023/9/7 10:39
 */
@Setter
@Getter
public class DataSetSqlParams {

    private HashMap<String, Object> params;
    private String sql;
}
