package cn.ecasoft.dataexchange.config;

import feign.FeignException;
import feign.Response;
import feign.RetryableException;
import feign.codec.ErrorDecoder;

/**
 * @Author: LiangChun
 * @Description: TODO
 * @DateTime: 2023/7/28
 **/
public class CustomErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        FeignException exception = FeignException.errorStatus(methodKey, response);
        int status = response.status();
        return new RetryableException(
                status,
                "出现错误为:" + exception.getMessage(),
                response.request().httpMethod(),
                exception,
                null,
                response.request());
    }
}
