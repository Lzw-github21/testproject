package cn.ecasoft.dataexchange.common.cloudsign;


//import cn.ecasoft.utils.HttpSimpleMessage;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

/**
 * @author: asdfg
 * @Date: 2019/9/9 09:37
 * @Description:
 */
public class RestFulUtil {

    public HttpSimpleMessage post(String url, Map<String, String> headers, byte[] body)
            throws Exception {
        CloseableHttpResponse resp = null;
        HttpSimpleMessage result = new HttpSimpleMessage();
        try {

            CloseableHttpClient client = HttpClientFactory.getInstance();
            HttpPost httpPost = getHttpPost(url);
            //请求体
            if (body != null) {
                httpPost.setEntity(new ByteArrayEntity(body));
            }

            //请求头
            if (headers != null) {
                for (Map.Entry<String, String> headerProperty : headers.entrySet()) {
                    String key = headerProperty.getKey();
                    String value = headerProperty.getValue();
                    //去重
                    if (httpPost.containsHeader(key)) {
                        httpPost.removeHeaders(key);
                    }
                    httpPost.addHeader(key, value);
                }
            }
            //发送
            resp = client.execute(httpPost);

            // 获取响应头和体
            for (Header header : resp.getAllHeaders()) {
                result.headers.put(header.getName(), header.getValue());
            }
            HttpEntity entity = resp.getEntity();
            if (entity != null) {
                InputStream instream = entity.getContent();
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                copy(instream, output);
                byte[] byteArray = output.toByteArray();
                instream.close();
                result.setBody(byteArray);
            }
        } finally {
            closeHttpPost(resp);
        }

        return result;
    }

    private static int copy(InputStream input, OutputStream output)
            throws IOException {
        long count = copyLarge(input, output);
        if (count > 2147483647L) {
            return -1;
        }
        return (int) count;
    }

    private static long copyLarge(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte['?'];
        long count = 0L;
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }

    private HttpPost getHttpPost(String url) {

        HttpPost httppost = new HttpPost(url);
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(50000)
                .setSocketTimeout(50000)
                .setConnectTimeout(50000).build();
        httppost.setConfig(requestConfig);
        return httppost;
    }


    private void closeHttpPost(CloseableHttpResponse response) {
        try {
            if (response != null) {
                response.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
