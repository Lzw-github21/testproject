package cn.ecasoft.dataexchange.mapper;

import org.apache.commons.lang3.StringUtils;
import org.nutz.dao.Sqls;
import org.nutz.dao.sql.Sql;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;

/**
 * @author XuLiuKai
 */
@Service
public class CommonMapper {

    /**
     * 遍历实体类，设置为 confirmSql
     * insert into test (cityNumCode,cityName,level,scDate) values( @cityNumCode,@cityName,@level,@scDate)
     *
     * @param object 实体类
     */
    public Sql getInsertSqlByEntity(Object object, String tableName) {
        // 获取实体类的所有字段
        Field[] fields = object.getClass().getDeclaredFields();

        StringBuilder sqlStr = new StringBuilder();
        StringBuilder sqlValue = new StringBuilder();

        sqlStr.append("insert into ").append(tableName).append(" (");
        sqlValue.append("values( ");
        // 遍历字段并输出字段名和值 拼接字段和值占位符
        //例如 insert tableA(a,b,c) valuse(@a,@b,@c)
        for (Field field : fields) {
            try {
                //设置字段的访问权限，使其可以访问私有字段
                field.setAccessible(true);
                // 获取字段的值
                Object value = field.get(object);
                // 输出字段名和值
                if (value != null && StringUtils.isNotBlank(value.toString())) {
                    sqlStr.append(field.getName()).append(",");
                    sqlValue.append("@").append(field.getName()).append(",");
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        //去掉最后一个逗号并加上）
        sqlStr.deleteCharAt(sqlStr.length() - 1).append(") ");
        sqlValue.deleteCharAt(sqlValue.length() - 1).append(") ");
        //sql拼接
        sqlStr.append(sqlValue);
        return setParams(object, fields, sqlStr);
    }

    // 重要： 这里的 primaryKey 字段需要和实体类的name一致
    public Sql getUpdateSqlByEntity(Object object, String tableName, String primaryKey) {
        // 获取实体类的所有字段
        Field[] fields = object.getClass().getDeclaredFields();

        StringBuilder sqlStr = new StringBuilder();
        sqlStr.append("update ").append(tableName).append(" set ");
        // 遍历字段并输出字段名和值 拼接字段和值占位符
        //例如 update tableA set a=@a,b=@b where id=@id
        for (Field field : fields) {
            try {
                //设置字段的访问权限，使其可以访问私有字段
                field.setAccessible(true);
                // 获取字段的值
                Object value = field.get(object);
                // 输出字段名和值,这里不能把条件也塞进update语句里面
                if (value != null && StringUtils.isNotBlank(value.toString()) && !field.getName().toLowerCase().equals(primaryKey.toLowerCase())) {
                    sqlStr.append(field.getName()).append("=@").append(field.getName()).append(",");
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        sqlStr.deleteCharAt(sqlStr.length() - 1).append(" where ").append(primaryKey).append("=@").append(primaryKey);
        //sql拼接
        return setParams(object, fields, sqlStr);
    }

    private Sql setParams(Object object, Field[] fields, StringBuilder sqlStr) {
        Sql confirmSql = Sqls.create(sqlStr.toString());

        for (Field field : fields) {
            try {
                //设置字段的访问权限，使其可以访问私有字段
                field.setAccessible(true);
                // 获取字段的值
                Object value = field.get(object);
                // 输出字段名和值
                if (value != null && StringUtils.isNotBlank(value.toString())) {
                    confirmSql.setParam(field.getName(), value);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return confirmSql;
    }


}
