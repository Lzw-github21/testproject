package cn.ecasoft.dataexchange.entity.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MaterialListClass {

    /**
     * 附件分组id
     */
    private String GROUP_ID;
    /**
     * 项目代码
     */
    private String PROJECT_CODE;
    /**
     * 统一办建号
     */
    private String UNIQUE_NUM;
    /**
     * 材料主键
     */
    private String MATERIAL_UUID;
    /**
     * 材料编码
     */
    private String MATE_SORT_UUID;
    /**
     * 材料名称
     */
    private String MATERIAL_NAME;
    /**
     * 材料类型
     */
    private String MATERIAL_TYPE;
    /**
     * 材料状态
     */
    private String MATERIAL_STATE;
    /**
     * 是否承诺
     */
    private String IS_CNZ;
    /**
     * 材料是否必选
     */
    private String IS_REQUIRED;
    /**
     * 材料文件名称
     */
    private String FILE_NAME;
    /**
     * 材料文件地址，材料文件oss地址
     */
    private String FILE_PATH;
    /**
     * 数据更新时间
     */
    private String EXCHANGE_TIME;
}
