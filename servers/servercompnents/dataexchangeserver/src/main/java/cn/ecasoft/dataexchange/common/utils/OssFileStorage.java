package cn.ecasoft.dataexchange.common.utils;


import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.CopyObjectRequest;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.ObjectMetadata;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

@Slf4j
@Getter
@Setter
@Component
public class OssFileStorage implements FileStorage {

//    volatile是Java提供的一种轻量级的同步机制,在并发编程中,也扮演着比较重要的角色.
//    同synchronized相比(synchronized通常称为重量级锁),volatile更轻量级,相比使用
//    synchronized所带来的庞大开销,倘若能恰当的合理的使用volatile,则wonderful
    //之前方法oss和OSSClientBuilder都是静态的，但是在多oss数据源的情况下就会出现问题
    private OSSClientBuilder ossClientBuilder;
    private OSS client;
    private String accessKey = "";
    private String secretKey;
    private String endPoint;

    public OssFileStorage(String accessKey, String secretKey, String endPoint) {
        this.accessKey = accessKey;
        this.secretKey = secretKey;
        this.endPoint = endPoint;
    }

    public OssFileStorage() {
    }

    private OSS getOSSClient() {
        if (client == null) {
            client = getOSSClientBuilder().build(endPoint, accessKey, secretKey);
        }
        //打印下地址
        log.error("oss client 地址--->" + client.toString());
        return client;
    }

    private OSSClientBuilder getOSSClientBuilder() {
        System.out.println("获取OSSClientBuilder");
        if (ossClientBuilder == null) {
            System.out.println("OSSClientBuilder为空,创建中");
            if (ossClientBuilder == null) {
                System.out.println("进入同步实例化OSSClientBuilder");
                ossClientBuilder = new OSSClientBuilder();
            }
        }
        return ossClientBuilder;
    }

    /**
     * 上传到OSS服务器 如果同名文件会覆盖服务器上的
     *
     * @param bucketName
     * @param objectName
     * @param instream
     * @return
     * @throws Exception
     */
    @Override
    public Boolean upload(String bucketName, String objectName, InputStream instream) {
        OSS ossClient = getOSSClient();
        try {
            // 创建上传Object的Metadata
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(instream.available());
            objectMetadata.setCacheControl("no-cache");
            objectMetadata.setHeader("Pragma", "no-cache");
            objectMetadata.setContentType(getContentType(objectName));
            // 上传文件
            ossClient.putObject(bucketName, objectName, instream, objectMetadata);
            return true;
        } catch (Exception e) {
            throw new RuntimeException("oss上传文件异常：" + e.getMessage() + e);
        }
    }


    @Override
    public Boolean upload(String bucketName, String objectName, byte[] data) {
        OSS ossClient = getOSSClient();
        try {
            // 创建上传Object的Metadata
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(data.length);
            objectMetadata.setCacheControl("no-cache");
            objectMetadata.setHeader("Pragma", "no-cache");
            objectMetadata.setContentType(getContentType(objectName));
            // 上传文件
            InputStream stream = new ByteArrayInputStream(data);
            ossClient.putObject(bucketName, objectName, stream, objectMetadata);
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            throw new RuntimeException("oss上传文件异常：" + e.getMessage() + e);
        }
    }

    /**
     * @param fileName 文件路径
     * @Title: getContentType
     * @Description: 根据文件名称获取相应类型
     * @return: String
     */
    private String getContentType(String fileName) {
        // 文件的后缀名
        String fileExtension = fileName.substring(fileName.lastIndexOf("."));
        if (".bmp".equalsIgnoreCase(fileExtension)) {
            return "image/bmp";
        }
        if (".gif".equalsIgnoreCase(fileExtension)) {
            return "image/gif";
        }
        if (".jpeg".equalsIgnoreCase(fileExtension) || ".jpg".equalsIgnoreCase(fileExtension)
                || ".png".equalsIgnoreCase(fileExtension)) {
            return "image/jpeg";
        }
        if (".html".equalsIgnoreCase(fileExtension)) {
            return "text/html";
        }
        if (".txt".equalsIgnoreCase(fileExtension)) {
            return "text/plain";
        }
        if (".vsd".equalsIgnoreCase(fileExtension)) {
            return "application/vnd.visio";
        }
        if (".ppt".equalsIgnoreCase(fileExtension) || "pptx".equalsIgnoreCase(fileExtension)) {
            return "application/vnd.ms-powerpoint";
        }
        if (".doc".equalsIgnoreCase(fileExtension) || "docx".equalsIgnoreCase(fileExtension)) {
            return "application/msword";
        }
        if (".xml".equalsIgnoreCase(fileExtension)) {
            return "text/xml";
        }
        if (".pdf".equalsIgnoreCase(fileExtension)) {
            return "application/pdf";
        }
        // 默认返回类型
        return "image/jpeg";
    }

    /**
     * @param bucketName 桶名
     * @param objectName 文件路径
     * @Title: download
     * @Description: 根据文件路径获取InputStream流
     * @return: InputStream
     */
    @Override
    public InputStream download(String bucketName, String objectName) {
        OSS ossClient = getOSSClient();
        // ossObject包含文件所在的存储空间名称、文件名称、文件元信息以及一个输入流。
        OSSObject ossObject = ossClient.getObject(bucketName, objectName);
        try {

            // ossObject对象使用完毕后必须关闭，否则会造成连接泄漏，导致请求无连接可用，程序无法正常工作。
            ossObject.close();
            return ossObject.getObjectContent();
        } catch (Exception e) {
            throw new RuntimeException("oss下载文件异常：" + e.getMessage() + e);
        } finally {
            // ossObject对象使用完毕后必须关闭，否则会造成连接泄漏，导致请求无连接可用，程序无法正常工作。
            try {
                ossObject.close();
            } catch (IOException e) {
                throw new RuntimeException("oss下载文件异常：" + e.getMessage() + e);
            }
        }
    }

    @Override
    public String getOnlineAddress(String bucketName, String objectName, Date expiration) {
        OSS ossClient = getOSSClient();
        String ret = null;
        try {
            URL url = ossClient.generatePresignedUrl(bucketName, objectName, expiration);
            ret = url.toString();
        } catch (ClientException e) {
            throw new RuntimeException("oss获取链接异常：" + e.getMessage() + e);
        }
        return ret;
    }

    /**
     * @param bucketName 桶名
     * @param objectName 文件路径
     * @Title: downloadByte
     * @Description: 根据文件路径获取Byte流
     * @return: byte[]
     */
    @Override
    public byte[] downloadByte(String bucketName, String objectName) {
        OSS ossClient = getOSSClient();
        // ossObject包含文件所在的存储空间名称、文件名称、文件元信息以及一个输入流。
        OSSObject ossObject = ossClient.getObject(bucketName, objectName);
        try {

            InputStream inputStream = ossObject.getObjectContent();

            return IOUtils.toByteArray(inputStream);
        } catch (Exception e) {
            throw new RuntimeException("oss下载文件异常：" + e.getMessage() + e);
        } finally {
            // ossObject对象使用完毕后必须关闭，否则会造成连接泄漏，导致请求无连接可用，程序无法正常工作。
            try {
                ossObject.close();
            } catch (IOException e) {
                throw new RuntimeException("oss下载文件异常：" + e.getMessage() + e);
            }
        }
    }

    /**
     * @param bucketName 桶名
     * @param objectName 文件路径
     * @Title: getFileExist
     * @Description: 文件是否存在
     * @return: boolean
     */
    @Override
    public Boolean exist(String bucketName, String objectName) {
        OSS ossClient = getOSSClient();
        boolean exist = false;
        try {
            exist = ossClient.doesObjectExist(bucketName, objectName);
        } catch (Exception ignored) {
            throw new RuntimeException("文件查找异常：" + ignored.getMessage() + ignored);
        }
        return exist;
    }

    /**
     * @param bucketName 桶名
     * @param objectName 文件路径
     * @Title: delete
     * @Description: 删除文件
     * @return:
     */
    @Override
    public Boolean delete(String bucketName, String objectName) {
        try {
            getOSSClient().deleteObject(bucketName, objectName);
            return true;
        } catch (Exception e) {
            throw new RuntimeException("oss删除文件异常：" + e.getMessage() + e);
        }
    }

    /**
     * 文件复制
     *
     * @param bucketName
     * @param sourceObj
     * @param targetObj
     * @return
     */
    public Boolean copy(String bucketName, String sourceObj, String targetObj) {
        return this.copy(bucketName, sourceObj, bucketName, targetObj);
    }

    /**
     * 文件复制
     *
     * @param bucketName
     * @param sourceObj
     * @param targetBucket
     * @param targetObj
     * @return
     */
    public Boolean copy(String bucketName, String sourceObj, String targetBucket, String targetObj) {
        try {
            CopyObjectRequest req = new CopyObjectRequest(bucketName, sourceObj, targetBucket, targetObj);
            getOSSClient().copyObject(req);
            return true;
        } catch (Exception e) {
            throw new RuntimeException("oss复制文件异常：" + e.getMessage() + e);
        }
    }


    @Override
    public Boolean move(String bucketName, String sourceObj, String targetObj) {
        return this.copy(bucketName, sourceObj, targetObj) && this.delete(bucketName, sourceObj);
    }
}
