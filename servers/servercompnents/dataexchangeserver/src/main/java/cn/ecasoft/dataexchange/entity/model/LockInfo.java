package cn.ecasoft.dataexchange.entity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Dee
 * @date 2023/7/13
 * <p>Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LockInfo {

    /**
     锁定人
     */
    private String lockName;
    /**
     锁定人账号
     */
    private String lockNum;
    /**
     锁定原因
     */
    private String lockMark;
    /**
     锁定类型（经理，总监）
     */
    private String lockType;
    /**
     锁定操作类型
     */
    private int typeState;
    /**
     企业名称
     */
    private String corpName;
    /**
     人员姓名
     */
    private String personName;
    /**
     市
     */
    private String cityNum;
    /**
     区县
     */
    private String countyNum;
    /**
     审核账号
     */
    private String manageAdminAreaNum;
    /**
     审核账号所在市
     */
    private String manageCityNum;
    /**
     审核账号所在区县
     */
    private String manageCountyNum;
    /**
     审批人
     */
    private String auditName;
    /**
     审批时间
     */
    private String auditDateTime;
    /**
     审批意见
     */
    private String auditMark;
}
