package cn.ecasoft.dataexchange.mapper;

import cn.ecasoft.dataexchange.common.utils.Constants;
import cn.ecasoft.dataexchange.config.DbHelperExtend;
import cn.ecasoft.dataexchange.entity.db.*;
import org.nutz.dao.Sqls;
import org.nutz.dao.entity.Record;
import org.nutz.dao.sql.Sql;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;


/**
 * @author XuLiuKai
 * @Desc 这是施工许可推省平台，调用的数据库是诚信平台的数据库
 */
@Service
public class SGXKMapper extends CommonMapper {

    @Resource
    private DbHelperExtend dbHelperExtend;

    public List<Record> getTbRecordInfoByCorpCode(String CorpCode, String CorpCode10) {
        String sql = "select row_guid from DMSTAND.tbrecordinfo where RecordType in (0,4) and RecordStatus in (2,5) and ( corpcode = @CorpCode or  corpcode = @CorpCode10 or CONCAT(subString(corpcode,9,8),'-',subString(corpcode,17,1)) = @CorpCode10 )";
        Sql confirmSql = Sqls.create(sql).setParam("CorpCode", CorpCode).setParam("CorpCode10", CorpCode10);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }


    public List<Record> getTbUserInfoRowGuidByIdCard(String strIDCard, String CorpCode, String CorpCode10) {
        String sql = "select tbuserinfo.Row_Guid " +
                "from DMSTAND.tbuserinfo " +
                "inner join DMSTAND.tbrecordinfo re on(tbuserinfo.RecordGuid = re.Row_Guid) " +
                "WHERE IDCard = @strIDCard and (re.CorpCode = @CorpCode or re.CorpCode = @CorpCode10 or " +
                "CONCAT(subString(re.corpcode, 9, 8), '-', subString(re.corpcode, 17, 1)) = @CorpCode10 ) " +
                "AND (re.RecordType in (2, 5)) and (re.RecordStatus in (2, 5)) and tbuserinfo.IsDelete = 0 " +
                "ORDER By RecordDateTime DESC LIMIT 1 ";
        Sql confirmSql = Sqls.create(sql).setParam("strIDCard", strIDCard).setParam("CorpCode", CorpCode).setParam("CorpCode10", CorpCode10);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getTbUserInfoStatusByIdCard(String strIDCard, String CorpCode, String CorpCode10) {
        String sql = "select TBCorpUserRelation.IsExist from DMSTAND.tbuserinfo " +
                "inner join DMSTAND.tbrecordinfo re on(tbuserinfo.RecordGuid = re.Row_Guid) " +
                "inner join DMSTAND.TBCorpUserRelation on TBCorpUserRelation.userguid = tbuserinfo.row_guid " +
                "WHERE IDCard = @strIDCard and(re.CorpCode = @CorpCode or re.CorpCode = @CorpCode10 or CONCAT(subString(re.corpcode, 9, 8), '-', subString(re.corpcode, 17, 1)) = @CorpCode10 ) " +
                "AND(re.RecordType in(2, 5)) and(re.RecordStatus in(2, 5)) and tbuserinfo.IsDelete = 0 ORDER By RecordDateTime DESC LIMIT 1 ";
        Sql confirmSql = Sqls.create(sql).setParam("strIDCard", strIDCard).setParam("CorpCode", CorpCode).setParam("CorpCode10", CorpCode10);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getTbUserInfoRowGuidByIdCardSW(String strIDCard, String CorpCode, String CorpCode10) {
        String sql = "select tbuserinfo.Row_Guid " +
                "from DMSTAND.tbuserinfo " +
                "inner join DMSTAND.tbrecordinfo re on(tbuserinfo.RecordGuid = re.Row_Guid) " +
                "WHERE IDCard = @strIDCard and ( re.CorpCode = @CorpCode or re.CorpCode = @CorpCode10 or " +
                "CONCAT( subString(re.corpcode, 9, 8), '-', subString(re.corpcode, 17, 1)) = @CorpCode10 ) " +
                "AND (re.RecordType in (3)) and (re.RecordStatus in (2, 3, 5)) and tbuserinfo.IsDelete = 0 " +
                "ORDER By RecordDateTime DESC LIMIT 1 ";
        Sql confirmSql = Sqls.create(sql).setParam("strIDCard", strIDCard).setParam("CorpCode", CorpCode).setParam("CorpCode10", CorpCode10);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getTbUserInfoStatusByIdCardSW(String strIDCard, String CorpCode, String CorpCode10) {
        String sql = "select TBCorpUserRelation.IsExist from DMSTAND.tbuserinfo " +
                "inner join DMSTAND.tbrecordinfo re on(tbuserinfo.RecordGuid = re.Row_Guid) " +
                "inner join DMSTAND.TBCorpUserRelation on TBCorpUserRelation.userguid = tbuserinfo.row_guid " +
                "WHERE IDCard = @strIDCard and (re.CorpCode = @CorpCode or re.CorpCode = @CorpCode10 or CONCAT(subString(re.corpcode, 9, 8), '-', subString(re.corpcode, 17, 1)) = @CorpCode10 ) " +
                "AND (re.RecordType in (3)) and (re.RecordStatus in (2, 3, 5)) and tbuserinfo.IsDelete = 0 ORDER By RecordDateTime DESC LIMIT 1";
        Sql confirmSql = Sqls.create(sql).setParam("strIDCard", strIDCard).setParam("CorpCode", CorpCode).setParam("CorpCode10", CorpCode10);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getTbUserInfoIsLock(String strIDCard) {
        String sql = "select personName from tbuserinfo a " +
                "left join tbrecordinfo b on b.Row_Guid = a.RecordGuid " +
                "join TBCorpUserRelation on TBCorpUserRelation.userguid = a.row_guid " +
                "where a.isLock = 1 and b.RecordStatus in (2, 5)and a.IDCard = @strIDCard and TBCorpUserRelation.IsExist = 1 ";
        Sql confirmSql = Sqls.create(sql).setParam("strIDCard", strIDCard);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getTbxzqdmdicInfoByClass3ID(String Class3ID) {
        String sql = "SELECT * FROM tbxzqdmdic WHERE Class3ID= @Class3ID ";
        Sql confirmSql = Sqls.create(sql).setParam("Class3ID", Class3ID);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getNum(String xzqhnum) {
        String sql = "select num from(\n" +
                "select left(RIGHT(num,4),2) as num from DMSTAND.tbbuilderlicencemanagenum_nb where num like @xzqhnum \n" +
                "union \n" +
                "select left(RIGHT(builderlicencenum,4),2)as num from DMSTAND.tbbuilderlicencemanage where builderlicencenum like @xzqhnum) a \n" +
                "ORDER BY num DESC LIMIT 0,1";
        Sql confirmSql = Sqls.create(sql).setParam("xzqhnum", xzqhnum + "%");
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public void insertTbbuilderlicencemanagenum_nb(String num) {
        String sql = "INSERT into DMSTAND.tbbuilderlicencemanagenum_nb(num) VALUES(@num)";
        Sql confirmSql = Sqls.create(sql).setParam("num", num);
        dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getPrjnum(String params) {
        String sql = "select prjnum from " +
                "(select left(RIGHT(prjnum, 2), 2) as prjnum, prjnum as d from DMSTAND.tbprojectnum_nb where prjnum like @params " +
                "union  " +
                "select left(RIGHT(prjnum, 2), 2) as prjnum, prjnum as d from DMSTAND.tbprojectinfo where prjnum like @params) a " +
                "order by prjnum desc LIMIT 0, 1";
        Sql confirmSql = Sqls.create(sql).setParam("params", params + "%");
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public void insertTbprojectnum_nb(String num) {
        String sql = "INSERT into DMSTAND.tbprojectnum_nb(prjnum) VALUES(@num)";
        Sql confirmSql = Sqls.create(sql).setParam("num", num);
        dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getDataFromTBBUILDERLICENCEMANAGEByFilter(String guid, String optionType) {
        String sql = "select tbprojectinfo.CITYNUM,tbprojectinfo.COUNTYNUM,TBBUILDERLICENCEMANAGE.* " +
                "from DMSTAND.TBBUILDERLICENCEMANAGE inner join DMSTAND.tbprojectinfo on tbprojectinfo.row_guid = TBBUILDERLICENCEMANAGE.PrjGuid " +
                "where(IsFilish < > '1' or IsFilish is null) AND CENSORSTATUSNUM = 128 AND TBBUILDERLICENCEMANAGE.row_guid = @guid ";
        if ("4".equals(optionType)) {
            sql += " and TBBUILDERLICENCEMANAGE.OptionType = 3";
        } else {
            sql += " and TBBUILDERLICENCEMANAGE.OptionType in (0,2,4,1)";
        }
        Sql confirmSql = Sqls.create(sql).setParam("guid", guid);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getDataFromTBBUILDERLICENCEMANAGEByFilter(String sgxkGuid) {
        String sql = "select * from DMSTAND.TBSFCorpInfo where SgxkGuid = @sgxkGuid ";
        Sql confirmSql = Sqls.create(sql).setParam("sgxkGuid", sgxkGuid);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    //判断合同是否已经注销
    public List<Record> determineContractIsExpired(String row_guid) {
        String sql = "SELECT row_guid FROM DMSTAND.tbcontractrecordmanage WHERE STATUSNUM=115 AND OptionType=1 AND row_guid = @row_guid ";
        Sql confirmSql = Sqls.create(sql).setParam("row_guid", row_guid);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public void updateOptionTypeByRowGuid(String OptionType, String ROW_GUID) {
        String sql = " update DMSTAND.tbbuilderlicencemanage set OptionType = @OptionType where ROW_GUID = @ROW_GUID ";
        Sql confirmSql = Sqls.create(sql).setParam("OptionType", OptionType).setParam("ROW_GUID", ROW_GUID);
        dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public void deleteOptionBySgxkGuidAndAuditDate(String sgxkGuid, String AuditDate) {
        String sql = "DELETE FROM tbbuilderlicencemanage_option WHERE sgxkGuid = @sgxkGuid and  AuditDate = @AuditDate";
        Sql confirmSql = Sqls.create(sql).setParam("sgxkGuid", sgxkGuid).setParam("AuditDate", AuditDate);
        dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public void insertDataToTbbuilderlicencemanage_option(String ROW_GUID, String SgxkGuid, String OptionType, int CancelType, String CreateUser,
                                                          String CreateDate, String OptionCase, int AuditStatus, String AuditMark, String AuditDate,
                                                          String ManageUserName, String ManageDeptName, String ConsCorpCode, String UploadDate) {
        String ID = dbHelperExtend.GetPkValue(Constants.DM_MASTER_GUID, "tbbuilderlicencemanage_option", "ID").toString();
        String sql = "insert into DMSTAND.tbbuilderlicencemanage_option( \n" +
                "ROW_GUID,SgxkGuid,OptionType,CancelType,CreateUser,CreateDate,OptionCase,AuditStatus,AuditMark,AuditDate, \n" +
                "ManageUserName,ManageDeptName,ConsCorpCode,UploadDate,ID) values( \n" +
                "@ROW_GUID, '@SgxkGuid', '@OptionType', '@CancelType', '@CreateUser', '@CreateDate', '@OptionCase', '@AuditStatus', '@AuditMark', '@AuditDate', \n" +
                " '@ManageUserName', '@ManageDeptName', '@ConsCorpCode', '@UploadDate', '@ID'";
        Sql confirmSql = Sqls.create(sql).setParam("ROW_GUID", ROW_GUID).setParam("SgxkGuid", SgxkGuid).setParam("OptionType", OptionType)
                .setParam("CancelType", CancelType).setParam("CreateUser", CreateUser).setParam("CreateDate", CreateDate)
                .setParam("OptionCase", OptionCase).setParam("AuditStatus", AuditStatus).setParam("AuditMark", AuditMark)
                .setParam("AuditDate", AuditDate).setParam("ManageUserName", ManageUserName).setParam("ManageDeptName", ManageDeptName)
                .setParam("ConsCorpCode", ConsCorpCode).setParam("UploadDate", UploadDate).setParam("ID", ID);
        dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }


    public List<Record> getRowGuidFromTbbuilderlicencemanageByliceNum(String builderLicenceNum) {
        String sql = " select row_guid from DMSTAND.tbbuilderlicencemanage where CENSORSTATUSNUM = '128' and BUILDERLICENCENUM = @builderLicenceNum ";
        Sql confirmSql = Sqls.create(sql).setParam("builderLicenceNum", builderLicenceNum);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getTbxzqdmdicInfoByCityType(String CityType) {
        String sql = " select * from DMSTAND.tbxzqdmdic WHERE Class2ID = 330000 AND ClassDeep = 3 AND AdminAreaClassID = @CityType";
        Sql confirmSql = Sqls.create(sql).setParam("CityType", CityType);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public int insertDataToTBBuilderLicenceManageNew(TBBuilderLicenceManageNew entity) {
        Sql confirmSql = getInsertSqlByEntity(entity, "DMSTAND.TBBuilderLicenceManage_New");
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer getPKValue(String tableName, String fieldName) {
        return dbHelperExtend.GetPkValue(Constants.DM_MASTER_GUID, tableName, fieldName);
    }

    public Integer getTBBuilderLicenceManage_NewCountByRowGuid(String rowGuid) {
        String sql = "select count(*) count from DMSTAND.TBBuilderLicenceManage_New where ROW_GUID = @rowGuid";
        Sql confirmSql = Sqls.create(sql).setParam("rowGuid", rowGuid);
        return dbHelperExtend.queryCount(Constants.DM_MASTER_GUID, confirmSql);
    }

    public int insertDataToTbprojectinfo_New(TBProjectInfo_New entity) {
        Sql confirmSql = getInsertSqlByEntity(entity, "DMSTAND.TBProjectInfo_New");
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer getTbprojectinfo_NewCountByRowGuid(String rowGuid) {
        String sql = "select count(*) count from DMSTAND.tbprojectinfo_New where ROW_GUID = @rowGuid";
        Sql confirmSql = Sqls.create(sql).setParam("rowGuid", rowGuid);
        return dbHelperExtend.queryCount(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer getTBBuilderLicenceUnitInfo_NewCountByRowGuid(String rowGuid) {
        String sql = "select count(*) count from DMSTAND.TBBuilderLicenceUnitInfo_New where ROW_GUID = @rowGuid";
        Sql confirmSql = Sqls.create(sql).setParam("rowGuid", rowGuid);
        return dbHelperExtend.queryCount(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer insertTBBuilderLicenceUnitInfo_New(TBBuilderLicenceUnitInfo_New entity) {
        Sql confirmSql = getInsertSqlByEntity(entity, "DMSTAND.tbbuilderlicenceunitinfo_new");
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer getCountTbsfcorpinfo_newByRowGuid(String rowGuid) {
        String sql = "select count(*) count from DMSTAND.TBSFCorpInfo_New where ROW_GUID = @rowGuid";
        Sql confirmSql = Sqls.create(sql).setParam("rowGuid", rowGuid);
        return dbHelperExtend.queryCount(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer insertTBSFCorpInfo_New(TBSFCorpInfoNew entity) {
        Sql confirmSql = getInsertSqlByEntity(entity, "DMSTAND.TBSFCorpInfo_New");
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer getCountPre_sgxk_clqkByRowGuid(String rowGuid) {
        String sql = "select count(*) count from DMSTAND.TBSFCorpInfo_New where ROW_GUID = @rowGuid";
        Sql confirmSql = Sqls.create(sql).setParam("rowGuid", rowGuid);
        return dbHelperExtend.queryCount(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer insertPre_sgxk_clqk(String rowGuid, String type, String istb) {
        String sql = "insert DMSTAND.pre_sgxk_clqk (sgxkguid,type,istb) " +
                "select row_guid, @type as type, @istb as istb from dmstand.tbbuilderlicencemanage_new " +
                "where row_guid = @rowGuid and not exists" +
                "(select * from dmstand.pre_sgxk_clqk where sgxkguid = @rowGuid and type = @type) ";
        Sql confirmSql = Sqls.create(sql).setParam("rowGuid", rowGuid).setParam("type", type).setParam("istb", istb);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer getTbbuilderlicencemanage_changeCountByRowGuid(String rowGuid) {
        String sql = "select count(1) count from DMSTAND.tbbuilderlicencemanage_change where row_guid = @rowGuid ";
        Sql confirmSql = Sqls.create(sql).setParam("rowGuid", rowGuid);
        return dbHelperExtend.queryCount(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer insertDataToTBBuilderLicenceManage_Change(BuilderLicenceManageChange entity) {
        Sql confirmSql = getInsertSqlByEntity(entity, "DMSTAND.TBBuilderLicenceManage_Change");
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer insertDataTotbbuilderlicencemanage_changedetails(ChangeDetailsEntity entity) {
        Sql confirmSql = getInsertSqlByEntity(entity, "DMSTAND.TBBuilderLicenceManage_ChangeDetails");
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer insertDataToTBProjectInfo_Change(TBProjectInfoChange entity) {
        Sql confirmSql = getInsertSqlByEntity(entity, "DMSTAND.TBProjectInfo_Change");
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer insertDataToTBBuilderLicenceUnitInfo_Change(TBBuilderLicenceUnitInfoChange entity) {
        Sql confirmSql = getInsertSqlByEntity(entity, "DMSTAND.TBBuilderLicenceUnitInfo_Change");
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer insertDataToTBBuilderLicenceUnitInfo_Change(TBSFCorpInfoChange entity) {
        Sql confirmSql = getInsertSqlByEntity(entity, "DMSTAND.tbsfcorpinfo_change");
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer updateTBBuilderLicenceManage_ChangeByCondition(TBBuilderLicenceManage_New entity, String condition) {
        Sql confirmSql = getUpdateSqlByEntity(entity, "DMSTAND.TBBuilderLicenceManage_new", condition);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer updateTBPROJECTINFO_NEWByCondition(TBProjectInfoChange entity, String condition) {
        Sql confirmSql = getUpdateSqlByEntity(entity, "DMSTAND.TBProjectInfo_Change", condition);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> selectDataFromTBBuilderLicenceUnitInfo_NewBysgxkGuid(String sgxkGuid) {
        String sql = "select * from DMSTAND.TBBuilderLicenceUnitInfo_New where sgxkGuid = @sgxkGuid ";
        Sql confirmSql = Sqls.create(sql).setParam("sgxkGuid", sgxkGuid);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer getCountTBBuilderLicenceUnitInfo_NewByRowGuid(String ROW_GUID) {
        String sql = "select count(1) count from DMSTAND.TBBuilderLicenceUnitInfo_New where  ROW_GUID = @ROW_GUID ";
        Sql confirmSql = Sqls.create(sql).setParam("ROW_GUID", ROW_GUID);
        return dbHelperExtend.queryCount(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer updateTBBuilderLicenceUnitInfo_NewByRowGuid(TBBuilderLicenceUnitInfo_New entity, String condition) {
        Sql confirmSql = getUpdateSqlByEntity(entity, "DMSTAND.tbbuilderlicenceunitinfo_new", condition);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> selectDataFromTBBuilderLicenceUnitInfo_New(String sgxkGuid, String rowGuids) {
        String sql = "select * from DMSTAND.TBBuilderLicenceUnitInfo_New where SgxkGuid = @sgxkGuid and ROW_GUID not in ( @rowGuids ) ";
        Sql confirmSql = Sqls.create(sql).setParam("sgxkGuid", sgxkGuid).setParam("rowGuids", rowGuids);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    //"delete from TBBuilderLicenceUnitInfo_New where  ROW_GUID ='{0}'"
    public Integer deleteTBBuilderLicenceUnitInfo_NewByRowGuid(String rowGuid) {
        String sql = "delete from DMSTAND.TBBuilderLicenceUnitInfo_New where  ROW_GUID = @rowGuid";
        Sql confirmSql = Sqls.create(sql).setParam("rowGuid", rowGuid);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    //TBBUILDERLICENCEMANAGE
    public Integer updateTBBUILDERLICENCEMANAGEByRowGuid(TBBuilderLicenceManage entity, String condition) {
        Sql confirmSql = getUpdateSqlByEntity(entity, "DMSTAND.TBBuilderLicenceManage", condition);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    //通用的update 方法
    public Integer updateTableByCondition(Object entity, String condition, String tableName) {
        Sql confirmSql = getUpdateSqlByEntity(entity, tableName, condition);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    //通用的insert 方法
    public Integer insertTable(Object entity, String tableName) {
        Sql confirmSql = getInsertSqlByEntity(entity, tableName);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getDataFromTBBuilderLicenceUnitInfoBySgxkGuid(String sgxkGuid) {
        String sql = "select * from DMSTAND.TBBuilderLicenceUnitInfo  where sgxkGuid = @sgxkGuid ;";
        Sql confirmSql = Sqls.create(sql).setParam("sgxkGuid", sgxkGuid);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    //"select count(1) from TBBuilderLicenceUnitInfo where  ROW_GUID='{0}' "
    public Integer getCountTBBuilderLicenceUnitInfoByRowGuid(String ROW_GUID) {
        String sql = "select count(1) from DMSTAND.TBBuilderLicenceUnitInfo where  ROW_GUID = @ROW_GUID ;";
        Sql confirmSql = Sqls.create(sql).setParam("ROW_GUID", ROW_GUID);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getDataFromTBBuilderLicenceUnitInfoBySgxkGuid(String sgxkGuid, String row_guid) {
        String sql = "select * from DMSTAND.TBBuilderLicenceUnitInfo where sgxkGuid = @sgxkGuid and ROW_GUID not in ( @row_guid )";
        Sql confirmSql = Sqls.create(sql).setParam("sgxkGuid", sgxkGuid).setParam("row_guid", row_guid);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }

    public Integer deleteTBBuilderLicenceUnitInfoByRowGuid(String ROW_GUID) {
        String sql = "delete from DMSTAND.TBBuilderLicenceUnitInfo where  ROW_GUID = @ROW_GUID ";
        Sql confirmSql = Sqls.create(sql).setParam("ROW_GUID", ROW_GUID);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getDataFromTBSFCorpInfo_NewBySgxk(String sgxkGuid) {
        String sql = "select * from DMSTAND.TBSFCorpInfo_New where SgxkGuid = @sgxkGuid";
        Sql confirmSql = Sqls.create(sql).setParam("sgxkGuid", sgxkGuid);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }


    public Integer deleteTBSFCorpInfo_NewBySgxkGuid(String sgxkGuid) {
        String sql = "delete from DMSTAND.TBSFCorpInfo_New where SgxkGuid = @sgxkGuid";
        Sql confirmSql = Sqls.create(sql).setParam("sgxkGuid", sgxkGuid);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public List<Record> getDataFromTBSFCorpInfoBySgxk(String sgxkGuid) {
        String sql = "select * from DMSTAND.TBSFCorpInfo where SgxkGuid = @sgxkGuid";
        Sql confirmSql = Sqls.create(sql).setParam("sgxkGuid", sgxkGuid);
        return dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
    }


    public Integer delTBSFCorpInfoBySgxkGuid(String sgxkGuid){
        String sql = "delete from TBSFCorpInfo where SgxkGuid = @sgxkGuid ";
        Sql confirmSql = Sqls.create(sql).setParam("sgxkGuid", sgxkGuid);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public int getCountFrom(String row_guid){
        String sql = "select row_guid from tbbuilderlicencemanage where row_guid = @row_guid UNION ALL " +
                "select row_guid from tbbuilderlicencemanage_new where row_guid = @row_guid ";
        Sql confirmSql = Sqls.create(sql).setParam("row_guid", row_guid);
        List<Record> records = dbHelperExtend.queryList(Constants.DM_MASTER_GUID, confirmSql);
        return records.size();
    }

    //"Delete from SYS_ECAFILES where TO_ROW_GUID='{0}' AND GROUPGUID='6c780c47-318e-46fb-a75b-ddce15284d49'", strYeWuID);
    //
    public int deleteSYS_ECAFILESByTO_ROW_GUID(String TO_ROW_GUID){
        String sql = "Delete from SYS_ECAFILES where TO_ROW_GUID = @TO_ROW_GUID AND GROUPGUID = '6c780c47-318e-46fb-a75b-ddce15284d49'";
        Sql confirmSql = Sqls.create(sql).setParam("TO_ROW_GUID", TO_ROW_GUID);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

    public int insertSYS_ECAFILES(HashMap<String,Object> paramsMap){
        String sql = "INSERT INTO SYS_ECAFILES(AFFIXID, FILENAME, FILETYPE, FILESIZE, CREATEUSERID, CREATEDATE, VIRTUALPATH, " +
                "ACTUALNAME, GROUPGUID, ROW_GUID, REMARK, ISCRYPTED, ISDELETED, UPLOADUSERID, UPLOADUSERNAME, FILEGUID, TO_ROW_GUID) " +
                "VALUES(@AFFIXID,@FILENAME,@FILETYPE,@FILESIZE,@CREATEUSERID,@CREATEDATE,@VIRTUALPATH,@ACTUALNAME,@GROUPGUID," +
                "@ROW_GUID,@REMARK,@ISCRYPTED,@ISDELETED,@UPLOADUSERID,@UPLOADUSERNAME,@FILEGUID,@TO_ROW_GUID);";
        Sql confirmSql = Sqls.create(sql).setParams(paramsMap);
        return dbHelperExtend.queryInt(Constants.DM_MASTER_GUID, confirmSql);
    }

}
