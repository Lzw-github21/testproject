package cn.ecasoft.dataexchange.common.mms.model;

import cn.ecasoft.dataexchange.common.mms.util.SDKToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @description: 移动短信
 * @author: 杨攀  2020/8/12 16:52
 */
public class SubmitRsp {
    private boolean success;
    private String rspcod;
    private String msgId;

    public SubmitRsp() {
    }

    public SubmitRsp(boolean success, String rspcod) {
        this.success = success;
        this.rspcod = rspcod;
    }

    public SubmitRsp(boolean success, String rspcod, String msgId) {
        this.success = success;
        this.rspcod = rspcod;
        this.msgId = msgId;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getRspcod() {
        return this.rspcod;
    }

    public void setRspcod(String rspcod) {
        this.rspcod = rspcod;
    }

    public String getMsgId() {
        return this.msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String toString() {
        ToStringBuilder tsb = new ToStringBuilder(this, SDKToStringStyle.getInstance());
        tsb.append("success", this.success);
        tsb.append("rspcod", this.rspcod);
        tsb.append("msgId", this.msgId);
        return tsb.toString();
    }
}
