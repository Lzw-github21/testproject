package cn.ecasoft.dataexchange.services.impl;

import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.common.utils.Constants;
import cn.ecasoft.dataexchange.mapper.OFDWebMapper;
import cn.ecasoft.dataexchange.services.DZZZService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author XuLiuKai
 */
@Service
public class DZZZServiceImpl implements DZZZService {
    private final String filePath = "D:\\ECA-JAVA\\publish\\DZZS\\";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private OFDWebMapper ofdWebMapper;

    public static String SignElectronicCertRequest(String appsecret, Map<String, String> parameters) throws Exception {
        Map<String, String> sortedParams = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        sortedParams.putAll(parameters);
        StringBuilder query = new StringBuilder();
        for (Map.Entry<String, String> entry : sortedParams.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (!StringUtils.isEmpty(key) && !StringUtils.isEmpty(value) && !key.equals("sign")) {
                query.append(key).append("=").append(value).append("&");
            }
        }
        query.append("appsecret=").append(appsecret);
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        byte[] bytes = md5.digest(query.toString().toLowerCase().getBytes(StandardCharsets.UTF_8));
        return Arrays.toString(bytes);
    }


    public boolean FileToBase64String(String Url, String CityNum, String rowguid, String eCertID, String newFile) {
        try {
            if (!new File(this.filePath + CityNum + "/" + rowguid).exists()) {
                new File(this.filePath + CityNum + "/" + rowguid).mkdirs();
            }
            if (new File(newFile).exists()) {
                new File(newFile).delete();
            }
            FileOutputStream fos = new FileOutputStream(newFile, true);
            // 设置参数
            HttpURLConnection request = (HttpURLConnection) new URL(Url).openConnection();
            //发送请求并获取相应回应数据
            HttpURLConnection response = (HttpURLConnection) request.getURL().openConnection();
            InputStream responseStream = response.getInputStream();
            //创建本地文件写入流
            byte[] bArr = new byte[1024];
            int size = responseStream.read(bArr, 0, bArr.length);
            while (size > 0) {
                fos.write(bArr, 0, size);
                size = responseStream.read(bArr, 0, bArr.length);
            }
            fos.close();
            responseStream.close();
            return true;
        } catch (Exception e) {

            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String PushToDZZZ(String RowGuid, String strVersion, String strJson) {
        try {
//            String filePath = this.filePath + CityNum + File.separator + RowGuid + File.separator + eCertID + ".OFD";
//            boolean flag = FileToBase64String(strFilePath, CityNum, RowGuid, eCertID, filePath);
//            if (!flag) {
//                return RetMsgUtil.fail("图片操作失败").toJsonString();
//            }
            DataTable dataTable = ofdWebMapper.getInfoByRecordGuid(RowGuid);
            String PrjCode = dataTable.get(0).getString("PrjCode");
            String ECertID = dataTable.get(0).getString("ECertID");
            String unique_num = dataTable.get(0).getString("UniqueNum");

            //需要推送的文件地址： http://59.202.42.251/jstjzscjg/oss/upload/ScanImageList/SGXKDZZS/2023-330102-65-01-000231/330100231103TZ0000468/test.OFD
            //格式：http://59.202.42.251/jstjzscjg/oss/upload/ScanImageList/SGXKDZZS/PrjCode/unique_num/ECertID.ofd
            String ossurl = Constants.OSS_END_POINT + "/" + Constants.OSS_BUCKET_NAME +
                    "/oss/upload/ScanImageList/SGXKDZZS/" + PrjCode + "/" + unique_num + "/" + ECertID + ".ofd";

            String urlString = "http://210.12.219.18/EleCertWebAPI_ZS/OpenAPI/v1/AddElectronCert";
            //下面的请求代码改自c#代码，c#代码是直接从电子证照库提供的示例代码中复制的
            //文件名字：20200519-建办市[2020]25号-住房和城乡建设部办公厅关于全面推行建筑工程施工许可证电子证照的通知-附件2-业务规程.doc
            String method = "AddElectronCert";
            String appid = "7f606529-6069-4307-b71f-623792aac42c";
            String format = "json";
            Map<String, String> dic = new HashMap<>();
            dic.put("appid", appid);
            dic.put("data", strJson);
            dic.put("format", format);
            dic.put("method", method);
            dic.put("version", strVersion);
            dic.put("fileUrl", ossurl);

            //生成sign
            String sign = SignElectronicCertRequest("154909d8-4314-4e0e-9e10-e8ffc0fcc163", dic);

            byte[] fileContentByte = new byte[1024]; //文件内容二进制

            String boundary = "zhejiang";
            String Enter = "\r\n";
            String fileName = ECertID + ".ofd";
            String methodStr = "--" + boundary + Enter + "Content-Disposition: form-data; name=\"method\"" + Enter + Enter + method;
            String versionStr = Enter + "--" + boundary + Enter + "Content-Disposition: form-data; name=\"version\"" + Enter + Enter + strVersion;
            String appidStr = Enter + "--" + boundary + Enter + "Content-Disposition: form-data; name=\"appid\"" + Enter + Enter + appid;
            String formatStr = Enter + "--" + boundary + Enter + "Content-Disposition: form-data; name=\"format\"" + Enter + Enter + format;
            String signStr = Enter + "--" + boundary + Enter + "Content-Disposition: form-data; name=\"sign\"" + Enter + Enter + sign;
            String fileUrlStr = Enter + "--" + boundary + Enter + "Content-Disposition: form-data; name=\"fileUrl\"" + Enter + Enter + filePath;
            String fileContentStr = Enter + "--" + boundary + Enter + "Content-Type:application/octet-stream" + Enter + "Content-Disposition: form-data; name=\"fileContent\"; filename=\"" + fileName + "\"" + Enter + Enter;
            String dataStr = Enter + "--" + boundary + Enter + "Content-Disposition: form-data; name=\"data\"" + Enter + Enter + strJson + Enter + "--" + boundary + "--";

            byte[] methodStrByte = methodStr.getBytes(StandardCharsets.UTF_8);
            byte[] signStrByte = signStr.getBytes(StandardCharsets.UTF_8);
            byte[] dataStrByte = dataStr.getBytes(StandardCharsets.UTF_8);
            byte[] appidStrByte = appidStr.getBytes(StandardCharsets.UTF_8);
            byte[] formatStrByte = formatStr.getBytes(StandardCharsets.UTF_8);
            byte[] fileUrlStrByte = fileUrlStr.getBytes(StandardCharsets.UTF_8);
            byte[] versionStrByte = versionStr.getBytes(StandardCharsets.UTF_8);
            byte[] fileContentStrByte = fileContentStr.getBytes(StandardCharsets.UTF_8);

            // 创建HttpURLConnection对象
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

            // 获取输出流
            OutputStream outputStream = connection.getOutputStream();

            // 将各个二进制数据按顺序写入请求流
            outputStream.write(methodStrByte);
            outputStream.write(versionStrByte);
            outputStream.write(appidStrByte);
            outputStream.write(formatStrByte);
            outputStream.write(signStrByte);
            outputStream.write(fileUrlStrByte);
            outputStream.write(fileContentStrByte);
            outputStream.write(fileContentByte);
            outputStream.write(dataStrByte);

            // 获取响应流
            InputStream myResponseStream = connection.getInputStream();
            // 创建StreamReader，并指定编码为UTF-8
            InputStreamReader myStreamReader = new InputStreamReader(myResponseStream, StandardCharsets.UTF_8);
            // 读取整个响应并将其转换为字符串
            String retString = new BufferedReader(myStreamReader).readLine();
            System.out.println(retString);
            // 关闭流
            myStreamReader.close();
            myResponseStream.close();
            // 关闭输出流
            outputStream.close();
            return retString;
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));
            return RetMsgUtil.fail("系统出现异常").toJsonString();
        }
    }


}
