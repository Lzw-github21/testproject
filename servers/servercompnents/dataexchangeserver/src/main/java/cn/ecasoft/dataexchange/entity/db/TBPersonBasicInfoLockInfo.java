package cn.ecasoft.dataexchange.entity.db;

import org.nutz.dao.entity.annotation.Table;

import java.util.Date;

@Table("TBPersonBasicInfoLockInfo")
public class TBPersonBasicInfoLockInfo {
    public String row_Guid;
    public Integer id;
    public String corpCode;
    public Integer personNum;
    public String idCardTypeNum;
    public String idCard;
    public String lockManageDepNum;
    public String lockUserName;
    public Date lockDate;
    public String lockMark;
    public Integer isDeBlock;
    public Integer deBlockManageDepNum;
    public String deBlockUserName;
    public Date deBlockDate;
    public String deBlockMark;
    public Integer lockType;
    public String cityNum;
    public String countyNum;
    public int typeState;
    public String sgxkGuid;
    public String recordGuid;
}
