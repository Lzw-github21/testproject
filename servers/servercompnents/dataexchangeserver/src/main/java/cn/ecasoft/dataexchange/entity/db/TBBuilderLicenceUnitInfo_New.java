package cn.ecasoft.dataexchange.entity.db;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Table;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Setter
@Getter
@Table("tbbuilderlicenceunitinfo_new")
public class TBBuilderLicenceUnitInfo_New {
    public String row_Guid;
    public Integer id;
    public String unitCode;
    public String subPrjName;
    public BigDecimal invest;
    public BigDecimal buildArea;
    public String floorCount;
    public String bottomFloorCount;
    public BigDecimal buildHeight;
    public String prjLevelNum;
    public String pjrSize;
    public String memo;
    public BigDecimal subProjectLength;
    public BigDecimal subProjectSpan;
    public String structureTypeNum;
    public BigDecimal floorBuildArea;
    public BigDecimal bottomFloorBuildArea;
    public String prjGuid;
    public String isSelf;
    public String sgxkGuid;
    public BigDecimal rfBottomArea;
    public String zlNum;
    public Integer isShockisolationBuilding;
    public Integer isGreenBuilding;
    public String greenBuidingLevel;
    public String seismicIntensityScale;
    public Integer isSuperHightBuilding;
    public Integer suiteCount;
    public BigDecimal structureHeight;
    public BigDecimal singleSpanRC;
    public BigDecimal singleSpanHS;
    public Timestamp createTime;
    public Timestamp upDateTime;
}
