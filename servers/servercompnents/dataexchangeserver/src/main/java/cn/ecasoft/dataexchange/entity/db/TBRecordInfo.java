package cn.ecasoft.dataexchange.entity.db;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Table;


import java.util.Date;

@Setter
@Getter
@Table("TBRecordInfo")
public class TBRecordInfo {

    private int id;
    private String row_Guid;
    private Integer recordType;
    private Integer recordStatus;
    private String corpName;
    private String corpCode;
    private Date recordDateTime;
    private Date passDateTime;
    private Integer provinceNum;
    private Integer cityNum;
    private Integer countyNum;
    private Date upDateTm;
    private String eca_FullTextField;
    private Integer isDelete;
    private Integer isUpload;
    private Date createTime;
    private Date upDateTime;
    private String auditStatus;
    private String auditOption;
    private String manageUserName;
    private String manageDeptName;
    private Date auditTime;
    private String overTime;
    private Date pe_RecordTime;
    private Integer pe_RecordStatus;
    private String pe_RecordOption;


}

