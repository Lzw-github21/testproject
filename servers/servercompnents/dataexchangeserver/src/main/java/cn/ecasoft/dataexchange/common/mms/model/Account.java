package cn.ecasoft.dataexchange.common.mms.model;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

/**
 * @description: 移动短信
 * @author: 杨攀  2020/8/12 17:15
 */
public class Account {
    private String name;
    private String password;
    private String ecname;

    public Account(String name, String password, String ecname) {
        this.setName(name);
        this.setPassword(password);
        this.setEcname(ecname);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("name is blank");
        } else {
            this.name = name;
        }
    }

    public String getPassword() {
        return this.password;
    }

    public String getEcname() {
        return this.ecname;
    }

    public void setEcname(String ecname) {
        this.ecname = ecname;
    }

    public void setPassword(String password) {
        if (password == null) {
            throw new NullPointerException("password");
        } else {
            this.password = DigestUtils.md5Hex(password);
        }
    }

    public String toString() {
        return this.name + ":" + this.password + ":" + this.ecname;
    }
}
