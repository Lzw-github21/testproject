package cn.ecasoft.dataexchange.services;

/**
 * @author XuLiuKai
 * @Desc 推送电子证照库
 */
public interface DZZZService {

    String PushToDZZZ(String RowGuid, String strVersion, String strJson);

}
