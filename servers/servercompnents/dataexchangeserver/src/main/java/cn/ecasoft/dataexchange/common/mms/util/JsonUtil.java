package cn.ecasoft.dataexchange.common.mms.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * @description: 移动短信
 * @author: 杨攀  2020/8/12 16:47
 */
public class JsonUtil {
    public JsonUtil() {
    }

    public static String toJsonString(Object o) {
        return JSON.toJSONString(o, new SerializerFeature[]{SerializerFeature.IgnoreNonFieldGetter});
    }

    public static <T> T fromJsonString(String text, Class<T> clazz) {
        return JSON.parseObject(text, clazz);
    }
}

