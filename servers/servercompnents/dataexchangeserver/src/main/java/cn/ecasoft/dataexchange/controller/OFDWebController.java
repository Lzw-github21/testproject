package cn.ecasoft.dataexchange.controller;

import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.services.OFDWebService;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author XuLiuKai
 */
@RestController
@RequestMapping(value = "/ofd/KYUCHGK2P2E", produces = "application/json;charset=utf-8")
public class OFDWebController {

    @Resource
    private OFDWebService ofdWebService;
    @Resource
    private QrConfig qrConfig;

    /**
     * @param zb
     * @param fb
     * @param qrCodeBase64Content
     * @param sealCode
     * @param row_guid            备案表的row_guid,其他表中的RecordGuid
     * @param type                用于区分新申请还是变更
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/getOFDFile")
    public RetMsgUtil<Object> getOFDFile(@RequestParam("zb") String zb,
                                         @RequestParam("fb") String fb,
                                         @RequestParam("qrCodeBase64Content") String qrCodeBase64Content,
                                         @RequestParam("sealCode") String sealCode,
                                         @RequestParam("row_guid") String row_guid) throws Exception {
        if (StringUtils.isAnyBlank(zb, fb, qrCodeBase64Content, sealCode, row_guid)) {
            return RetMsgUtil.failFromBadParam();
        }
        //将qrCodeBase64Content转化为二维码数据，然后去掉标头
        qrCodeBase64Content = QrCodeUtil.generateAsBase64(qrCodeBase64Content, qrConfig, "PNG").
                replace("data:image/PNG;base64,", "");
        return ofdWebService.getOFDFile(zb, fb, qrCodeBase64Content, sealCode, row_guid);
    }

    @PostMapping(value = "/getOSSParams")
    public RetMsgUtil<Object> getOSSParams(){
        return ofdWebService.getOSSParams();
    }

}
