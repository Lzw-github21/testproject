package cn.ecasoft.dataexchange.entity.enums;

/**
 * @author Dee
 * @date 2023/7/10
 * <p>Description: Sql枚举类
 */
public enum SqlRepos {

    TEST_SQL("select * from sys_staffdef limit 1"),
    SELECT_GUID_FROM_BUILD("select row_guid from tbbuilderlicencemanage where censorstatusnum='128' and builderlicencenum=@builderlicencenum"),
    SELECT_XZQH_BY_COND("select * from tbxzqdmdic where class2id=330000 and classdeep=3 and adminareaclassid=@citytype"),
    // 查询四方主体信息
    SELECT_SF_INFO("select e.CorpName, e.CorpCode, e.CorpLeader, e.CorpLeaderCardNum, e.CorpLeaderPhone from tbbuilderlicencemanage_new b LEFT JOIN tbrecordinfo_new r ON b.rowguid=r.rowguid left join tbsfcorpinfo_new e on e.sgxkguid = b.Guid WHERE r.Status=104 AND b.rowguid=@guid and e.CorpTypeNum = @corptype")
    // 查询项目信息




    ;

    public final String sql;

    SqlRepos(String sql) {
        this.sql = sql;
    }
}
