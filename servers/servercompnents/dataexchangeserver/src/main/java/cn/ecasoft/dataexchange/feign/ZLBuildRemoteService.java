package cn.ecasoft.dataexchange.feign;


import cn.ecasoft.dataexchange.config.MyFeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 和浙里建数据交互
 *
 * @author yangsen
 * @date 2021/5/11 14:21
 */
@FeignClient(url = "https://www.zhelibuild.com", name = "ZLBuildRemoteService", configuration = MyFeignConfig.class)
public interface ZLBuildRemoteService {


    /**
     * 通过企业名称模糊匹配获取“勘察设计”企业列表
     *
     * @param code
     * @return 浙里建单点登录code
     */
    @PostMapping(value = "/authserver/auth/GetAccessToken")
    String getAccessToken(@RequestParam("code") String code);

}
