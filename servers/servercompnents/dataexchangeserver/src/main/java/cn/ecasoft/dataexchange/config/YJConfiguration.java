package cn.ecasoft.dataexchange.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "yjdatalibconfig")
public class YJConfiguration {
    private String allPersonInfo;//阳江市诚信平台人员信息
    private String allEntInfo;//阳江市诚信平台企业
    private String interior;//广东省省内接口
    private String external;//进粤的接口
    private String interiorQuaGuid;//省内企业资质
    private String interiorBaseGuid;//省内企业基本信息
    private String interiorSafeLicenceGuid;//企业安全生产许可证信息
    private String interiorPreBaseGuid;//省内人员基本信息
    private String interiorRegInfoGuid;//省内人员注册证书信息
    private String interiorTitleGuid;//省内职称证书信息
    private String interiorSafeGuid;//安全考核合格证书信息
    private String interiorSpecGuid;//特种作业证书信息
    private String interiorFiveGuid;//五大员证书信息（预算员、监理员、质安员、施工员、材料员）
    private String interiorWorkGuid;//建筑类八大工种、园林绿化类工种从业资格证书信息
    private String externalBaseGuid;//进粤备案企业总部信息
    private String externalEntQuaGuid;//进粤备案企业总部资质信息
    private String externalEntBranchGuid;//进粤备案企业驻粤分支机构信息
    private String externalPreBaseGuid;//进粤备案人员基本信息
    private String externalPerRegGuid;//进粤备案人员执业注册证信息
    private String externalPerSafeGuid;//进粤备案人员安全生产考核合格证信息
    private String externalPerSpecGuid;//进粤备案人员特种作业操作资格证信息
    private String externalPerCertGuid;//进粤备案人员职业资格证信息 UserName
    private String userName;//接口账号
    private String password;//接口密码


    //-----------------------------------诚信平台人员信息-----------------------------------------------------------------
    public String getAllPersonInfo() {
        return allPersonInfo;
    }

    public void setAllPersonInfo(String PersonInfo) {
        this.allPersonInfo = PersonInfo;
    }

    //-----------------------------------诚信平台人员企业-----------------------------------------------------------------
    public String getAllEntInfo() {
        return allEntInfo;
    }

    public void setAllEntInfo(String EntInfo) {
        this.allEntInfo = EntInfo;
    }

    //-----------------------------------广东省省内接口-------------------------------------------------------------------
    public String getInterior() {
        return interior;
    }

    public void setInterior(String interior) {
        this.interior = interior;
    }

    //-----------------------------------进粤的接口----------------------------------------------------------------------
    public String getExternal() {
        return external;
    }

    public void setExternal(String external) {
        this.external = external;
    }

    //-----------------------------------省内企业资质--------------------------------------------------------------------
    public String getInteriorQuaGuid() {
        return interiorQuaGuid;
    }

    public void setInteriorQuaGuid(String interiorQuaGuid) {
        this.interiorQuaGuid = interiorQuaGuid;
    }

    //-----------------------------------省内企业基本信息-----------------------------------------------------------------
    public String getInteriorBaseGuid() {
        return interiorBaseGuid;
    }

    public void setInteriorBaseGuid(String interiorBaseGuid) {
        this.interiorBaseGuid = interiorBaseGuid;
    }

    //-----------------------------------企业安全生产许可证信息------------------------------------------------------------
    public String getInteriorSafeLicenceGuid() {
        return interiorSafeLicenceGuid;
    }

    public void setInteriorSafeLicenceGuid(String interiorSafeLicenceGuid) {
        this.interiorSafeLicenceGuid = interiorSafeLicenceGuid;
    }

    //-----------------------------------省内人员基本信息-----------------------------------------------------------------
    public String getInteriorPreBaseGuid() {
        return interiorPreBaseGuid;
    }

    public void setInteriorPreBaseGuid(String interiorPreBaseGuid) {
        this.interiorPreBaseGuid = interiorPreBaseGuid;
    }

    //-----------------------------------省内人员注册证书信息--------------------------------------------------------------
    public String getInteriorRegInfoGuid() {
        return interiorRegInfoGuid;
    }

    public void setInteriorRegInfoGuid(String interiorRegInfoGuid) {
        this.interiorRegInfoGuid = interiorRegInfoGuid;
    }

    //-----------------------------------省内职称证书信息-----------------------------------------------------------------
    public String getInteriorTitleGuid() {
        return interiorTitleGuid;
    }

    public void setInteriorTitleGuid(String interiorTitleGuid) {
        this.interiorTitleGuid = interiorTitleGuid;
    }

    //-----------------------------------安全考核合格证书信息--------------------------------------------------------------
    public String getInteriorSafeGuid() {
        return interiorSafeGuid;
    }

    public void setInteriorSafeGuid(String interiorSafeGuid) {
        this.interiorSafeGuid = interiorSafeGuid;
    }

    //-----------------------------------特种作业证书信息-----------------------------------------------------------------
    public String getInteriorSpecGuid() {
        return interiorSpecGuid;
    }

    public void setInteriorSpecGuid(String interiorSpecGuid) {
        this.interiorSpecGuid = interiorSpecGuid;
    }

    //-----------------------------------五大员证书信息（预算员、监理员、质安员、施工员、材料员）--------------------------------
    public String getInteriorFiveGuid() {
        return interiorFiveGuid;
    }

    public void setInteriorFiveGuid(String interiorFiveGuid) {
        this.interiorFiveGuid = interiorFiveGuid;
    }

    //-----------------------------------建筑类八大工种、园林绿化类工种从业资格证书信息----------------------------------------
    public String getInteriorWorkGuid() {
        return interiorWorkGuid;
    }

    public void setInteriorWorkGuid(String interiorWorkGuid) {
        this.interiorWorkGuid = interiorWorkGuid;
    }

    //-----------------------------------进粤备案企业总部信息--------------------------------------------------------------
    public String getExternalBaseGuid() {
        return externalBaseGuid;
    }

    public void setExternalBaseGuid(String externalBaseGuid) {
        this.externalBaseGuid = externalBaseGuid;
    }

    //-----------------------------------进粤备案企业总部资质信息-----------------------------------------------------------
    public String getExternalEntQuaGuid() {
        return externalEntQuaGuid;
    }

    public void setExternalEntQuaGuid(String externalEntQuaGuid) {
        this.externalEntQuaGuid = externalEntQuaGuid;
    }

    //-----------------------------------进粤备案企业驻粤分支机构信息-------------------------------------------------------
    public String getExternalEntBranchGuid() {
        return externalEntBranchGuid;
    }

    public void setExternalEntBranchGuid(String externalEntBranchGuid) {
        this.externalEntBranchGuid = externalEntBranchGuid;
    }

    //-----------------------------------进粤备案人员基本信息--------------------------------------------------------------
    public String getExternalPreBaseGuid() {
        return externalPreBaseGuid;
    }

    public void setExternalPreBaseGuid(String externalPreBaseGuid) {
        this.externalPreBaseGuid = externalPreBaseGuid;
    }

    //-----------------------------------进粤备案人员执业注册证信息---------------------------------------------------------
    public String getExternalPerRegGuid() {
        return externalPerRegGuid;
    }

    public void setExternalPerRegGuid(String externalPerRegGuid) {
        this.externalPerRegGuid = externalPerRegGuid;
    }

    //-----------------------------------进粤备案人员安全生产考核合格证信息--------------------------------------------------
    public String getExternalPerSafeGuid() {
        return externalPerSafeGuid;
    }

    public void setExternalPerSafeGuid(String externalPerSafeGuid) {
        this.externalPerSafeGuid = externalPerSafeGuid;
    }

    //-----------------------------------进粤备案人员特种作业操作资格证信息--------------------------------------------------
    public String getExternalPerSpecGuid() {
        return externalPerSpecGuid;
    }

    public void setExternalPerSpecGuid(String externalPerSpecGuid) {
        this.externalPerSpecGuid = externalPerSpecGuid;
    }

    //-----------------------------------进粤备案人员职业资格证信息---------------------------------------------------------
    public String getExternalPerCertGuid() {
        return externalPerCertGuid;
    }

    public void setExternalPerCertGuid(String externalPerCertGuid) {
        this.externalPerCertGuid = externalPerCertGuid;
    }

    //-----------------------------------接口账号------------------------------------------------------------------------
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    //-----------------------------------接口密码------------------------------------------------------------------------
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

