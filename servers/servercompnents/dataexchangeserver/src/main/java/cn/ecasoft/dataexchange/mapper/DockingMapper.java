package cn.ecasoft.dataexchange.mapper;

import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.common.utils.Constants;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @Description 对接专用
 * @Date 2023/9/19 10:27
 * @Created by liang
 */
@Service
public class DockingMapper extends CommonMapperByDBHelp{
    public DataTable getAttachedFiles(String row_guid) {
        String sql = "select a.row_guid,a.filename,a.filetype,a.groupguid,b.row_guid from sys_ecafiles a inner join tbrecordinfo_new b on a.to_row_guid =b.row_guid where b.row_guid=@row_guid";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("row_guid", row_guid);
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, paramsMap);
    }
}
