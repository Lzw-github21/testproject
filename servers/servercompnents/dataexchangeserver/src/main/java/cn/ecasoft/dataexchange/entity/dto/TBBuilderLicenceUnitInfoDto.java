package cn.ecasoft.dataexchange.entity.dto;

import cn.ecasoft.dataexchange.entity.db.TBBuilderLicenceUnitInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * @author XuLiuKai
 */
@Setter
@Getter
public class TBBuilderLicenceUnitInfoDto extends TBBuilderLicenceUnitInfo {

    private String structureType;
    private String aLLFloorCount;
    private String guid;
    private String rowGuid;
    private String JDGCSUserName;
    private String JDGCSIdCard;
    private String JDGCSPhone;
    private String JDGCSTEL;
    private String ZLJDYUserName;
    private String ZLJDYIdcard;
    private String ZLJDYTEL;
    private String ZLJDYPhone;
    private String NHLevel;
    private String UNITGuid;
}
