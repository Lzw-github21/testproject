package cn.ecasoft.dataexchange.entity.dto;

import cn.ecasoft.dataexchange.entity.db.BuilderLicenceManageChange;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author XuLiuKai
 */
@Getter
@Setter
public class TBBuilderLicenceManageChangeDto extends BuilderLicenceManageChange {
    private String rowGuid;
    private String PrjTypeNum;
    private String PrjNum;
    private String LegalName;
    private String LEGALMANIDCARD;
    private String BUILDERCORPLEADERPHONE;
    private String oldBuildeName;
    private Date PLANBDATE;
    private Date PLANEDATE;
    private String ConsCorpName;
    private String CONSCORPLEADER;
    private String CONSCORPLEADERCARDNUM;
    private String Guid;
}
