package cn.ecasoft.dataexchange.entity.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author XuLiuKai
 */
@Setter
@Getter
public class PushToDZZZDto {

    private String strJson;
    //地市编号
    private String CityNum;
    private String RowGuid;
    //文件的名字
    private String eCertID;
    //本地的文件位置
    private String strFilePath;
    //1.0
    private String strVersion;

}
