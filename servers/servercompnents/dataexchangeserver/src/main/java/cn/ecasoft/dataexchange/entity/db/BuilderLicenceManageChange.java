package cn.ecasoft.dataexchange.entity.db;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Table;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@Table("TBBuilderLicenceManage_Change")
public class BuilderLicenceManageChange {
    private String ROW_GUID;
    private Integer ID;
    private String BuilderLicenceNum;
    private String PrjName;
    private BigDecimal ContractMoney;
    private BigDecimal Area;
    private BigDecimal Length;
    private BigDecimal Span;
    private String PrjSize;
    private String BargainDays;
    private String ManageAdminAreaNum;
    private Integer IsBulu;
    private String SgxkGuid;
    private String PrjGuid;
    private String ChangeContent;
    private Date CreateDate;
    private Date UploadDate;
    private Date AuditDate;
    private Integer StatusNum;
    private String Mark;
    private String AuditMark;
    private String BuildCorpName;
    private String BuildCorpCode;
    private String BarCode;
    private String Address;
    private String TenderNum;
    private String CensorNum;
    private String BuldPlanNum;
    private String ProjectPlanNum;
    private Integer TradeTypeNum;
    private Integer TradeTypeBoundNum;
    private Date BargainBDate;
    private Date BargainEDate;
    private String ReleaseDate;
    private String ReleaseDeptName;
    private String AuditManagerName;
    private String PrjCode;
    private String PrjCode1;
    private String DecorateCorpName;
    private String DecorateCorpCode;
    private String DecorateCorpLeader;
    private String DecorateCorpLeaderIdCard;
    private BigDecimal DecorateArea;
    private String DecorateMark;
    private Integer IS_Qzx;
    private Integer isdzzz_shbg;
    private String AuditDeptName;
    private String BuildCorpCodeType;
    private String BuilderCorpLeaderIdCardType;
    private String ReleaseDeptCode;
    private String RissuAuth;
    private String eCertID;
    private String DataLevel;
    private Integer IsDelete;
    private Date CreateTime;
    private Date UpDateTime;
    private Integer IsUpload;
    private Date UploadTime;
    private String ChangeGuid;
    private String ZSChangeContent;
    private Integer ChangeID;
    private String UnitChangeContent;

}