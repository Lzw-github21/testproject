package cn.ecasoft.dataexchange.entity.db;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Table;

import java.math.BigDecimal;
@Getter
@Setter
@Table("TBBuilderLicenceUnitInfo_Change")
public class TBBuilderLicenceUnitInfoChange {
    private int ID;
    private String Row_Guid;
    private String UnitCode;
    private String SubPrjName;
    private BigDecimal Invest;
    private String BuildArea;
    private String FloorCount;
    private String BottomFloorCount;
    private String BuildHeight;
    private String PrjLevelNum;
    private String PjrSize;
    private String Memo;
    private String SubProjectLength;
    private String SubProjectSpan;
    private String StructureTypeNum;
    private String FloorBuildArea;
    private String BottomFloorBuildArea;
    private String PrjGuid;
    private String IsSelf;
    private String SgxkGuid;
    private String RfBottomArea;
    private String ZLNum;
    private String ChangeGuid;
    private int IsShockisolationBuilding;
    private int IsGreenBuilding;
    private String GreenBuidingLevel;
    private String SeismicIntensityScale;
    private int ISSuperHightBuilding;
    private int SuiteCount;
    private BigDecimal StructureHeight;
    private BigDecimal SingleSpanRC;
    private BigDecimal SingleSpanHS;
    private String ToRowGuid;
    private int ChangeID;
}