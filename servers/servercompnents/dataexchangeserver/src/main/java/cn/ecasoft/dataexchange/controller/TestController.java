package cn.ecasoft.dataexchange.controller;

import cn.ecasoft.basic.JsonData;
import cn.ecasoft.basic.datatable.DataRow;
import cn.ecasoft.basic.datatable.DataSet;
import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.common.CommonUtil;
import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.common.utils.OssFileStorage;
import cn.ecasoft.dataexchange.services.PushToZLJService;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import cn.hutool.http.webservice.SoapClient;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import static cn.ecasoft.dataexchange.common.utils.Constants.*;

/**
 * @author XuLiuKai
 */
@RestController
@RequestMapping(value = "/test/b84e0e67", produces = "application/json;charset=utf-8")
public class TestController {

    @Resource
    private PushToZLJService pushToZLBService;
    @Resource
    private OssFileStorage ossFileStorage;

    private final OssFileStorage FG_OSS = new OssFileStorage(FG_OSS_END_POINT, FG_OSS_ACCESS_KEY, FG_OSS_SECRET_KEY);

    @GetMapping("hello")
    public RetMsgUtil<String> helloWorld() {
        return RetMsgUtil.ok("hello, sgxk new system");
    }

    /**
     * 测试施工许可推送浙里建
     *
     * @return
     * @throws Exception
     */
    @GetMapping("3a650ec66952")
    public RetMsgUtil<String> pushToZlbTest() throws Exception {
        pushToZLBService.pushToZljFunction();
        return RetMsgUtil.ok("success");
    }

    //    @GetMapping("UUu4kgtvyoMJ7SvrBM6IEw")
    public RetMsgUtil<Object> convertToClass(@RequestBody String voStr) {
        JSONObject jo = JSONObject.parseObject(voStr);
        DataSet dataSet = new DataSet();
        jo.forEach((k, v) -> {
            DataTable dataTable = new DataTable();
            dataTable.setName(k);
            try {
                JSONArray array = JSON.parseArray(JSONObject.toJSONString(v));
                for (Object o : array) {
                    String s = JSONObject.toJSONString(o);
                    DataRow dataRow = JSONObject.parseObject(s, DataRow.class);
                    dataTable.add(dataRow);
                }
            } catch (Exception e) {

            }
            dataSet.add(dataTable);
        });

        return RetMsgUtil.ok(dataSet);
    }

    @GetMapping("8PtNiH00yIeTl")
    public String doCSharpPost(@RequestParam("OPERATE_TYPE") String OPERATE_TYPE,
                               @RequestParam("PROJECT_CODE") String PROJECT_CODE,
                               @RequestParam("UNIQUE_NUM") String UNIQUE_NUM) throws DocumentException {
        String SECURITY_KEY = "ff8080815dfd7c36015dfd9a8e2d028b";
        String fgURL = "http://223.4.69.50:18182/tzxmspall/service/projectInfoWeb?wsdl/getBuildApproveInfo";
        String soapXml = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "  <soap:Body>\n" +
                "    <getBuildApproveInfo xmlns=\"http://interaction.platform.webserviceinfo.neusoft.com/\">\n" +
                "      <SECURITY_KEY>" + SECURITY_KEY + "</SECURITY_KEY>\n" +
                "      <PROJECT_CODE>" + PROJECT_CODE + "</PROJECT_CODE>\n" +
                "      <UNIQUE_NUM>" + UNIQUE_NUM + "</UNIQUE_NUM>\n" +
                "      <OPERATE_TYPE>" + OPERATE_TYPE + "</OPERATE_TYPE>\n" +
                "    </getBuildApproveInfo>\n" +
                "  </soap:Body>\n" +
                "</soap:Envelope>";

        String resposeStr = CommonUtil.doPostSoap(fgURL, soapXml);
        System.out.println("--------------------->");
        System.out.println(resposeStr);
        JsonData signatureObj = CommonUtil.xml2Json(resposeStr);
        JsonData Body = (JsonData) signatureObj.get("Body");
        JsonData getItemMaterialListResponse = (JsonData) Body.get("getBuildApproveInfoResponse");
        JsonData out = (JsonData) getItemMaterialListResponse.get("out");
        return JSONObject.toJSONString(out);
    }


    @GetMapping("zdMon37SzrNUJA79w")
    public String PushingOFDOSSFile() {
        String eCertID = "test_eCertID";
        String citynum = "test_citynum";
        String rowguid = UUID.randomUUID().toString().replace("-", "");
        String prjcode = "test_prjcode";
        String unique_num = "test_unique_num";
        String ossfileurl = prjcode + "/" + unique_num + "/" + eCertID + ".OFD";

        //https://spgl.jst.zj.gov.cn/SgxkWeb/PSJGWeb/NewBuilderManager/Template/DZZS/330300/299ca38dacb04bfa80fe2e1a61efc749/1.2.156.3005.2.11100000000013338W009.11330324MB0U3550X1.2023082102.001.0.OFD

        String filename = eCertID + ".OFD";
        String fileurl = "http://223.4.75.212/PSJGWeb/NewBuilderManager/Template/DZZS/" + citynum + "/" + rowguid + "/" + filename;
        String fgURL = "http://223.4.65.214/OSSService/OSSWebService.asmx/wsdl/UpOss";
        String soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "  <soap:Body>\n" +
                "    <UpOss xmlns=\"http://tempuri.org/\">\n" +
                "      <strFileUrl>" + fileurl + "</strFileUrl>\n" +
                "      <strOssUrl>" + ossfileurl + "</strOssUrl>\n" +
                "      <strServiceFileName>" + filename + "</strServiceFileName>\n" +
                "    </UpOss>\n" +
                "  </soap:Body>\n" +
                "</soap:Envelope>";

        String resposeStr = CommonUtil.doPostSoap(fgURL, soapXml);
        System.out.println(resposeStr);
        JsonData signatureObj = new JsonData();
        try {
            signatureObj = CommonUtil.xml2Json(resposeStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonData Body = (JsonData) signatureObj.get("Body");
        JsonData getItemMaterialListResponse = (JsonData) Body.get("UpOssResponse");
        return getItemMaterialListResponse.toString();
//        return "success";
    }

    /**
     * @param file
     * @return
     * @throws IOException
     */
    @GetMapping("rJJTgeKWNyb7Vwgi3CsciQ")
    public String filePush(@RequestParam("buf") MultipartFile file) throws IOException {
        //初始化oss
        ossFileStorage.setEndPoint(OSS_END_POINT);
        ossFileStorage.setAccessKey(OSS_ACCESS_KEY);
        ossFileStorage.setSecretKey(OSS_SECRET_KEY);

        String objectName = "oss/upload/ScanImageList/" + UUID.randomUUID() + ".jpg";
        if (!ossFileStorage.upload(OSS_BUCKET_NAME, objectName, file.getBytes())) {
            return "上传失败";
        }
        return objectName;
    }


    @GetMapping("4il5wgdwuDG1Mnev64fB4g")
    public JsonData webserviceTest(@RequestParam("OPERATE_TYPE") String OPERATE_TYPE,
                                   @RequestParam("PROJECT_CODE") String PROJECT_CODE,
                                   @RequestParam("UNIQUE_NUM") String UNIQUE_NUM,
                                   @RequestParam("METHOD") String METHOD) {
        SoapClient huClient = SoapClient
                .create("http://59.202.39.226:18181/service/projectInfoWeb?wsdl/" + METHOD)
                .setMethod("nsl:" + METHOD, "http://interaction.platform.webserviceinfo.neusoft.com")
                .setParam("SECURITY_KEY", "ff8080815dfd7c36015dfd9a8e2d028b")
                .setParam("PROJECT_CODE", PROJECT_CODE)
                .setParam("UNIQUE_NUM", UNIQUE_NUM)
                .setParam("OPERATE_TYPE", OPERATE_TYPE);

        String send = huClient.send(true);
        System.out.println("response xml: " + send);
        JsonData signatureObj = new JsonData();
        try {
            signatureObj = CommonUtil.xml2Json(send);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonData Body = (JsonData) signatureObj.get("Body");
        JsonData getItemMaterialListResponse = (JsonData) Body.get(METHOD + "Response");
        return (JsonData) getItemMaterialListResponse.get("out");
    }


    @GetMapping("tvvX1m06h3Vr9")
    public byte[] filestream_tzfj(@RequestParam("url") String url) {
        RestTemplate restTemplate = new RestTemplate();
        System.out.println("------------------------->");
        System.out.println(url);
        return restTemplate.getForObject(url, byte[].class);
    }

    @GetMapping("fileTest")
    public byte[] fileTest() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://59.202.42.251/zxptv3oss/2023/10/1869ead4b6f645549cdd153b497cda27/187c723a54084395948717ae6e867a62/163e810fbadf41aea7882ed7b2466053.pdf?Expires=1698905644&OSSAccessKeyId=vMEEKZ8oBXTbrWER&Signature=eFgJSW73QK1WxPWYwdroDCF2770%3D";
        return restTemplate.getForObject(url, byte[].class);
    }


    //getItemMaterialList
    @GetMapping("08JMyOB7ETkWIS0")
    public String getItemMaterialListTest(@RequestParam("PROJECT_CODE") String PROJECT_CODE,
                                          @RequestParam("UNIQUE_NUM") String UNIQUE_NUM,
                                          @RequestParam("METHOD") String METHOD) {
        SoapClient huClient = SoapClient
                .create("http://59.202.39.226:18181/service/projectInfoWeb?wsdl/" + METHOD)
                .setMethod("nsl:" + METHOD, "http://interaction.platform.webserviceinfo.neusoft.com")
                .setParam("SECURITY_KEY", "ff8080815dfd7c36015dfd9a8e2d028b")
                .setParam("PROJECT_CODE", PROJECT_CODE)
                .setParam("UNIQUE_NUM", UNIQUE_NUM);
        String resposeStr = huClient.send(true);
        JsonData signatureObj = new JsonData();
        try {
            signatureObj = CommonUtil.xml2Json(resposeStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonData Body = (JsonData) signatureObj.get("Body");
        JsonData getItemMaterialListResponse = (JsonData) Body.get("getItemMaterialListResponse");
        JsonData out = (JsonData) getItemMaterialListResponse.get("out");
        JsonData data = (JsonData) out.get("data");
        JSONArray vo = (JSONArray) data.get("WebServiceItemMaterialVO");
        return vo.toString();
    }

    @GetMapping("Aw3h8MEtJtntl")
    public String setItemRecord() {
        String fgURL = "http://223.4.69.50:18182/tzxmspall/service/projectInfoWeb?wsdl/setItemRecord";
        String soapXml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:int=\"http://interaction.platform.webserviceinfo.neusoft.com\" xmlns:vo=\"http://vo.platform.webserviceinfo.neusoft.com\">\n" +
                "<soapenv:Body>\n" +
                "<int:setItemRecord>\n" +
                "<int:in0>ff8080815dfd7c36015dfd9a8e2d028b</int:in0>\n" +
                "   <int:in1>\n" +
                "   <vo:deal_deptid>331081</vo:deal_deptid>\n" +
                "   <vo:deal_deptname>台州市温岭建工局</vo:deal_deptname>\n" +
                "   <vo:deal_opinion>同意</vo:deal_opinion>\n" +
                "   <vo:deal_state>ITEM_BJ</vo:deal_state>\n" +
                "   <vo:deal_time>2023-10-30 11:22:05</vo:deal_time>\n" +
                "   <vo:deal_userid>33108103</vo:deal_userid>\n" +
                "   <vo:deal_username>接口自动退回</vo:deal_username>\n" +
                "   <vo:outer_key>4d29bf9a-0490-4e52-97f1-f3116df2db24</vo:outer_key>\n" +
                "   <vo:outer_type>SGXK</vo:outer_type>\n" +
                "   <vo:project_code>2203-331081-04-01-986038</vo:project_code>\n" +
                "   <vo:reply_file>2203-331081-04-01-986038/331081220926TZ0066184/1.2.156.3005.2.11100000000013338W009.113310810026792569.2023103002.002.8.OFD</vo:reply_file>\n" +
                "   <vo:reply_file_name>建筑工程施工许可证.OFD</vo:reply_file_name>\n" +
                "   <vo:reply_number>331081202209260201</vo:reply_number>\n" +
                "   <vo:reply_result>1</vo:reply_result>\n" +
                "   <vo:unique_num>331081220926TZ0066184</vo:unique_num>\n" +
                "   <vo:upload_date>2023-10-30 11:22:59</vo:upload_date>\n" +
                "</int:in1>\n" +
                "</int:setItemRecord>\n" +
                "</soapenv:Body>\n" +
                "</soapenv:Envelope>";

        HttpEntity<String> httpEntity = new HttpEntity<>(soapXml);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.postForEntity(fgURL, httpEntity, String.class);
        JsonData signatureObj = new JsonData();
        String resposeStr = response.getBody();
        try {
            signatureObj = CommonUtil.xml2Json(resposeStr);
        } catch (Exception e) {
        }
        JsonData Body = (JsonData) signatureObj.get("Body");
        //error msg
        JsonData fault = (JsonData) Body.get("Fault");
        if (fault != null && !fault.isEmpty()) {
            //返回错误信息
            return "{\"code\":\"0\"}";
        }
        JsonData getItemMaterialListResponse = (JsonData) Body.get("setItemRecordResponse");
        JsonData out = (JsonData) getItemMaterialListResponse.get("out");
        return JSONObject.toJSONString(out);
    }

    @GetMapping("HujYNXDliOcivX1leSqW1Q")
    public String loadFileTest(@RequestParam("Url") String Url) throws Exception {
        URL url = new URL(Url);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setDoInput(true);
        connection.connect();

        InputStream inputStream = connection.getInputStream();
        byte[] buffer = new byte[4096];
        int bytesRead;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }

        ossFileStorage.setEndPoint(OSS_END_POINT);
        ossFileStorage.setAccessKey(OSS_ACCESS_KEY);
        ossFileStorage.setSecretKey(OSS_SECRET_KEY);

        String objectName = "oss/sgxk/UploadImages/" + UUID.randomUUID() + ".ofd";
        if (!ossFileStorage.upload(OSS_BUCKET_NAME, objectName, buffer)) {
            return "上传失败";
        }
        Date expiration = new Date(System.currentTimeMillis() + 3600 * 1000 * 24 * 7);
        return ossFileStorage.getOnlineAddress(OSS_BUCKET_NAME, objectName, expiration);
    }

    @GetMapping("JAETajDYLes9Nwr8R7A")
    public HashMap<String, Object> getFileInfo(@RequestParam("url") String url) throws IOException {

        URL u = new URL(url);
        URLConnection conn = u.openConnection();
        HashMap<String, Object> re = new HashMap<>();
        // 获取文件名
        String filename = u.getFile();
        System.out.println("文件原名：" + filename);
        String[] split = filename.split("/");
        String s = split[split.length - 1];
        int lastIndex = s.lastIndexOf(".");
        if (lastIndex != -1) {
            s = s.substring(0, lastIndex);
        }
        re.put("文件名: ", s);
        // 获取文件后缀
        String fileExtension = "";
        int lastDotIndex = filename.lastIndexOf('.');
        if (lastDotIndex >= 0) {
            fileExtension = filename.substring(lastDotIndex + 1);
        }
        re.put("后缀: ", fileExtension);
        // 获取文件大小
        long fileSize = conn.getContentLength();
        re.put("大小: ", fileSize + "bytes");

        return re;
    }

    @GetMapping("g87VBtFex67")
    public String testSplitName(@RequestParam("fileName") String fileName) {
        String[] split = fileName.split("/");
        String simpleName = split[split.length - 1];
        int lastIndex = simpleName.lastIndexOf(".");
        if (lastIndex != -1) {
            simpleName = simpleName.substring(0, lastIndex);
        }
        return simpleName;
    }

    @GetMapping("5nGmbo0y")
    public String getData() throws Exception {
        SoapClient huClient = SoapClient
                .create("http://223.4.69.50:18182/tzxmspall/service/projectInfoWeb?wsdl/getBuildApproveInfo")
                .setMethod("nsl:getBuildApproveInfo", "http://interaction.platform.webserviceinfo.neusoft.com")
                .setParam("SECURITY_KEY", "ff8080815dfd7c36015dfd9a8e2d028b")
                .setParam("PROJECT_CODE", "2201-330329-04-01-760138")
                .setParam("UNIQUE_NUM", "330300231030TZ0000447")
                .setParam("OPERATE_TYPE", "1");
        String resposeStr = huClient.send(true);
        System.out.println(resposeStr);
        return resposeStr;
    }

    @GetMapping("getS")
    public String getS() throws Exception {
        String fgURL = "http://223.4.69.50:18182/tzxmspall/service/projectInfoWeb?wsdl/setItemRecord";
        String soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:int=\"http://interaction.platform.webserviceinfo.neusoft.com\" xmlns:vo=\"http://vo.platform.webserviceinfo.neusoft.com\">\n" +
                "<soapenv:Body>\n" +
                "<int:setItemRecord>\n" +
                "<int:in0>ff8080815dfd7c36015dfd9a8e2d028b</int:in0>\n" +
                "   <int:in1>\n" +
                "   <vo:deal_deptid>330000</vo:deal_deptid>\n" +
                "   <vo:deal_deptname>接口自动退回</vo:deal_deptname>\n" +
                "   <vo:deal_opinion>以下字段为空不能上报施工许可信息施工单位项目负责人不能为空单体【】工程总造价（万元）建筑面积工程等级建筑高度（米）结构体系</vo:deal_opinion>\n" +
                "   <vo:deal_state>ITEM_TB</vo:deal_state>\n" +
                "   <vo:deal_time>2023-10-31 10:53:48</vo:deal_time>\n" +
                "   <vo:deal_userid>330000</vo:deal_userid>\n" +
                "   <vo:deal_username>接口自动退回</vo:deal_username>\n" +
                "   <vo:outer_key>071518ab-94b2-4e9a-9cf1-a7e5dbf13f64</vo:outer_key>\n" +
                "   <vo:outer_type>TEST-xlk-xlk</vo:outer_type>\n" +
                "   <vo:project_code>2201-330329-04-01-760138</vo:project_code>\n" +
                "   <vo:reply_file></vo:reply_file>\n" +
                "   <vo:reply_file_name></vo:reply_file_name>\n" +
                "   <vo:reply_number>1</vo:reply_number>\n" +
                "   <vo:reply_result>1</vo:reply_result>\n" +
                "   <vo:unique_num>330100231030TZ0000445</vo:unique_num>\n" +
                "   <vo:upload_date></vo:upload_date>\n" +
                "</int:in1>\n" +
                "</int:setItemRecord>\n" +
                "</soapenv:Body>\n" +
                "</soapenv:Envelope>";
        return CommonUtil.doPostSoap(fgURL, soapXml);
    }

    @Autowired
    private QrConfig qrConfig;

    //二维码测试
    @GetMapping("getE")
    public String getE() {
        String content = "11100000000013338W009^1133038200253126XP^1.2.156.3005.2.11100000000013338W009.1133038200253126XP.2023102302.002.L^330382202308240202^乐清市南塘镇南浦村雨污管网改造工程^乐清市水环境处理有限责任公司^3303822308240201^91330382671628093G";
        String imageType = "PNG";
        String erStr = QrCodeUtil.generateAsBase64(content, qrConfig, imageType);
        return erStr.replace("data:image/PNG;base64,", "");
    }


    @PostMapping("pushPicsToFgOSS")
    public String pushPicsToFgOSS(@RequestParam("buf") MultipartFile file) throws IOException {
        String objectName = "oss/upload/ScanImageList/SGXKDZZS/testPushToFgOSS/panda.jpg";
        FG_OSS.upload(FG_OSS_BUCKET_NAME, objectName, file.getBytes());
        return "success";
    }
}
