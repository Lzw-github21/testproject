package cn.ecasoft.dataexchange.entity.dto;

import lombok.Data;

/**
 * @author Dee
 * @date 2023/7/13
 * <p>Description:
 */
@Data
public class BuilderLicenceManageDto {
    private String builderLicenceNum;
    private String guid;
    private String prjName;
    private String consCorpName;
    private String consCorpCode;
    private String consCorpLeader;
    private String consCorpLeaderCardNum;
    private String superCorpName;
    private String superCorpCode;
    private String superCorpLeader;
    private String superCorpLeaderCardNum;
    private String manageAdminAreaNum;
    private String releaseDate;


}
