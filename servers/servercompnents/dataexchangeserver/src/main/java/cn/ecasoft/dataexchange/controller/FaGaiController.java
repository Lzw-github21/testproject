package cn.ecasoft.dataexchange.controller;

import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.entity.model.TempMessage;
import cn.ecasoft.dataexchange.services.FaGaiService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;


/**
 * @author Dee
 * @date 2023/7/10
 * <p>Description: 发改控制器
 */
@RestController
@RequestMapping(value = "/fg/J3TVRSVYP3s", produces = "application/json;charset=utf-8")
public class FaGaiController {
    @Autowired
    private FaGaiService faGaiService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostMapping("/notice")
    public TempMessage fgNotice(@RequestParam("message_type") String messageType,
                                @RequestParam("project_code") String projectCode,
                                @RequestParam("unique_num") String uniqueNum,
                                @RequestParam("exchange_time") String exchangeTime) throws Exception {
        logger.error("调取了notice接口,projectCode:" + projectCode + ",uniqueNum:" + uniqueNum + ",message_type:" + messageType);
        TempMessage tempMessage = new TempMessage();
        if (StringUtils.isAnyBlank(messageType, projectCode)) {
            tempMessage.message = "传入参数有误";
            tempMessage.code = "0";
            return tempMessage;
        }

        FutureTask<String> task = new FutureTask((Callable<String>) () -> {
            faGaiService.fgNotice(messageType, projectCode, uniqueNum, exchangeTime);
            return "";
        });
        new Thread(task).start();

        tempMessage.code = "1";
        tempMessage.message = "审批端接收到数据，已经提交审批";
        return tempMessage;
    }

    //不传对象的原因是 eca 那边不传请求体，所以只能穿formdata
    @PostMapping("/PushingProcess")
    public RetMsgUtil<Object> PushingProcess(@RequestParam(value = "deal_state") String deal_state,
                                             @RequestParam(value = "prjcode") String prjcode,
                                             @RequestParam(value = "unique_num") String unique_num,
                                             @RequestParam(value = "citynum") String citynum,
                                             @RequestParam(value = "rowguid") String rowguid,
                                             @RequestParam(value = "sgxkguid") String sgxkguid,
                                             @RequestParam(value = "BuilderLicenceNum") String BuilderLicenceNum,
                                             @RequestParam(value = "OpentionId") String OpentionId,
                                             @RequestParam(value = "deal_deptid") String deal_deptid,
                                             @RequestParam(value = "deal_deptname") String deal_deptname,
                                             @RequestParam(value = "deal_opinion") String deal_opinion,
                                             @RequestParam(value = "deal_userid") String deal_userid,
                                             @RequestParam(value = "deal_username") String deal_username,
                                             @RequestParam(value = "reply_result") String reply_result,
                                             @RequestParam(value = "upload_date") String upload_date,
                                             @RequestParam(value = "deal_time") String deal_time) {

        return faGaiService.PushingProcess(deal_state, prjcode, unique_num, citynum, rowguid,
                sgxkguid, BuilderLicenceNum, OpentionId, deal_deptid, deal_deptname,
                deal_opinion, deal_userid, deal_username, reply_result, upload_date, deal_time) ?
                RetMsgUtil.ok() : RetMsgUtil.fail("操作失败");
    }

    @PostMapping("/PushingProcess_Change")
    public RetMsgUtil<Object> PushingProcess_Change(@RequestParam(value = "deal_state") String deal_state,
                                                    @RequestParam(value = "prjcode") String prjcode,
                                                    @RequestParam(value = "unique_num") String unique_num,
                                                    @RequestParam(value = "citynum") String citynum,
                                                    @RequestParam(value = "rowguid") String rowguid,
                                                    @RequestParam(value = "sgxkguid") String sgxkguid,
                                                    @RequestParam(value = "BuilderLicenceNum") String BuilderLicenceNum,
                                                    @RequestParam(value = "OpentionId") String OpentionId,
                                                    @RequestParam(value = "deal_deptid") String deal_deptid,
                                                    @RequestParam(value = "deal_deptname") String deal_deptname,
                                                    @RequestParam(value = "deal_opinion") String deal_opinion,
                                                    @RequestParam(value = "deal_userid") String deal_userid,
                                                    @RequestParam(value = "deal_username") String deal_username,
                                                    @RequestParam(value = "reply_result") String reply_result) {
        return faGaiService.PushingProcess_Change(deal_state, prjcode, unique_num, citynum,
                rowguid, sgxkguid, BuilderLicenceNum, OpentionId, deal_deptid, deal_deptname,
                deal_opinion, deal_userid, deal_username, reply_result) ?
                RetMsgUtil.ok() : RetMsgUtil.fail("操作失败");
    }


    @PostMapping("/PushingProcess_zx")
    public RetMsgUtil<Object> PushingProcess_zx(@RequestParam(value = "deal_state") String deal_state,
                                                @RequestParam(value = "prjcode") String prjcode,
                                                @RequestParam(value = "unique_num") String unique_num,
                                                @RequestParam(value = "sgxkguid") String sgxkguid,
                                                @RequestParam(value = "BuilderLicenceNum") String BuilderLicenceNum,
                                                @RequestParam(value = "OpentionId") String OpentionId,
                                                @RequestParam(value = "deal_deptid") String deal_deptid,
                                                @RequestParam(value = "deal_deptname") String deal_deptname,
                                                @RequestParam(value = "deal_opinion") String deal_opinion,
                                                @RequestParam(value = "deal_userid") String deal_userid,
                                                @RequestParam(value = "deal_username") String deal_username,
                                                @RequestParam(value = "reply_result") String reply_result,
                                                @RequestParam(value = "operate_type") String operate_type) {

        return faGaiService.PushingProcess_zx(deal_state, prjcode, unique_num,
                sgxkguid, BuilderLicenceNum, OpentionId, deal_deptid, deal_deptname,
                deal_opinion, deal_userid, deal_username, reply_result, operate_type) ?
                RetMsgUtil.ok() : RetMsgUtil.fail("操作失败");
    }

}
