package cn.ecasoft.dataexchange.mapper;

import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.common.utils.Constants;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @author XuLiuKai
 * @Desc 这里是推送浙里建的数据，数据源是施工许可系统，数据目标源是浙里建
 */
@Service
public class PushToZLJMapper extends CommonMapperByDBHelp {

    public void insertData(String sqlKey, HashMap<String, Object> paramsMap) throws Exception {
        super.dBhelper.QueryInt(Constants.MYSQL_ZLJ_GUID, sqlKey, paramsMap);
    }

    public int updateTSRecord(String tableName, List<String> guids) throws Exception {
        StringBuilder guidsStr = new StringBuilder();
        for (String guid : guids) {
            guidsStr.append("'").append(guid).append("'").append(",");
        }
        guidsStr.deleteCharAt(guidsStr.length() - 1);
        String sql = "update " + tableName + " set isTS = 1 where guid in (" + guidsStr + " )";
        String sqlKey = UUID.randomUUID().toString();
        ecaSqlsConfig.getMap().put(sqlKey, sql);
        HashMap<String, Object> hashMap = new HashMap<>();
        return super.dBhelper.QueryInt(Constants.SGXK_MYSQL_GUID, sqlKey, hashMap);
    }


    public DataTable getUnPushTbBuilderLicenceManageData() {
        String sql = "select DISTINCT A.Guid,A.BUILDERLICENCENUM,A.PrjCode as PrjNum,A.PRJNAME,C.PrjTypeName," +
                "A.ProvinceNum,D.AdminAreaName as ProvinceName,A.CityNum,E.AdminAreaName as CityName,A.CountyNum," +
                "F.AdminAreaName as CountyName ,A.PRJSIZE,A.ADDRESS,G.ECONTYPENAME,A.BUILDCORPNAME,A.BUILDCORPCODE," +
                "A.LEGALNAME,A.LEGALMANIDCARD,A.BUILDERCORPLEADER,A.BUILDERCORPLEADERIDCARD, A.BUILDERCORPLEADERPHONE," +
                "A.BUILDERCORPLEADERIDCARDTYPE,A.BUILDCORPPERSON,A.BUILDCORPPERSONPHONE,A.AREA,A.LENGTH,A.SPAN," +
                "A.CONTRACTMONEY,A.BARGAINDAYS, A.BARGAINBDATE,A.BARGAINEDATE,A.PLANBDATE,A.PLANEDATE,I.RoleTypeNum,case I.RoleTypeNum " +
                "when 1 then I.ProvinceNum when 2 then I.citynum when 3 then I.countynum else null end as AeptAreaCode, " +
                "case A.OptionType when 0 then '正常' when 2 then '延期' when 3 then '中止' when 4 then '恢复' when 1 then '注销' else '' end RevokeStatus," +
                "A.CreateTime,A.UpdateTime,A.ReleaseDate,A.ReleaseDeptName,B.ManageAdminAreaNum  " +
                "from tbbuilderlicencemanage_new A " +
                "inner join tbrecordinfo_new B on A.RowGuid=B.rowguid " +
                "left join tbprjtypedic C on A.PrjTypeNum=C.PRJTYPENUM " +
                "left join tbxzqdmdic D on A.ProvinceNum=D.AdminAreaClassID " +
                "left join tbxzqdmdic E on A.CityNum=E.AdminAreaClassID " +
                "left join tbxzqdmdic F on A.CountyNum=F.AdminAreaClassID " +
                "left join tbecontypedic G on A.econtypeNum=G.EconTypeNum " +
                "left join tbrecordopinioninfo_new_index H on A.RowGuid=H.rowguid " +
                "left join tbadminuser_index I on I.LoginName=H.OpinionUser " +
                "where b.Status=104 and (B.isdzzz_sgxk=1 or B.isdzzz_dzzz=1) and H.Status=104 and i.ProvinceNum>1 and A.ReleaseDate>'2018-12-31' " +
                "and (A.isTS IS NULL OR A.isTS = 0);";
        String sqlKey = "bOIvkSvm1dEGuN";
        HashMap<String, Object> paramsMap = new HashMap<>();
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }


    public DataTable getUnPushTbsfcorpinfoData() {
        String sql = "select GUID,SGXKGUID,CASE CORPTYPENUM WHEN 1 THEN '勘察' WHEN 2 THEN '设计' WHEN 3 THEN '施工' WHEN 4  THEN '监理' when 5 then '总承包' END AS CORPROLENAME," +
                "CORPNAME,CORPCODE,CORPLEADER,CORPLEADERCARDNUM,CORPLEADERPHONE from tbsfcorpinfo_new " +
                "where SgxkGuid in(" +
                "   select A.guid from tbbuilderlicencemanage_new A inner join tbrecordinfo_new B on A.RowGuid=B.rowguid " +
                "   where b.Status=104 and (B.isdzzz_sgxk=1 or B.isdzzz_dzzz=1) and A.ReleaseDate>'2018-12-31') " +
                "and (tbsfcorpinfo_new.isTS IS NULL OR tbsfcorpinfo_new.isTS = 0);";
        String sqlKey = "uG3dO5457jQ";
        HashMap<String, Object> paramsMap = new HashMap<>();
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public DataTable getUnPushTbprojectfinishunitinfoData() {
        String sql = "select GUID,SGXKGUID,STRUCTURETYPE,BUILDAREA,ALLFLOORCOUNT,INVEST,FLOORBUILDAREA,BOTTOMFLOORBUILDAREA," +
                "FLOORCOUNT,BOTTOMFLOORCOUNT,BUILDHEIGHT,PRJLEVELNUM,SUBPROJECTLENGTH,SUBPROJECTSPAN,STRUCTURETYPENUM," +
                "RFBOTTOMAREA,PJRSIZE,SUBPRJNAME,NHLEVEL,UNITCODE from tbprojectfinishunitinfo_new " +
                "where SgxkGuid in(" +
                "    select A.guid from tbbuilderlicencemanage_new A inner join tbrecordinfo_new B on A.RowGuid=B.rowguid  " +
                "    where b.Status=104 and (B.isdzzz_sgxk=1 or B.isdzzz_dzzz=1) and A.ReleaseDate>'2018-12-31') " +
                "and (tbprojectfinishunitinfo_new.isTS IS NULL OR tbprojectfinishunitinfo_new.isTS = 0);";
        String sqlKey = "DQINVUj8I7b8";
        HashMap<String, Object> paramsMap = new HashMap<>();
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    //strTableName
    public DataTable getTableStructure(String strTableName) {
        String sql = "select * from " + strTableName + " where 1<>1";
        String sqlKey = UUID.randomUUID().toString();
        HashMap<String, Object> paramsMap = new HashMap<>();
        return super.executeSqlForDataTable(Constants.MYSQL_ZLJ_GUID, sql, sqlKey, paramsMap);
    }


}
