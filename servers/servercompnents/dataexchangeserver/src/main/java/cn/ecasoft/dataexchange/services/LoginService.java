package cn.ecasoft.dataexchange.services;

import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;

/**
 * @author xuliukai
 * @since 2023/10/30 17:24
 */
public interface LoginService {

    RetMsgUtil<Object> loginZWW(String ticket) throws Exception;

    RetMsgUtil<Object> PasswordMigration(String st_id, String lageid);

}
