package cn.ecasoft.dataexchange.mapper;

import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.configproperties.EcaSqlsConfig;
import cn.ecasoft.dataexchange.entity.model.EntitySql;
import cn.ecasoft.utils.DBhelper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.UUID;

/**
 * @author XuLiuKai
 * @Desc 每天定时推送浙里办，增量推送
 */
@Service
public class CommonMapperByDBHelp {

    @Resource
    public DBhelper dBhelper;
    @Resource
    public EcaSqlsConfig ecaSqlsConfig;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public DataTable executeSqlForDataTable(String dbSource, String sql, String sqlKey, HashMap<String, Object> paramsMap) {
        DataTable dataTable;
        ecaSqlsConfig.getMap().put(sqlKey, sql);
        try {
            dataTable = dBhelper.QueryDataTable(dbSource, sqlKey, paramsMap);
        } catch (Exception e) {
            logger.error(e.toString());
            throw new RuntimeException("executeSqlForDataTable fail : " + e.getMessage());
        } finally {
            ecaSqlsConfig.getMap().remove(sqlKey);
        }
        return dataTable;
    }

    public DataTable executeSqlForDataTable(String dbSource, String sql, HashMap<String, Object> paramsMap) {
        DataTable dataTable;
        String sqlKey = UUID.randomUUID().toString();
        ecaSqlsConfig.getMap().put(sqlKey, sql);
        try {
            dataTable = dBhelper.QueryDataTable(dbSource, sqlKey, paramsMap);
        } catch (Exception e) {
            logger.error(e.toString());
            throw new RuntimeException("executeSqlForDataTable fail : " + e.getMessage());
        } finally {
            ecaSqlsConfig.getMap().remove(sqlKey);
        }
        return dataTable;
    }

    public int executeSqlForCount(String dbSource, String sql, String sqlKey, HashMap<String, Object> paramsMap) {
        int count;
        ecaSqlsConfig.getMap().put(sqlKey, sql);
        try {
            count = dBhelper.QueryInt(dbSource, sqlKey, paramsMap);
        } catch (Exception e) {
            logger.error(e.toString());
            throw new RuntimeException("executeSqlForCount fail : " + e.getMessage());
        } finally {
            ecaSqlsConfig.getMap().remove(sqlKey);
        }
        return count;
    }

    public int executeSqlForCount(String dbSource, String sql, HashMap<String, Object> paramsMap) {
        int count;
        String sqlKey = UUID.randomUUID().toString();
        ecaSqlsConfig.getMap().put(sqlKey, sql);
        try {
            count = dBhelper.QueryInt(dbSource, sqlKey, paramsMap);
        } catch (Exception e) {
            logger.error(e.toString());
            throw new RuntimeException("executeSqlForCount fail : " + e.getMessage());
        } finally {
            ecaSqlsConfig.getMap().remove(sqlKey);
        }
        return count;
    }

    public EntitySql entityToInsertSql(Object object, String tableName) {
        EntitySql entitySql = new EntitySql();

        // 获取实体类的所有字段
        Field[] fields = object.getClass().getDeclaredFields();
        HashMap<String, Object> params = new HashMap<>();
        StringBuilder sqlStr = new StringBuilder();
        StringBuilder sqlValue = new StringBuilder();

        sqlStr.append("insert into ").append(tableName).append(" (");
        sqlValue.append("values( ");
        // 遍历字段并输出字段名和值 拼接字段和值占位符
        //例如 insert tableA(a,b,c) valuse(@a,@b,@c)
        for (Field field : fields) {
            try {
                //设置字段的访问权限，使其可以访问私有字段
                field.setAccessible(true);
                // 获取字段的值
                Object value = field.get(object);
                // 输出字段名和值
                if (value != null && StringUtils.isNotBlank(value.toString())) {
                    sqlStr.append(field.getName()).append(",");
                    sqlValue.append("@").append(field.getName()).append(",");
                    params.put(field.getName(), value);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        //去掉最后一个逗号并加上）
        sqlStr.deleteCharAt(sqlStr.length() - 1).append(") ");
        sqlValue.deleteCharAt(sqlValue.length() - 1).append(") ");
        //sql拼接
        sqlStr.append(sqlValue);

        entitySql.setSql(sqlStr.toString());
        entitySql.setParams(params);
        return entitySql;
    }

    /*
    pageindex=startline/pagesize+1;
    pageindex从1开始
     */
    public DataTable executePageSqlForDataTable(String dbSource, String sql, HashMap<String, Object> paramsMap, int startline, int pageSize) {
        DataTable dataTable;
        String sqlKey = UUID.randomUUID().toString();
        ecaSqlsConfig.getMap().put(sqlKey, sql);
        try {
            dataTable = dBhelper.QueryDataTable(dbSource, sqlKey, paramsMap, startline, pageSize);
        } catch (Exception e) {
            logger.error(e.toString());
            throw new RuntimeException("executeSqlForDataTable fail : " + e.getMessage());
        } finally {
            ecaSqlsConfig.getMap().remove(sqlKey);
        }
        return dataTable;
    }


}
