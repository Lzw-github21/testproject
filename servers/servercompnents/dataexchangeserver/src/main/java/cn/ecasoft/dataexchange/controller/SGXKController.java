package cn.ecasoft.dataexchange.controller;

import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.entity.dto.CancelOptionDto;
import cn.ecasoft.dataexchange.entity.dto.ISKBQZMNDTO;
import cn.ecasoft.dataexchange.entity.dto.TSSGXKBGDto;
import cn.ecasoft.dataexchange.services.PushToCXPTService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author XuLiuKai
 * @deprecated 这里的都搬到了诚信平台中，controller、service、serviceImpl、mapper都搬过去了
 */
@RestController
@RequestMapping("/sgxk/PXDdR3k2MzF-localhost")
public class SGXKController {

    @Resource
    private PushToCXPTService pushToCXPTService;


    @PostMapping(value = "/ReadPersonnelLeaveStatus")
    public RetMsgUtil<Object> ReadPersonnelLeaveStatus(String CorpCode, String strIDCard, String strErrMsg) {
        if (StringUtils.isAnyBlank(strIDCard, CorpCode)) {
            return RetMsgUtil.failFromBadParam();
        }
        return pushToCXPTService.ReadPersonnelLeaveStatus(CorpCode, strIDCard, strErrMsg);
    }


    /**
     * 获取施工许可证编号或者项目编号
     *
     * <param name="xzqhnum">行政区划代码</param>
     * <param name="getdate">当前的日期 格式是yyyy-MM-dd (必须为今天的日期。与服务器当前日期做比较)</param>
     * <param name="type">类型（1为获取项目编号，2为获取施工许可编号）</param>
     * <param name="xmfl">项目分类（参考项目分类字典表:01,02,03）</param>
     *
     * @return
     */
    @PostMapping(value = "/getxmnum")
    public RetMsgUtil<Object> getxmnum(String xzqhnum, String getdate, String type, String xmfl) {
        String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        if (StringUtils.isAnyBlank(xzqhnum, getdate, type, xmfl)) {
            return RetMsgUtil.failFromBadParam();
        }
        if ((!type.equals("1") && !type.equals("2")) || (!xmfl.equals("01") && !xmfl.equals("02") && !xmfl.equals("03") && !xmfl.equals("99")) || xzqhnum.length() != 6 || !getdate.equals(today)) {
            return RetMsgUtil.failFromBadParam();
        }
        return pushToCXPTService.getxmnum(xzqhnum, getdate, type, xmfl);
    }

    //注销
    @PostMapping(value = "/CancelSgxk_QZ")
    public RetMsgUtil<Object> CancelSgxk_QZ(@RequestBody CancelOptionDto dto) {
        if (!dto.paramsCheck()) {
            return RetMsgUtil.failFromBadParam();
        }
        return pushToCXPTService.CancelSgxk_QZ(dto);
    }

    //3.0施工许可变更推送省库
    @PostMapping(value = "/InsertTSSKBGInfo_New")
    public RetMsgUtil<Object> InsertTSSKBGInfo_New(@RequestBody TSSGXKBGDto dto) {
        if (!dto.checkParams()) {
            return RetMsgUtil.failFromBadParam();
        }
        return pushToCXPTService.InsertTSSKBGInfo_New(dto);
    }

    /**
     * 3.0施工许可推送至省平台(推送到省正式库)
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "/InsertSKByQZ_Manager_New")
    public RetMsgUtil<Object> InsertSKByQZ_Manager_New(@RequestBody ISKBQZMNDTO dto) {
        if (!dto.checkParams()) {
            return RetMsgUtil.failFromBadParam();
        }
        return pushToCXPTService.InsertSKByQZ_Manager_New(dto);
    }

    /**
     * 上传扫描件
     * @param strYeWuID
     * @param dto
     * @return
     */
    @PostMapping(value = "/UploadImageInfo_QZ")
    public RetMsgUtil<Object> UploadImageInfo_QZ(@RequestParam("strYeWuID") String strYeWuID, @RequestParam("dto") String dto) {
        if (StringUtils.isAnyBlank(strYeWuID, dto)) {
            return RetMsgUtil.failFromBadParam();
        }
        return pushToCXPTService.UploadImageInfo_QZ(strYeWuID, dto);
    }

}
