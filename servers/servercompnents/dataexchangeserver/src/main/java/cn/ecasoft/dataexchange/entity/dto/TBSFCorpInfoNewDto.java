package cn.ecasoft.dataexchange.entity.dto;

import cn.ecasoft.dataexchange.entity.db.TBSFCorpInfoNew;
import lombok.Getter;
import lombok.Setter;

/**
 * @author XuLiuKai
 */
@Setter
@Getter
public class TBSFCorpInfoNewDto extends TBSFCorpInfoNew {
    private String guid;
}
