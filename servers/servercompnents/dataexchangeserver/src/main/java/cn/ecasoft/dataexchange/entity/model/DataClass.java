package cn.ecasoft.dataexchange.entity.model;

import cn.ecasoft.basic.datatable.DataSet;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DataClass {

    public String MESSAGE_TYPE;
    public String PROJECT_CODE;
    public String UNIQUE_NUM;
    public String exchange_time;
    public String Data;

    public DataSet dataSet;
//    public HttpServerUtility server;

    public DataClass(String MESSAGE_TYPE, String PROJECT_CODE, String UNIQUE_NUM, String exchange_time, String data, DataSet dataSet) {
        this.MESSAGE_TYPE = MESSAGE_TYPE;
        this.PROJECT_CODE = PROJECT_CODE;
        this.UNIQUE_NUM = UNIQUE_NUM;
        this.exchange_time = exchange_time;
        this.Data = data;
        this.dataSet = dataSet;
    }
}
