package cn.ecasoft.dataexchange.entity.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author XuLiuKai
 */
@Getter
@Setter
public class ReturnParam {
    private Boolean result;
    private String strErrMsg;

    public ReturnParam(Boolean result, String strErrMsg) {
        this.result = result;
        this.strErrMsg = strErrMsg;
    }

    public ReturnParam() {
    }
}
