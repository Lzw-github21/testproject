package cn.ecasoft.dataexchange.entity.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author xuliukai
 * @since 2023/10/27 9:23
 */
@Getter
@Setter
public class FileInfo {
    private byte[] fileByte;
    private String fileName;
    private Long size;
    private String fileExtension;
    private String simpleFileName;

    //提取文件扩展名，适合发改
    //发改返回的文件名示例： a/b/c/d/e.ofd?Expires=1698905644&OSSAccessKeyId=vMEEKZ8oBXTbrWER&Signature=eFgJSW73QK1WxPWYwdroDCF2770%3D
    public void setFileExtensionForFg() {
        String[] split = this.getFileName().split("\\?");
        this.fileExtension = "." + split[0].substring(split[0].lastIndexOf("."));

//        String[] split1 = split[0].split("\\.");
//        this.simpleFileName = split1[0].substring(split1[0].lastIndexOf("/") + 1);
    }
}
