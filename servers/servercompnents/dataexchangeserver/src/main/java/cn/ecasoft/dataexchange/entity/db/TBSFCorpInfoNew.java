package cn.ecasoft.dataexchange.entity.db;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Table;

import java.sql.Timestamp;

@Table("TBSFCorpInfo_New")
@Setter
@Getter
public class TBSFCorpInfoNew {
    public String row_Guid;
    public Integer id;
    public String corpTypeNum;
    public String corpName;
    public String corpCode;
    public String corpLeader;
    public Integer corpLeaderCardType;
    public String corpLeaderCardNum;
    public String sgxkGuid;
    public String corpLeaderPhone;
    public String recordGuid;
    public String prjGuid;
    public Timestamp updateTime;
}
