package cn.ecasoft.dataexchange.services;

import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.entity.dto.CancelOptionDto;
import cn.ecasoft.dataexchange.entity.dto.ISKBQZMNDTO;
import cn.ecasoft.dataexchange.entity.dto.TSSGXKBGDto;

/**
 * @author XuLiuKai
 */
public interface PushToCXPTService {

    RetMsgUtil<Object> ReadPersonnelLeaveStatus(String CorpCode, String strIDCard, String strErrMsg);

    RetMsgUtil<Object> getxmnum(String xzqhnum, String getdate, String type, String xmfl);

    RetMsgUtil<Object> CancelSgxk_QZ(CancelOptionDto dto);

    //3.0施工许可变更推送省库
    RetMsgUtil<Object> InsertTSSKBGInfo_New(TSSGXKBGDto dto);

    //3.0施工许可推送至省平台(推送到省正式库)
    RetMsgUtil<Object> InsertSKByQZ_Manager_New(ISKBQZMNDTO dto);

    RetMsgUtil<Object> UploadImageInfo_QZ(String strYeWuID, String dto);
}
