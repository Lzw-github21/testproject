package cn.ecasoft.dataexchange.entity.db;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Table;

@Getter
@Setter
@Table("TBBuilderLicenceManage_ChangeDetails")
public class ChangeDetailsEntity {

    private Integer id;

    private String toRowGuid;

    private String dataFilled;

    private String filedName;

    private String oldValue;

    private String newValue;

    private String row_Guid;

}