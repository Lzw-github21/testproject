package cn.ecasoft.dataexchange.common.mms.model;

import cn.ecasoft.dataexchange.common.mms.util.SDKToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @description: 移动短信
 * @author: 杨攀  2020/8/12 16:36
 */
public class MgcLoginRsp {
    private boolean success;
    private String rspcod;
    private String sysflag;
    private String url;

    public MgcLoginRsp() {
    }

    public MgcLoginRsp(boolean success, String rspcod) {
        this.success = success;
        this.rspcod = rspcod;
    }

    public MgcLoginRsp(boolean success, String sysflag, String url) {
        this.success = success;
        this.sysflag = sysflag;
        this.url = url;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getRspcod() {
        return this.rspcod;
    }

    public void setRspcod(String rspcod) {
        this.rspcod = rspcod;
    }

    public String getSysflag() {
        return this.sysflag;
    }

    public void setSysflag(String sysflag) {
        this.sysflag = sysflag;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String toString() {
        ToStringBuilder tsb = new ToStringBuilder(this, SDKToStringStyle.getInstance());
        tsb.append("success", this.success);
        tsb.append("rspcod", this.rspcod);
        tsb.append("sysflag", this.sysflag);
        tsb.append("url", this.url);
        return tsb.toString();
    }
}
