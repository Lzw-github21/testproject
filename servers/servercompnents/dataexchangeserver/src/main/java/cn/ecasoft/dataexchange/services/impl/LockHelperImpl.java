package cn.ecasoft.dataexchange.services.impl;

import cn.ecasoft.dataexchange.entity.db.PersonBasicInfoDeBlockInfo;
import cn.ecasoft.dataexchange.entity.db.TBPersonBasicInfoLockInfo;
import cn.ecasoft.dataexchange.entity.db.TBXmjlTongJi;
import cn.ecasoft.dataexchange.entity.model.LockInfoModel;
import cn.ecasoft.dataexchange.entity.model.LockBack;
import cn.ecasoft.dataexchange.mapper.LockHelperMapper;
import cn.ecasoft.dataexchange.mapper.SGXKMapper;
import cn.ecasoft.dataexchange.services.LockHelper;
import org.nutz.dao.entity.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author XuLiuKai
 */
@Service
public class LockHelperImpl implements LockHelper {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private LockHelperMapper lockHelperMapper;
    @Resource
    private SGXKMapper sgxkMapper;

    @Override
    public LockBack LockInfo(String Idcard, String sgxkGuid, String LockName, String LockNum, String LockMark, String lockType, String prjName, String corpcode, int TypeState, LockInfoModel model, String strErrMsg) {
        LockBack result = new LockBack();
        result.setStrErrMsg(strErrMsg);
        logger.info("3.0注销延期废止" + "施工许可恢复" + "--" + Idcard + "--" + sgxkGuid + "--" + LockName + "--" + LockNum + "---" + LockMark + "---" + lockType + "---" + prjName + "----" + corpcode + "----" + TypeState);
        try {
            String IDCARDTYPENUM = "";
            //项目经理
            if (lockType.equals("1")) {
                //锁定项目经理
                List<Record> counts = lockHelperMapper.getCountTbuserinfoByRowGuid(Idcard);
                //省内
                if (counts != null && counts.size() > 0) {
                    lockHelperMapper.updateTbuserinfoLock(Idcard);
                    Integer i = lockHelperMapper.getCountTbuserinfo(Idcard);
                    IDCARDTYPENUM = i == null ? null : i.toString();
                }

                //添加锁定记录
                TBPersonBasicInfoLockInfo lockParams = new TBPersonBasicInfoLockInfo();
                handleTBPERSONBASICINFOLOCKINFOParams(lockParams, corpcode, IDCARDTYPENUM, Idcard, LockName, "作为 " + prjName + " 项目经理",
                        0, sgxkGuid, 1, TypeState, model.ManageCityNum, model.ManageCityNum, LockNum);
                lockHelperMapper.insertDataToTBPersonBasicInfoLockInfo(lockParams);

                //同步更新项目经理汇总表
                TBXmjlTongJi tbXmjlTongJi = new TBXmjlTongJi();
                handleTBXmjlTongJi(tbXmjlTongJi, model.CorpName, corpcode, model.PersonName, Idcard, model.CityNum, model.CountyNum,
                        model.ManageAdminAreaNum, sgxkGuid, 1, 1);

                lockHelperMapper.insertDataToTbxmjltongji(tbXmjlTongJi);

                // 同步更新项目经理汇总表
                lockHelperMapper.updateTbxmjltongjiLockByIdcard(Idcard, 1);
            } else if ("2".equals(lockType)) {
                //项目总监
                //锁定项目总监
                int count = 0;
                //项目总监超过3个项目的时候锁定
                Integer projectDirectorCount = lockHelperMapper.getProjectDirectorCount(Idcard);
                //监理工程师最多同时负责3个项目
                if (projectDirectorCount != null && projectDirectorCount > 2) {
                    count = projectDirectorCount;
                    List<Record> records = lockHelperMapper.getCountTbuserinfoByRowGuid(Idcard);
                    if (records != null && records.size() > 0) {
                        lockHelperMapper.updateTbuserinfoLock(Idcard);
                        Integer integer = lockHelperMapper.getCountTbuserinfo(Idcard);
                        IDCARDTYPENUM = integer != null ? integer.toString() : null;
                    }

                    TBPersonBasicInfoLockInfo params = new TBPersonBasicInfoLockInfo();
                    //参数处理
                    handleTBPERSONBASICINFOLOCKINFOParams(params, corpcode, IDCARDTYPENUM, Idcard, LockName, "同时作为3个项目的项目总监",
                            0, sgxkGuid, 2, TypeState, model.ManageCityNum, model.ManageCountyNum, LockNum);
                    //添加锁定记录
                    lockHelperMapper.insertDataToTBPersonBasicInfoLockInfo(params);
                }
                // 同步更新项目经理汇总表
                TBXmjlTongJi tbXmjlTongJi = new TBXmjlTongJi();
                handleTBXmjlTongJi(tbXmjlTongJi, model.CorpName, corpcode, model.PersonName, Idcard, model.CityNum, model.CountyNum,
                        model.ManageAdminAreaNum, sgxkGuid, 2, null);
                lockHelperMapper.insertDataToTbxmjltongji(tbXmjlTongJi);
                if (count >= 2) {
                    // 同步更新总监汇总表
                    lockHelperMapper.updateTbxmjltongjiLockByIdcard(Idcard, 1);
                }
            }
            result.setResult(true);
        } catch (Exception e) {
            result.setStrErrMsg(result.getStrErrMsg() + e.getMessage());
            result.setResult(false);
        }
        return result;
    }

    @Override
    public LockBack Un_LockInfo(String Idcard, String sgxkGuid, String UnLockName, String UnLockNum, String UnLockMark, String lockType, String prjName, String corpcode, int TypeState, LockInfoModel model, String strErrMsg) {
        LockBack lockBack = new LockBack();
        lockBack.setStrErrMsg(strErrMsg);
        int zj;
        int jl;
        Integer c = lockHelperMapper.getCountTbbuilderlicencemanage(Idcard, 3);
        jl = c == null ? 0 : c;
        Integer c1 = lockHelperMapper.getCountTbbuilderlicencemanage(Idcard, 4);
        zj = c1 == null ? 0 : c1;
        try {
            if ("1".equals(lockType)) {
                //#region 解锁项目经理
                if ((jl <= 1 && zj <= 3) || TypeState == 1 || TypeState == 16) {
                    List<Record> records = lockHelperMapper.getCountTbuserinfoByRowGuid(Idcard);
                    if (records != null && records.size() > 0) {
                        lockHelperMapper.updateTbuserinfoLockBy(Idcard, 2);
                    }

                    PersonBasicInfoDeBlockInfo params = new PersonBasicInfoDeBlockInfo();
                    handleTBPersonBasicInfodeBlockInfoParams(params, Idcard, sgxkGuid, UnLockNum, UnLockName, UnLockMark, 1, corpcode,
                            TypeState, model.ManageCityNum, model.ManageCountyNum, model.auditName, model.auditDateTime, model.auditMark);
                    lockHelperMapper.insertDataToTBPERSONBASICINFODEBLOCKINFO(params);
                    try {
                        // 同步更新项目经理汇总表
                        lockHelperMapper.updateTbxmjltongjiLockByIdcard(Idcard, 1);
                        logger.info("解锁 :" + Idcard);
                    } catch (Exception ex) {
                        logger.info("解锁失败 :" + Idcard + ";    " + Arrays.toString(ex.getStackTrace()));
                    }

                }

            } else if ("2".equals(lockType)) {
                //项目总监
                if ((zj <= 3 && jl < 1) || TypeState == 1 || TypeState == 16) {
                    List<Record> data = lockHelperMapper.getDataFromTbuserinfoByIdCard(Idcard);
                    if (data != null && data.size() > 0) {
                        lockHelperMapper.updateTbuserinfoLockBy(Idcard, 2);
                    }
                    PersonBasicInfoDeBlockInfo params = new PersonBasicInfoDeBlockInfo();
                    handleTBPersonBasicInfodeBlockInfoParams(params, Idcard, sgxkGuid, UnLockNum, UnLockName, UnLockMark, 2, corpcode,
                            TypeState, model.ManageCityNum, model.ManageCountyNum, model.auditName, model.auditDateTime, model.auditMark);

                    try {
                        // 同步更新项目总监汇总表
                        lockHelperMapper.updateTbxmjltongjiLockByIdcard(Idcard, 2);
                    } catch (Exception ex) {
                        logger.error("解锁 , 2错误信息：");
                        logger.error(Arrays.toString(ex.getStackTrace()));
                    }
                }
            }
            //如果是施工许可变更（变更了项目经理、总监），则删除统计表的数据 TypeState == 6 || TypeState == 12
            if (TypeState != 1 && TypeState != 16) {
                try {
                    lockHelperMapper.deleteTbxmjltongjiByIdCard(Idcard, sgxkGuid);
                } catch (Exception ex) {
                    logger.error("解锁 2错误信息 ： " + Arrays.toString(ex.getStackTrace()));
                }
            }
            lockBack.setResult(true);
        } catch (Exception e) {
            lockBack.setStrErrMsg(lockBack.getStrErrMsg() + e.getMessage());
            lockBack.setResult(false);
        }
        return lockBack;
    }

    @Override
    public LockBack HaveNumProject(String lockType, String Idcard, String personName, String strErrMsg) {
        logger.info("3.0注销延期废止" + "施工许可恢复" + lockType + "" + Idcard + "" + personName);
        LockBack lockBack = new LockBack();
        try {
            if ("1".equals(lockType)) {
                Integer num1 = lockHelperMapper.getCountTbbuilderlicencemanage(Idcard, 3);
                Integer c = lockHelperMapper.getTbuserinfoCountByIdCard(Idcard);
                int num2 = c == null ? 0 : c;
                if (num1 > 1) {
                    if (num2 != 2) {
                        strErrMsg += "该条施工许可申请中的项目经理" + personName + "承接了其它未竣工的项目，已被锁定，请确认后再进行审核！";
                        lockBack.setResult(false);
                        return lockBack;
                    }
                }
            } else {
                int num = lockHelperMapper.getProjectDirectorCount(Idcard);
                Integer c = lockHelperMapper.getTbuserinfoCountByIdCard(Idcard);
                int num1 = c == null ? 0 : c;
                if (num >= 3 && num1 != 2) {
                    strErrMsg += "该条施工许可申请中的项目总监" + personName + "承接了3个其它未竣工的项目，已被锁定，请确认后再进行审核！";
                    lockBack.setResult(false);
                    return lockBack;
                }
            }
            lockBack.setResult(true);
        } catch (Exception e) {
            strErrMsg += e.getMessage();
            lockBack.setResult(false);
        }
        lockBack.setStrErrMsg(strErrMsg);
        return lockBack;
    }

    private void handleTBPERSONBASICINFOLOCKINFOParams(TBPersonBasicInfoLockInfo params, String corpcode, String IDCARDTYPENUM,
                                                       String Idcard, String LockName, String mark, int isDeBlock, String sgxkGuid,
                                                       int lockType, int TypeState, String cityNum, String countyNum, String LockNum) {
        params.row_Guid = UUID.randomUUID().toString().replace("-", "");
        params.id = sgxkMapper.getPKValue("TBPERSONBASICINFOLOCKINFO", "ID");
        params.corpCode = corpcode;
        params.idCardTypeNum = IDCARDTYPENUM;
        params.idCard = Idcard;
        params.lockUserName = LockName;
        params.lockDate = new Date();
        params.lockMark = mark;
        params.isDeBlock = isDeBlock;
        params.sgxkGuid = sgxkGuid;
        params.lockType = lockType;
        params.typeState = TypeState;
        params.cityNum = cityNum;
        params.countyNum = countyNum;
        params.lockManageDepNum = LockNum;
    }

    private void handleTBXmjlTongJi(TBXmjlTongJi params, String CorpName, String corpcode, String PersonName, String Idcard,
                                    String CityNum, String CountyNum, String ManageAdminAreaNum, String sgxkGuid, Integer personType, Integer isLock) {
        params.row_Guid = UUID.randomUUID().toString().replace("-", "");
        params.corpName = CorpName;
        params.corpCode = corpcode;
        params.personName = PersonName;
        params.idCard = Idcard;
        params.cityNum = CityNum;
        params.countyNum = CountyNum;
        params.manageAdminAreaNum = ManageAdminAreaNum;
        params.sgxkGuid = sgxkGuid;
        params.personType = personType;
        params.isLock = isLock;
    }

    private void handleTBPersonBasicInfodeBlockInfoParams(PersonBasicInfoDeBlockInfo params, String Idcard, String sgxkGuid,
                                                          String UnLockNum, String UnLockName, String UnLockMark, Integer lockType,
                                                          String corpcode, int TypeState, String cityNum, String countyNum, String auditName,
                                                          String auditDateTime, String auditMark) {
        params.row_Guid = UUID.randomUUID().toString().replace("-", "");
        params.id = lockHelperMapper.getPKValue(" TBPERSONBASICINFODEBLOCKINFO ", " ID ");
        params.idCard = Idcard;
        params.sgxkGuid = sgxkGuid;
        params.deBlockManageDepNum = UnLockNum;
        params.deBlockUserName = UnLockName;
        params.deBlockDate = new Date();
        params.deBlockMark = UnLockMark;
        params.lockType = lockType;
        params.corpCode = corpcode;
        params.typeState = TypeState;
        params.cityNum = cityNum;
        params.countyNum = countyNum;
        params.deBlockManageDepNum = UnLockNum;
        params.auditName = auditName;
        params.auditDateTime = auditDateTime;
        params.auditMark = auditMark;
    }
}
