package cn.ecasoft.dataexchange.controller;

import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.services.PushSgxkToSjcInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xuliukai
 * @since 2023/10/25 17:42
 * @Desc 推送数据仓接口
 */
@RestController
@RequestMapping(value = "/pushTosjc/fEnRU0jso", produces = "application/json;charset=utf-8")
public class PushSgxkToSjcInfoController {

    @Resource
    private PushSgxkToSjcInfoService pushSgxkToSjcInfoService;

    @PostMapping(value = "/pushSGXK")
    public RetMsgUtil<Object> PushSgxkToSjcInfo(@RequestParam("row_guid") String row_guid) {
        if (StringUtils.isBlank(row_guid)) {
            return RetMsgUtil.failFromBadParam("row_guid is null");
        }

        return pushSgxkToSjcInfoService.PushSgxkToSjcInfo(row_guid);
    }


}
