package cn.ecasoft.dataexchange.controller;

import cn.ecasoft.dataexchange.services.DZZZService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(value = "/dzzz/G7O3zgUqYHo", produces = "application/json;charset=utf-8")
public class DZZZController {

    @Resource
    private DZZZService dzzzService;

    @PostMapping(value = "/pushToDZZZ")
    public String pushToDZZZ(@RequestParam(value = "RowGuid") String RowGuid,
                             @RequestParam(value = "strVersion") String strVersion,
                             @RequestParam(value = "strJson") String strJson) {
        return dzzzService.PushToDZZZ(RowGuid, strVersion, strJson);
    }

}
