package cn.ecasoft.dataexchange.services;

import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;

/**
 * @author XuLiuKai
 */
public interface PushSgxkToSjcInfoService {

    RetMsgUtil<Object> PushSgxkToSjcInfo(String row_guid);
}
