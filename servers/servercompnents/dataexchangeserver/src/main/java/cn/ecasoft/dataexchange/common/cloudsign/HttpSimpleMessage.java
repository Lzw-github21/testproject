package cn.ecasoft.dataexchange.common.cloudsign;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 简单的http请求和响应容器：请求头、请求体
 * @author asdfg
 */
public class HttpSimpleMessage implements Serializable {

    private static final long serialVersionUID = 1L;
    private byte[] body = new byte[0];
    public Map<String, String> headers = new HashMap<String, String>();

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }
}
