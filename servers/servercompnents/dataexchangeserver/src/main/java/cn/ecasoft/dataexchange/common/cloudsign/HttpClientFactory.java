package cn.ecasoft.dataexchange.common.cloudsign;


import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;

import javax.net.ssl.SSLContext;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;


public class HttpClientFactory {

    private static PoolingHttpClientConnectionManager connectionManager;
    private static CloseableHttpClient httpClient;


    static {

        // 设置协议http和https对应的处理socket链接工厂的对象
        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.INSTANCE)
                .register("https", createSSLConnSocketFactory()).build();
        connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        connectionManager.setDefaultMaxPerRoute(500);
        connectionManager.setMaxTotal(3000);
        httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();

        startNewMonitorThread(connectionManager);
    }

    private static void startNewMonitorThread(PoolingHttpClientConnectionManager connectionManager) {
        IdleConnectionMonitorThread t = new IdleConnectionMonitorThread(connectionManager);
        t.setDaemon(true);
        t.start();
    }

    public static CloseableHttpClient getInstance() {
        return httpClient;
    }

    public static CloseableHttpClient createInstance() {
        CloseableHttpClient httpClient = HttpClients.custom().build();
        return httpClient;
    }


    /**
     * 创建SSL安全连接
     *
     * @return
     */
    private static SSLConnectionSocketFactory createSSLConnSocketFactory() {
        SSLConnectionSocketFactory sfy = null;
        SSLContext sslContext = null;
        try {
            InputStream inputStream = HttpClientFactory.class.getResourceAsStream("/client.keystore");
            if (null != inputStream) {
                KeyStore ks = KeyStore.getInstance("JKS");
                ks.load(inputStream, "111111".toCharArray());
                sslContext = new SSLContextBuilder().loadTrustMaterial(ks, new TrustSelfSignedStrategy()).build();
            } else {
                // 找不到keystore时，不做服务端证书验证。
                sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
                    @Override
                    public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                        // 保留下面注释的代码，握手时方便获取服务端的证书链。调试时有用
                        /*
                         * for (int i = 0; i < chain.length; i++) { try {
                         * System.out.println(new
                         * String(Base64Util.encode(chain[i].getEncoded()))); }
                         * catch (NewCSSException e) { e.printStackTrace(); } }
                         */
                        return true;
                    }
                }).build();
            }

            sfy = new SSLConnectionSocketFactory(sslContext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        } catch (Exception e) {
        }
        return sfy;
    }

}

class IdleConnectionMonitorThread extends Thread {

    private final PoolingHttpClientConnectionManager connMgr;
    private volatile boolean shutdown;

    public IdleConnectionMonitorThread(PoolingHttpClientConnectionManager connMgr) {
        super();
        this.connMgr = connMgr;
        this.setName("idle-connection-monitor");
    }

    @Override
    public void run() {
        while (!shutdown) {
            synchronized (this) {
                try {
                    wait(5000);
                    connMgr.closeExpiredConnections();
                    connMgr.closeIdleConnections(60, TimeUnit.SECONDS);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void shutdown() {
        shutdown = true;
        synchronized (this) {
            notifyAll();
        }
    }

}
