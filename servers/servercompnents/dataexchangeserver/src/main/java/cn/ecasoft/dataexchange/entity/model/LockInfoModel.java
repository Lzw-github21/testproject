package cn.ecasoft.dataexchange.entity.model;

public class LockInfoModel {
    /**
     * 锁定人
     */
    public String LockName;
    /**
     * 锁定人账号
     */
    public String LockNum;
    /**
     * 锁定原因
     */
    public String LockMark;
    /**
     * 锁定类型（经理，总监）
     */
    public String lockType;
    /**
     * 锁定操作类型
     */
    public int TypeState;
    /**
     * 企业名称
     */
    public String CorpName;
    /**
     * 人员姓名
     */
    public String PersonName;
    /**
     * 市
     */
    public String CityNum;
    /**
     * 区县
     */
    public String CountyNum;
    /**
     * 审核账号
     */
    public String ManageAdminAreaNum;
    /**
     * 审核账号所在市
     */
    public String ManageCityNum;
    /**
     * 审核账号所在区县
     */
    public String ManageCountyNum;
    /**
     * 审批人
     */
    public String auditName;
    /**
     * 审批时间
     */
    public String auditDateTime;
    /**
     * 审批意见
     */
    public String auditMark;

}