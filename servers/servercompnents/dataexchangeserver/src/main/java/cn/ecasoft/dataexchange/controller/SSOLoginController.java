package cn.ecasoft.dataexchange.controller;

import cn.ecasoft.basic.JsonData;
import cn.ecasoft.basic.UserInfo;
import cn.ecasoft.basic.datatable.DataRow;
import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.configproperties.EcaSqlsConfig;
import cn.ecasoft.dataexchange.common.mms.util.CookieUtil;
import cn.ecasoft.dataexchange.common.mms.util.HttpUtil;
import cn.ecasoft.dataexchange.common.mms.util.TokenHelper;
import cn.ecasoft.dataexchange.feign.ZLBuildRemoteService;
import cn.ecasoft.redis.RedisService;
import cn.ecasoft.utils.DBhelper;
import cn.ecasoft.utils.EcaConfig;
import cn.ecasoft.utils.OperLogUtil;
import cn.ecasoft.utils.SecurityHelper;
import cn.ecasoft.utils.SessionHelper;
import cn.ecasoft.utils.StringUtils;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author wm
 * @Package cn.ecasoft.dataexchange.controller
 * @date 2022/4/28 11:27
 */
@RestController
@RequestMapping("/sso")
public class SSOLoginController {
    @Autowired
    private RedisService redisService;
    @Autowired
    private TokenHelper tokenHelper;
    @Autowired
    private DBhelper dBhelper;
    @Autowired
    private EcaConfig ecaConfig;
    @Autowired
    private SecurityHelper securityHelper;
    @Autowired
    private EcaSqlsConfig ecaSqlsConfig;
    @Autowired
    SessionHelper sessionHelper;
    protected Logger logger = LoggerFactory.getLogger(SSOLoginController.class);

    @Autowired
    ZLBuildRemoteService zlBuildRemoteService;

    @RequestMapping("/login")
    public void ssologin(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //1.先获取到ticket,在跟根据对方的令牌（ticket）查询到对方的用户信息。
        String ticket = request.getParameter("ticket");
        String appKey = request.getParameter("appKey");
        String redirecturl = request.getParameter("redirecturl");
        //1.2验证令牌和重定向地址是否为空
        if (StringUtils.isBlank(ticket) || StringUtils.isBlank(appKey) || StringUtils.isBlank(redirecturl)) {
            responseWriteln(response, "缺少传递参数 ticket、type或redirecturl ");
            return;
        }
        //1.3根据传递的type 来确定单点登录配置表的记录；
        String strSql = "select * from sys_ssologininfo where appKey = @APPKEY";
        String sqlKey = UUID.randomUUID().toString();
        ecaSqlsConfig.getMap().put(sqlKey, strSql);
        JsonData params = new JsonData();
        params.put("APPKEY", appKey);
        DataTable dtSSOConfig = dBhelper.QueryDataTable("", sqlKey, params);
        if (!dtSSOConfig.HasRow()) {
            responseWriteln(response, "请检查APPKEY是否存在，请联系管理员！ ");
            return;
        }
        DataRow drSSOConfig = dtSSOConfig.getFirst();
        String ticketUrl = drSSOConfig.getString("TICKETURL");
        String PWD = drSSOConfig.getString("PWD");
        if (StringUtils.isBlank(ticketUrl) || StringUtils.isBlank(PWD)) {
            responseWriteln(response, "ticketUrl或pwd未配置，请联系管理员！ ");
            return;
        }
        //1.4如果验证通过需要拿着 ticket 去获取对方的用户信息
        String body = HttpUtil.HttpGet(ticketUrl + ticket, null);
        JSONObject jsonObject = JSONObject.parseObject(body);
        if (!"200".equals(jsonObject.getString("code"))) {
            responseWriteln(response, "获取用户信息失败");
            return;
        }
        JSONObject userData = JSONObject.parseObject(jsonObject.getString("data"));
        //1.5获取单点登录中记录的映射关系；
        //1.5.1 获取对方库中的唯一字段名称
        String otheruniquefieldname = drSSOConfig.getString("UNIQUEFIELDNAME");
        //1.5.2 获取对方库中用户信息如果包含这几个去除即可；
        JSONObject map = JSONObject.parseObject(drSSOConfig.getString("MAP"));
        if (map.containsKey("ST_ID")) {
            map.remove("ST_ID");
        }
        if (map.containsKey("st_id")) {
            map.remove("st_id");
        }
        if (map.containsKey("ROW_GUID")) {
            map.remove("ROW_GUID");
        }
        if (map.containsKey("row_guid")) {
            map.remove("row_guid");
        }
        if (map.containsKey("ST_PASSWD")) {
            map.remove("ST_PASSWD");
        }
        if (map.containsKey("st_password")) {
            map.remove("st_password");
        }
        //1.5.2 获取我方系统库中的唯一字段名称
        String uniqueName = map.getString(otheruniquefieldname);
        //1.5.3 获取系统库唯一字段的键值
        String otheruniquefieldnameValue = userData.getString(otheruniquefieldname);
        String selectStaffdefSql = String.format("select * from sys_staffdef where %s=@VALUE", uniqueName);
        sqlKey = UUID.randomUUID().toString();
        ecaSqlsConfig.getMap().put(sqlKey, selectStaffdefSql);
        params.put("VALUE", otheruniquefieldnameValue);
        DataTable dtUser = dBhelper.QueryDataTable("", sqlKey, params);
        UserInfo userInfo = new UserInfo();
        if (dtUser.HasRow()) {
            DataRow drUser = dtUser.getFirst();
            userInfo.setLoginName(drUser.getString("ST_LOGINNAME"));
            userInfo.setUserName(drUser.getString("ST_NAME"));
            userInfo.setMobileTel(drUser.getString("MOBILETEL"));
            userInfo.setIDCARD(drUser.getString("IDCARD"));
            userInfo.setORGCODE(null);
            userInfo.setUserId(Integer.parseInt(drUser.getString("ST_ID")));
        } else {
            InitUserAndRoleAndDept(userInfo, map, userData, drSSOConfig);
        }

        if (!org.springframework.util.StringUtils.isEmpty(userInfo.getUserName())) {
            //把角色信息和部门信息同时获取出来
            userInfo.setRoles(this.GetUserRols(userInfo.getUserId()));
            HashMap<String, String> depts = new HashMap<>();
            DataTable dtDepts = this.GetDtUserDepts(userInfo.getUserId());
            // depts.put("Depts", JsonData.toString(this.GetDtUserDepts(userInfo.getUserId())));
            HashMap<String, String> dept = new HashMap<>();
            if (dtDepts.HasRow()) {
                for (int i = 0; i < dtDepts.size(); i++) {
                    DataRow dataRow = dtDepts.get(i);
                    if (dataRow != null && dataRow.size() > 0) {
                        String key2 = "";
                        String value2 = "";
                        for (String key : dataRow.keySet()) {
                            if ("DEPT_NAME".equals(key)) {
                                value2 = dataRow.get(key).toString();
                            }
                            if ("DEPTID".equals(key)) {
                                key2 = dataRow.get(key).toString();
                            }
                        }
                        dept.put(key2, value2);
                        userInfo.setMainDeptID(key2);
                        userInfo.setMainDeptName(value2);
                    }
                }
            }
            userInfo.setDepts(dept);
        }
        //2.初始化角色完成 开始跳转登录
        String logintoken = this.tokenHelper.GetAuthToken(userInfo, "c9406731-50bc-4e23-b3f5-b7e7d3093875");
        CookieUtil.set(response, "access_token", logintoken, false);
        String code = this.tokenHelper.CreateCode(logintoken);
        this.redisService.set(code, logintoken);
        if (StringUtils.isNotBlank(redirecturl)) {
            String sucurl = redirecturl + "?code=" + code + "&foreignToken=" + ticket;
            response.sendRedirect(sucurl);
        }
    }


    public HashMap<String, String> GetUserRols(int userID) throws Exception {
        HashMap<String, String> roles = new HashMap<>();
        HashMap<String, Object> param = new HashMap<>();
        param.put("userID", userID);
        DataTable result = dBhelper.QueryDataTable("", "perm-getuserroles", param);
        if (result.size() > 0) {
            for (DataRow roleRow : result) {
                roles.put(roleRow.getString("ROLEID"), roleRow.getString("ROLE_NAME"));
            }
        }
        return roles;
    }
    /**
     * 浙里建省监管端单点登录入口
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @GetMapping("/zljSso")
    public void zljSso(HttpServletRequest request, HttpServletResponse response) throws Exception {

        JsonData ZLBuildUserInfo = null;
        try {
            String loginCode = request.getParameter("code");
            //1.2验证令牌和重定向地址是否为空
            if (StringUtils.isBlank(loginCode)) {
                responseWriteln(response, "缺少必要鉴权参数,非法跳转!");
                return;
            }
            String result = zlBuildRemoteService.getAccessToken(loginCode);
            JsonData zlBuildResult = JsonData.fromString(result);
            if(!zlBuildResult.containsKey("userInfo")){
                responseWriteln(response, zlBuildResult);
                return;
            }
            ZLBuildUserInfo = JsonData.fromString(zlBuildResult.get("userInfo").toString());
            //解析企业统一社会信用代码
            String redirectUrl = request.getParameter("redirectUrl");
            String mobileTle = ZLBuildUserInfo.get("mobileTel").toString();
            //1.2验证重定向地址是否为空
            if (StringUtils.isBlank(redirectUrl)) {
                redirectUrl = ecaConfig.GetGroupConfig("Server", "zhelibuild_ssoRedirecturl");
            }
            if (StringUtils.isBlank(redirectUrl)) {
                responseWriteln(response, "未配置redirectUrl!");
                return;
            }
            String zzdLoginName = null;
            HashMap<String, Object> param = new HashMap<>();
            DataTable dt = new DataTable();
            if(ZLBuildUserInfo.containsKey("zZDLOGINNAME") && ObjectUtils.isNotEmpty(ZLBuildUserInfo.get("zZDLOGINNAME"))){
                zzdLoginName = ZLBuildUserInfo.get("zZDLOGINNAME").toString();
            }
            //先通过浙政钉loginName匹配用户信息。
            if(StringUtils.isNotBlank(zzdLoginName)){
                this.ecaSqlsConfig.getMap().put("zljSso_getuserinfo_byZzdLoginName", "SELECT ST_ID, ST_NAME, ST_LOGINNAME, MOBILETEL FROM SYS_STAFFDEF WHERE zzdLoginName= @zzdLoginName AND (ISECAUSER IS NULL OR ISECAUSER = 0) AND (NFLAG = 0 OR NFLAG IS NULL) AND (ISRETIREMENT = 0 OR ISRETIREMENT IS NULL)");
                param.put("zzdLoginName", zzdLoginName);
                dt = this.dBhelper.QueryDataTable("", "zljSso_getuserinfo_byZzdLoginName", param);
                ecaSqlsConfig.getMap().remove("zljSso_getuserinfo_byZzdLoginName");
            }
            //匹配不上通过手机号进行匹配
            if(!dt.HasRow() && StringUtils.isNotBlank(mobileTle)){
                this.ecaSqlsConfig.getMap().put("zljSso_getuserinfo_byMobileTel", "SELECT ST_ID, ST_NAME, ST_LOGINNAME, MOBILETEL FROM SYS_STAFFDEF WHERE MOBILETEL= @MOBILETEL AND (ISECAUSER IS NULL OR ISECAUSER = 0) AND (NFLAG = 0 OR NFLAG IS NULL) AND (ISRETIREMENT = 0 OR ISRETIREMENT IS NULL)");
                param.put("MOBILETEL", mobileTle);
                dt = this.dBhelper.QueryDataTable("", "zljSso_getuserinfo_byMobileTel", param);
                ecaSqlsConfig.getMap().remove("zljSso_getuserinfo_byMobileTel");
                if(dt.HasRow()){
                   //手机号匹配成功更新浙政钉登录名
                    this.ecaSqlsConfig.getMap().put("zljSso_updateuserinfo_byMobileTel", "update SYS_STAFFDEF set ZZDLOGINNAME = @zzdLoginName WHERE MOBILETEL= @MOBILETEL AND (ISECAUSER IS NULL OR ISECAUSER = 0) AND (NFLAG = 0 OR NFLAG IS NULL) AND (ISRETIREMENT = 0 OR ISRETIREMENT IS NULL)");
                    this.dBhelper.QueryDataTable("", "zljSso_updateuserinfo_byMobileTel", param);
                    ecaSqlsConfig.getMap().remove("zljSso_updateuserinfo_byMobileTel");
                }
            }
            if (dt.HasRow()) {
                DataRow drUser = dt.getFirst();
                UserInfo userInfo = new UserInfo();
                userInfo.setUserId(drUser.getInt("ST_ID"));
                userInfo.setLoginName(drUser.getString("ST_LOGINNAME"));
                userInfo.setUserName(drUser.getString("ST_NAME"));
                userInfo.setMobileTel(drUser.getString("MOBILETEL"));
                OperLogUtil.getInstance().addOperLogTask("011", userInfo.getUserId(), userInfo.getLoginName(), OperLogUtil.getInstance().getIpAddress(request));
                String token = this.tokenHelper.GetAuthToken(userInfo, "c9406731-50bc-4e23-b3f5-b7e7d3093875");
                CookieUtil.set(response, "access_token", token, false);
                String code = this.tokenHelper.CreateCode(token);
                response.sendRedirect(redirectUrl + "?code=" + code);
                response.sendRedirect(redirectUrl);
            } else {
                //手机号脱敏处理
                String safMobileTel = mobileTle.substring(0, 3) + "****" + mobileTle.substring(7);
                //单点失败重定向地址
                String failRedirectUrl = "https://zlj.jst.zj.gov.cn/app_zhelibuild_jg/duck/noAccount.html";
                response.sendRedirect(failRedirectUrl + "?mobiletel=" + safMobileTel);
            }
        } catch (Exception ex) {
            responseWriteln(response, ex.getMessage());
        }
    }

    public DataTable GetDtUserDepts(int userID) throws Exception {
        HashMap<String, String> depts = new HashMap<>();
        HashMap<String, Object> param = new HashMap<>();
        param.put("userID", userID);
        DataTable result = dBhelper.QueryDataTable("", "perm-getuserdepts", param);
        return result;
    }
    /**
     * 插入用户和默认角色
     */
    private void InitUserAndRoleAndDept(UserInfo userInfo, JSONObject map, JSONObject userData, DataRow drSSOConfig) throws Exception {
        int userId = dBhelper.GetPkValue("", "SYS_STAFFDEF", "ST_ID");
        userInfo.setUserId(userId);
        userInfo.setORGCODE(null);
        {
            //不存在用户信息，需要初始化用户信息
            StringBuilder sbFIeld = new StringBuilder("INSERT INTO SYS_STAFFDEF(ROW_GUID, ST_ID,ST_PASSWD");
            StringBuilder sbValues = new StringBuilder("VALUES (@ROW_GUID, @ST_ID, @ST_PASSWD");
            JsonData insertParams = new JsonData();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                //对方的键
                String otherkey = entry.getKey();
                //本系统键值
                String key = entry.getValue().toString();
                String value = userData.getString(otherkey);
                sbFIeld.append("," + key.toUpperCase());
                sbValues.append(",@" + key.toUpperCase());
                insertParams.put(key.toUpperCase(), value);
                if (key.equalsIgnoreCase("ST_LOGINNAME")) {
                    userInfo.setLoginName(value);
                }
                if (key.equalsIgnoreCase("ST_NAME")) {
                    userInfo.setUserName(value);
                }
                if (key.equalsIgnoreCase("MOBILETEL")) {
                    userInfo.setMobileTel(value);
                }
                if (key.equalsIgnoreCase("IDCARD")) {
                    userInfo.setIDCARD(value);
                }
            }
            insertParams.put("ST_ID", userId);
            insertParams.put("ST_PASSWD", securityHelper.MD5HashString(drSSOConfig.getString("PWD")));
            insertParams.put("ROW_GUID", UUID.randomUUID().toString());
            sbFIeld.append(",isecauser) ");
            sbValues.append(", 0 )");
            String inserSql = sbFIeld.toString() + sbValues.toString();
            String insertstaff = UUID.randomUUID().toString();
            ecaSqlsConfig.getMap().put(insertstaff, inserSql);
            int affectedrows = dBhelper.QueryInt("", insertstaff, insertParams);
            if (affectedrows == 0) {
                throw new Exception("插入人员失败");
            }
        }
        String defaultRoleIDs = drSSOConfig.getString("ROLEIDS");
        //新增默认角色
        {
            int[] defaultRoleids = Arrays.stream(defaultRoleIDs.split(",")).mapToInt(Integer::parseInt).toArray();
            for (int roleid : defaultRoleids) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("ROLEID", roleid);
                String selectrelationid = UUID.randomUUID().toString();
                ecaSqlsConfig.getMap().put(selectrelationid, "SELECT RELATIONID FROM SYS_USERROLE WHERE NODEID = @ROLEID AND ISUSER = 0");
                DataTable dtRoleRelationId = dBhelper.QueryDataTable("", selectrelationid, param);
                if (dtRoleRelationId.HasRow()) {
                    int roleRelationId = dtRoleRelationId.getFirst().getInt("RELATIONID");
                    if (roleRelationId == 0) {
                        continue;
                    }
                    int id = dBhelper.GetPkValue("", "SYS_USERROLE", "RELATIONID");
                    param.clear();
                    param.put("RELATIONID", id);
                    param.put("NODEID", userId);
                    param.put("PNODEID", roleRelationId);
                    param.put("ISUSER", 1);
                    param.put("ROW_GUID", UUID.randomUUID().toString());
                    String insert_userrole = UUID.randomUUID().toString();
                    ecaSqlsConfig.getMap().put(insert_userrole, "INSERT INTO SYS_USERROLE (RELATIONID, NODEID, PNODEID, ISUSER, ROW_GUID) VALUES (@RELATIONID, @NODEID, @PNODEID, @ISUSER, @ROW_GUID)");
                    dBhelper.QueryVoid("", insert_userrole, param);
                }
            }
        }
        String deptId = drSSOConfig.getString("DEPTID");
        //新增部门
        {
            if (StringUtils.isNotBlank(deptId)) {
                //INSERT INTO SYS_UserDept (RelationId,NodeId,PNodeId,isUser,Appointer,ROW_GUID,LastModifyDate)
                // VALUES (116,120,7,1,null,'{5E4F60B1-0934-4268-9592-0096D72289C1}','2021-07-29 09:36:53')
                HashMap<String, Object> param = new HashMap<>();
                param.put("deptId", deptId);
                String selectrelationid = UUID.randomUUID().toString();
                ecaSqlsConfig.getMap().put(selectrelationid, "SELECT RELATIONID FROM sys_userdept WHERE NODEID = @deptId AND ISUSER = 0");
                DataTable dtRoleRelationId = dBhelper.QueryDataTable("", selectrelationid, param);
                if (dtRoleRelationId.HasRow()) {
                    int deptRelationId = dtRoleRelationId.getFirst().getInt("RELATIONID");
                    if (deptRelationId != 0) {
                        int id = dBhelper.GetPkValue("", "sys_userdept", "RELATIONID");
                        param.clear();
                        param.put("RELATIONID", id);
                        param.put("NODEID", userId);
                        param.put("PNODEID", deptRelationId);
                        param.put("ISUSER", 1);
                        param.put("ROW_GUID", UUID.randomUUID().toString());
                        String insert_userrole = UUID.randomUUID().toString();
                        ecaSqlsConfig.getMap().put(insert_userrole, "INSERT INTO sys_userdept (RELATIONID, NODEID, PNODEID, ISUSER, ROW_GUID) VALUES (@RELATIONID, @NODEID, @PNODEID, @ISUSER, @ROW_GUID)");
                        dBhelper.QueryVoid("", insert_userrole, param);
                    }
                }
            }
        }
    }

    private void responseWriteln(HttpServletResponse response, Object o) throws IOException {
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-Type", "text/html;charset=utf-8");
        response.getWriter().println(o);
    }


}
