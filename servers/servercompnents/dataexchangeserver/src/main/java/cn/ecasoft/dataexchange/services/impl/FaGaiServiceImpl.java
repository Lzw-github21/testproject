package cn.ecasoft.dataexchange.services.impl;


import cn.ecasoft.basic.JsonData;
import cn.ecasoft.basic.datatable.DataColumn;
import cn.ecasoft.basic.datatable.DataRow;
import cn.ecasoft.basic.datatable.DataSet;
import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.common.CommonUtil;
import cn.ecasoft.dataexchange.common.utils.ChangeJson;
import cn.ecasoft.dataexchange.common.utils.DataSetConversionSql;
import cn.ecasoft.dataexchange.common.utils.FileUtils;
import cn.ecasoft.dataexchange.common.utils.OssFileStorage;
import cn.ecasoft.dataexchange.entity.db.SGXKBGDetails;
import cn.ecasoft.dataexchange.entity.db.SysEcafiles;
import cn.ecasoft.dataexchange.entity.enums.AnnexMapping;
import cn.ecasoft.dataexchange.entity.enums.SGXKFieldMapping;
import cn.ecasoft.dataexchange.entity.model.*;
import cn.ecasoft.dataexchange.entity.vo.WebServiceItemRecordInVO;
import cn.ecasoft.dataexchange.mapper.FGMapper;
import cn.ecasoft.dataexchange.mapper.OFDWebMapper;
import cn.ecasoft.dataexchange.mapper.OSSMapper;
import cn.ecasoft.dataexchange.services.FaGaiService;
import cn.hutool.http.webservice.SoapClient;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.nutz.aop.interceptor.async.Async;
import org.nutz.trans.Trans;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static cn.ecasoft.dataexchange.common.utils.Constants.*;

/**
 * @author XuLiuKai
 * 从发改那边拉取数据
 * 这个接口是给发改调用的
 * 逻辑:发改调我们的接口A(fgNotice()方法),我们在接口A中调取他们的接口B 拉取数据
 */
@Service
public class FaGaiServiceImpl implements FaGaiService {

    @Resource
    private OSSMapper ossMapper;

    @Resource
    private FGMapper fgMapper;
    @Resource
    private OFDWebMapper ofdWebMapper;
    //我们自己的oss
    private final OssFileStorage OUR_OSS = new OssFileStorage(OSS_ACCESS_KEY, OSS_SECRET_KEY, OSS_END_POINT);
    //发改的oss
    private final OssFileStorage FG_OSS = new OssFileStorage(FG_OSS_ACCESS_KEY, FG_OSS_SECRET_KEY, FG_OSS_END_POINT);

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final String SECURITY_KEY = "ff8080815dfd7c36015dfd9a8e2d028b";
    //发改正式环境
//    private final String FGUrl = "http://59.202.39.226:18181/service/projectInfoWeb?wsdl";
    //发改测试环境
    private final String FGUrl = "http://223.4.69.50:18182/tzxmspall/service/projectInfoWeb?wsdl";

    //已测
    private boolean SaveImageFileContent(String strFileName, String RowGuid, String strFileExt, FileInfo fileInfo, String GROUPGUID) {
        String virtualPath = "oss/upload/ScanImageList/";
        String actualName = RowGuid + "/" + strFileName;
        String objectName = virtualPath + actualName;
        if (!OUR_OSS.upload(OSS_BUCKET_NAME, objectName, fileInfo.getFileByte())) {
            throw new RuntimeException("上传文件失败");
        }
        //上传成功之后,我们需要保存到数据库中
        SysEcafiles sysEcafiles = new SysEcafiles();
        sysEcafiles.setCREATEDATE(new Date());
        sysEcafiles.setFILENAME(fileInfo.getSimpleFileName());
        sysEcafiles.setFILETYPE(fileInfo.getFileExtension());
        sysEcafiles.setFILESIZE(fileInfo.getSize());
        sysEcafiles.setRow_Guid(UUID.randomUUID().toString().replace("-", ""));
        sysEcafiles.setBUCKETNAME(OSS_BUCKET_NAME);
        sysEcafiles.setGROUPGUID(GROUPGUID);
        sysEcafiles.setTO_ROW_GUID(RowGuid);
        sysEcafiles.setCREATEUSERID(-1);
        sysEcafiles.setVIRTUALPATH(virtualPath + RowGuid);
        sysEcafiles.setACTUALNAME(strFileName);
        ossMapper.insertOss(sysEcafiles);
        return true;
    }

    public boolean FileUpload(MaterialListClass file, String RowGuid) {
        if (file == null) {
            return false;
        }
        if (StringUtils.isAnyBlank(file.getFILE_PATH(), file.getFILE_NAME(), file.getMATE_SORT_UUID())) {
            return false;
        }

        try {
            String fileguid = UUID.randomUUID().toString();
            FileInfo fileInfo = FileUtils.getFileInfo(file.getFILE_PATH(), true);
            //设置文件后缀
            fileInfo.setFileExtensionForFg();
            String strFileType = fileInfo.getFileExtension();
            fileInfo.setSimpleFileName(file.getFILE_NAME());
            //上传文件,并保存文件相关数据到sys_ecafile表中
            SaveImageFileContent(fileguid + strFileType, RowGuid, strFileType, fileInfo, file.getGROUP_ID());
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));
            return false;
        }

        return true;
    }

    private String getItemMaterialList(String projectCode, String uniqueNum) {
        String fgURL = this.FGUrl + "/getItemMaterialList";
        String soapXml = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "  <soap:Body>\n" +
                "    <getItemMaterialList xmlns=\"http://UPDA2020.org/\">\n" +
                "      <SECURITY_KEY>" + SECURITY_KEY + "</SECURITY_KEY>\n" +
                "      <PROJECT_CODE>" + projectCode + "</PROJECT_CODE>\n" +
                "      <UNIQUE_NUM>" + uniqueNum + "</UNIQUE_NUM>\n" +
                "    </getItemMaterialList>\n" +
                "  </soap:Body>\n" +
                "</soap:Envelope>";

        String resposeStr = CommonUtil.doPostSoap(fgURL, soapXml);
        JsonData signatureObj = new JsonData();
        try {
            signatureObj = CommonUtil.xml2Json(resposeStr);
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));
        }

        JsonData Body = (JsonData) signatureObj.get("Body");

        JsonData fault = (JsonData) Body.get("Fault");
        if (fault != null && !fault.isEmpty()) {
            //返回错误信息
            return "{\"code\":\"0\"}";
        }

        JsonData getItemMaterialListResponse = (JsonData) Body.get("getItemMaterialListResponse");
        JsonData out = (JsonData) getItemMaterialListResponse.get("out");
        return JSONObject.toJSONString(out);
    }


    /**
     * 调用发改的接口
     * 获取施工许可事项信息
     *
     * @param projectCode
     * @param uniqueNum
     * @param type
     * @return
     */
    private String getDataFormFG(String projectCode, String uniqueNum, String type) {
        SoapClient huClient = SoapClient
                .create(this.FGUrl + "/getBuildApproveInfo")
                .setMethod("nsl:getBuildApproveInfo", "http://interaction.platform.webserviceinfo.neusoft.com")
                .setParam("SECURITY_KEY", "ff8080815dfd7c36015dfd9a8e2d028b")
                .setParam("PROJECT_CODE", projectCode)
                .setParam("UNIQUE_NUM", uniqueNum)
                .setParam("OPERATE_TYPE", type);
        String resposeStr = huClient.send(true);
        JsonData signatureObj = new JsonData();
        try {
            signatureObj = CommonUtil.xml2Json(resposeStr);
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));

        }

        JsonData Body = (JsonData) signatureObj.get("Body");

        JsonData fault = (JsonData) Body.get("Fault");
        if (fault != null && !fault.isEmpty()) {
            //返回错误信息
            return "{\"code\":\"0\"}";
        }

        JsonData getItemMaterialListResponse = (JsonData) Body.get("getBuildApproveInfoResponse");
        JsonData out = (JsonData) getItemMaterialListResponse.get("out");
        return JSONObject.toJSONString(out);
    }

    /**
     * 获取事项申报材料信息
     *
     * @param prjcode
     * @param unique_num
     * @param RowGuid
     * @return
     */
    public boolean GetItemMaterialList(String prjcode, String unique_num, String RowGuid) {
        if (StringUtils.isBlank(RowGuid)) {
            return false;
        }
        //从发改那边那边获取附件数据
        String resultStr = this.getItemMaterialList(prjcode, unique_num);
        if (resultStr.isEmpty()) {
            return false;
        }
        JSONObject result = JSONObject.parseObject(resultStr);
        if (!Objects.equals(result.getString("code"), "1")) {
            return false;
        }
        String data = result.getString("data");
        if (data.isEmpty()) {
            return false;
        }
        JSONObject temp = JSONObject.parseObject(data);
        logger.error("WebServiceItemMaterialVO------>" + temp.getString("WebServiceItemMaterialVO"));
        List<MaterialListClass> matlist = JSONArray.parseArray(temp.getString("WebServiceItemMaterialVO"), MaterialListClass.class);
        if (matlist.isEmpty()) {
            return false;
        }

        for (MaterialListClass item : matlist) {
            //修改文件material_name 字段
            //这里需要注意,新申请的附件类型和变更的附件类型不一样
            //这里是message_type状态5,也message_type状态3一样,走的附件对照表都是新申请的,c#源码逻辑如此
            //这里需要手动去匹配文件类型，有些文件类型，发改那边推送过来的就是和我们这边对应不上的
            //ex:建设工程规划许可证或者乡村建设规划许可证（依法不需要取得规划许可的装饰装修工程等无需提供）---> 建设工程规划许可证或者乡村建设规划许可证
            item.setGROUP_ID(AnnexMapping.FJ_MAP_XSQ.get(item.getMATERIAL_NAME().replaceAll("（[^）]*）", "")));
            //上传文件
            if (!this.FileUpload(item, RowGuid)) {
                return false;
            }
        }
        return true;
    }

    private String ChickFileInfo_Change(DataSet ds) {
        String strTitle = "";//标题
        try {
            String strTitle1 = "";
            String strTitle2 = "";
            StringBuilder strTitle3 = new StringBuilder();
            String strTitle4 = "";
            String strTitle5 = "";
            StringBuilder strTitle6 = new StringBuilder();
            String strTitle7 = "";
            StringBuilder strTitle8 = new StringBuilder();
            String strTitle9 = "";
            String strTitle10 = "";
            String strTitle11 = "";
            //检查备案表
            DataTable tbrecord = ds.getTable("tbrecordinfo_new");
            if (tbrecord == null || tbrecord.size() == 0) {
                strTitle8 = new StringBuilder("备案表不能为空;");
            } else {
                for (Map.Entry<String, String> entry : AnnexMapping.BEI_AN_BIAO.entrySet()) {
                    if (StringUtils.isBlank(tbrecord.get(0).getString(entry.getKey()))) {
                        strTitle8.append(",").append(entry.getValue());
                    }
                }
            }

            //检查施工许可信息
            DataTable dt4 = ds.getTable("tbbuilderLicencemanage_new_change");
            if (dt4 == null || dt4.size() == 0) {
                strTitle3.append(" 施工许可表不能为空");
            } else {
                DataRow dataRow3 = dt4.get(0);
                for (Map.Entry<String, String> entry : AnnexMapping.SGXK_BIAO.entrySet()) {
                    if (StringUtils.isBlank(dataRow3.getString(entry.getKey()))) {
                        strTitle3.append(",").append(entry.getValue());
                    }
                }
                if (Objects.equals(dataRow3.getString("PrjTypeNum"), "01")) {
                    if (StringUtils.isBlank(dataRow3.getString("allArea"))) {
                        strTitle3.append(",面积(平方米)");
                    }
                }
                if (Objects.equals(dataRow3.getString("PrjTypeNum"), "02")) {
                    if (StringUtils.isBlank(dataRow3.getString("Length"))) {
                        strTitle3.append(",长度");
                    }
                }
            }

            DataTable Data = ds.getTable("tbsfcorpinfo_new_change");
            if (Data == null || Data.size() == 0) {
                strTitle3.append(",四方主体单位不能为空");
            } else {
                DataRow[] dr = Data.Select("corptypenum='3'");
                if (dr.length > 0) {
                    StringBuilder sas = new StringBuilder();
                    for (DataRow item : dr) {
                        sas.append(item.getString("CORPLEADER"));
                    }
                    if (sas.toString().equals("")) {
                        strTitle3.append(",施工单位项目负责人不能为空");
                    }
                } else {
                    strTitle3.append(",施工单位不能为空");
                }

            }

            if (!StringUtils.isBlank(strTitle3.toString()) && strTitle3.length() > 1) {
                strTitle3 = new StringBuilder("施工许可信息:" + strTitle3.substring(1) + ";");
            }

            // 单体 tbprojectfinishunitinfo_new
            DataTable dt6 = ds.getTable("tbprojectfinishunitinfo_new");
            if (dt6 == null || dt6.size() == 0) {
                strTitle6 = new StringBuilder("单体信息至少添加一条;");
            } else {
                for (DataRow item : dt6) {
                    StringBuilder strunit = new StringBuilder();
                    for (Map.Entry<String, String> entry : AnnexMapping.DT_BIAO.entrySet()) {
                        if (StringUtils.isBlank(item.getString(entry.getKey()))) {
                            strunit.append(",").append(entry.getValue());
                        }
                    }
                    if (!StringUtils.isBlank(strunit.toString())) {
                        strTitle6.append("单体[").append(item.getString("SubPrjName")).append("]:").append(strunit);
                    }
                }
            }

            //附件
            if (Objects.equals(ds.getTable("tbrecordinfo_new").get(0).getString("CensorTypeNum"), "1")) {
                if (ds.getTable("TBFileInfo_new_change").Select("mate_sort_uuid='391'").length == 0) {
                    strTitle7 += "特殊建设工程的项目'施工图设计文件审查合格书(含消防审查,人防审查意见)'材料必须上传";
                }
            }
            strTitle = strTitle1 + strTitle2 + strTitle3 + strTitle4 + strTitle5 + strTitle6 + strTitle7 + strTitle8 + strTitle9 + strTitle10 + strTitle11;
            if (!StringUtils.isBlank(strTitle)) {
                strTitle = "以下字段为空,不能上报!" + strTitle;
            }

            return strTitle;
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));

            return strTitle;
        }
    }


    public boolean GetItemMaterialList_Change(DataTable dtFile, String RowGuid) {
        if (StringUtils.isBlank(RowGuid)) {
            return false;
        }
        if (dtFile == null || dtFile.size() == 0) {
            return false;
        }

        List<MaterialListClass> matlist;
        try {
            matlist = ConvertToList(dtFile);
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));
            return false;
        }

        if (matlist == null || matlist.size() == 0) {
            return false;
        }

        for (MaterialListClass item : matlist) {
            //这里是变更
            item.setGROUP_ID(AnnexMapping.FJ_MAP_BG.get(item.getMATERIAL_NAME().replaceAll("（[^）]*）", "")));
            boolean isfile = FileUpload(item, RowGuid);
            if (!isfile) {
                return false;
            }
        }
        return true;

    }

    private boolean SaveDataSet_Change(DataSet ds, String PRJ_Code, String UNIQUE_NUM) {
        try {
            ds.forEach((k, v) -> {
                String tableName = v.getName();
                if (Objects.equals(tableName, "TBFileInfo_new_change") || Objects.equals(tableName, "tbbuilderlicencemanage_new_changedetails")) {
                    return;
                }
                DataTable tempdt = fgMapper.getTableStructure(tableName);
                HashSet<String> columnsSet = new HashSet<>();
                tempdt.getColumns().forEach((k1, v1) -> columnsSet.add(k1.toLowerCase()));

                //循环插入数据
                for (DataRow dataRow : v) {
                    if (dataRow.containsKey("BARGAINDAYS")) {
                        dataRow.put("bsrgaindays", dataRow.getString("BARGAINDAYS"));
                    }
                    dataRow.put("UniqueNum", UNIQUE_NUM);
                    dataRow.put("OptionType", "0");

                    //生成sql和参数Map
                    DataSetSqlParams dataSetSqlParams = DataSetConversionSql.getInsertSqlForFG(dataRow, tableName, columnsSet);
                    fgMapper.executeSql(dataSetSqlParams.getSql(), dataSetSqlParams.getParams());
                }
            });
            return true;
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));
            return false;
        }
    }

    private boolean ComparSgxkAllInfo(String oldrowguid, String newrowguid) {
        String ZSchangeContent = "";
        StringBuilder UnitChangeContent = new StringBuilder();

        try {
            //获取变更前的记录数据
            DataSet dsold = fgMapper.getBGAfter(oldrowguid);
            //获取变更之后的记录数据
            DataSet dsnew = fgMapper.getBGBefore(newrowguid);
            //详情表
            List<SGXKBGDetails> details = new ArrayList<>();
            StringBuilder compareStr = new StringBuilder();
            //比较施工许可表
            DataTable oldTable = dsold.getTable("tbbuilderlicencemanage_New");
            DataTable newTable = dsnew.getTable("tbbuilderlicencemanage_New_Change");
            compareStr.append(BJSGXKInfo(oldTable, newTable, details));

            //比较单体表
            oldTable = dsold.getTable("tbprojectfinishunitinfo_new");
            newTable = dsold.getTable("tbprojectfinishunitinfo_new_change");
            compareStr.append(BJDTInfo(oldTable, newTable, details));

            //比较勘察单位信息
            oldTable = dsold.getTable("tbeconcorpinfo_new");
            newTable = dsold.getTable("tbeconcorpinfo_new_change");
            compareStr.append(BDEconCorpInfo(oldTable, newTable, details));

            //比较设计单位信息
            oldTable = dsold.getTable("tbdesigncorpinfo_new");
            newTable = dsold.getTable("tbdesigncorpinfo_new_change");
            compareStr.append(BDDesignCorpInfo(oldTable, newTable, details));

            //比较施工单位信息
            oldTable = dsold.getTable("tbconscorpinfo_new");
            newTable = dsold.getTable("tbconscorpinfo_new_change");
            compareStr.append(BDConsCorpInfo(oldTable, newTable, details));

            //比较监理单位信息
            oldTable = dsold.getTable("tbsupercorpinfo_new");
            newTable = dsold.getTable("tbsupercorpinfo_new_change");
            compareStr.append(BDSuperCorpInfo(oldTable, newTable, details));

            //比较总承包单位信息
            oldTable = dsold.getTable("tbzcbcorpinfo_new");
            newTable = dsold.getTable("tbzcbcorpinfo_new_change");
            compareStr.append(BDZcbCorpInfo(oldTable, newTable, details));

            //消防人防
            oldTable = dsold.getTable("tbrecordinfo_new");
            newTable = dsold.getTable("tbrecordinfo_new_Change");
            compareStr.append(XFRFInfo(oldTable, newTable, details));

            return EditChangeContentInfo("", newrowguid, compareStr.toString(), ZSchangeContent, UnitChangeContent.toString(),
                    details, dsold.getTable("tbbuilderlicencemanage_New"));
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));

            return false;
        }
    }

    private boolean EditChangeContentInfo(String mark, String RowGuid, String changeContent, String ZSchangeContent,
                                          String UnitChangeContent, List<SGXKBGDetails> detailsTable, DataTable dtold) {
        try {
            HashMap<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("RowGuid", RowGuid);
            paramsMap.put("changeContent", changeContent);
            paramsMap.put("ZSchangeContent", ZSchangeContent);
            paramsMap.put("UnitChangeContent", UnitChangeContent);
            paramsMap.put("BuilderLicenceNum", dtold.get(0).getString("BuilderLicenceNum"));
            paramsMap.put("PrjNum", dtold.get(0).getString("PrjNum"));
            paramsMap.put("mark", mark);
            fgMapper.updateTbbuilderlicencemanage_new_change(paramsMap);

            fgMapper.deleteChangedetails(RowGuid);
            fgMapper.saveTable(detailsTable, "tbbuilderlicencemanage_new_changedetails");
            return true;
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));

            return false;
        }
    }

    /**
     * 对比变更前后两条数据
     *
     * @param oldRow       旧数据
     * @param newRow       新数据
     * @param detailsTable 变更细节
     * @param mapKey       对照表区分字段
     * @return
     */
    private String handleCompare(DataRow oldRow, DataRow newRow, List<SGXKBGDetails> detailsTable, String mapKey) {
        StringBuilder compareStr = new StringBuilder();
        String rowGuid = oldRow.getString("rowguid");
        //k->字段名称
        //v->字段值
        oldRow.forEach((k, v) -> {
            if (AnnexMapping.IGNORE_FIELD.contains(k.toLowerCase())) {
                return;
            }
            String newRowValue = newRow.getString(k);
            if (!Objects.equals(v.toString(), newRowValue)) {
                SGXKBGDetails details = new SGXKBGDetails();
                details.setDataFilled(k);
                details.setNewValue(newRowValue);
                details.setOldValue(v.toString());
                details.setToRowGuid(rowGuid);
                detailsTable.add(details);
                String filedName = SGXKFieldMapping.ALL_TABLE.get(mapKey).get(k.toLowerCase());
                compareStr.append("字段<").append(filedName).append(">由").append("<").append(v)
                        .append(">变更为<").append(newRowValue).append(">,");
            }
        });
        //去掉最后一个逗号
        return compareStr.substring(1);
    }

    /**
     * 旧表与新表数据比较记录
     *
     * @param oldtable     就数据,可能是多条
     * @param newtable     新数据,可能是多条
     * @param detailsTable 变更细节记录表
     * @param mappingField 映射字段
     * @param mapKey
     * @return
     */
    private String handleCompare(DataTable oldtable, DataTable newtable, List<SGXKBGDetails> detailsTable, String mappingField, String mapKey) {
        if (oldtable.isEmpty() || newtable.isEmpty()) {
            return "-1";
        }
        StringBuilder sb = new StringBuilder();
        for (DataRow oldRow : oldtable) {
            String guid = oldRow.getString(mappingField);
            for (DataRow newRow : newtable) {
                if (newRow.getString(mappingField).equalsIgnoreCase(guid)) {
                    sb.append(handleCompare(oldRow, oldRow, detailsTable, mapKey));
                }
            }
        }

        return sb.toString();
    }

    private String XFRFInfo(DataTable oldtable, DataTable newtable, List<SGXKBGDetails> detailsTable) {
        if (oldtable.isEmpty() || newtable.isEmpty()) {
            return "-1";
        }
        DataRow oldRow = oldtable.get(0);
        DataRow newRow = newtable.get(0);

        StringBuilder compareStr = new StringBuilder();
        String rowGuid = oldRow.getString("rowguid");
        //k->字段名称
        //v->字段值
        oldRow.forEach((k, v) -> {
            if (AnnexMapping.IGNORE_FIELD.contains(k.toLowerCase())) {
                return;
            }
//            if (AnnexMapping.XFRF_SET.contains(k.toLowerCase())) {
//                //todo 消防人防
//            }
            String newRowValue = newRow.getString(k);
            if (!Objects.equals(v.toString(), newRowValue)) {
                SGXKBGDetails details = new SGXKBGDetails();
                details.setDataFilled(k);
                details.setNewValue(newRowValue);
                details.setOldValue(v.toString());
                details.setToRowGuid(rowGuid);
                detailsTable.add(details);
                String filedName = SGXKFieldMapping.XFRF_TABLE.get(k.toLowerCase());
                compareStr.append("字段<").append(filedName).append(">由").append("<").append(v)
                        .append(">变更为<").append(newRowValue).append(">,");
            }
        });
        //去掉最后一个逗号
        return compareStr.substring(1);
    }

    private String BJSGXKInfo(DataTable oldtable, DataTable newtable, List<SGXKBGDetails> detailsTable) {
        if (oldtable.isEmpty() || newtable.isEmpty()) {
            return "-1";
        }
        DataRow oldRow = oldtable.get(0);
        DataRow newRow = newtable.get(0);

        return this.handleCompare(oldRow, newRow, detailsTable, "tbbuilderlicencemanage_New");
    }

    private String BJDTInfo(DataTable oldtable, DataTable newtable, List<SGXKBGDetails> detailsTable) {
        return handleCompare(oldtable, newtable, detailsTable, "guid", "tbprojectfinishunitinfo_new");
    }

    private String BDEconCorpInfo(DataTable oldtable, DataTable newtable, List<SGXKBGDetails> detailsTable) {
        return handleCompare(oldtable, newtable, detailsTable, "corpcode", "tbeconcorpinfo_new");
    }

    private String BDDesignCorpInfo(DataTable oldtable, DataTable newtable, List<SGXKBGDetails> detailsTable) {
        return handleCompare(oldtable, newtable, detailsTable, "corpcode", "tbdesigncorpinfo_new");
    }

    private String BDConsCorpInfo(DataTable oldtable, DataTable newtable, List<SGXKBGDetails> detailsTable) {
        return handleCompare(oldtable, newtable, detailsTable, "corpcode", "tbconscorpinfo_new");
    }

    private String BDSuperCorpInfo(DataTable oldtable, DataTable newtable, List<SGXKBGDetails> detailsTable) {
        return handleCompare(oldtable, newtable, detailsTable, "corpcode", "tbsupercorpinfo_new");
    }

    private String BDZcbCorpInfo(DataTable oldtable, DataTable newtable, List<SGXKBGDetails> detailsTable) {
        return handleCompare(oldtable, newtable, detailsTable, "corpcode", "tbzcbcorpinfo_new");
    }

    private String Thread_SaveData_Change(DataClass data) {
        String PROJECT_CODE = data.PROJECT_CODE;
        String UNIQUE_NUM = data.UNIQUE_NUM;
        DataSet ds = ChangeJson.changeVoToDataSet(data.getData());
        ds.remove("ExtensionData");
        if (ds.isEmpty()) {
            return "ds数据为空,请联系管理人员";
        }

        try {
            DataTable sr = fgMapper.getSRFromTbrecordInfoNew(UNIQUE_NUM, ds.getTable("tbrecordinfo_new").get(0).getString("rowguid"), "2");
            String state = sr.getFirst().getString("status");

            for (DataRow dataRow : ds.getTable("tbbuilderlicencemanage_new_change")) {
                dataRow.put("Area", dataRow.getString("allArea"));
            }

            if (ds.getTable("tbeconcorpinfo_new_change") != null &&
                    ds.getTable("tbdesigncorpinfo_new_change") != null ||
                    ds.getTable("tbconscorpinfo_new_change") != null ||
                    ds.getTable("tbsupercorpinfo_new_change") != null ||
                    ds.getTable("tbprojectfinishunitinfo_new_new_change") != null) {

                ds.getTable("tbrecordinfo_new").get(0).put("shenqnum", "2");
                ds.getTable("tbrecordinfo_new").get(0).put("status", "2");
                DataTable sfinfo = new DataTable();
                sfinfo = fgMapper.getTableStructure("tbsfcorpinfo_new_change");
                sfinfo.setName("tbsfcorpinfo_new_change");

                String guid = ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("guid");
                for (DataRow item : ds.getTable("tbeconcorpinfo_new_change")) {
                    DataRow row = new DataRow();
                    row.put("CorpTypeNum", "1");
                    row.put("CorpName", item.getString("econCorpName"));
                    row.put("CorpCode", item.getString("econCorpCode"));
                    row.put("CORPLEADER", item.getString("ECONCORPLEADER"));
                    row.put("CORPLEADERCARDTYPE", item.getString("ECONCORPLEADERCARDTYPE"));
                    row.put("CORPLEADERCARDNUM", item.getString("ECONCORPLEADERCARDNUM"));
                    row.put("CorpLeaderPhone", item.getString("ECONCORPLEADERPHONE"));
                    row.put("sgxkguid", guid);
                    row.put("Guid", item.getString("guid"));
                    row.put("rowguid", item.getString("rowguid"));
                    sfinfo.add(row);
                }
                for (DataRow item : ds.getTable("tbdesigncorpinfo_new_change")) {
                    DataRow row = new DataRow();
                    row.put("CorpTypeNum", "2");
                    row.put("CorpName", item.getString("designCorpName"));
                    row.put("CorpCode", item.getString("designCorpCode"));
                    row.put("CORPLEADER", item.getString("DESIGNCORPLEADER"));
                    row.put("CORPLEADERCARDTYPE", item.getString("DESIGNCORPLEADERCARDTYPE"));
                    row.put("CORPLEADERCARDNUM", item.getString("DESIGNCORPLEADERCARDNUM"));
                    row.put("CorpLeaderPhone", item.getString("DESIGNCORPLEADERPHONE"));
                    row.put("sgxkguid", guid);
                    row.put("Guid", item.getString("guid"));
                    row.put("rowguid", item.getString("rowguid"));
                    sfinfo.add(row);
                }
                for (DataRow item : ds.getTable("tbconscorpinfo_new_change")) {
                    DataRow row = new DataRow();
                    row.put("CorpTypeNum", "3");
                    row.put("CorpName", item.getString("consCorpName"));
                    row.put("CorpCode", item.getString("consCorpCode"));
                    row.put("CORPLEADER", item.getString("CONSCORPLEADER"));
                    row.put("CORPLEADERCARDTYPE", item.getString("CONSCORPLEADERCARDTYPE"));
                    row.put("CORPLEADERCARDNUM", item.getString("CONSCORPLEADERCARDNUM"));
                    row.put("CorpLeaderPhone", item.getString("CONSCORPLEADERPHONE"));
                    row.put("sgxkguid", guid);
                    row.put("Guid", item.getString("guid"));
                    row.put("rowguid", item.getString("rowguid"));
                    sfinfo.add(row);
                }
                for (DataRow item : ds.getTable("tbsupercorpinfo_new_change")) {
                    DataRow row = new DataRow();
                    row.put("CorpTypeNum", "4");
                    row.put("CorpName", item.getString("superCorpName"));
                    row.put("CorpCode", item.getString("superCorpCode"));
                    row.put("CORPLEADER", item.getString("SUPERCORPLEADER"));
                    row.put("CORPLEADERCARDTYPE", item.getString("SUPERCORPLEADERCARDTYPE"));
                    row.put("CORPLEADERCARDNUM", item.getString("SUPERCORPLEADERCARDNUM"));
                    row.put("CorpLeaderPhone", item.getString("SUPERCORPLEADERPHONE"));
                    row.put("sgxkguid", guid);
                    row.put("Guid", item.getString("guid"));
                    row.put("rowguid", item.getString("rowguid"));
                    sfinfo.add(row);
                }

                DataRow t2 = ds.getTable("tbbuilderlicencemanage_new_change").get(0);
                String tt = t2.getString("BuildCorpCodeType");
                if (!Objects.equals(tt, "")) {
                    for (DataRow item : ds.getTable("tbzcbcorpinfo_new_change")) {
                        DataRow row = new DataRow();
                        row.put("CorpTypeNum", "5");
                        row.put("CorpName", item.getString("zcbCorpName"));
                        row.put("CorpCode", item.getString("zcbCorpCode"));
                        row.put("CORPLEADER", item.getString("ZCBCORPLEADER"));
                        row.put("CORPLEADERCARDTYPE", item.getString("ZCBCORPLEADERCARDTYPE"));
                        row.put("CORPLEADERCARDNUM", item.getString("ZCBCORPLEADERCARDNUM"));
                        row.put("CorpLeaderPhone", item.getString("ZCBCORPLEADERPHONE"));
                        row.put("sgxkguid", guid);
                        row.put("Guid", item.getString("guid"));
                        row.put("rowguid", item.getString("rowguid"));
                        sfinfo.add(row);
                    }
                    ds.remove("tbzcbcorpinfo_new_change");
                }
                for (DataRow item : ds.getTable("tbprojectfinishunitinfo_new_change")) {
                    if (Objects.equals(item.getString("sgxkguid"), "")) {
                        item.put("sgxkguid", t2.getString("guid"));
                    }
                }
                ds.add(sfinfo);
                ds.remove("tbeconcorpinfo_new_change");
                ds.remove("tbdesigncorpinfo_new_change");
                ds.remove("tbconscorpinfo_new_change");
                ds.remove("tbsupercorpinfo_new_change");
                ds.getTable("tbrecordinfo_new").get(0).put("CreateTime", getNowTimeStr());
                String result = ChickFileInfo_Change(ds);
                if (!StringUtils.isBlank(result)) {
                    this.PushingProcess_Change("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                            ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("citynum"),
                            ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("rowguid"),
                            ds.getTable("tbbuilderlicencemanage_new").get(0).getString("guid"),
                            "", getNowTimeHHmmss(), "330000", "变更接口自动退回",
                            result, "330000", "变更接口自动退回", "1");
                    return result;
                }

                //查询是否已经插入该办建记录,已上报,退回的状态可以删除再次添加,已审核的则不可以
                DataTable tbrecord = ds.getTable("tbrecordinfo_new");
                //保存附件
                boolean bcfj = GetItemMaterialList_Change(ds.getTable("TBFileInfo_new_change"), tbrecord.get(0).getString("rowguid"));
                if (!bcfj) {
                    //推送失败后要推办件过程,重新推
                    this.PushingProcess_Change("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                            ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("citynum"),
                            ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("rowguid"),
                            ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("guid"),
                            "", getNowTimeHHmmss(), "330000", "变更接口自动退回",
                            "获取附件或者保存附件出现异常", "330000", "变更接口自动退回", "1");
                    return "获取附件或者保存附件出现异常";
                }
                String tbrecordinfo_new_RowGuid = ds.getTable("tbrecordinfo_new").get(0).getString("RowGuid");
                DataTable dtrecordinfo = fgMapper.getDataFromTbrecordinfo_new(tbrecordinfo_new_RowGuid);

                if (StringUtils.isBlank(state) || state.equals("3")) {
                    if (!SaveDataSet_Change(ds, PROJECT_CODE, UNIQUE_NUM)) {
                        //推送失败后要推办件过程,重新推
                        this.PushingProcess_Change("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("citynum"),
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("rowguid"),
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("guid"),
                                "", getNowTimeHHmmss(), "330000", "变更接口自动退回",
                                "系统发生错误,对比数据并保存到数据库中出现异常,请联系管理人员之后重新上报", "330000", "变更接口自动退回", "1");
                        return "系统发生错误,对比数据并保存到数据库中出现异常,请联系管理人员之后重新上报";
                    }

                    //这里是对比数据并保存到数据库中
                    if (ComparSgxkAllInfo(dtrecordinfo.get(0).getString("FirstRowGuid"), tbrecordinfo_new_RowGuid)) {
                        return "";
                    } else {
                        this.PushingProcess_Change("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("citynum"),
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("rowguid"),
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("guid"),
                                "", getNowTimeHHmmss(), "330000", "变更接口自动退回",
                                "系统出现异常,对比数据并保存到数据库中出现异常,请联系管理人员后重新上报", "330000", "变更接口自动退回", "1");
                        return "系统出现异常,对比数据并保存到数据库中出现异常,请联系管理人员后重新上报";
                    }
                }

                if (state.equals("2") || state.equals("4") || state.equals("5")) {
                    //执行删除然后插入
                    if (!DeleteTable(ds)) {
                        //推送失败后要推办件过程,重新推
                        this.PushingProcess_Change("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("citynum"),
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("rowguid"),
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("guid"),
                                "", getNowTimeHHmmss(), "330000", "变更接口自动退回",
                                "变更系统发生错误,执行删除失败,请联系管理人员之后重新上报", "330000", "变更接口自动退回", "1");

                        return "变更系统发生错误,执行删除失败,请联系管理人员之后重新上报";
                    }

                    if (!SaveDataSet_Change(ds, PROJECT_CODE, UNIQUE_NUM)) {
                        //推送失败后要推办件过程,重新推
                        this.PushingProcess_Change("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("citynum"),
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("rowguid"),
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("guid"),
                                "", getNowTimeHHmmss(), "330000", "变更接口自动退回",
                                "系统发生错误,保存变更失败，请联系管理人员之后重新上报", "330000", "变更接口自动退回", "1");
                        return "系统发生错误,保存变更失败，请联系管理人员之后重新上报";

                    }

                    if (!ComparSgxkAllInfo(dtrecordinfo.get(0).getString("FirstRowGuid"), tbrecordinfo_new_RowGuid)) {
                        //推送失败后要推办件过程,重新推
                        this.PushingProcess_Change("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("citynum"),
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("rowguid"),
                                ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("guid"),
                                "", getNowTimeHHmmss(), "330000", "变更接口自动退回",
                                "系统发生错误,对比变更失败，请联系管理人员之后重新上报", "330000", "变更接口自动退回", "1");
                        return "系统发生错误,对比变更失败，请联系管理人员之后重新上报";
                    }
                    return "";
                }
            }
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));
            //推送失败后要推办件过程,重新推
            this.PushingProcess_Change("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                    ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("citynum"),
                    ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("rowguid"),
                    ds.getTable("tbbuilderlicencemanage_new_change").get(0).getString("guid"),
                    "", getNowTimeHHmmss(),
                    "330000", "变更接口自动退回", "变更系统错误,请先联系变更系统管理人员，再重新提交", "330000",
                    "变更接口自动退回", "1");

            return "变更系统错误,请先联系变更系统管理人员，再重新提交";
        }
        return "状态在2,3,4,5,6,之外，请先联系变更系统管理人员，再重新提交";
    }

    private boolean SaveDataSet_zx(DataSet ds, String PRJ_Code, String UNIQUE_NUM) {

        try {
            Trans.exec(() -> {
                ds.forEach((k, v) -> {
                    String strTableName = ds.get(k).getName();
                    HashSet<String> columnsSet = fgMapper.getTableStructureToHashSet(strTableName);
                    //循环插入
                    for (DataRow dataRow : v) {
                        if (dataRow.containsKey("BARGAINDAYS")) {
                            dataRow.put("bsrgaindays", dataRow.getString("BARGAINDAYS"));
                        }
                        dataRow.put("UniqueNum", UNIQUE_NUM);
                        dataRow.put("OptionType", "0");

                        DataSetSqlParams sqlParams = DataSetConversionSql.getInsertSqlForFG(dataRow, strTableName, columnsSet);
                        fgMapper.executeSql(sqlParams.getSql(), sqlParams.getParams());
                    }
                });
            });
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));

            return false;
        }
        return true;
    }

    private String Thread_SaveData_zx(String DyType, DataClass data) {
        String PROJECT_CODE = data.PROJECT_CODE;
        String UNIQUE_NUM = data.UNIQUE_NUM;
        String strErrMsg = "";
        DataSet dss = ChangeJson.changeVoToDataSet(data.getData());

        try {
            dss.getTable("tbrecordinfo_new").getColumns().put("CreateTime", new DataColumn());
            dss.getTable("tbrecordinfo_new").get(0).put("CreateTime", getNowTimeStr());

            String state = fgMapper.getStatusByShenQNumIn(UNIQUE_NUM, dss.getTable("tbrecordinfo_new").get(0).getString("rowguid"), "3,4,5,6");

            if (StringUtils.isBlank(state)) {
                if (!SaveDataSet_zx(dss, PROJECT_CODE, UNIQUE_NUM)) {
                    //推送失败后要推办件过程,重新推
                    PushingProcess_zx("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                            dss.getTable("tbbuilderlicencemanage_new").get(0).getString("rowguid"),
                            "", getNowTimeHHmmss(), "330000", "注销延期中止恢复接口自动退回",
                            "系统发生错误：状态为空&保存注销数据失败,变更请重新上报", "330000", "注销延期中止恢复接口自动退回", "1", DyType);

                    return "注销延期保存数据库出现问题,code:1";
                }
                return "注销延期中止恢复接口自动退回,状态为空&保存注销数据失败";
            }

            //执行删除然后插入
            if (DeleteTable(dss)) {
                if (!SaveDataSet_zx(dss, PROJECT_CODE, UNIQUE_NUM)) {
                    //推送失败后要推办件过程,重新推
                    this.PushingProcess_zx("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                            dss.getTable("tbbuilderlicencemanage_new_change").get(0).getString("rowguid"),
                            "", getNowTimeHHmmss(), "330000", "注销延期中止恢复接口自动退回",
                            "系统发生错误：删除成功但是保存数据失败,变更请重新上报", "330000", "注销延期中止恢复接口自动退回", "1", DyType);
                    return "系统发生错误：删除成功但是保存数据失败,变更请重新上报";
                }
                return "注销延期中止恢复接口自动退回";
            }

            this.PushingProcess_zx("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                    dss.getTable("tbbuilderlicencemanage_new_change").get(0).getString("rowguid"),
                    "", getNowTimeHHmmss(), "330000", "注销延期中止恢复接口自动退回",
                    "变更系统发生错误：执行删除失败,请重新上报", "330000", "注销延期中止恢复接口自动退回", "1", DyType);
            return "变更系统发生错误：执行删除失败";
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));
            //推送失败后要推办件过程,重新推
            this.PushingProcess_zx("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                    dss.getTable("tbbuilderlicencemanage_new_change").get(0).getString("rowguid"),
                    "", getNowTimeHHmmss(), "330000", "注销延期中止恢复接口自动退回",
                    "注销延期中止恢复系统错误,请重新提交", "330000", "注销延期中止恢复接口自动退回", "1", DyType);
            return "注销延期中止恢复系统错误,请重新提交";
        }
    }

    private String ChickFileInfo(DataSet ds) {
        String strTitle = "";//标题
        try {
            String strTitle1 = "";
            String strTitle2 = "";
            StringBuilder strTitle3 = new StringBuilder();
            String strTitle4 = "";
            String strTitle5 = "";
            StringBuilder strTitle6 = new StringBuilder();
            String strTitle7 = "";
            StringBuilder strTitle8 = new StringBuilder();
            String strTitle9 = "";
            String strTitle10 = "";
            String strTitle11 = "";
            // 备案表
            DataTable tbrecord = ds.getTable("tbrecordinfo_new");
            if (tbrecord == null || tbrecord.size() == 0) {
                strTitle8 = new StringBuilder("备案表不能为空;");
            } else {
                for (Map.Entry<String, String> entry : AnnexMapping.BEI_AN_BIAO.entrySet()) {
                    if (StringUtils.isBlank(tbrecord.get(0).getString(entry.getKey()))) {
                        strTitle8.append(",").append(entry.getValue());
                    }
                }
            }

            // 施工许可信息
            DataTable dt4 = ds.getTable("tbbuilderLicencemanage_new");
            if (dt4 == null || dt4.size() == 0) {
                strTitle3.append(" 施工许可表不能为空");
            } else {
                DataRow dataRow3 = dt4.get(0);
                for (Map.Entry<String, String> entry : AnnexMapping.SGXK_BIAO.entrySet()) {
                    if (StringUtils.isBlank(dataRow3.getString(entry.getKey()))) {
                        strTitle3.append(",").append(entry.getValue());
                    }
                }
                if (Objects.equals(dataRow3.getString("PrjTypeNum"), "01")) {
                    if (StringUtils.isBlank(dataRow3.getString("allArea"))) {
                        strTitle3.append(",面积(平方米)");
                    }
                }
                if (Objects.equals(dataRow3.getString("PrjTypeNum"), "02")) {
                    if (StringUtils.isBlank(dataRow3.getString("Length"))) {
                        strTitle3.append(",长度");
                    }
                }
                if (StringUtils.isBlank(dataRow3.getString("PLANBDATE"))) {
                    strTitle3.append(",计划开工日期");
                }
                if (StringUtils.isBlank(dataRow3.getString("PLANEDATE"))) {
                    strTitle3.append(",计划竣工日期");
                }
            }

            // 四方主体
            DataTable Data = ds.getTable("tbsfcorpinfo_new");
            if (Data == null || Data.size() == 0) {
                strTitle3.append(",四方主体单位不能为空");
            } else {
                DataRow[] dr = Data.Select("corptypenum='3'");
                if (dr.length > 0) {
                    StringBuilder sas = new StringBuilder();
                    for (DataRow item : dr) {
                        sas.append(item.getString("CORPLEADER"));
                    }
                    if (sas.toString().equals("")) {
                        strTitle3.append(",施工单位项目负责人不能为空");
                    }
                } else {
                    strTitle3.append(",施工单位不能为空");
                }

            }

            if (!StringUtils.isBlank(strTitle3) && strTitle3.length() > 1) {
                strTitle3 = new StringBuilder("施工许可信息:" + strTitle3.substring(1) + ";");
            }

            //#region 单体
            DataTable dt6 = ds.getTable("tbprojectfinishunitinfo_new");
            if (dt6 == null || dt6.size() == 0) {
                strTitle6 = new StringBuilder("单体信息至少添加一条;");
            } else {
                for (DataRow item : dt6) {
                    StringBuilder strunit = new StringBuilder();
                    for (Map.Entry<String, String> entry : AnnexMapping.DT_BIAO.entrySet()) {
                        if (StringUtils.isBlank(item.getString(entry.getKey()))) {
                            strunit.append(entry.getValue()).append(",");
                        }
                    }
                    if (!StringUtils.isBlank(strunit)) {
                        strTitle6.append("单体[").append(item.getString("SubPrjName")).append("]:").append(strunit);
                    }
                }
            }
            //#region 附件
            if (Objects.equals(ds.getTable("tbrecordinfo_new").get(0).getString("CensorTypeNum"), "1")) {
                if (ds.getTable("TBFileInfo").Select("mate_sort_uuid='378'").length == 0) {
                    strTitle7 += "特殊建设工程的项目'施工图设计文件审查合格书(含消防审查,人防审查意见)'材料必须上传";
                }
            }

            strTitle = strTitle1 + strTitle2 + strTitle3 + strTitle4 + strTitle5 + strTitle6 + strTitle7 + strTitle8 + strTitle9 + strTitle10 + strTitle11;
            if (!StringUtils.isBlank(strTitle)) {
                strTitle = "以下字段为空,不能上报!" + strTitle;
            }
            return strTitle;
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));
            return strTitle;
        }
    }

    private String getNowTimeStr() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return now.format(formatter);
    }

    private String getNowTimeHHmmss() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");
        return now.format(formatter);
    }


    public String setSgxkResult(String deal_deptid, String deal_deptname, String deal_opinion, String deal_state,
                                String deal_time, String deal_userid, String deal_username, String outer_key,
                                String project_code, String reply_file, String reply_file_name, String reply_number,
                                String reply_result, String unique_num, String upload_date, String operate_type,
                                String operate_result, String operate_time, String operate_opinion) {
        String fgURL = "http://223.4.69.50:18182/tzxmspall/service/projectInfoWeb?wsdl/setSgxkResult";
        String soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:int=\"http://interaction.platform.webserviceinfo.neusoft.com\" xmlns:vo=\"http://vo.platform.webserviceinfo.neusoft.com\">\n" +
                "<soapenv:Body>\n" +
                "<int:setSgxkResult>\n" +
                "<int:in0>" + this.SECURITY_KEY + "</int:in0>\n" +
                "   <int:in1>\n" +
                "   <vo:deal_deptid>" + deal_deptid + "</vo:deal_deptid>\n" +
                "   <vo:deal_deptname>" + deal_deptname + "</vo:deal_deptname>\n" +
                "   <vo:deal_opinion>" + deal_opinion + "</vo:deal_opinion>\n" +
                "   <vo:deal_state>" + deal_state + "</vo:deal_state>\n" +
                "   <vo:deal_time>" + deal_time + "</vo:deal_time>\n" +
                "   <vo:deal_userid>" + deal_userid + "</vo:deal_userid>\n" +
                "   <vo:deal_username>" + deal_username + "</vo:deal_username>\n" +
                "   <vo:outer_key>" + outer_key + "</vo:outer_key>\n" +
                "   <vo:outer_type>SGXK</vo:outer_type>\n" +
                "   <vo:project_code>" + project_code + "</vo:project_code>\n" +
                "   <vo:reply_file>" + reply_file + "</vo:reply_file>\n" +
                "   <vo:reply_file_name>" + reply_file_name + "</vo:reply_file_name>\n" +
                "   <vo:reply_number>" + reply_number + "</vo:reply_number>\n" +
                "   <vo:reply_result>" + reply_result + "</vo:reply_result>\n" +
                "   <vo:unique_num>" + unique_num + "</vo:unique_num>\n" +
                "   <vo:upload_date>" + upload_date + "</vo:upload_date>\n" +
                "   <vo:operate_type>" + operate_type + "</vo:operate_type>\n" +
                "   <vo:operate_result>" + operate_result + "</vo:operate_result>\n" +
                "   <vo:operate_time>" + operate_time + "</vo:operate_time>\n" +
                "   <vo:operate_opinion>" + operate_opinion + "</vo:operate_opinion>\n" +
                "</int:in1>\n" +
                "</int:setSgxkResult>\n" +
                "</soapenv:Body>\n" +
                "</soapenv:Envelope>";
        String resposeStr = CommonUtil.doPostSoap(fgURL, soapXml);
        JsonData signatureObj = new JsonData();
        try {
            signatureObj = CommonUtil.xml2Json(resposeStr);
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));

        }
        JsonData Body = (JsonData) signatureObj.get("Body");
        //error msg
        JsonData fault = (JsonData) Body.get("Fault");
        if (fault != null && !fault.isEmpty()) {
            //返回错误信息
            return "{\"code\":\"0\"}";
        }
        JsonData getItemMaterialListResponse = (JsonData) Body.get("setSgxkResultResponse");
        JsonData out = (JsonData) getItemMaterialListResponse.get("out");
        return JSONObject.toJSONString(out);
    }

    @Override
    public boolean PushingProcess_zx(String deal_state, String prjcode, String unique_num, String rowguid,
                                     String BuilderLicenceNum, String OpentionId, String deal_deptid, String deal_deptname,
                                     String deal_opinion, String deal_userid, String deal_username, String reply_result, String operate_type) {

        //办件过程推送方法
        try {
            OpentionId = UUID.randomUUID().toString();
            DataTable dataTable = ofdWebMapper.getInfoByRecordGuid(rowguid);
            String eCertID = dataTable.get(0).getString("ECertID");
            String SGXKDZZSossfileurl = "oss/upload/ScanImageList/SGXKDZZS/" + prjcode + "/" + unique_num + "/" + eCertID + ".OFD";
            String reply_file_name = "建筑工程施工许可证.OFD";
            String upload_date = getNowTimeStr();
            String deal_time = getNowTimeStr();


            if (Objects.equals(deal_state, "ITEM_BJ")) {
                logger.info("进入注销延期中止恢复办件过程推送方法推送办结");
            } else {
                SGXKDZZSossfileurl = "";
                reply_file_name = "";
                upload_date = "";
            }

            String bg_result;
            if (Objects.equals(deal_state, "ITEM_BLZ") || Objects.equals(deal_state, "ITEM_BJ")) {
                bg_result = "1";
            } else {
                bg_result = "0";
            }

            String objStr = this.setSgxkResult(deal_deptid, deal_deptname, deal_opinion, deal_state,
                    deal_time, deal_userid, deal_username, OpentionId, prjcode, SGXKDZZSossfileurl, reply_file_name,
                    BuilderLicenceNum, (Objects.equals(deal_state, "ITEM_BJ") ? reply_result : ""), unique_num, upload_date,
                    operate_type, bg_result, deal_time, deal_opinion);
            JSONObject obj = JSONObject.parseObject(objStr);
            if (Objects.equals(obj.getString("code"), "1")) {
                logger.info("办件过程推送成功");
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return false;
        }
    }

    //向发改推送电子证书
    private boolean PushingOFDOSSFile(String prjcode, String unique_num, String eCertID) {
        //获取当前文件的
        String objectName = "oss/upload/ScanImageList/SGXKDZZS/" + prjcode + "/" + unique_num + "/" + eCertID + ".OFD";
        logger.error("开始获取到oss文件");
        byte[] bytes = OUR_OSS.downloadByte(OSS_BUCKET_NAME, objectName);
        logger.error("已经获取到oss文件，文件大小：" + bytes.length);
        logger.error("开始上传文件");
        return FG_OSS.upload(FG_OSS_BUCKET_NAME, objectName, bytes);
    }

    public String setItemRecord(WebServiceItemRecordInVO vo) {
        String fgURL = "http://223.4.69.50:18182/tzxmspall/service/projectInfoWeb?wsdl/setItemRecord";
        String soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:int=\"http://interaction.platform.webserviceinfo.neusoft.com\" xmlns:vo=\"http://vo.platform.webserviceinfo.neusoft.com\">\n" +
                "<soapenv:Body>\n" +
                "<int:setItemRecord>\n" +
                "<int:in0>" + this.SECURITY_KEY + "</int:in0>\n" +
                "   <int:in1>\n" +
                "   <vo:deal_deptid>" + vo.getDEAL_DEPTID() + "</vo:deal_deptid>\n" +
                "   <vo:deal_deptname>" + vo.getDEAL_DEPTNAME() + "</vo:deal_deptname>\n" +
                "   <vo:deal_opinion>" + vo.getDEAL_OPINION() + "</vo:deal_opinion>\n" +
                "   <vo:deal_state>" + vo.getDEAL_STATE() + "</vo:deal_state>\n" +
                "   <vo:deal_time>" + vo.getDEAL_TIME() + "</vo:deal_time>\n" +
                "   <vo:deal_userid>" + vo.getDEAL_USERID() + "</vo:deal_userid>\n" +
                "   <vo:deal_username>" + vo.getDEAL_USERNAME() + "</vo:deal_username>\n" +
                "   <vo:outer_key>" + vo.getOUTER_KEY() + "</vo:outer_key>\n" +
                "   <vo:outer_type>" + vo.getOUTER_TYPE() + "</vo:outer_type>\n" +
                "   <vo:project_code>" + vo.getPROJECT_CODE() + "</vo:project_code>\n" +
                "   <vo:reply_file>" + vo.getREPLY_FILE() + "</vo:reply_file>\n" +
                "   <vo:reply_file_name>" + vo.getREPLY_FILE_NAME() + "</vo:reply_file_name>\n" +
                "   <vo:reply_number>" + vo.getREPLY_RESULT() + "</vo:reply_number>\n" +
                "   <vo:reply_result>" + vo.getREPLY_RESULT() + "</vo:reply_result>\n" +
                "   <vo:unique_num>" + vo.getUNIQUE_NUM() + "</vo:unique_num>\n" +
                "   <vo:upload_date>" + vo.getUPLOAD_DATE() + "</vo:upload_date>\n" +
                "</int:in1>\n" +
                "</int:setItemRecord>\n" +
                "</soapenv:Body>\n" +
                "</soapenv:Envelope>";
        String resposeStr = CommonUtil.doPostSoap(fgURL, soapXml);
        logger.error("resposeStr ----->" + resposeStr);
        JsonData signatureObj = new JsonData();
        try {
            signatureObj = CommonUtil.xml2Json(resposeStr);
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));

        }
        JsonData Body = (JsonData) signatureObj.get("Body");
        //error msg
        JsonData fault = (JsonData) Body.get("Fault");
        if (fault != null && !fault.isEmpty()) {
            //返回错误信息
            return "{\"code\":\"0\"}";
        }
        JsonData getItemMaterialListResponse = (JsonData) Body.get("setItemRecordResponse");
        JsonData out = (JsonData) getItemMaterialListResponse.get("out");
        return JSONObject.toJSONString(out);
    }

    @Override
    public boolean PushingProcess(String deal_state, String prjcode, String unique_num, String citynum, String rowguid,
                                  String sgxkguid, String BuilderLicenceNum, String OpentionId, String deal_deptid,
                                  String deal_deptname, String deal_opinion, String deal_userid, String deal_username,
                                  String reply_result, String upload_date, String deal_time) {

        logger.error("进入办件过程推送方法");

        upload_date = getNowTimeStr();
        deal_time = getNowTimeStr();
        OpentionId = UUID.randomUUID().toString();

        String eCertID = fgMapper.getECertIDFromTbbuilderlicencemanage_new(rowguid);
        String SGXKDZZSossfileurl = "oss/upload/ScanImageList/SGXKDZZS/" + prjcode + "/" + unique_num + "/" + eCertID + ".OFD";
        String reply_file_name = "建筑工程施工许可证.OFD";

        if (Objects.equals(deal_state, "ITEM_BJ")) {
            logger.error("进入办件过程推送方法推送办结");
            //附件一:施工许可证
            if (!PushingOFDOSSFile(prjcode, unique_num, eCertID)) {
                logger.error("走到这里就说明上传文件出现问题");
                return false;
            }
        } else {
            SGXKDZZSossfileurl = "";
            reply_file_name = "";
            upload_date = "";
        }

        WebServiceItemRecordInVO vo = new WebServiceItemRecordInVO(prjcode, unique_num, OpentionId
                , "SGXK", deal_userid, deal_username, deal_deptid, deal_deptname, deal_opinion, deal_state,
                deal_time, reply_result, BuilderLicenceNum, reply_file_name, SGXKDZZSossfileurl, upload_date);

        String objStr = this.setItemRecord(vo);
        JSONObject obj = JSONObject.parseObject(objStr);
        return Objects.equals(obj.getString("code"), "1");
    }

    private boolean PushingOSSFile(String ECertID, String prjcode, String unique_num, String rowguid, String sgxkguid, String citynum) {
        //需要将保存在我们的oss的电子证书传给发改
        //我们的保存在oss/upload/ScanImageList/SGXKDZZS/prjcode/unique_num/sgxkguid_SGXKDZZS.pdf
        //发改的保存在prjcode/unique_num/sgxkguid_type.pdf
        String virtualPath = "oss/upload/ScanImageList/SGXKDZZS" + "/" + prjcode + "/" + unique_num;
        String actualName = ECertID + ".ofd";
        String objectName = virtualPath + "/" + actualName;
        //从我们的oss获取数据
        byte[] bytes = OUR_OSS.downloadByte(OSS_BUCKET_NAME, objectName);
        //保存到fg的oss
        return FG_OSS.upload(FG_OSS_BUCKET_NAME, objectName, bytes);
    }

    @Override
    public boolean PushingProcess_Change(String deal_state, String prjcode, String unique_num, String citynum,
                                         String rowguid, String sgxkguid, String BuilderLicenceNum, String OpentionId,
                                         String deal_deptid, String deal_deptname, String deal_opinion, String deal_userid,
                                         String deal_username, String reply_result) {

        logger.info("进入办件过程推送方法");
        try {
            OpentionId = UUID.randomUUID().toString();
            DataTable dataTable = ofdWebMapper.getInfoByRecordGuid(rowguid);
            String eCertID = dataTable.get(0).getString("ECertID");
            String SGXKDZZSossfileurl = "oss/upload/ScanImageList/SGXKDZZS/" + prjcode + "/" + unique_num + "/" + eCertID + ".OFD";
            String reply_file_name = "建筑工程施工许可证.OFD";
            String upload_date = getNowTimeStr();
            String deal_time = getNowTimeStr();


            if (Objects.equals(deal_state, "ITEM_BJ")) {
                logger.info("进入办件过程推送方法推送办结");
                if (!this.PushingOSSFile(eCertID, prjcode, unique_num, rowguid, sgxkguid, citynum)) {
                    return false;
                }

            } else {
                SGXKDZZSossfileurl = "";
                reply_file_name = "";
                upload_date = "";
            }
            String bg_result;
            if (Objects.equals(deal_state, "ITEM_BLZ") || Objects.equals(deal_state, "ITEM_BJ")) {
                bg_result = "1";
            } else {
                bg_result = "0";
            }

            String objStr = this.setSgxkResult(deal_deptid, deal_deptname, deal_opinion, deal_state,
                    deal_time, deal_userid, deal_username, OpentionId, prjcode, SGXKDZZSossfileurl, reply_file_name,
                    BuilderLicenceNum, (Objects.equals(deal_state, "ITEM_BJ") ? reply_result : ""), unique_num, upload_date,
                    "2", bg_result, deal_time, deal_opinion);

            JSONObject obj = JSONObject.parseObject(objStr);
            return Objects.equals(obj.getString("code"), "1");
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));
            return false;
        }
    }

    public List<MaterialListClass> ConvertToList(DataTable dt) throws IllegalAccessException {
        // 定义集合
        List<MaterialListClass> list = new ArrayList<>();
        if (dt.isEmpty()) {
            return list;
        }

        //遍历DataTable中所有的数据行
        for (DataRow dataRow : dt) {
            MaterialListClass target = new MaterialListClass();
            Field[] destFields = target.getClass().getDeclaredFields();

            for (Field destField : destFields) {
                String lowStr = dataRow.getString(destField.getName().toLowerCase());
                String upStr = dataRow.getString(destField.getName().toUpperCase());
                if (StringUtils.isAllBlank(lowStr, upStr)) {
                    continue;
                }
                destField.setAccessible(true);
                if (StringUtils.isBlank(lowStr)) {
                    destField.set(target, lowStr);
                } else {
                    destField.set(target, upStr);
                }
            }
            list.add(target);
        }

        return list;
    }

    //新申请获取附件
    public boolean GetItemMaterialList_New(DataTable dtFile, String prjcode, String unique_num, String RowGuid, DataClass data) {
        if (StringUtils.isBlank(RowGuid)) {
            return false;
        }
        if (dtFile.isEmpty()) {
            return false;
        }
        //从发改那边获取数据
        logger.error("开始--->从发改那边获取数据getItemMaterialList" + new Date());
        String dataFormFG = this.getItemMaterialList(prjcode, unique_num);
        logger.error("结束--->从发改那边获取数据getItemMaterialList" + new Date());
        JSONObject result = JSON.parseObject(dataFormFG);

        logger.error("getItemMaterialList result:" + dataFormFG);
        if (!Objects.equals(result.getString("code"), "1")) {
            return false;
        }
        if (StringUtils.isBlank(result.getString("data"))) {
            return false;
        }

        List<MaterialListClass> matlist = new ArrayList<>();
        try {
            matlist = ConvertToList(dtFile);
        } catch (Exception e) {
            logger.error("转化为 matlist 失败");
            logger.error(Arrays.toString(e.getStackTrace()));
        }
        logger.error("开始--->上传文件 matlist" + new Date());
        for (MaterialListClass item : matlist) {
            //修改文件material_name 字段
            //这里需要注意,新申请的附件类型和变更的附件类型不一样
            item.setGROUP_ID(AnnexMapping.FJ_MAP_XSQ.get(item.getMATERIAL_NAME()));
            //上传文件
            FileUpload(item, RowGuid);
        }
        logger.error("开始--->上传文件 matlist" + new Date());
        return true;
    }

    //新申请插入数据方法
    private boolean SaveDataSet(DataSet ds, String PRJ_Code, String UNIQUE_NUM) {
        try {
            ds.forEach((k, v) -> {
                //只有一部分需要保存,如果不在保存名单之内,就不需要保存
                if (!AnnexMapping.FG_TABLE_NEED_SAVE.contains(k)) {
                    return;
                }
                String strTableName = v.getName();
                //计算企业基本信息表的sql语句
                DataTable table = ds.getTable(strTableName);
                for (DataRow dataRow : table) {
                    if (dataRow.containsKey("BARGAINDAYS")) {
                        dataRow.put("bsrgaindays", dataRow.getString("BARGAINDAYS"));
                    }
                    dataRow.put("UniqueNum", UNIQUE_NUM);
                    dataRow.put("OptionType", "0");
                    //获取需要保存表的数据结构
                    String tableName = table.getName();
                    DataTable tempdt = fgMapper.getTableStructure(tableName);
                    HashSet<String> columnsSet = new HashSet<>();
                    tempdt.getColumns().forEach((k1, v1) -> columnsSet.add(k1.toLowerCase()));

                    DataSetSqlParams dataSetSqlParams = DataSetConversionSql.getInsertSqlForFG(dataRow, table.getName(), columnsSet);
                    dataSetSqlParams.getParams().remove("id");
                    if ("tbrecordinfo_new".equalsIgnoreCase(table.getName())) {
                        dataSetSqlParams.getParams().put("status", 102);
                    }
                    //执行sql
                    fgMapper.executeSql(dataSetSqlParams.getSql(), dataSetSqlParams.getParams());
                }
            });
            return true;
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));

            return false;
        }
    }


    private boolean DeleteTable(DataSet ds) {
        try {
            ds.forEach((strTableName, dataRows) -> {
                if (dataRows == null || dataRows.isEmpty()) {
                    return;
                }
                String strSQL1;
                String row_guid;
                if (Objects.equals(strTableName, "TBFileInfo_new_change") || Objects.equals(strTableName, "TBFileInfo")) {
                    return;
                }
                row_guid = dataRows.get(0).getString("rowguid");
                if (!Objects.equals(strTableName, "tbrecordinfo_new")) {
                    strSQL1 = "delete from " + strTableName + " where RecordGuid = @param";
                } else {
                    strSQL1 = "delete from " + strTableName + " where row_guid = @param";
                }
                HashMap<String, Object> paramsMap = new HashMap<>();
                paramsMap.put("param", row_guid);
                fgMapper.executeSql(strSQL1, paramsMap);
            });
            return true;
        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));

            return false;
        }
    }

    private String Thread_SaveData(DataClass data) {
        String PROJECT_CODE = data.PROJECT_CODE;
        String UNIQUE_NUM = data.UNIQUE_NUM;
        String result;
        DataSet ds = data.dataSet;
        ds.remove("ExtensionData");
        try {
            DataTable status = fgMapper.getStatus(UNIQUE_NUM, ds.getTable("tbrecordinfo_new").get(0).getString("rowguid"), "1");

            //修改施工许可的总面积
            for (DataRow dataRow : ds.getTable("tbbuilderlicencemanage_new")) {
                dataRow.put("Area", dataRow.getString("allArea"));
            }

            DataTable sfinfo = fgMapper.getTableStructure("tbsfcorpinfo_new");
            String guid = ds.get("tbbuilderlicencemanage_new").get(0).getString("guid");
            for (DataRow item : ds.get("tbeconcorpinfo")) {
                DataRow row = new DataRow();
                row.put("CorpTypeNum", "1");
                row.put("CorpName", item.getString("econCorpName"));
                row.put("CorpCode", item.getString("econCorpCode"));
                row.put("CORPLEADER", item.getString("ECONCORPLEADER"));
                row.put("CORPLEADERCARDTYPE", item.getString("ECONCORPLEADERCARDTYPE"));
                row.put("CORPLEADERCARDNUM", item.getString("ECONCORPLEADERCARDNUM"));
                row.put("CorpLeaderPhone", item.getString("ECONCORPLEADERPHONE"));
                row.put("sgxkguid", guid);
                row.put("guid", item.getString("guid"));
                row.put("rowguid", item.getString("rowguid"));
                sfinfo.add(row);
            }
            for (DataRow item : ds.get("tbdesigncorpinfo")) {
                DataRow row = new DataRow();
                row.put("CorpTypeNum", "2");
                row.put("CorpName", item.getString("designCorpName"));
                row.put("CorpCode", item.getString("designCorpCode"));
                row.put("CORPLEADER", item.getString("DESIGNCORPLEADER"));
                row.put("CORPLEADERCARDTYPE", item.getString("DESIGNCORPLEADERCARDTYPE"));
                row.put("CORPLEADERCARDNUM", item.getString("DESIGNCORPLEADERCARDNUM"));
                row.put("CorpLeaderPhone", item.getString("DESIGNCORPLEADERPHONE"));
                row.put("sgxkguid", guid);
                row.put("guid", item.getString("guid"));
                row.put("rowguid", item.getString("rowguid"));
                sfinfo.add(row);
            }
            for (DataRow item : ds.get("tbconscorpinfo")) {
                DataRow row = new DataRow();
                row.put("CorpTypeNum", "3");
                row.put("CorpName", item.getString("consCorpName"));
                row.put("CorpCode", item.getString("consCorpCode"));
                row.put("CORPLEADER", item.getString("CONSCORPLEADER"));
                row.put("CORPLEADERCARDTYPE", item.getString("CONSCORPLEADERCARDTYPE"));
                row.put("CORPLEADERCARDNUM", item.getString("CONSCORPLEADERCARDNUM"));
                row.put("CorpLeaderPhone", item.getString("CONSCORPLEADERPHONE"));
                row.put("sgxkguid", guid);
                row.put("guid", item.getString("guid"));
                row.put("rowguid", item.getString("rowguid"));
                sfinfo.add(row);
            }

            for (DataRow item : ds.get("tbsupercorpinfo")) {
                DataRow row = new DataRow();
                row.put("CorpTypeNum", "4");
                row.put("CorpName", item.getString("superCorpName"));
                row.put("CorpCode", item.getString("superCorpCode"));
                row.put("CORPLEADER", item.getString("SUPERCORPLEADER"));
                row.put("CORPLEADERCARDTYPE", item.getString("SUPERCORPLEADERCARDTYPE"));
                row.put("CORPLEADERCARDNUM", item.getString("SUPERCORPLEADERCARDNUM"));
                row.put("CorpLeaderPhone", item.getString("SUPERCORPLEADERPHONE"));
                row.put("sgxkguid", guid);
                row.put("guid", item.getString("guid"));
                row.put("rowguid", item.getString("rowguid"));
                sfinfo.add(row);
            }

            for (DataRow item : ds.get("tbprojectfinishunitinfo_new")) {
                if (Objects.equals(item.getString("sgxkguid"), "")) {
                    item.put("sgxkguid", ds.getTable("tbbuilderlicencemanage_new").get(0).getString("guid"));
                }
            }
            String s = ds.get("tbbuilderlicencemanage_new").get(0).getString("BuildCorpCodeType");
            if (StringUtils.isNotBlank(s)) {
                for (DataRow item : ds.get("tbzcbcorpinfo")) {
                    DataRow row = new DataRow();
                    row.put("CorpTypeNum", "5");
                    row.put("CorpName", item.getString("zcbCorpName"));
                    row.put("CorpCode", item.getString("zcbCorpCode"));
                    row.put("CORPLEADER", item.getString("ZCBCORPLEADER"));
                    row.put("CORPLEADERCARDTYPE", item.getString("ZCBCORPLEADERCARDTYPE"));
                    row.put("CORPLEADERCARDNUM", item.getString("ZCBCORPLEADERCARDNUM"));
                    row.put("CorpLeaderPhone", item.getString("ZCBCORPLEADERPHONE"));
                    row.put("sgxkguid", guid);
                    row.put("guid", item.getString("guid"));
                    row.put("rowguid", item.getString("rowguid"));
                    sfinfo.add(row);
                }
                ds.remove("tbzcbcorpinfo");
            }
            sfinfo.setName("tbsfcorpinfo_new");
            ds.add(sfinfo);
            ds.remove("tbeconcorpinfo");
            ds.remove("tbdesigncorpinfo");
            ds.remove("tbconscorpinfo");
            ds.remove("tbsupercorpinfo");
            ds.getTable("tbrecordinfo_new").get(0).put("CreateTime", getNowTimeStr());
            //检查发改推来的数据是否有缺少的
            logger.error("开始--->ChickFileInfo" + new Date());
            result = ChickFileInfo(ds);
            logger.error("开始--->ChickFileInfo" + new Date());
            if (!StringUtils.isBlank(result)) {
                //有缺少的,要推一下退件过程,让企业重新补充数据
                this.PushingProcess("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                        ds.getTable("tbbuilderlicencemanage_new").get(0).getString("citynum"),
                        ds.getTable("tbbuilderlicencemanage_new").get(0).getString("rowguid"),
                        ds.getTable("tbbuilderlicencemanage_new").get(0).getString("guid"),
                        "", getNowTimeStr(), "330000", "接口自动退回", result,
                        "330000", "接口自动退回", "1", "", getNowTimeStr());
                return result;
            }

            //查询是否已经插入该办建记录,已上报,退回的状态可以删除再次添加,已审核的则不可以
            DataTable tbrecord = ds.getTable("tbrecordinfo_new");
            //这里的逻辑先保存附件再保存数据，有点难以理解，但是c#源码逻辑如此
            boolean bcfj = GetItemMaterialList_New(ds.getTable("TBFileInfo"), PROJECT_CODE, UNIQUE_NUM, tbrecord.get(0).getString("rowguid"), data);
            if (!bcfj) {
                //推送失败后要推办件过程,重新推
                this.PushingProcess("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                        ds.getTable("tbbuilderlicencemanage_new").get(0).getString("citynum"),
                        ds.getTable("tbbuilderlicencemanage_new").get(0).getString("rowguid"),
                        ds.getTable("tbbuilderlicencemanage_new").get(0).getString("guid"),
                        "", getNowTimeHHmmss(),
                        "330000", "接口自动退回", "获取附件或者保存附件失败", "330000", "接口自动退回", "1", "", getNowTimeStr());
                return "获取附件或者保存附件失败";
            }

            String state = null;
            //设置状态
            if (status != null && !status.isEmpty()) {
                state = status.get(0).getString("status");
            }
            //如果状态是空的，就说明数据库没有该项目数据
            if (StringUtils.isBlank(state)) {
                //保存数据
                boolean saveDataSet = SaveDataSet(ds, PROJECT_CODE, UNIQUE_NUM);
                //保存数据失败
                if (!saveDataSet) {
                    //推送失败后要推办件过程,重新推
                    this.PushingProcess("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                            ds.getTable("tbbuilderlicencemanage_new").get(0).getString("citynum"),
                            ds.getTable("tbbuilderlicencemanage_new").get(0).getString("rowguid"),
                            ds.getTable("tbbuilderlicencemanage_new").get(0).getString("guid"),
                            "", getNowTimeHHmmss(),
                            "330000", "接口自动退回", "数据入库时出现异常", "330000", "接口自动退回", "1", "", getNowTimeStr());
                    return "数据入库时出现异常";
                }
                //走到这里说明就是推送成功了。
                return "";
            }
            //如果是下面的状态就选择删掉之后再插入数据
            //在前面的判断中已经去掉104、106状态
            if (Objects.equals(state, "105") || Objects.equals(state, "102") || Objects.equals(state, "103")) {
                //执行删除然后插入
                if (!DeleteTable(ds)) {
                    //推送失败后要推办件过程,重新推
                    this.PushingProcess("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                            ds.getTable("tbbuilderlicencemanage_new").get(0).getString("citynum"),
                            ds.getTable("tbbuilderlicencemanage_new").get(0).getString("rowguid"),
                            ds.getTable("tbbuilderlicencemanage_new").get(0).getString("guid"),
                            "", getNowTimeHHmmss(),
                            "330000", "接口自动退回", "数据入库时删除原数据失败", "330000", "接口自动退回", "1", "", getNowTimeStr());
                    return "数据入库时删除原数据失败";
                }

                //保存数据
                boolean saveDataSet = SaveDataSet(ds, PROJECT_CODE, UNIQUE_NUM);
                if (saveDataSet) {
                    return "";
                }
                //推送失败后要推办件过程,重新推
                this.PushingProcess("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                        ds.getTable("tbbuilderlicencemanage_new").get(0).getString("citynum"),
                        ds.getTable("tbbuilderlicencemanage_new").get(0).getString("rowguid"),
                        ds.getTable("tbbuilderlicencemanage_new").get(0).getString("guid"),
                        "", getNowTimeHHmmss(),
                        "330000", "接口自动退回", "保存数据库的时候出现异常", "330000", "接口自动退回", "1", "", getNowTimeStr());
                return "保存数据库的时候出现异常";
            }

        } catch (Exception e) {
            logger.error(Arrays.toString(e.getStackTrace()));
            //推送失败后要推办件过程,重新推
            this.PushingProcess("ITEM_TB", PROJECT_CODE, UNIQUE_NUM,
                    ds.getTable("tbbuilderlicencemanage_new").get(0).getString("citynum"),
                    ds.getTable("tbbuilderlicencemanage_new").get(0).getString("rowguid"),
                    ds.getTable("tbbuilderlicencemanage_new").get(0).getString("guid"), "", getNowTimeHHmmss(),
                    "330000", "接口自动退回", "系统错误,请联系管理员", "330000", "接口自动退回", "1", "", getNowTimeStr());
            return "系统错误,请联系管理员";
        }

        return "该事项状态未在102、103、104、105、106中，请联系管理人员";
    }

    //修改表名,发改给我们的表名和我们的都不一样,都需要修改
    public DataSet changeTableName(DataSet ds) {
        DataSet newDs = new DataSet();
        AnnexMapping.FG_TABLE_NAME_MAP.forEach((k, v) -> {
            DataTable table = new DataTable();
            if (ds.get(k) != null) {
                table = ds.get(k);
            }
            table.setName(v);
            newDs.put(v, table);
        });
        return newDs;
    }

    @Override
    @Async
    public TempMessage fgNotice(String messageType, String projectCode, String uniqueNum, String exchangeTime) throws Exception {
        TempMessage ph = new TempMessage();
        DataSet ds = new DataSet();
        DataClass data;
        String dataFormFG;
        JSONObject vo;
        String voStr = null;
        //新申请
        if (messageType.trim().equals("3")) {
            //从发改那边获取数据
            logger.error("开始--->从发改 getBuildApproveInfo 接口获取数据" + new Date());
            dataFormFG = this.getDataFormFG(projectCode, uniqueNum, "1");
            logger.error("结束--->从发改 getBuildApproveInfo 接口获取数据" + new Date());
            vo = JSON.parseObject(dataFormFG);
            if (vo.isEmpty() || vo.getString("data") == null || "".equals(vo.getString("data"))) {
                ph.code = "0";
                ph.message = "错误来源:发改3.0;错误信息:调取的项目信息数据源为空!";
                return ph;
            }

            if (Objects.equals(vo.getString("code"), "0")) {
                ph.code = "0";
                ph.message = "错误来源:发改3.0;错误信息:" + vo.getString("message");
                return ph;
            }

            voStr = vo.getString("data");
            if (StringUtils.isAnyBlank(voStr)) {
                ph.code = "0";
                ph.message = "错误来源:发改3.0;包含项目信息的json字符串为空";
                return ph;
            }

            JSONObject jo = JSONObject.parseObject(voStr);
            jo.remove("ExtensionData");
            logger.error("开始--->修改表的结构" + new Date());
            //将String类型转化为Json
            ds = ChangeJson.changeVoToDataSet(voStr);
            //修改表结构
            ds = changeDataSetStructure(ds);
            //修改表名
            ds = changeTableName(ds);
            logger.error("结束--->修改表的结构" + new Date());
            DataTable tbrecordinfoNew = ds.get("tbrecordinfo_new");
            logger.error("开始--->从数据库获取Status" + new Date());
            DataTable list = fgMapper.getStatus(uniqueNum, tbrecordinfoNew.get(0).getString("rowguid"), "1");
            logger.error("结束--->从数据库获取Status" + new Date());
            if (list != null && !list.isEmpty()) {
                String status = list.get(0).getString("status");
                if (Objects.equals(status, "106") || Objects.equals(status, "104")) {
                    ph.code = "0";
                    ph.message = "该事项已受理或已审核无法再次入库";
                    return ph;
                }
            }
            //修改施工许可的总面积
            for (DataRow dataRow : ds.getTable("tbbuilderlicencemanage_new")) {
                dataRow.put("Area", dataRow.getString("allArea"));
            }
            if (ds.isEmpty() || ds.getTable("tbeconcorpinfo") == null || ds.getTable("tbdesigncorpinfo") == null || ds.getTable("tbconscorpinfo") == null || ds.getTable("tbsupercorpinfo") == null || ds.getTable("tbprojectfinishunitinfo_new") == null) {
                ph.code = "0";
                ph.message = "错误来源:发改3.0;四方主体信息或单体信息为空";
                return ph;
            }
            //设置局部变量,在c#代码中使用的是全局变量,但是在高并发的情况下,会出现问题。
            data = new DataClass(messageType, projectCode, uniqueNum, exchangeTime, voStr, ds);
            //保存数据
            String cs = Thread_SaveData(data);
            if (StringUtils.isBlank(cs)) {
                ph.code = "1";
                ph.message = "成功";
            } else {
                ph.code = "0";
                ph.message = cs;
            }

        } else if (messageType.trim().equals("5")) {
            //补交材料
            String rowGuid = fgMapper.getRowGuid(projectCode);
            if (StringUtils.isAnyBlank(rowGuid)) {
                ph.code = "0";
                ph.message = "错误来源:发改3.0;错误信息:未能根据[projectCode]查询到[RowGuid],无法进行下一步操作";
                return ph;
            }
            if (!GetItemMaterialList(projectCode, uniqueNum, rowGuid)) {
                ph.code = "0";
                ph.message = "错误来源:省施工许可;错误信息:获取事项申报材料失败";
                return ph;
            }
            ph.code = "1";
            ph.message = "获取事项申报材料成功";
            //变更
        } else if (messageType.trim().equals("8")) {
            try {
                dataFormFG = this.getDataFormFG(projectCode, uniqueNum, "2");
                vo = JSON.parseObject(dataFormFG);
                if (vo.isEmpty() || vo.getString("data") == null || "".equals(vo.getString("data"))) {
                    ph.code = "0";
                    ph.message = "错误来源:发改3.0;错误信息:调取的项目信息数据源为空!";
                    return ph;
                }

                if (Objects.equals(vo.getString("code"), "0")) {
                    ph.code = "0";
                    ph.message = "错误来源:发改3.0;错误信息:" + vo.getString("message");
                    return ph;
                }

                voStr = vo.getString("data");
                if (StringUtils.isAnyBlank(voStr)) {
                    ph.code = "0";
                    ph.message = "错误来源:发改3.0;包含项目信息的json字符串为空";
                    return ph;
                }

                JSONObject jo = JSONObject.parseObject(voStr);
                jo.remove("ExtensionData");
                ds = ChangeJson.changeVoToDataSet(voStr);
                //修改表结构
                ds = changeDataSetStructure(ds);
                ds = changeTableName(ds);
                DataTable tbrecordinfoNew = ds.getTable("tbrecordinfo_new");
                if (!(ds.isEmpty()
                        && ds.getTable("tbeconcorpinfo_new_change") != null
                        && ds.getTable("tbdesigncorpinfo_new_change") != null
                        || ds.getTable("tbconscorpinfo_new_change") != null
                        || ds.getTable("tbsupercorpinfo_new_change") != null
                        || ds.getTable("tbprojectfinishunitinfo_new_change") != null)) {
                    ph.code = "0";
                    ph.message = "错误来源:发改3.0变更;四方主体信息或单体信息为空";
                    return ph;
                }

                //修改施工许可的总面积
                for (DataRow dataRow : ds.getTable("tbbuilderlicencemanage_new_change")) {
                    dataRow.put("Area", dataRow.getString("allArea"));
                }
                DataTable list1 = fgMapper.getStatus(uniqueNum, tbrecordinfoNew.get(0).getString("rowguid"), "2");
                String state = list1.get(0).getString("status");
                DataTable list2 = fgMapper.getStatus(uniqueNum, tbrecordinfoNew.get(0).getString("rowguid"), "1");
                String states = list2.get(0).getString("status");
                if (Objects.equals(state, "6")) {
                    ph.code = "0";
                    ph.message = "变更该事项已受理或已审核无法再次入库";
                    return ph;
                }
                if (!Objects.equals(states, "104")) {
                    ph.code = "0";
                    ph.message = "新申请项目未办结";
                    return ph;
                }
                DataTable dtstate = fgMapper.getSRFromTbrecordInfoNew(uniqueNum, tbrecordinfoNew.get(0).getString("rowguid"), "2");
                if (dtstate != null || dtstate.isEmpty()) {
                    for (DataRow dataRow : dtstate) {
                        if ((Objects.equals(dataRow.getString("status"), "6") ||
                                Objects.equals(dataRow.getString("status"), "3")) &&
                                Objects.equals(dataRow.getString("rowguid"), tbrecordinfoNew.get(0).getString("rowguid"))) {
                            ph.code = "0";
                            ph.message = "变更该事项已受理或已审核无法再次入库";
                            return ph;
                        }
                    }
                }
                data = new DataClass(messageType, projectCode, uniqueNum, exchangeTime, voStr, ds);
                String cs = Thread_SaveData_Change(data);

                if (StringUtils.isBlank(cs)) {
                    ph.code = "1";
                    ph.message = "成功";
                } else {
                    ph.code = "0";
                    ph.message = cs;
                }

            } catch (Exception e) {
                ph.code = "0";
                ph.message = "系统错误,请重新提交";
            }
            //注销,延期,中止,恢复
        } else if (messageType.trim().equals("10") || messageType.trim().equals("11") || messageType.trim().equals("12") || messageType.trim().equals("13")) {
            String DyType = "";
            switch (messageType) {
                case "10":
                    DyType = "3";
                    break;
                case "11":
                    DyType = "4";
                    break;
                case "12":
                    DyType = "5";
                    break;
                case "13":
                    DyType = "6";
                    break;
            }

            dataFormFG = this.getDataFormFG(projectCode, uniqueNum, DyType);
            vo = JSON.parseObject(dataFormFG);
            if (vo.isEmpty() || vo.getString("data") == null || "".equals(vo.getString("data"))) {
                ph.code = "0";
                ph.message = "错误来源:发改3.0;错误信息:调取的项目信息数据源为空!";
                return ph;
            }

            if (Objects.equals(vo.getString("code"), "0")) {
                ph.code = "0";
                ph.message = "错误来源:发改3.0;错误信息:" + vo.getString("message");
                return ph;
            }

            voStr = vo.getString("data");
            if (StringUtils.isAnyBlank(voStr)) {
                ph.code = "0";
                ph.message = "错误来源:发改3.0;包含项目信息的json字符串为空";
                return ph;
            }

            JSONObject jo = JSONObject.parseObject(voStr);
            jo.remove("ExtensionData");
            ds = ChangeJson.changeVoToDataSet(voStr);
            //修改表结构
            ds = changeDataSetStructure(ds);
            ds = changeTableName(ds);
            DataTable tbrecordinfoNew = ds.getTable("tbrecordinfo_new");
            if (ds.isEmpty() || ds.getTable("tbeconcorpinfo_new_change") == null ||
                    ds.getTable("tbdesigncorpinfo_new_change") != null ||
                    ds.getTable("tbconscorpinfo_new_change") != null ||
                    ds.getTable("tbsupercorpinfo_new_change") != null ||
                    ds.getTable("tbprojectfinishunitinfo_new_change") != null) {
                ph.code = "0";
                ph.message = "错误来源:发改3.0变更;四方主体信息或单体信息为空";
                return ph;
            }
            DataTable list = fgMapper.getStatus(uniqueNum, tbrecordinfoNew.get(0).getString("rowguid"), "1");
            String state = list.get(0).getString("status");
            if (!"104".equals(state)) {
                ph.code = "0";
                ph.message = "注销项目未办结";
                return ph;
            }
            data = new DataClass(messageType, projectCode, uniqueNum, exchangeTime, voStr, ds);
            String cs = Thread_SaveData_zx(DyType, data);

            if (StringUtils.isBlank(cs)) {
                ph.code = "1";
                ph.message = "成功";
            } else {
                ph.code = "0";
                ph.message = cs;
            }

        } else if (messageType.trim().equals("14")) {
            //发改项目注销
            ph.code = "1";
            ph.message = "成功";
        } else {
            ph.code = "0";
            ph.message = "错误来源:省施工许可,在约定类型之外;";
        }

        return ph;
    }


    private DataSet changeDataSetStructure(DataSet oldDs) {
        DataSet re = new DataSet();
        re.put("projectInfoVO", oldDs.get("projectInfoVO"));

        re.put("projectRFVO", oldDs.get("projectRFVO"));

        re.put("builderLicenceManageOptionNewVO", oldDs.get("builderLicenceManageOptionNewVO"));

        re.put("contractRecordManageVO", oldDs.get("contractRecordManageVO"));

        re.put("builderLicenceManageVO", oldDs.get("builderLicenceManageVO"));

        re.put("projectXFVO", oldDs.get("projectXFVO"));

        re.put("recordinfoVO", oldDs.get("recordinfoVO"));

        //=====
        re.put("unitsfztrelVOList", new DataTable());
        //=====
        re.put("sgxkZbzVOList", new DataTable());
        //=====
        re.put("tenderInfoVOList", new DataTable());
        //=====
        re.put("projectChangeDetailVOList", new DataTable());

        //=====
        DataTable zcbcorpInfoVOList = oldDs.get("zcbcorpInfoVOList");
        if (!(zcbcorpInfoVOList == null || zcbcorpInfoVOList.isEmpty())) {
            String TbZcbcorpInfoVOStr = zcbcorpInfoVOList.get(0).get("TbZcbcorpInfoVO").toString();
            if (!(TbZcbcorpInfoVOStr.startsWith("[") && TbZcbcorpInfoVOStr.endsWith("]"))) {
                TbZcbcorpInfoVOStr = "[" + TbZcbcorpInfoVOStr + "]";
            }
            DataTable TbZcbcorpInfoTable = JSON.parseObject(TbZcbcorpInfoVOStr, DataTable.class);
            re.put("zcbcorpInfoVOList", TbZcbcorpInfoTable);
        }

        //====
        DataTable supercorpInfoVOList = oldDs.get("supercorpInfoVOList");
        if (!(supercorpInfoVOList == null || supercorpInfoVOList.isEmpty())) {
            String TbSupercorpInfoVOStr = supercorpInfoVOList.get(0).get("TbSupercorpInfoVO").toString();
            if (!(TbSupercorpInfoVOStr.startsWith("[") && TbSupercorpInfoVOStr.endsWith("]"))) {
                //将{"ak":"a","bk":"b"} 转化为 [{"ak":"a","bk":"b"}]
                TbSupercorpInfoVOStr = "[" + TbSupercorpInfoVOStr + "]";
            }
            DataTable TbSupercorpInfoVOTable = JSON.parseObject(TbSupercorpInfoVOStr, DataTable.class);
            re.put("supercorpInfoVOList", TbSupercorpInfoVOTable);
        }

        //====
        DataTable designcorpInfoVOList = oldDs.get("designcorpInfoVOList");
        if (!(designcorpInfoVOList == null || designcorpInfoVOList.isEmpty())) {
            String TbDesigncorpInfoVOStr = designcorpInfoVOList.get(0).get("TbDesigncorpInfoVO").toString();
            if (!(TbDesigncorpInfoVOStr.startsWith("[") && TbDesigncorpInfoVOStr.endsWith("]"))) {
                TbDesigncorpInfoVOStr = "[" + TbDesigncorpInfoVOStr + "]";
            }
            DataTable TbDesigncorpInfoTable = JSON.parseObject(TbDesigncorpInfoVOStr, DataTable.class);
            re.put("designcorpInfoVOList", TbDesigncorpInfoTable);
        }
        //=====
        DataTable econcorpInfoVOListTable = oldDs.get("econcorpInfoVOList");
        if (!(econcorpInfoVOListTable == null || econcorpInfoVOListTable.isEmpty())) {
            String TbEconcorpInfoVOStr = econcorpInfoVOListTable.get(0).get("TbEconcorpInfoVO").toString();
            if (!(TbEconcorpInfoVOStr.startsWith("[") && TbEconcorpInfoVOStr.endsWith("]"))) {
                TbEconcorpInfoVOStr = "[" + TbEconcorpInfoVOStr + "]";
            }
            DataTable TbEconcorpInfoVOTable = JSON.parseObject(TbEconcorpInfoVOStr, DataTable.class);
            re.put("econcorpInfoVOList", TbEconcorpInfoVOTable);
        }

        //=====
        DataTable dataTable = oldDs.get("projectFinishUnitInfoVOList");
        if (!(dataTable == null || dataTable.isEmpty())) {
            String sTemp = dataTable.get(0).get("TbProjectFinishUnitInfoVO").toString();
            if (!(sTemp.startsWith("[") && sTemp.endsWith("]"))) {
                //将{"ak":"a","bk":"b"} 转化为 [{"ak":"a","bk":"b"}]
                sTemp = "[" + sTemp + "]";
            }
            DataTable dataTable1 = JSON.parseObject(sTemp, DataTable.class);
            re.put("projectFinishUnitInfoVOList", dataTable1);
        }

        //=====
        DataTable conscorpInfoVOListTable = oldDs.get("conscorpInfoVOList");
        if (!(conscorpInfoVOListTable == null || conscorpInfoVOListTable.isEmpty())) {
            String tbConscorpInfoVOStr = conscorpInfoVOListTable.get(0).get("TbConscorpInfoVO").toString();
            if (!(tbConscorpInfoVOStr.startsWith("[") && tbConscorpInfoVOStr.endsWith("]"))) {
                tbConscorpInfoVOStr = "[" + tbConscorpInfoVOStr + "]";
            }
            DataTable tbConscorpInfoTable = JSON.parseObject(tbConscorpInfoVOStr, DataTable.class);
            re.put("conscorpInfoVOList", tbConscorpInfoTable);
        }

        //=====
        DataTable itemMaterialVOList = oldDs.get("itemMaterialVOList");
        if (!(itemMaterialVOList == null || itemMaterialVOList.isEmpty())) {
            String itemMaterialVOListStr = itemMaterialVOList.get(0).get("WebServiceItemMaterialSgxkVO").toString();
            if (!(itemMaterialVOListStr.startsWith("[") && itemMaterialVOListStr.endsWith("]"))) {
                itemMaterialVOListStr = "[" + itemMaterialVOListStr + "]";
            }
            DataTable itemMaterialVOListTable = JSON.parseObject(itemMaterialVOListStr, DataTable.class);
            re.put("itemMaterialVOList", itemMaterialVOListTable);
        }

        return re;
    }
}
