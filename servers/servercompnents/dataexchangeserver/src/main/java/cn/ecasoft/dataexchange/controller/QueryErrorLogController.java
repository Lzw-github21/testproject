package cn.ecasoft.dataexchange.controller;

import cn.ecasoft.basic.datatable.DataRow;
import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.configproperties.EcaSqlsConfig;
import cn.ecasoft.dataexchange.common.utils.Constants;
import cn.ecasoft.utils.DBhelper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * @author xuliukai
 * @since 2023/9/19 13:23
 */
@RestController
@RequestMapping(value = "/getErrorLog/jpdZbjvvqBlwAH",produces = "application/json;charset=utf-8")
public class QueryErrorLogController {

    @Resource
    private DBhelper dBhelper;
    @Resource
    private EcaSqlsConfig ecaSqlsConfig;

    @PostMapping("/QueryErrorLog")
    public DataRow logQuery(@RequestParam(value = "pkid") String pkid) throws Exception {
        HashMap<String, Object> queryParam = new HashMap<>();
        queryParam.put("pkid", pkid);
        ecaSqlsConfig.getMap().put("OOT1J8STmXfciD1XW4buEw", "select * from sys_log_service where pkid = @pkid");
        DataTable dataRows = dBhelper.QueryDataTable(Constants.SGXK_MYSQL_GUID, "OOT1J8STmXfciD1XW4buEw", queryParam);
        return dataRows.getFirst();
    }
}
