package cn.ecasoft.dataexchange.entity.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SysEcafiles {
    private int affixId;
    private String row_Guid;
    private String FILENAME;
    private String FILETYPE;
    private Long FILESIZE;
    private int CREATEUSERID;
    private Date CREATEDATE;
    private String VIRTUALPATH;
    private String ACTUALNAME;
    private String GROUPGUID;
    private String REMARK;
    private int ISCRYPTED;
    private int ISDELETED;
    private String UPLOADUSERID;
    private String UPLOADUSERNAME;
    private String FILEGUID;
    private String TO_ROW_GUID;
    private String BUCKETNAME;
    private String sign_status;
}