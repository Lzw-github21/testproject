package cn.ecasoft.dataexchange.entity.dto;

import cn.ecasoft.dataexchange.entity.db.TBBuilderLicenceUnitInfoChange;
import lombok.Getter;
import lombok.Setter;

/**
 * @author XuLiuKai
 */
@Setter
@Getter
public class TBBuilderLicenceUnitInfoChangeDto extends TBBuilderLicenceUnitInfoChange {
    private String Guid;
    private String rowguid;
    private String JDGCSUserName;
    private String JDGCSIdCard;
    private String JDGCSphone;
    private String ZLJDYUserName;
    private String ZLJDYidcard;
    private String ZLJDYphone;
    private String JDGCSTEL;
    private String ZLJDYTEL;
    private String NHLEVEL;
    private String UNITCODE1;
    private String UNITGuid;

}
