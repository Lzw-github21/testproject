package cn.ecasoft.dataexchange.mapper;

import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.common.utils.Constants;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @author xuliukai
 * @since 2023/10/31 13:27
 */
@Service
public class LoginZWWMapper extends CommonMapperByDBHelp {

    public DataTable getTbadminuserByUserid(String userid) {
        String sql = "select * from TBAdminUser where RecordGuid = @userid ";
        String sqlKey = "mKGT2QVdDfwZX";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("userid", userid);
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public DataTable getGovtPeopleTableByUserid(String userid) {
        String sql = "select * from TBGovtPeopleTable where Row_Guid = @userid";
        String sqlKey = "F5SKlSSGwroLynZhNUoJWLj4";
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("userid", userid);
        return super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public int insertGovPeople(HashMap paramsMap){
        String sql = "INSERT INTO tbGovtPeopleTable(Row_Guid,RecordGuid,mobilephone,loginname,title,username,sex,officephone,email,orderby) " +
                "VALUES(@Row_Guid,@RecordGuid,@mobilephone,@loginname,@title,@username,@sex,@officephone,@email,@orderby)";
        String sqlKey = "8XquLHhLM2d4";
        return super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, sql, sqlKey, paramsMap);
    }

    public int getTotalStaffCount(String st_id, String lageid){
        String sqlstring = "select count(0) as TOTAL from sys_staffdef where st_id >@st_id and st_id <@lageid";
        DataTable stId = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sqlstring, new HashMap<String, Object>() {{
            put("st_id", st_id);
            put("lageid",lageid);
        }});
        return stId.get(0).getInt("TOTAL");
    }
    public DataTable getStaffPassWord(String st_id, String lageid, int startline, int pagesize){
        String sqlstring = "select st_id,st_passwd from sys_staffdef where st_id >@st_id and st_id < @lageid order by st_id asc";
        DataTable passwords = super.executePageSqlForDataTable(Constants.SGXK_MYSQL_GUID, sqlstring, new HashMap<String, Object>() {{
            put("st_id", st_id);
            put("lageid", lageid);
        }}, startline, pagesize);

        return passwords;
    }
    public int updateStaffPassWord(String st_id,String st_password){
        String sqlstring = "update sys_staffdef set st_passwd = @st_password where st_id = @st_id";
        return super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, sqlstring, new HashMap<String, Object>() {{
            put("st_id", st_id);
            put("st_password", st_password);
        }});

    }

}
