package cn.ecasoft.dataexchange.services.impl;

import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.mapper.DockingMapper;
import cn.ecasoft.dataexchange.services.DockingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description TODO
 * @Date 2023/9/19 10:14
 * @Created by liang
 */
@Service
public class DockingServiceImpl implements DockingService {
    @Autowired
    private DockingMapper dockingMapper;
    @Override
    public RetMsgUtil<Object> getAttachedFiles(String rowGuid) {
        DataTable attachedFiles = dockingMapper.getAttachedFiles(rowGuid);
        if (attachedFiles.size()<=0){
            return RetMsgUtil.fail("该项目无关联附件");
        }
        return RetMsgUtil.ok(attachedFiles);
    }
}
