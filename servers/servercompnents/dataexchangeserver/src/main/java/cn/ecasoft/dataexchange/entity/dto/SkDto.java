package cn.ecasoft.dataexchange.entity.dto;

import lombok.Data;

/**
 * @author Dee
 * @date 2023/7/12
 * <p>Description:
 */
@Data
public class SkDto {
    private String isTsFG;
    private String rowGuid;
    private Integer cityNum;
}
