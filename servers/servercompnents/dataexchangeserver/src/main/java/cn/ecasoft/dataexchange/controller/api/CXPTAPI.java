package cn.ecasoft.dataexchange.controller.api;

import cn.ecasoft.dataexchange.common.utils.CXPTApiUrlMapping;
import cn.ecasoft.dataexchange.common.utils.HTTPRequestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author xuliukai
 * @Desc 现在由于 施工许可-诚信平台 相关的代码部署在诚信平台上，由于eca调用会出现跨域之类的问题
 * 所以需要写api调用诚信平台接口
 * @since 2023/9/19 9:43
 */
@RestController
@RequestMapping(value = "/sgxk/PXDdR3k2MzF", produces = "application/json;charset=utf-8")
public class CXPTAPI {

    @Resource
    private StringRedisTemplate stringRedisTemplate;


    private String getUrl(String interfaceName) {
        String cof = stringRedisTemplate.opsForValue().get(CXPTApiUrlMapping.ENV_CONFIG);
        if (StringUtils.isBlank(cof)) {
            //初始化为开发环境，如果需要访问正式环境的话，需要手动切到正式环境
            stringRedisTemplate.opsForValue().set(CXPTApiUrlMapping.ENV_CONFIG, "dev");
        }
        if ("pro".equals(cof)) {
            return CXPTApiUrlMapping.PRODUCE_CXPT_URL.get(interfaceName);
        }
        return CXPTApiUrlMapping.DEVELOP_CXPT_URL.get(interfaceName);
    }

    @PostMapping(value = "/testConnect")
    public String testConnect(@RequestParam("dto") String dto) {
        return dto;
    }

    @PostMapping(value = "/ReadPersonnelLeaveStatus")
    public String ReadPersonnelLeaveStatus(String CorpCode, String strIDCard, String strErrMsg) throws Exception {
        MultiValueMap<String, Object> formDataParam = new LinkedMultiValueMap<>();
        formDataParam.add("CorpCode", CorpCode);
        formDataParam.add("strIDCard", strIDCard);
        formDataParam.add("strErrMsg", strErrMsg);
        return HTTPRequestUtils.requestToCXPT(this.getUrl("ReadPersonnelLeaveStatus"), formDataParam);
    }

    @PostMapping(value = "/getxmnum")
    public String getxmnum(String xzqhnum, String getdate, String type, String xmfl) throws Exception {
        MultiValueMap<String, Object> formDataParam = new LinkedMultiValueMap<>();
        formDataParam.add("xzqhnum", xzqhnum);
        formDataParam.add("getdate", getdate);
        formDataParam.add("type", type);
        formDataParam.add("xmfl", xmfl);
        return HTTPRequestUtils.requestToCXPT(this.getUrl("getxmnum"), formDataParam);
    }

    /**
     *
     * @param dto 这里之所以用formdata去接收数据而不是用json去接受数据，主要原因是为了配合eca，eca没有封装发送json的请求工具类，所以改用formdata
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/InsertTSSKBGInfo_New")
    public String InsertTSSKBGInfo_New(@RequestParam("dto") String dto) throws Exception {
        return HTTPRequestUtils.requestToCXPTByBody(this.getUrl("InsertTSSKBGInfo_New"), dto);
    }

    @PostMapping(value = "/InsertSKByQZ_Manager_New")
    public String InsertSKByQZ_Manager_New(@RequestParam("dto") String dto) throws Exception {
        return HTTPRequestUtils.requestToCXPTByBody(this.getUrl("InsertSKByQZ_Manager_New"), dto);
    }

    //注销
    @PostMapping(value = "/CancelSgxk_QZ")
    public String CancelSgxk_QZ(@RequestParam("dto") String dto) throws Exception {
        return HTTPRequestUtils.requestToCXPTByBody(this.getUrl("CancelSgxk_QZ"), dto);
    }

    @PostMapping(value = "/UploadImageInfo_QZ")
    public String UploadImageInfo_QZ(@RequestParam("dto") String dto,@RequestParam("strYeWuID") String strYeWuID) throws Exception {
        MultiValueMap<String, Object> formDataParam = new LinkedMultiValueMap<>();
        formDataParam.add("dto", dto);
        formDataParam.add("strYeWuID", strYeWuID);
        return HTTPRequestUtils.requestToCXPT(this.getUrl("UploadImageInfo_QZ"), formDataParam);
    }
}
