package cn.ecasoft.dataexchange.common.Ret;

public enum  State {
    OPERATE_SUCCESS(0, "请求成功",true),
    OPERATION_FAILED(500, "操作失败",false),
    BAD_PARAM(301, "参数错误", false),
    PWD_ERROR(302, "用户名或密码错误!", false),
    LICENCE_REPEAT(303, "施工许可证存在重复！", false),


    ;
    private int code;
    private String msg;
    private boolean success;

    private State(int number, String description,boolean bo) {
        this.code = number;
        this.msg = description;
        this.success = bo;
    }

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

    public boolean getSuccess() {
        return this.success;
    }
}
