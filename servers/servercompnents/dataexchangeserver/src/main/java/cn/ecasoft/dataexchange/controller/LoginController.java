package cn.ecasoft.dataexchange.controller;

import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.services.LoginService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author xuliukai
 * @since 2023/10/30 13:39
 * @Desc 登录内容接口
 */
@RestController
@RequestMapping(value = "/login/9gRmDNB6", produces = "application/json;charset=utf-8")
public class LoginController {

    @Resource
    LoginService loginService;

    /**
     * 政务服务网单点登录
     * @param ticket
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/loginZWW")
    public RetMsgUtil<Object> loginZWW(@RequestParam("ticket") String ticket) throws Exception {
        if (StringUtils.isBlank(ticket)) {
            return RetMsgUtil.failFromBadParam();
        }

        return loginService.loginZWW(ticket);
    }

    /**
     * 用户迁移时密码加密方式修改
     * @param st_id
     * @return
     * @throws Exception
     */
    @GetMapping("/passwordMigration")
    public RetMsgUtil<Object> passwordMigration(@RequestParam("st_id") String st_id,@RequestParam("lageid") String lageid) {
        return loginService.PasswordMigration(st_id, lageid);
    }

}
