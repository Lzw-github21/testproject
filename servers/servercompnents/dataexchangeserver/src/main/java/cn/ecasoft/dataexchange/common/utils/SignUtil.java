package cn.ecasoft.dataexchange.common.utils;

import org.apache.commons.codec.binary.Base64;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class SignUtil {
    /**
     * 生成签名
     * 使用md5加密，再base64摘要
     * @param value 明文
     * @return 签名
     * @throws Exception
     */
    public static String getSign(String value) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(value.getBytes(StandardCharsets.UTF_8));
        return new String(Base64.encodeBase64(md.digest()), StandardCharsets.UTF_8);
    }


}