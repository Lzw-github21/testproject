package cn.ecasoft.dataexchange.services;

import cn.ecasoft.dataexchange.entity.model.TempMessage;

/**
 * @author Dee
 * @date 2023/7/10
 * <p>Description:
 */
public interface FaGaiService {

    TempMessage fgNotice(String messageType, String projectCode, String uniqueNum, String exchangeTime) throws Exception;

    boolean PushingProcess(String deal_state, String prjcode, String unique_num, String citynum, String rowguid,
                           String sgxkguid, String BuilderLicenceNum, String OpentionId, String deal_deptid,
                           String deal_deptname, String deal_opinion, String deal_userid, String deal_username,
                           String reply_result, String upload_date, String deal_time);

    boolean PushingProcess_Change(String deal_state, String prjcode, String unique_num, String citynum,
                                  String rowguid, String sgxkguid, String BuilderLicenceNum, String OpentionId,
                                  String deal_deptid, String deal_deptname, String deal_opinion, String deal_userid,
                                  String deal_username, String reply_result);


    boolean PushingProcess_zx(String deal_state, String prjcode, String unique_num,
                              String sgxkguid, String BuilderLicenceNum, String OpentionId, String deal_deptid,
                              String deal_deptname, String deal_opinion, String deal_userid, String deal_username,
                              String reply_result, String operate_type);

}
