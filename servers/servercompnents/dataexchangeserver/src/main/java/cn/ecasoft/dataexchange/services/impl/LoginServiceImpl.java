package cn.ecasoft.dataexchange.services.impl;

import cn.ecasoft.basic.JsonData;
import cn.ecasoft.basic.datatable.DataRow;
import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.common.JsUtil;
import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.mapper.LoginZWWMapper;
import cn.ecasoft.dataexchange.services.LoginService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

/**
 * @author xuliukai
 * @since 2023/10/30 17:24
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Resource
    private LoginZWWMapper loginZWWMapper;
    private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

    //政务服务网单点登录服务
    private static final String ZWW_URL = "http://10.145.12.71/sgxkloginservice/ClientDataService/LoginService.asmx";
    //测试
//    private static final String URL = "http://117.107.138.120/sgxkwebservice/ClientDataService/LoginService.asmx";

    private String getNowTimeStr() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        return now.format(formatter);
    }


    private String GetMd5String(String timeDate) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] result = md.digest(("jzscjgycxxx" + "cVmrJfVV" + timeDate).getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for (byte b : result) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public String ValidationResult(String ticketValidationUrl, String ticket, String timeDate, String md5Sign) {
        //type=0是浙江省施工许可，1为浙江外省
        //传OPID是验证是否已经上传过该条项目
        try {
            String postData = ticket + "&servicecode=jzscjgycxxx&time=" + timeDate + "&sign=" + md5Sign;
            HttpURLConnection webrequest;
            URL url = new URL(ticketValidationUrl + postData);
            webrequest = (HttpURLConnection) url.openConnection();
            webrequest.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(webrequest.getInputStream()));
            return in.readLine();
        } catch (Exception ex) {
            return "";
        }
    }


    @Override
    public RetMsgUtil<Object> loginZWW(String ticket) throws Exception {
        logger.info("进入单点登录政务网，ticket----> " + ticket);
        //调用的接口服务url基础地址，后面需手动机上方法名
        String serverUrl = "https://gmuser.zjzwfw.gov.cn/idm/servlet/auth/";
        String timeDate = getNowTimeStr();
        //签名字符串加密
        String md5Sign = GetMd5String(timeDate);
        String tokenData;
        JsonData result = new JsonData();
        String jsonData = this.ValidationResult(serverUrl + "ticketValidation", "?ticket=" + ticket, timeDate, md5Sign);
//        jsonData 返回示例:{"uid":"ff8080812c499597012c49e228010048","result":"0","token":"bb4a9da360ea21010160fc823c5a5739-token","errmsg":"成功"}
        if (StringUtils.isBlank(jsonData) || !jsonData.contains("成功")) {
            logger.error("获取token失败--->ticket: " + ticket);
            return RetMsgUtil.fail("获取token失败");
        }

        JSONObject jsonObject = JSON.parseObject(jsonData);
        tokenData = this.ValidationResult(serverUrl + "getUserInfo", "@token=" + jsonObject.get("token"), timeDate, md5Sign);
//        tokenData 返回示例:{"result":"0","serialnum":"20180116132025460416","errmsg":"成功","user":{"uid":"ff8080812c499597012c49e228010049","sex":1,"userorgs":[{"uid":"ff8080812c499597012c49e228010048","positionids":"","oid":"ff8080815d551320015d598b27ad007f","orderby":26808,"jobids":""}],"mobilephone_":"135****3630","loginname_":"h********c","username_":"技*持","loginname":"hzyh2.zszc","mobilephone":"13588843630","username":"技术支持","email":"756129428@qq.com","showname":"135****3630","email_":"75******8@qq.com"}}
        if (StringUtils.isNotBlank(tokenData) && tokenData.contains("成功")) {
            JSONObject obj = JSONObject.parseObject(tokenData);
            JSONObject user = (JSONObject) obj.get("user");
            String uid = user.getString("uid");
            DataTable dt = loginZWWMapper.getTbadminuserByUserid(uid);

            JSONObject userorgs = (JSONObject) user.get("userorgs");
            DataTable dt1 = loginZWWMapper.getGovtPeopleTableByUserid(userorgs.getString("uid"));

            if (dt1 != null && !dt1.isEmpty()) {
                HashMap<String, Object> parameter = new HashMap<>();
                parameter.put("Row_Guid", userorgs.getString("uid"));//主键
                parameter.put("RecordGuid", userorgs.getString("uid"));//组织主键
                parameter.put("mobilephone", user.getString("mobilephone"));//名称
                parameter.put("loginname", user.getString("loginname"));//父主键
                parameter.put("title", "");
                parameter.put("username", user.getString("username"));//组织类型
                parameter.put("sex", user.getString("sex"));//组织的唯一编码
                parameter.put("officephone", user.getString("mobilephone"));//电话
                parameter.put("email", user.getString("email"));//邮箱
                parameter.put("orderby", userorgs.getString("orderby"));//排序
                loginZWWMapper.insertGovPeople(parameter);
            }

            return RetMsgUtil.ok(dt.getFirst());
        }

        return RetMsgUtil.fail("从政务服务网获取数据失败，请联系系统管理员进行授权后方可登录！");
    }

    public RetMsgUtil<Object> PasswordMigration(String st_id, String lageid) {
        int total = loginZWWMapper.getTotalStaffCount(st_id, lageid);
        int pageSize = 1000;
        Integer pages = total / pageSize;
        for (int i = 0; i < pages + 1; i++) {
            DataTable dataRows = loginZWWMapper.getStaffPassWord(st_id, lageid, i * pageSize, pageSize);
            for (DataRow x:dataRows) {
                String s = "";
                try {
                    s = JsUtil.DES_Decrypt(x.getString("ST_PASSWD"), "consmkey", new byte[]{0x12, 0x34, 0x56, 0x78, (byte) 0x90, (byte) 0xab, (byte) 0xcd, (byte) 0xef});
                } catch (Exception e) {
                    logger.error("解密失败:{" + x.getString("ST_ID") + "}----->" + e.getMessage());
                    loginZWWMapper.updateStaffPassWord(x.getString("ST_ID"), "//error:" + x.getString("ST_PASSWD"));
                    continue;
                }
                String finalS = s;
                try {
                    loginZWWMapper.updateStaffPassWord(x.getString("ST_ID"), JsUtil.getsign(finalS));
                } catch (Exception e) {
                    loginZWWMapper.updateStaffPassWord(x.getString("ST_ID"), "//error:" + x.getString("ST_PASSWD"));
                }
            }
        }
        return new RetMsgUtil<>().ok();
    }
}
