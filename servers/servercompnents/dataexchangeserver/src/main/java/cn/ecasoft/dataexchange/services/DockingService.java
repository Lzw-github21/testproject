package cn.ecasoft.dataexchange.services;

import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;

/**
 * @Description TODO
 * @Date 2023/9/19 10:13
 * @Created by liang
 */
public interface DockingService {
    RetMsgUtil<Object> getAttachedFiles(String rowGuid);
}
