package cn.ecasoft.dataexchange.common.mms.util;


import cn.ecasoft.SpringContextUtil;
import cn.ecasoft.basic.JsonData;
import cn.ecasoft.basic.UserInfo;
import cn.ecasoft.redis.FastJson2JsonRedisSerializer;
import cn.ecasoft.redis.RedisService;
import cn.ecasoft.utils.EcaConfig;
import cn.ecasoft.utils.StringUtils;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Encoder;

import java.util.*;

/**
 * @Description： 生成token类
 * @Date：Created in 13:47 2019/10/30
 * @Modified By：
 */
@Component
public class TokenHelper {
    //密钥 -- 根据实际项目，这里可以做成配置

    public final int TIMEJWT = 60 * 1000;
    public final int TIMEREDIS = 60;
    @Value("${authTimeOut.codeTimeOut:5}")
    public Integer codeTimeOut;
    @Value("${authTimeOut.tokenTimeOut:60}")
    public Integer tokenTimeOut;
    @Value("${authTimeOut.multiaddressUsing:false}")
    public boolean multiaddressUsing;

    @Autowired
    EcaConfig ecaConfig;

    final BASE64Encoder encoder = new BASE64Encoder();
//    final BASE64Decoder decoder = new BASE64Decoder();

    /**
     * 创建token
     *
     * @param userInfo
     * @param appKey
     * @return
     * @throws Exception
     */
    public String GetAuthToken(UserInfo userInfo, String appKey) throws Exception {
        RedisService redisService = SpringContextUtil.getBean(RedisService.class);
        FastJson2JsonRedisSerializer<UserInfo> userInfoFastJson2JsonRedisSerializer = new FastJson2JsonRedisSerializer<>(UserInfo.class);
        //创建token
        HashMap<String, Object> claims = new HashMap<>();
        String sessionId = encoder.encode((String.valueOf(userInfo.getUserId())+appKey).getBytes("UTF-8")) + System.currentTimeMillis();
        String LimitRoleIds = ecaConfig.GetGroupConfig("Server", "LimitRoleIds");
        Boolean IsLimitRoleId=false;
        if(StringUtils.isNotBlank(LimitRoleIds) && multiaddressUsing){
            List<String> limitRoleIds = Arrays.asList(LimitRoleIds.split(","));
            Set<String> roleIds = userInfo.getRoles().keySet();
            for (String roleId : roleIds) {
                if(limitRoleIds.contains(roleId)){
                    IsLimitRoleId = true ;
                    break;
                }
            }
        }
        //如果为设置多人登录，登录人包含不让登录的角色，也不让登录（设置该人的sessionId 不加时间戳）
        if ((!multiaddressUsing)||IsLimitRoleId) {
            sessionId = encoder.encode((String.valueOf(userInfo.getUserId())+appKey).getBytes("UTF-8"));
        }
        //将用户信息放入redis
        long time = System.currentTimeMillis();
        claims.put("sessionId", sessionId);
        claims.put("signInTime", String.valueOf(time));
        String token = JwtUtil.createJWT(UUID.randomUUID().toString(), "ECA", claims, TIMEJWT * 7 * 24 * 60);
        userInfo.setNowTime(String.valueOf(time));
        userInfo.setRefreshTime(String.valueOf(time));
        redisService.set(sessionId, JSON.toJSON(userInfo), TIMEREDIS * tokenTimeOut);
        return token;
    }

    /**
     * 创建code
     *
     * @param access_token
     * @return
     */
    public String CreateCode(String access_token) {
        String code = UUID.randomUUID().toString();
        code = code.replace("-", "");
        //五分钟
        RedisService redisService = SpringContextUtil.getBean(RedisService.class);
        redisService.set(code, access_token, codeTimeOut * TIMEREDIS);
        return code;
    }
}
