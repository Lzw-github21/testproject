package cn.ecasoft.dataexchange.services;

import cn.ecasoft.basic.JsonData;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 李志威
 * @Description
 * @date 2023/8/24
 */
public interface StaffExtendService {
    /**
     * 查询子账户列表
     */
    JsonData getAccountList(HttpServletRequest request) throws Exception;
    /**
     * 通过部门名称获取部门下人员列表
     */
    JsonData getAccountNameList( String countyNum,HttpServletRequest request) throws Exception;
    /**
     * 通过人员id查询子账号详情
     */
    JsonData getAccountByUserId(String userId) throws Exception;
    /**
     * 创建子账户
     */
    JsonData createChildAccount(String stName,String stLoginName,String ableMoudles,String provinceNum,String cityNum,String password,
                                String areaNum,String idCard,String mobileTel,String userType,String isAreaAdmin,String organName,String telephone,String roleTypeNum, HttpServletRequest request) throws Exception;
    /**
     * 修改子账户
     */
    JsonData modifyChildAccount(String stName,String userId,String password, String ableMoudles, String mobileTel,
                                String idCard,String cityNum,String areaNum, String deptName,
                                String isAreaAdmin,String telephone,String roleTypeNum,HttpServletRequest request) throws Exception;
    /**
     * 删除子用户
     */
    JsonData deleteChildAccount(String userId, HttpServletRequest request) throws Exception;
    /**
     * 添加子用户标签
     */
    /**
     * 生成子账号登录用户名
     */
    JsonData getLoginName(String classID) throws Exception;
    /**
     * 获取系统部门列表
     */
    JsonData getDepExtendtList(String roleNum,HttpServletRequest request) throws Exception;
    /**
     * 通过userId禁用账号登录
     */
    JsonData disableAccount(String userId,String status, HttpServletRequest request) throws Exception;
    /**
     * 账号过滤
     */
    JsonData accountFilter(String condition, HttpServletRequest request) throws Exception;
    /**
     * 通过userId查看账号的禁用状态
     */
    JsonData selectDisable(String userId) throws Exception;








}
