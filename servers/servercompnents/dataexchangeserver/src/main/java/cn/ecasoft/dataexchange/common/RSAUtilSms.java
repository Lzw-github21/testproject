package cn.ecasoft.dataexchange.common;

import cn.ecasoft.basic.JsonData;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <br>Title:12329短信平台调用demo
 * <br>Description:12329短信平台调用demo
 */
public class RSAUtilSms {
	//政务V3云平台调用地址，正式应用建议放在配置文件中
    private final static String SMS12329URL ="http://10.142.4.191:9316";
	
	private final static String KEY_RSA = "RSA";//定义加密方式
    private final static String KEY_RSA_SIGNATURE = "MD5withRSA";//定义签名算法
    private final static String KEY_RSA_PUBLICKEY = "RSAPublicKey";//定义公钥算法
    private final static String KEY_RSA_PRIVATEKEY = "RSAPrivateKey";//定义私钥算法
    private static final int MAX_ENCRYPT_BLOCK = 117;//RSA最大加密明文大小
    private static final int MAX_DECRYPT_BLOCK = 128;//RSA最大解密密文大小
    
    //参数加密秘钥
    private final static String publicKeyStrSms="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDj6Lgm27YYh6e7ce4vQuj3W2id5DAg7JmTZJnub9SGaI8etj8ylzJd1VmCCkojwuNF3X+8IEIp5Zkrw+o35LqemWg9xckw8P8lFtFT0AvmwdWaowG1jKuGXGtoOUbMqBJxT4qfN0hrNTKpLVdo1RtdfF8kUjymawFpTJ7gyYbLswIDAQAB";
	
    /**
     * <br>Description:调用测试函数
     */
	public static void main(String[] args) {
		//测试时，先修改为自己的手机号
		//JsonData returnJSON = RSAUtilSms.sendsms("18667196427","建筑业监管平台提醒：截止当前时间，您的x号台风受灾情况未上报，请及时填报！");
		//System.out.println(returnJSON.toString());
    }
	
    /**
     * <br>Description:调用主函数
     */
	public static JsonData sendsms(String smsUrl,String sjhm,String dxnr){
		JsonData returnJSON = new JsonData();
		try {
            JsonData smsJson = new JsonData();
			smsJson.put("sjhm", sjhm);
			smsJson.put("dxnr",dxnr);
	        String miwen = encryptByPublicKey(publicKeyStrSms,smsJson.toString());
	        List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();    	
	    	list.add(new BasicNameValuePair("user", "JZYJGSMS"));//固定参数：浙江省建筑业监管分析平台（JZYJGSMS）
	    	list.add(new BasicNameValuePair("para", miwen));
	    	String returnStr=RSAUtilSms.requestByPostMethod(list, smsUrl + "/nbsjgx/PostSendSmsServlet");
	    	returnJSON = JsonData.fromString(returnStr);
		} catch (Exception ex) {
        	ex.printStackTrace();
        	returnJSON.put("fsjg","0");
        	returnJSON.put("msg","系统异常，短信发送失败，请稍后再试。");
        }
		return returnJSON;
	}
	
	/**
	 * <br>Description:初始方法
	 */
    public RSAUtilSms() {
    }

	/**
	 * <br>Description:创建密钥
	 *  @return HashMap 公钥、私钥对
	 */
    public static Map<String, Object> generateKey() {
        Map<String, Object> map = null;
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance(KEY_RSA);
            generator.initialize(1024);
            KeyPair keyPair = generator.generateKeyPair();
            RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();// 公钥
            RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();// 私钥
            //将密钥封装为map
            map = new HashMap(2);
            map.put(KEY_RSA_PUBLICKEY, publicKey);
            map.put(KEY_RSA_PRIVATEKEY, privateKey);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        return map;
    }
	/**
	 * <br>Description:用私钥对信息生成数字签名
	 * @param data       加密数据
     * @param privateKey 私钥
	 * @return String 签名信息
	 */
    public static String signPrivateKey(String privateKey, byte[] data) {
        String str = "";
        try {
            // 解密由base64编码的私钥
            byte[] bytes = decryptBase64(privateKey);
            // 构造PKCS8EncodedKeySpec对象
            PKCS8EncodedKeySpec pkcs = new PKCS8EncodedKeySpec(bytes);
            // 指定的加密算法
            KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
            // 取私钥对象
            PrivateKey key = factory.generatePrivate(pkcs);
            // 用私钥对信息生成数字签名
            Signature signature = Signature.getInstance(KEY_RSA_SIGNATURE);
            signature.initSign(key);
            signature.update(data);
            str = encryptBase64(signature.sign());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return str;
    }

	/**
	 * <br>Description:用私钥对信息生成数字签名
	 * @param data       加密数据字符串
     * @param privateKey 私钥
	 * @return String 签名信息
	 */
    public static String signPrivateKey(String privateKey, String dataStr) {
        String str = "";
        try {
            byte[] data = dataStr.getBytes("UTF-8");
            return signPrivateKey(privateKey, data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	/**
	 * <br>Description:用公钥校验数字签名
	 * @param data      加密数据
     * @param publicKey 公钥
     * @param sign      数字签名
	 * @return 校验成功返回true，失败返回false
	 */
    public static boolean verifyPublicKey(String publicKey, byte[] data, String sign) {
        boolean flag = false;
        try {
            // 解密由base64编码的公钥
            byte[] bytes = decryptBase64(publicKey);
            // 构造X509EncodedKeySpec对象
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
            // 指定的加密算法
            KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
            // 取公钥对象
            PublicKey key = factory.generatePublic(keySpec);
            // 用公钥验证数字签名
            Signature signature = Signature.getInstance(KEY_RSA_SIGNATURE);
            signature.initVerify(key);
            signature.update(data);
            flag = signature.verify(decryptBase64(sign));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return flag;
    }

	/**
	 * <br>Description:用公钥校验数字签名
	 * @param data      加密数据
     * @param publicKey 公钥
     * @param sign      数字签名
	 * @return 校验成功返回true，失败返回false
	 */
    public static boolean verifyPublicKey(String publicKey, String dataStr, String sign) {
        try {
            byte[] data = dataStr.getBytes("UTF-8");
            return verifyPublicKey(publicKey, data, sign);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

	/**
	 * <br>Description:用对方的公钥加密
     * @param key  公钥
     * @param data 待加密数据
	 * @return 加密后的数据
	 */
    public static byte[] encryptByPublicKey(String key, byte[] data) {
        try {
            // 获取公钥字符串时,进行了encryptBase64操作,因此此处需对公钥钥解密
            byte[] bytes = decryptBase64(key);
            // 取得公钥
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
            KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
            PublicKey publicKey = factory.generatePublic(keySpec);
            // 对数据加密
            Cipher cipher = Cipher.getInstance(factory.getAlgorithm());
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            int inputLen = data.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段加密
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                    cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(data, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_ENCRYPT_BLOCK;
            }
            byte[] encryptedData = out.toByteArray();
            out.close();
            return encryptedData;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

	/**
	 * <br>Description:用对方的公钥加密
     * @param keyStr  公钥
     * @param data 待加密数据
	 * @return 加密后的数据
	 */
    public static String encryptByPublicKey(String keyStr, String dataStr) {
        try {
            byte[] result = encryptByPublicKey(keyStr, dataStr.getBytes("UTF-8"));
            return encryptBase64(result);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

	/**
	 * <br>Description:用自己的私钥解密
     * @param data 加密数据
     * @param keyStr  私钥
	 * @return 加密后的数据
	 */
    public static byte[] decryptByPrivateKey(String keyStr, byte[] data) {
        try {
            // 获取私钥字符串时,进行了encryptBase64操作,因此此处需对私钥解密
            byte[] bytes = decryptBase64(keyStr);
            // 取得私钥
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
            KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
            PrivateKey privateKey = factory.generatePrivate(keySpec);
            // 对数据解密
            Cipher cipher = Cipher.getInstance(factory.getAlgorithm());
            cipher.init(Cipher.DECRYPT_MODE, privateKey);

            int inputLen = data.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段解密
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                    cache = cipher
                            .doFinal(data, offSet, MAX_DECRYPT_BLOCK);
                } else {
                    cache = cipher
                            .doFinal(data, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_DECRYPT_BLOCK;
            }
            byte[] decryptedData = out.toByteArray();
            out.close();
            return decryptedData;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
	/**
	 * <br>Description:用自己的私钥解密
     * @param data 加密数据
     * @param keyStr  私钥
	 * @return 加密后的数据
	 */
    public static String decryptByPrivateKey(String keyStr, String dataStr) {
        try {
            byte[] result = decryptByPrivateKey(keyStr, decryptBase64(dataStr));
            return new String(result,"UTF-8");
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

	/**
	 * <br>Description:用私钥加密
     * @param data 加密数据
     * @param keyStr  私钥
	 * @return 加密后的数据
	 */
    public static byte[] encryptByPrivateKey(String keyStr, byte[] data) {
        try {
            byte[] bytes = decryptBase64(keyStr);
            // 取得私钥
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
            KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
            PrivateKey privateKey = factory.generatePrivate(keySpec);
            // 对数据加密
            Cipher cipher = Cipher.getInstance(factory.getAlgorithm());
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            int inputLen = data.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段加密
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                    cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(data, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_ENCRYPT_BLOCK;
            }
            byte[] encryptedData = out.toByteArray();
            out.close();
            return encryptedData;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

	/**
	 * <br>Description:用私钥加密
     * @param data 加密数据
     * @param keyStr  私钥
	 * @return 加密后的数据
	 */
    public static String encryptByPrivateKey(String key, String dataStr) {
        try {
            byte[] result = encryptByPrivateKey(key, dataStr.getBytes("UTF-8"));
            return encryptBase64(result);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
 
	/**
	 * <br>Description:用公钥解密
     * @param data 加密数据
     * @param keyStr  公钥
	 * @return 解密后的数据
	 */
    public static byte[] decryptByPublicKey(String keyStr, byte[] data) {
        try {
            // 对公钥解密
            byte[] bytes = decryptBase64(keyStr);
            // 取得公钥
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
            KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
            PublicKey publicKey = factory.generatePublic(keySpec);
            // 对数据解密
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            
            cipher.init(Cipher.DECRYPT_MODE, publicKey);

            int inputLen = data.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段解密
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                    cache = cipher.doFinal(data, offSet, MAX_DECRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(data, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_DECRYPT_BLOCK;
            }
            byte[] decryptedData = out.toByteArray();
            out.close();
            return decryptedData;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

	/**
	 * <br>Description:用公钥解密
     * @param data 加密数据
     * @param keyStr  公钥
	 * @return 解密后的数据
	 */
    public static String decryptByPublicKey(String keyStr, String dataStr) {
        try {
            byte[] result = decryptByPublicKey(keyStr, decryptBase64(dataStr));
            return new String(result,"UTF-8");
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

	/**
	 * <br>Description:获取公钥
     * @param Map 公、私秘钥对
	 * @return 公钥
	 */
    public static String getPublicKey(Map<String, Object> map) {
        String str = "";
        try {
            Key key = (Key) map.get(KEY_RSA_PUBLICKEY);
            str = encryptBase64(key.getEncoded());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return str;
    }

	/**
	 * <br>Description:获取私钥
     * @param Map 公、私秘钥对
	 * @return 私钥
	 */
    public static String getPrivateKey(Map<String, Object> map) {
        String str = "";
        try {
            Key key = (Key) map.get(KEY_RSA_PRIVATEKEY);
            str = encryptBase64(key.getEncoded());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return str;
    }
 
	/**
	 * <br>Description:BASE64 解密
     * @param key 需要解密的字符串
     * @return 字节数组
	 */
    public static byte[] decryptBase64(String key) {
        return javax.xml.bind.DatatypeConverter.parseBase64Binary(key);
    }
 
	/**
	 * <br>Description:BASE64 加密
     * @param key 需要加密的字节数组
     * @return 字符串
	 */
    public static String encryptBase64(byte[] key) {
        return javax.xml.bind.DatatypeConverter.printBase64Binary(key);
    }

    protected String bytesToString(byte[] encrytpByte) {
        String result = "";
        for (Byte bytes : encrytpByte) {
            result += bytes.toString() + " ";
        }
        return result;
    }
	public static String requestByPostMethod(List<BasicNameValuePair> list, String url) throws Exception {
        String strResult = "";
        if (url == null || "".equals(url)) {
            strResult = "{\"result\": 998,\"msg\":\"请求接口url地址为空！\"}";
        } else {
            CloseableHttpClient httpClient = null;
            try {
                CloseableHttpResponse httpResponse = null;
                HttpPost post = new HttpPost(url);
                HttpEntity entity = null;
                try {
                    RequestConfig params = RequestConfig.custom().setConnectTimeout(30000).setConnectionRequestTimeout(30000).setSocketTimeout(30000).build();
                    PoolingHttpClientConnectionManager pccm = new PoolingHttpClientConnectionManager();
                    pccm.setMaxTotal(200); // 连接池最大并发连接数
                    pccm.setDefaultMaxPerRoute(100); // 单路由最大并发数
                    pccm.closeExpiredConnections();
                    pccm.closeIdleConnections(15000, TimeUnit.MILLISECONDS);
                    httpClient = HttpClients.custom().setConnectionManager(pccm).setDefaultRequestConfig(params).build();

                    UrlEncodedFormEntity uefEntity = new UrlEncodedFormEntity(list, "UTF-8");
                    post.setEntity(uefEntity);
                    httpResponse = httpClient.execute(post);
                    if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                        entity = httpResponse.getEntity();
                        if (null != entity) {
                            strResult = EntityUtils.toString(entity, "UTF-8");
                        }
                    } else {
                        strResult = "{\"result\": 999,\"msg\":\"调用接口请求失败：" + httpResponse.getStatusLine().getStatusCode() + "\"}";
                        post.abort();
                    }
                    EntityUtils.consume(entity);
                } catch (Exception e) {
                    strResult = "{\"result\":\"999\",\"msg\":\"调用接口请求失败：" + e.getMessage() + "\"}";
                } finally {
                    if (httpResponse != null) {
                        httpResponse.close();
                    }
                    if (post != null) {
                        post.releaseConnection();
                    }
                }
            } finally {
                try {
                    httpClient.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return strResult;
    }
}
