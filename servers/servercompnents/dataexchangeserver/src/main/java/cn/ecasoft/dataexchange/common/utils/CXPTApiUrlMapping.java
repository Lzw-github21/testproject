package cn.ecasoft.dataexchange.common.utils;

import java.util.HashMap;

/**
 * @author xuliukai
 * @since 2023/9/19 11:47
 */
public class CXPTApiUrlMapping {

    public static String ENV_CONFIG = "CXPTURL:Environmental:configuration";
    //开发环境
    public static HashMap<String, String> DEVELOP_CXPT_URL = new HashMap<>();
    //生成环境
    public static HashMap<String, String> PRODUCE_CXPT_URL = new HashMap<>();
    //开发环境前缀
    private static String DEVELOP_CXPT_URL_PREFIX = "http://223.4.75.235:9090/dataexchangeserver";
    //生成环境前缀
    private static String PRODUCE_CXPT_URL_PREFIX = "https://jzsc.jst.zj.gov.cn/dataexchangeserver";

    static {
        DEVELOP_CXPT_URL.put("ReadPersonnelLeaveStatus", DEVELOP_CXPT_URL_PREFIX + "/sgxk/PXDdR3k2MzF/ReadPersonnelLeaveStatus");
        DEVELOP_CXPT_URL.put("getxmnum", DEVELOP_CXPT_URL_PREFIX + "/sgxk/PXDdR3k2MzF/getxmnum");
        DEVELOP_CXPT_URL.put("InsertTSSKBGInfo_New", DEVELOP_CXPT_URL_PREFIX + "/sgxk/PXDdR3k2MzF/InsertTSSKBGInfo_New");
        DEVELOP_CXPT_URL.put("InsertSKByQZ_Manager_New", DEVELOP_CXPT_URL_PREFIX + "/sgxk/PXDdR3k2MzF/InsertSKByQZ_Manager_New");
        DEVELOP_CXPT_URL.put("CancelSgxk_QZ", DEVELOP_CXPT_URL_PREFIX + "/sgxk/PXDdR3k2MzF/CancelSgxk_QZ");
        DEVELOP_CXPT_URL.put("UploadImageInfo_QZ", DEVELOP_CXPT_URL_PREFIX + "/sgxk/PXDdR3k2MzF/UploadImageInfo_QZ");

        PRODUCE_CXPT_URL.put("ReadPersonnelLeaveStatus", PRODUCE_CXPT_URL_PREFIX + "/sgxk/PXDdR3k2MzF/ReadPersonnelLeaveStatus");
        PRODUCE_CXPT_URL.put("getxmnum", PRODUCE_CXPT_URL_PREFIX + "/sgxk/PXDdR3k2MzF/getxmnum");
        PRODUCE_CXPT_URL.put("InsertTSSKBGInfo_New", PRODUCE_CXPT_URL_PREFIX + "/sgxk/PXDdR3k2MzF/InsertTSSKBGInfo_New");
        PRODUCE_CXPT_URL.put("InsertSKByQZ_Manager_New", PRODUCE_CXPT_URL_PREFIX + "/sgxk/PXDdR3k2MzF/InsertSKByQZ_Manager_New");
        PRODUCE_CXPT_URL.put("CancelSgxk_QZ", PRODUCE_CXPT_URL_PREFIX + "/sgxk/PXDdR3k2MzF/CancelSgxk_QZ");
        PRODUCE_CXPT_URL.put("UploadImageInfo_QZ", PRODUCE_CXPT_URL_PREFIX + "/sgxk/PXDdR3k2MzF/UploadImageInfo_QZ");
    }
}
