package cn.ecasoft.dataexchange.controller;

import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.services.DockingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description 对外数据对接接口
 * @Date 2023/9/19 10:08
 * @Created by liang
 */
@RestController
@RequestMapping("/docking")
public class DockingController {
    @Autowired
    private DockingService dockingService;

    /**
     * 根据备案表主键获取项目下所有类型附件
     * @param row_guid
     * @return
     */
    @RequestMapping("/attachedfiles")
    public RetMsgUtil<Object> getAttachedFiles(@RequestParam("ROW_GUID") String row_guid){
        return dockingService.getAttachedFiles(row_guid);
    }
}
