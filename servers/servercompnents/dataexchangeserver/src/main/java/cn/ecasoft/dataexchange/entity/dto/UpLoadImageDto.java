package cn.ecasoft.dataexchange.entity.dto;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Setter
@Getter
public class UpLoadImageDto {
    private byte[] byts;
    private String strYeWuID;

    public boolean checkParams() {
        if (byts.length == 0) {
            return false;
        }
        return !StringUtils.isAnyBlank(strYeWuID);
    }
}
