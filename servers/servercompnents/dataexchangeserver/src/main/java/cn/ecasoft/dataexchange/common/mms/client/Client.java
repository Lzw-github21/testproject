package cn.ecasoft.dataexchange.common.mms.client;

import cn.ecasoft.dataexchange.common.mms.list.MD5Util;
import cn.ecasoft.dataexchange.common.mms.list.SubmitReq;
import cn.ecasoft.dataexchange.common.mms.model.*;
import cn.ecasoft.dataexchange.common.mms.util.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import io.micrometer.core.instrument.util.NamedThreadFactory;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @description: 移动短信
 * @author: 杨攀  2020/8/12 16:29
 */
public class Client {
    private static final Logger logger = LoggerFactory.getLogger(Client.class);
    private AtomicBoolean logined;
    private String sendUrl;
    public static final int SUCCESS = 1;
    public static final int NoopContent = 101;
    public static final int NullMobiles = 102;
    public static final int NoopMobiles = 103;
    public static final int IllegalMobiles = 104;
    public static final int UnAuth = 105;
    public static final int NoopSign = 106;
    public static final int InternalError = 107;
    public static final int JmsError = 108;
    public static final int DupMobiles = 109;
    public static final int Status_MaxMobiles = 110;
    public static final int NoopTempId = 111;
    public static final int ThreadPoolBusiz = 112;
    public static final int maxMobiles = 5000;
    public static final int maxQueueSize = 200000;
    public static final int requestInterval = 10;
    int num;
    private String apId;
    private String apSecretKey;
    private String ecName;
    private int poolSize;
    private int maxJobs;
    private Lock reportLock;
    private Lock moLock;
    private ThreadPoolExecutor nThreadPool;
    private ScheduledThreadPoolExecutor moExecutor;
    private List<MoModel> moModelList;
    private ScheduledThreadPoolExecutor reportExecutor;
    private List<StatusReportModel> reportModelList;

    private Client() {
        this.logined = new AtomicBoolean(false);
        this.num = 0;
        this.poolSize = 16;
        this.maxJobs = 8192;
        this.reportLock = new ReentrantLock();
        this.moLock = new ReentrantLock();
        this.nThreadPool = new ThreadPoolExecutor(this.poolSize, this.poolSize, 0L, TimeUnit.SECONDS, new ArrayBlockingQueue(this.maxJobs), new NamedThreadFactory("thread-sendDsms"));
        this.moExecutor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1, new NamedThreadFactory("sdk2.0-scheduled-getMo"));
        this.moModelList = new ArrayList();
        this.reportExecutor = (ScheduledThreadPoolExecutor)Executors.newScheduledThreadPool(1, new NamedThreadFactory("sdk2.0-scheduled-getReport"));
        this.reportModelList = new ArrayList();
        this.reportExecutor.scheduleWithFixedDelay(new Client.ReportTask(), 0L, 10L, TimeUnit.SECONDS);
        this.moExecutor.scheduleWithFixedDelay(new Client.MoTask(), 0L, 10L, TimeUnit.SECONDS);
    }

    public static final Client getInstance() {
        return Client.InstanceHolder.instance;
    }

    public String getSendUrl() {
        return this.sendUrl;
    }

    public void setSendUrl(String sendUrl) {
        this.sendUrl = sendUrl;
    }

    public boolean login(String url, String userAccount, String password, String ecname) {
        synchronized(this) {
            if (this.logined.get()) {
                logger.error("user lgoined already");
                return true;
            }
        }

        LoginReq req = new LoginReq();
        req.setApId(userAccount);
        req.setSecretKey("");
        req.setEcName(ecname);

        try {
            MgcLoginRsp rsp = this.doLogin(url, req);
            if (rsp.isSuccess()) {
                this.sendUrl = rsp.getUrl();
                this.apId = userAccount;
                this.apSecretKey = password;
                this.ecName = ecname;
                this.logined.set(true);
            } else {
                logger.info("登录失败：{}", JSON.toJSON(rsp));
            }
        } catch (Exception var7) {
            logger.info("登录异常:{}", var7);
        }

        return this.logined.get();
    }

    private MgcLoginRsp doLogin(String baseUrl, LoginReq req) throws Exception {
        String url = MixUtil.mergeUrl(baseUrl, "/sms/login");
        CloseableHttpResponse resp = null;

        MgcLoginRsp var8;
        try {
            String reqText = JsonUtil.toJsonString(req);
            String encode = Base64.encodeBase64String(reqText.getBytes("utf-8"));
            //logger.info("loginreq\t{}", reqText);
            resp = HttpUtil.post(url, encode, ContentType.APPLICATION_JSON);
            if (resp.getStatusLine().getStatusCode() != 200) {
                logger.error("login to {} exception, code = {}", url, resp.getStatusLine().getStatusCode());
                throw new RuntimeException("login exception");
            }

            String respText = HttpUtil.readHttpResponse(resp);
            if (StringUtils.isBlank(respText)) {
                logger.error("not found loginrsp data");
                throw new RuntimeException("login exception: not found loginrsp data");
            }

            //logger.info("loginrsp\t{}", respText);
            var8 = (MgcLoginRsp) JsonUtil.fromJsonString(respText, MgcLoginRsp.class);
        } catch (Exception var12) {
            logger.error("login to {} exception, error = ", url, var12);
            throw var12;
        } finally {
            HttpUtil.safeClose(resp);
        }

        return var8;
    }

    public int sendDSMS(String[] mobiles, String smsContent, String addSerial, int smsPriority, String sign, String msgGroup, boolean isMo) {
        if (!this.logined.get()) {
            return 105;
        } else if (mobiles == null) {
            return 102;
        } else if (mobiles.length == 0) {
            return 103;
        } else if (mobiles.length > 5000) {
            return 110;
        } else {
            Set<String> mbSet = new HashSet();
            String[] arr$ = mobiles;
            int len$ = mobiles.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                String mobile = arr$[i$];
                mbSet.add(mobile);
            }

            if (mbSet.size() != mobiles.length) {
                return 109;
            } else if (StringUtils.isBlank(smsContent)) {
                return 101;
            } else if (StringUtils.isBlank(sign)) {
                return 106;
            } else {
                SubmitReq submit = new SubmitReq();
                submit.setApId(this.apId);
                submit.setApReqId(msgGroup != null ? msgGroup : MixUtil.uuid());
                if (StringUtils.isBlank(addSerial)) {
                    addSerial = "";
                }

                if (StringUtils.isBlank(this.apSecretKey)) {
                    this.apSecretKey = "";
                }

                submit.setMac(MD5Util.md5Encode(this.apId + this.apSecretKey + this.ecName + smsContent + sign + addSerial));
                submit.setContent(smsContent);
                submit.setMobiles(new ArrayList(mbSet));
                submit.setSrcId(addSerial);
                submit.setServiceId(sign);
                submit.setRegReport(isMo);
                submit.setEcName(this.ecName);
                Object task = null;

                try {
                    new Client.SubmitTask(submit, mobiles, this.sendUrl);
                    this.nThreadPool.execute(new Client.SubmitTask(submit, mobiles, this.sendUrl));
                    return 1;
                } catch (RejectedExecutionException var13) {
                    ((Runnable)task).run();
                    return 112;
                }
            }
        }
    }

    public int sendTSMS(String[] mobiles, String tempID, String[] params, String addSerial, int smsPriority, String sign, String msgGroup) {
        if (!this.logined.get()) {
            return 105;
        } else if (mobiles == null) {
            return 102;
        } else if (mobiles.length == 0) {
            return 103;
        } else if (mobiles.length > 5000) {
            return 110;
        } else {
            Set<String> mbSet = new HashSet();
            String[] arr$ = mobiles;
            int len$ = mobiles.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                String mobile = arr$[i$];
                mbSet.add(mobile);
            }

            if (mbSet.size() != mobiles.length) {
                return 109;
            } else if (params != null && params.length != 0) {
                if (StringUtils.isBlank(sign)) {
                    return 106;
                } else if (StringUtils.isBlank(tempID)) {
                    return 111;
                } else {
                    SubmitReq submit = new SubmitReq();
                    submit.setApId(this.apId);
                    submit.setApReqId(msgGroup != null ? msgGroup : MixUtil.uuid());
                    submit.setMac(MD5Util.md5Encode(this.apId + this.apSecretKey + this.ecName + tempID + sign + addSerial));
                    submit.setContent(JsonUtil.toJsonString(params));
                    submit.setMobiles(new ArrayList(mbSet));
                    submit.setSrcId(addSerial);
                    submit.setServiceId(sign);
                    submit.setRegReport(true);
                    submit.setTemplateId(tempID);
                    submit.setEcName(this.ecName);
                    Object task = null;

                    try {
                        new Client.SubmitTask(submit, mobiles, this.sendUrl);
                        this.nThreadPool.execute(new Client.SubmitTask(submit, mobiles, this.sendUrl));
                        return 1;
                    } catch (RejectedExecutionException var13) {
                        ((Runnable)task).run();
                        return 112;
                    }
                }
            } else {
                return 101;
            }
        }
    }

    private SubmitRsp doSubmit(String baseUrl, SubmitReq req) throws Exception {
        String url = MixUtil.mergeUrl(baseUrl, "/sms/submit");
        CloseableHttpResponse resp = null;

        SubmitRsp var8;
        try {
            String reqText = JsonUtil.toJsonString(req);
            //logger.info("submitreq\t{}", reqText);
            String encode = Base64.encodeBase64String(reqText.getBytes("utf-8"));
            resp = HttpUtil.post(url, encode, ContentType.APPLICATION_JSON);
            if (resp.getStatusLine().getStatusCode() != 200) {
                logger.error("submit to {} exception, code = {}", url, resp.getStatusLine().getStatusCode());
                throw new RuntimeException("submit exception");
            }

            String respText = HttpUtil.readHttpResponse(resp);
            if (StringUtils.isBlank(respText)) {
                logger.error("not found submitrsp data");
                throw new RuntimeException("submit exception:not found submitrsp data");
            }

            logger.info("submitrsp\t{}", respText);
            var8 = (SubmitRsp) JsonUtil.fromJsonString(respText, SubmitRsp.class);
        } catch (Exception var12) {
            logger.error("submit to {} exception, error = ", url, var12);
            throw var12;
        } finally {
            HttpUtil.safeClose(resp);
        }

        return var8;
    }

    private String parseSubmitStatus(String rspcod) {
        if (McMgwError.InvalidUsrOrPwd.becauseOf(new String[]{rspcod})) {
            return McRptStatus.INVALID_USR_OR_PWD.getStatus();
        } else if (McMgwError.InvalidMessage.becauseOf(new String[]{rspcod})) {
            return McRptStatus.INVALID_MESSAGE.getStatus();
        } else if (McMgwError.NO_SIGN_ID.becauseOf(new String[]{rspcod})) {
            return McRptStatus.NO_SIGN_ID.getStatus();
        } else if (McMgwError.TOO_MANY_MOBILES.becauseOf(new String[]{rspcod})) {
            return McRptStatus.TOO_MANY_MOBILES.getStatus();
        } else {
            return McMgwError.ILLEGAL_SIGN_ID.becauseOf(new String[]{rspcod}) ? McRptStatus.ILLEGAL_SIGN_ID.getStatus() : "CM:9999";
        }
    }

    private List<StatusReportModel> doReport(String baseUrl, Account accout) throws Exception {
        new ArrayList();
        String url = MixUtil.mergeUrl(baseUrl, "/sms/report");
        CloseableHttpResponse resp = null;

        Object var9;
        try {
            String reqText = JsonUtil.toJsonString(accout);
            //logger.info("reportAccount\t{}", reqText);
            String encode = Base64.encodeBase64String(reqText.getBytes("utf-8"));
            resp = HttpUtil.post(url, encode, ContentType.APPLICATION_JSON);
            if (resp.getStatusLine().getStatusCode() != 200) {
                logger.error("report submit to {} exception, code = {}", url, resp.getStatusLine().getStatusCode());
                throw new RuntimeException("report submit exception");
            }

            String respText = HttpUtil.readHttpResponse(resp);
            if (StringUtils.isBlank(respText)) {
                logger.error("not found submitrsp data");
                throw new RuntimeException("report submit exception : not found submitrsp data");
            }

            //logger.info("report submitrsp\t{}", respText);

            Object statusReport;
            try {
                statusReport = JSONArray.parseArray(respText, StatusReportModel.class);
            } catch (Exception var14) {
                statusReport = new ArrayList();
            }

            var9 = statusReport;
        } catch (Exception var15) {
            logger.error("submit to {} exception, error = ", url, var15);
            throw var15;
        } finally {
            HttpUtil.safeClose(resp);
        }

        return (List)var9;
    }

    public List<StatusReportModel> getReport() {
        if (!this.logined.get()) {
            logger.error("获取状态报告失败：请先登陆！");
            return null;
        } else {
            this.reportLock.lock();

            List var2;
            try {
                List<StatusReportModel> statusReportModels = this.reportModelList;
                this.reportModelList = new ArrayList();
                var2 = statusReportModels;
            } finally {
                this.reportLock.unlock();
            }

            return var2;
        }
    }

    private List<MoModel> getMoMessage(String baseUrl, Account accout) throws Exception {
        new ArrayList();
        String url = MixUtil.mergeUrl(baseUrl, "/sms/deliver");
        CloseableHttpResponse resp = null;

        Object var9;
        try {
            String reqText = JsonUtil.toJsonString(accout);
            //logger.info("moAccount\t{}", reqText);
            String encode = Base64.encodeBase64String(reqText.getBytes("utf-8"));
            resp = HttpUtil.post(url, encode, ContentType.APPLICATION_JSON);
            if (resp.getStatusLine().getStatusCode() != 200) {
                logger.error("getMo submit to {} exception, code = {}", url, resp.getStatusLine().getStatusCode());
                throw new RuntimeException("getMo submit exception");
            }

            String respText = HttpUtil.readHttpResponse(resp);
            if (StringUtils.isBlank(respText)) {
                logger.error("not found submitrsp data");
                throw new RuntimeException("getMo submit exception : not found submitrsp data");
            }

            //logger.info("mo submitrsp\t{}", respText);

            Object statusReport;
            try {
                statusReport = JSONArray.parseArray(respText, MoModel.class);
            } catch (Exception var14) {
                statusReport = new ArrayList();
            }

            var9 = statusReport;
        } catch (Exception var15) {
            logger.error("submit to {} exception, error = ", url, var15);
            throw var15;
        } finally {
            HttpUtil.safeClose(resp);
        }

        return (List)var9;
    }

    public List<MoModel> getMO() {
        if (!this.logined.get()) {
            logger.error("获取上行消息失败：请先登陆！");
            return null;
        } else {
            this.moLock.lock();

            List var2;
            try {
                List<MoModel> moModels = this.moModelList;
                this.moModelList = new ArrayList();
                var2 = moModels;
            } finally {
                this.moLock.unlock();
            }

            return var2;
        }
    }

    public static void main(String[] args) throws UnsupportedEncodingException, InterruptedException {
        Client client = getInstance();
        boolean logined = client.login("http://112.35.4.197:15000", "umf417", "123qwe", "测试账号01");
        logger.info("logined : {}", logined);
        if (logined) {
            while(true) {
                Thread.sleep(10000L);
                List<StatusReportModel> statusReportlist = client.getReport();
                logger.info("getReport : {}", statusReportlist.size());
            }
        }

    }

    static {
        try {
            HttpClientHolder.getInstance().afterPropertiesSet();
        } catch (Exception var1) {
            logger.error("", var1);
        }

    }

    public final class SubmitTask implements Runnable {
        private SubmitReq submit;
        private String[] mobiles;
        private String sendUrl;

        public SubmitTask(SubmitReq submit, String[] mobiles, String sendUrl) {
            this.submit = submit;
            this.mobiles = mobiles;
            this.sendUrl = sendUrl;
        }

        public void run() {
            SubmitReportModel model = null;
            SubmitReportModel model2 = null;

            try {
                SubmitRsp rsp = Client.this.doSubmit(this.sendUrl, this.submit);
                model = new SubmitReportModel();
                model.setMobiles(this.mobiles);
                model.setMsgGroup(this.submit.getApReqId());
                model.setReportStatus(rsp.isSuccess() ? "CM:0000" : Client.this.parseSubmitStatus(rsp.getRspcod()));
                model.setSubmitDate(DateTimeUtil.getDate());
                model.setReceiveDate(DateTimeUtil.getDate());
                model.setErrorCode(rsp.getRspcod());
                if (rsp.isSuccess()) {
                    model2 = new SubmitReportModel();
                    model2.setMobiles(this.mobiles);
                    model2.setMsgGroup(this.submit.getApReqId());
                    model2.setReportStatus("CM:3000");
                    model2.setSubmitDate(DateTimeUtil.getDate());
                    model2.setReceiveDate(DateTimeUtil.getDate());
                    model2.setErrorCode(rsp.getRspcod());
                }
            } catch (Exception var4) {
                model = new SubmitReportModel();
                model.setMobiles(this.mobiles);
                model.setMsgGroup(this.submit.getApReqId());
                model.setReportStatus("发送错误");
                model.setSubmitDate(DateTimeUtil.getDate());
                model.setReceiveDate(DateTimeUtil.getDate());
                model.setErrorCode("CM:9999");
            }

        }
    }

    public final class ReportTask implements Runnable {
        public ReportTask() {
        }

        public void run() {
            if (Client.this.logined.get()) {
                if (!StringUtils.isBlank(Client.this.apId) && !StringUtils.isBlank(Client.this.ecName)) {
                    Account accout = new Account(Client.this.apId, Client.this.apSecretKey, Client.this.ecName);
                    new ArrayList();
                    Client.this.reportLock.lock();

                    try {
                        List<StatusReportModel> report = Client.this.doReport(Client.this.sendUrl, accout);
                        if (Client.this.reportModelList.size() < 200000) {
                            Client.this.reportModelList.addAll(report);
                        } else {
                            Client.logger.info("Report:本地缓存已超20万数据，以下数据将被丢弃：", JsonUtil.toJsonString(report));
                        }
                    } catch (Exception var7) {
                        Client.logger.info("获取状态报告失败：", var7);
                    } finally {
                        Client.this.reportLock.unlock();
                    }

                } else {
                    Client.logger.error("apId / ecName is null ,query stop!");
                }
            }
        }
    }

    public final class MoTask implements Runnable {
        public MoTask() {
        }

        public void run() {
            if (Client.this.logined.get()) {
                if (!StringUtils.isBlank(Client.this.apId) && !StringUtils.isBlank(Client.this.ecName)) {
                    Account accout = new Account(Client.this.apId, Client.this.apSecretKey, Client.this.ecName);
                    new ArrayList();
                    Client.this.moLock.lock();

                    try {
                        List<MoModel> mo = Client.this.getMoMessage(Client.this.sendUrl, accout);
                        if (Client.this.moModelList.size() < 200000) {
                            Client.this.moModelList.addAll(mo);
                        } else {
                            Client.logger.info("Deliver:本地缓存已超20万数据，以下数据将被丢弃：", JsonUtil.toJsonString(mo));
                        }
                    } catch (Exception var7) {
                        Client.logger.info("获取上行短信失败：", var7);
                    } finally {
                        Client.this.moLock.unlock();
                    }

                } else {
                    Client.logger.error("apId / ecName is null ,query stop!");
                }
            }
        }
    }

    private static final class InstanceHolder {
        public static final Client instance = new Client();

        private InstanceHolder() {
        }
    }
}
