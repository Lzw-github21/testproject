package cn.ecasoft.dataexchange.entity.db;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Table;

import java.math.BigDecimal;

@Getter
@Setter
@Table("TBProjectInfo_Change")
public class TBProjectInfoChange {
    private String ROW_GUID;
    private Integer ID;
    private Integer PrjID;
    private String PrjNum;
    private String PrjName;
    private String PrjTypeNum;
    private String BuildCorpName;
    private String BuildCorpCode;
    private String ProvinceNum;
    private String CityNum;
    private String CountyNum;
    private String PrjapProvalNum;
    private String PrjapProvalLevelnum;
    private String PrjapProvalDepart;
    private String PrjapProvalDate;
    private BigDecimal AllInvest;
    private BigDecimal AllArea;
    private String PrjSize;
    private String PrjPropertyNum;
    private String PrjFunctionNum;
    private String BDate;
    private String EDate;
    private String IsFinish;
    private String CreateUser;
    private String StatusNum;
    private String CreateDate;
    private String Address;
    private String ManageDepNum;
    private String ManageDepNumUserName;
    private String ManageDepDate;
    private String ManageDepMark;
    private String UserName;
    private String UserPwd;
    private String HPhone;
    private String PlanBDate;
    private String PlanEDate;
    private String LegalMan;
    private String LegalManIDCard;
    private String TechMan;
    private String ReturnCause;
    private String ManageDepIp;
    private String BarCode;
    private String ProjectManMobile;
    private String BuildCorpAddress;
    private String UploadDate;
    private String Guid;
    private String ManageAdminAreaNum;
    private String IsBuLu;
    private String BuluStatus;
    private String BuilderCorpLeader;
    private String BuilderCorpLeaderPhone;
    private String BuldPlanNum;
    private String ProjectPlanNum;
    private String LegalName;
    private String Issgt;
    private String PrjUploadID_HZ;
    private String Uploadtm_HZ;
    private String ChangeGuid;
    private String PrjCode;
    private String PrjCode1;
    private String CheckDepartName;
    private String DataLevel;
    private BigDecimal LocationX;
    private BigDecimal LocationY;
    private String FundSource;
    private BigDecimal NationalPercentTage;
    private BigDecimal AllLength;
    private Integer IsMajor;
    private String JZJNInfo;
    private String CXXMInfo;
    private String InvPropertyNum;
    private Integer IsDelete;
    private String CreateTime;
    private String UpDateTime;
    private String EcontypeNum;

}
