package cn.ecasoft.dataexchange.entity.dto;

import cn.ecasoft.dataexchange.entity.db.ChangeDetail;
import cn.ecasoft.dataexchange.entity.db.TBSFCorpInfoChange;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * @author XuLiuKai
 */
@Setter
@Getter
public class TSSGXKBGDto {

    private String CityType;
    private List<TBBuilderLicenceManageChangeDto> tbbuilderlicencemanage_change;
    private List<TBBuilderLicenceUnitInfoChangeDto> tbbuilderlicenceunitinfo_change;
    private List<ChangeDetail> tbbuilderlicencemanage_changedetails;
    private List<TBRecordInfoDto> tbrecordinfo;
    private List<TBProjectInfoChangeDto> tbprojectinfo_change;
    private List<TBSFCorpInfoChange> tbsfcorpinfo_change;

    public boolean checkParams() {
        if (StringUtils.isBlank(this.CityType)) {
            return false;
        }
        if (this.tbbuilderlicencemanage_change.size() == 0 ||
//                this.tbbuilderlicenceunitinfo_change.size() == 0 ||
//                this.tbbuilderlicencemanage_changedetails.size() == 0 ||
                this.tbrecordinfo.size() == 0 ||
                this.tbprojectinfo_change.size() == 0 ||
                this.tbsfcorpinfo_change.size() == 0) {
            return false;
        }


        return true;
    }
}
