package cn.ecasoft.dataexchange.common.cloudsign;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


public class HmacSha1Util {


    public static final String MAC_ALGORITHM_DEFAULT = "HmacSHA1";

    /**
     * 获取基于哈希的消息验证代码
     *
     * @param data 消息内容字节数组
     * @param key  签名密钥字节数组
     * @return
     */
    private static String hamcsha1(byte[] data, byte[] key) {
        try {
            SecretKeySpec signingKey = new SecretKeySpec(key, MAC_ALGORITHM_DEFAULT);
            Mac mac = Mac.getInstance(MAC_ALGORITHM_DEFAULT);
            mac.init(signingKey);
            return new sun.misc.BASE64Encoder().encode(mac.doFinal(data));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 接收方校验签名
     *
     * @param data      消息内容
     * @param key       加密key
     * @param signatrue 签名结果
     * @return
     */
    public static boolean verifySignature(byte[] data, byte[] key, String signatrue) {
        String checkSignatrue = hamcsha1(data, key);
        return checkSignatrue.equals(signatrue);
    }


    /**
     * 字节数组转16进制
     *
     * @param b 字节数组
     * @return
     */
    private static String byte2hex(byte[] b) {
        StringBuilder hs = new StringBuilder();
        String stmp;
        for (int n = 0; b != null && n < b.length; n++) {
            stmp = Integer.toHexString(b[n] & 0XFF);
            if (stmp.length() == 1) {
                hs.append('0');
            }
            hs.append(stmp);
        }
        return hs.toString().toUpperCase();
    }

    /**
     * md5加密 无盐值
     *
     * @param target 待加密字符串
     * @return 加密后的MD5字符串
     * @throws Exception 异常
     */
    private static String md5(byte[] target) {
        if (target == null || target.length <= 0) {
            return null;
        }
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        }
        return byte2hex(md5.digest(target));
    }


    /**
     * Restful签名
     *
     * @param headers   http请求头
     * @param body      http请求体
     * @param secretKey 加密key
     * @return
     */
    public static String sign4Rest(Map<String, String> headers, byte[] body, String secretKey) {

        Set<String> sortSet = new TreeSet<String>();
        Map<String, String> signMap = new HashMap<String, String>();

        if (headers != null) {
            // 以x-con-开头的请求头都参与签名
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                if (entry.getKey().toLowerCase().startsWith("af-")) {
                    sortSet.add(entry.getKey());
                    signMap.put(entry.getKey(), entry.getValue());
                }
            }
        }

        //  追加一个属性名为“body”，属性值为http请求body做md5后的摘要的签名参数。
        //  如果body中没有任何参数（http请求中body的长度为0），则不追加一个参数名为 body 的参数。
        if (body != null && body.length > 0) {
            sortSet.add("body");
            signMap.put("body", md5(body).toLowerCase());
        }

        // 使用( & )连接待签名参数，注意参数排序。排序规则为待签参数的key、value的集合中的key字符串,
        // 参数名按ASCII码从小到大排序（字典序）,然后使用( & )连接排好序的key=value集合
        // body=04A959AFB44161BE668EDD15ADDED92B&x-con-akid=This is accessId&x-con-date=2019-07-31 12:12:12

        StringBuilder sb = new StringBuilder();
        boolean isFirst = true;
        for (String key : sortSet) {
            if (isFirst) {
                sb.append(key).append("=").append(signMap.get(key));
                isFirst = false;
            } else {
                sb.append("&").append(key).append("=").append(signMap.get(key));
            }
        }

        return hamcsha1(sb.toString().getBytes(), secretKey.getBytes());
    }


    /**
     * Restful签名
     *
     * @param headers   http请求头
     * @param body      http请求体
     * @param secretKey 加密key
     * @param signature 签名值
     * @return
     */
    public static boolean verify4rest(Map<String, String> headers, byte[] body, String secretKey, String signature) {

        String checkSignature = sign4Rest(headers, body, secretKey);

        return checkSignature.equals(signature);
    }



}
