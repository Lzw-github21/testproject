package cn.ecasoft.dataexchange.services;

import cn.ecasoft.dataexchange.entity.model.LockInfoModel;
import cn.ecasoft.dataexchange.entity.model.LockBack;

/**
 * @author XuLiuKai
 */
public interface LockHelper {

    LockBack LockInfo(String Idcard, String sgxkGuid, String LockName, String LockNum, String LockMark, String lockType, String prjName, String corpcode, int TypeState, LockInfoModel model, String strErrMsg);

    LockBack Un_LockInfo(String Idcard, String sgxkGuid, String UnLockName, String UnLockNum, String UnLockMark, String lockType, String prjName, String corpcode, int TypeState, LockInfoModel model, String strErrMsg);

    LockBack HaveNumProject(String lockType, String Idcard, String personName, String strErrMsg);
}
