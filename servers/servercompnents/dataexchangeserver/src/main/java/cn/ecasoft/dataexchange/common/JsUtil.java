package cn.ecasoft.dataexchange.common;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.util.Base64Utils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Arrays;

/**
 * 嘉善使用工具类
 * 对接建行电子保函
 *
 * */
public class JsUtil {

    private static final char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private static MessageDigest messageDigest;

    static {
        Security.addProvider(new BouncyCastleProvider());
        Security.setProperty("crypto.policy", "unlimited");
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ignored) {
        }
    }

    /**
     * AES256加密
     *
     * @param data 待加密内容
     * @param key  32字节秘钥
     */
    public static String AES256Encrypt(String data, String key) throws Exception {
        try {
            SecretKey k = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
            cipher.init(Cipher.ENCRYPT_MODE, k);
            byte[] result = Base64Utils.encode(cipher.doFinal(data.getBytes(StandardCharsets.UTF_8)));
            return new String(result);
        } catch (InvalidKeyException ie) {
            throw new InvalidKeyException(ie.getMessage() + ". 请下载无政策限制版本的jre，并将local_policy.jar、US_export_policy.jar文件覆盖到jre/lib/security下");
        }
    }

    /**
     * AES解密
     *
     * @param data 待解密base64字符串
     * @param key  32字节秘钥
     */
    public static String AES256Decrypt(String data, String key) throws Exception {
        try {
            SecretKey k = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
            cipher.init(Cipher.DECRYPT_MODE, k);
            byte[] result = cipher.doFinal(Base64Utils.decode(data.getBytes(StandardCharsets.UTF_8)));
            return new String(result, StandardCharsets.UTF_8);
        } catch (InvalidKeyException ie) {
            throw new InvalidKeyException(ie.getMessage() + ". 请下载无政策限制版本的jre，并将local_policy.jar、US_export_policy.jar文件覆盖到jre/lib/security下");
        }
    }

    /*
     AES CBC PKCS5Padding
  */
    public static String AES_CBC_Encrypt(String sSrc, byte[] key, byte[] iv) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        IvParameterSpec _iv = new IvParameterSpec(iv);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, _iv);
        byte[] encrypted = cipher.doFinal(sSrc.getBytes("utf-8"));

        System.out.println(Arrays.toString(encrypted));
        System.out.println("resultHex: " + bufferToHex(encrypted));

        return new String(java.util.Base64.getEncoder().encode(encrypted));
    }

    public static String getsign(String data) throws Exception {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        byte[] keyArray = md5.digest(data.getBytes(StandardCharsets.UTF_8));
        java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
        return encoder.encodeToString(keyArray);
    }

    public static String filemd5(File file) throws Exception {
        FileInputStream in;
        try {
            in = new FileInputStream(file);
            FileChannel ch = in.getChannel();
            MappedByteBuffer byteBuffer = ch.map(FileChannel.MapMode.READ_ONLY, 0, file.length());
            messageDigest.update(byteBuffer);
        } catch (FileNotFoundException ignored) {
        }
        return bufferToHex(messageDigest.digest());
    }

    private static String bufferToHex(byte[] bytes) {
        return bufferToHex(bytes, 0, bytes.length);
    }

    private static String bufferToHex(byte[] bytes, int m, int n) {
        StringBuffer stringBuffer = new StringBuffer(2 * n);
        int k = m + n;
        for (int l = m; l < k; l++) {
            char c0 = hexDigits[(bytes[l] & 0xf0) >> 4];
            char c1 = hexDigits[bytes[l] & 0xf];
            stringBuffer.append(c0);
            stringBuffer.append(c1);
        }
        return stringBuffer.toString();
    }


    /*
     *  byte[] 转为 File
     * */
    public static File bytesToFile(byte[] buffer, final String fileName) throws Exception {
        String ext = StringUtils.substringAfterLast(fileName, ".");
        File file = File.createTempFile(RandomStringUtils.randomAlphanumeric(6), "." + ext);
        file.deleteOnExit();
        try (OutputStream output = new FileOutputStream(file); BufferedOutputStream bufferedOutput = new BufferedOutputStream(output)) {
            bufferedOutput.write(buffer);
        } catch (Exception ex) {
            return null;
        }
        return file;
    }

    /**
     * c# 转 java md5
     *
     * @param str
     * @param encoding  字符集 Encoding.Default.GetBytes==> utf-8
     * @return : java.lang.String
     */
    public static String getcMD5(String str, String encoding)  {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes(encoding));
            byte[] result = md.digest();
            StringBuffer sb = new StringBuffer(32);
            for (int i = 0; i < result.length; i++) {
                int val = result[i] & 0xff;
                if (val < 0xf) {
                    sb.append("0");
                }
                sb.append(Integer.toHexString(val));
            }
            return sb.toString().toUpperCase();
        } catch (Exception e) {
            System.out.println("MD5出错" + e.getMessage());
        }
        return "";
    }

    /**
     des解密
     */
    public static String DES_Decrypt(String content,String keys, byte[] ivs) throws Exception {
        if (org.apache.commons.lang3.StringUtils.isBlank(content) || org.apache.commons.lang3.StringUtils.isBlank(keys)) {
            return content;
        }
        try {
            DESKeySpec keySpec = new DESKeySpec(keys.getBytes(StandardCharsets.UTF_8));// 设置密钥参数

            IvParameterSpec iv = new IvParameterSpec(ivs);// 设置向量
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");// 获得密钥工厂
            Key key = keyFactory.generateSecret(keySpec);// 得到密钥对象
            Cipher enCipher = Cipher.getInstance("DES/CBC/PKCS5Padding");// 得到加密对象Cipher
            enCipher.init(Cipher.DECRYPT_MODE, key, iv);// 设置工作模式为加密模式，给出密钥和向量
            byte[] pasByte = enCipher.doFinal(Base64.decodeBase64(content.getBytes("utf-8")));
            return new String(pasByte);
        } catch (Exception ex) {
            throw new Exception("DES解密发生异常");
        }
    }

    public static void main(String[] args) {
        try {
            String password = JsUtil.DES_Decrypt("jRkeSQz+fPQ=", "consmkey", new byte[]{0x12, 0x34, 0x56, 0x78, (byte) 0x90, (byte) 0xab, (byte) 0xcd, (byte) 0xef});
            System.out.println(password);
            System.out.println(JsUtil.getsign("sgt@123456"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
