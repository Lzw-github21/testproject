package cn.ecasoft.dataexchange.entity.db;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Table;

/**
 * @author XuLiuKai
 */
@Setter
@Getter
@Table("tbbuilderlicencemanage_new")
public class TBBuilderLicenceManageNew {
    private Integer ID;
    private String ROW_GUID;
    private String BuildLicenceId;
    private String BuilderLicenceNum;
    private String PrjId;
    private String PrjName;
    private String BuildPlanNum;
    private String ProjectPlanNum;
    private String CensorNum;
    private Double ContractMoney;
    private Double Area;
    private Double PrjSize;
    private String BargainBDate;
    private String BargainEDate;
    private Integer BargainDays;
    private String IssuecertDate;
    private String SafeTycerId;
    private String CreateUser;
    private Integer CensorStatusNum;
    private String QualityAcceptDepart;
    private String QualityAcceptUser;
    private Integer QualityAcceptStatus;
    private String IsTaKan;
    private String TaKanNumber;
    private String CreateDate;
    private String ManageDepNum;
    private String ManageDepNumUserName;
    private String ManageDepDate;
    private String ManageDepMark;
    private String ManageDepNumAuditUserName;
    private String ManageDepAuditDate;
    private String ManageDepAuditMark;
    private String TaKanMark;
    private String HPhone;
    private String BarCode;
    private String UploadDate;
    private String QualityAcceptMark;
    private String BargainId;
    private String TenderNum;
    private String Mark;
    private String ZlAjNum;
    private String ZlAjZjr;
    private String ZlAjFjr;
    private String PrjGuid;
    private String HTGuid;
    private String ManageAdminAreaNum;
    private String IsBuLu;
    private String IsFilish;
    private String TradeTypeNum;
    private String TradeTypeBoundNum;
    private String TechnicalName;
    private String TechnicalData;
    private String ReleaseDeptName;
    private String ReleaseDate;
    private Double Length;
    private Double Span;
    private String EconCorpMark;
    private String DesignCorpMark;
    private String SuperCorpMark;
    private String BGInfo;
    private String SgxkUploadID_HZ;
    private String OptionType;
    private String QRCode;
    private String Uploadtm_HZ;
    private String PrjCode;
    private String PrjCode1;
    private String IS_GYH;
    private String DecorateCorpName;
    private String DecorateCorpCode;
    private String DecorateCorpLeader;
    private String DecorateCorpLeaderIdCard;
    private Double DecorateArea;
    private String DecorateMark;
    private String IS_Qzx;
    private String SceneState;
    private String ReportTime;
    private String ToExaminePerson;
    private String ToExamineOpinion;
    private String ToExamineTime;
    private String ZLNum;
    private String AQNum;
    private String BuildCorpCodeType;
    private String BuilderCorpLeaderIdCardType;
    private String ReleaseDeptCode;
    private String RissuAuth;
    private String eCertID;
    private String CheckDepartName;
    private Integer DataLevel;
    private String TaKanDate;
    private String ECA_FullTextField;
    private String CreateTime;
    private String UpDateTime;
    private String CheckPersonName;
    private String IstwoBL;
    private String IsTH;
}
