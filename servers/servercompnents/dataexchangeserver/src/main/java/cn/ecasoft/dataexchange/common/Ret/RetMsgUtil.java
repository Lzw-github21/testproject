package cn.ecasoft.dataexchange.common.Ret;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.io.Serializable;

public class RetMsgUtil<T> implements Serializable {
    private static final long serialVersionUID = -7383519722033987904L;
    /**
     * 响应码
     */
    private Integer code;
    /**
     * 响应信息
     */
    private String msg;
    /**
     * 响应是否成功
     */
    private boolean success;
    /**
     * 响应数据
     */
    private T data;
    private String exceptionMsg;



    public RetMsgUtil() {
        this.setState(State.OPERATE_SUCCESS);
    }

    public RetMsgUtil(Integer code, String msg, boolean success) {
        this.code = code;
        this.msg = msg;
        this.success = success;
    }
    public RetMsgUtil(T data) {
        this.setState(State.OPERATE_SUCCESS);
        this.data = data;
    }
    public RetMsgUtil(State errorCodeEn) {
        this.code = errorCodeEn.getCode();
        this.setMsg(errorCodeEn.getMsg());
        this.setSuccess(errorCodeEn.getSuccess());
    }

    public RetMsgUtil(State errorCodeEn, T data) {
        this.code = errorCodeEn.getCode();
        this.setMsg(errorCodeEn.getMsg());
        this.setSuccess(errorCodeEn.getSuccess());
        this.data = data;
    }

    public static RetMsgUtil<Object> ok(){
        return new RetMsgUtil<>();
    }

    public static <T> RetMsgUtil<T> ok(T data){
        return new RetMsgUtil<>(data);
    }

    public static <T> RetMsgUtil<T> ok(State codeEn, T data){
        return new RetMsgUtil<>(codeEn,data);
    }
    public static RetMsgUtil<Object> ok(Integer code, String msg){
        return new RetMsgUtil<>(code,msg,true);
    }

    public static RetMsgUtil<Object> fail(State errorCodeEn){
        return new RetMsgUtil<>(errorCodeEn);
    }
    public static RetMsgUtil<Object> fail(Integer code, String msg){
        return new RetMsgUtil<>(code,msg,false);
    }
    public static RetMsgUtil<Object> fail(String msg){
        return new RetMsgUtil<>(State.OPERATION_FAILED.getCode(),msg,false);
    }
    public static RetMsgUtil<Object> failFromBadParam(){
        return new RetMsgUtil<>(State.BAD_PARAM.getCode(),State.BAD_PARAM.getMsg(),false);
    }

    public static RetMsgUtil<Object> failFromBadParam(String errMsg){
        return new RetMsgUtil<>(State.OPERATE_SUCCESS.getCode(),errMsg,false);
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setState(State errorCodeEn) {
        this.code = errorCodeEn.getCode();
        this.setMsg(errorCodeEn.getMsg());
        this.setSuccess(errorCodeEn.getSuccess());
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Boolean getSuccess() {
        return this.success;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public String getExceptionMsg() {
        return this.exceptionMsg;
    }

    public void setExceptionMsg(String exceptionMsg) {
        this.exceptionMsg = exceptionMsg;
    }

    public  String toJsonString(){
        RetMsgUtil retMsgUtil = new RetMsgUtil();
        retMsgUtil.setSuccess(this.success);
        retMsgUtil.setMsg(this.msg);
        retMsgUtil.setCode(this.code);
        retMsgUtil.setData(this.data);
        return JSONObject.toJSON(retMsgUtil).toString();
    }

    public String toJsonStringWithDateFormat(){
        RetMsgUtil retMsgUtil = new RetMsgUtil();
        retMsgUtil.setSuccess(this.success);
        retMsgUtil.setMsg(this.msg);
        retMsgUtil.setCode(this.code);
        retMsgUtil.setData(this.data);
        return JSON.toJSONStringWithDateFormat(retMsgUtil, "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteDateUseDateFormat);
    }

    @Override
    public String toString() {
        return "RetMsgUtil{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", success=" + success +
                ", data=" + data +
                ", exceptionMsg='" + exceptionMsg + '\'' +
                '}';
    }
}
