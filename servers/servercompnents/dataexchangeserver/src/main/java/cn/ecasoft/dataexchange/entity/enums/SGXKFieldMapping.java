package cn.ecasoft.dataexchange.entity.enums;

import java.util.HashMap;

/**
 * @author xuliukai
 * @since 2023/9/11 13:41
 */
public class SGXKFieldMapping {
    public static final HashMap<String, HashMap<String, String>> ALL_TABLE = new HashMap<>();

    //施工许可对照字段
    private static final HashMap<String, String> SGXK_TABLE = new HashMap<>();
    //单体
    private static final HashMap<String, String> DT_TABLE = new HashMap<>();
    //勘察单位
    private static final HashMap<String, String> KCDW_TABLE = new HashMap<>();
    //设计单位
    private static final HashMap<String, String> SJDW_TABLE = new HashMap<>();
    //施工单位
    private static final HashMap<String, String> SGDW_TABLE = new HashMap<>();
    //监理单位
    private static final HashMap<String, String> JLDW_TABLE = new HashMap<>();
    //总承包单位
    private static final HashMap<String, String> ZCBDW_TABLE = new HashMap<>();
    //消防人防
    public static final HashMap<String, String> XFRF_TABLE = new HashMap<>();


    static {
        SGXK_TABLE.put("prjname", "工程名称");
        SGXK_TABLE.put("buildcorpname", "建设单位名称");
        SGXK_TABLE.put("buildcorpaddress", "建设单位地址");
        SGXK_TABLE.put("econtypenum", "建设单位经济性质");
        SGXK_TABLE.put("legalname", "建设单位法人");
        SGXK_TABLE.put("legalmanidcard", "建设单位法人身份证号");
        SGXK_TABLE.put("buildercorpleader", "建设单位项目负责人");
        SGXK_TABLE.put("buildercorpleaderidcard", "建设单位项目负责人身份证号");
        SGXK_TABLE.put("buildercorpleaderphone", "建设单位项目负责人联系电话");
        SGXK_TABLE.put("buildcorpperson", "建设单位联系人");
        SGXK_TABLE.put("buildcorppersonphone", "建设单位联系人手机号");
        SGXK_TABLE.put("bargaindays", "合同工期");
        SGXK_TABLE.put("bargainbdate", "合同开工日期");
        SGXK_TABLE.put("bargainedate", "合同竣工日期");
        SGXK_TABLE.put("contractmoney", "合同金额");
        SGXK_TABLE.put("area", "面积");
        SGXK_TABLE.put("length", "长度");
        SGXK_TABLE.put("span", "跨度");
        SGXK_TABLE.put("planbdate", "计划开工日期");
        SGXK_TABLE.put("planedate", "计划竣工日期");

        DT_TABLE.put("subprjname", "单体建（构）筑物名称");
        DT_TABLE.put("structuretype", "结构类型");
        DT_TABLE.put("buildarea", "建筑面积(平方米)");
        DT_TABLE.put("allfloorcount", "建筑层数");
        DT_TABLE.put("invest", "工程总造价(万元)");
        DT_TABLE.put("floorbuildarea", "地上建筑面积(平方米)");
        DT_TABLE.put("bottomfloorbuildarea", "地下建筑面积(平方米)");
        DT_TABLE.put("floorcount", "地上层数");
        DT_TABLE.put("bottomfloorcount", "地下层数");
        DT_TABLE.put("buildheight", "建筑高度(米)");
        DT_TABLE.put("prjlevelnum", "工程等级");
        DT_TABLE.put("subprojectlength", "长度(米)");
        DT_TABLE.put("subprojectspan", "跨度(米)");
        DT_TABLE.put("structuretypenum", "结构体系");
        DT_TABLE.put("rfbottomarea", "人防地下室面积(平方米)");
        DT_TABLE.put("nhlevel", "耐火等级");
        DT_TABLE.put("pjrsize", "工程规模");

        KCDW_TABLE.put("corpname", "勘察单位名称");
        KCDW_TABLE.put("corpcode", "勘察单位统一社会信用代码");
        KCDW_TABLE.put("corpleader", "勘察单位项目负责人");
        KCDW_TABLE.put("corpleadercardtype", "项目负责人证件类型");
        KCDW_TABLE.put("corpleadercardnum", "项目负责人证件号码");
        KCDW_TABLE.put("corpleaderphone", "项目负责人联系电话");

        SJDW_TABLE.put("corpname", "设计单位名称");
        SJDW_TABLE.put("corpcode", "设计单位统一社会信用代码");
        SJDW_TABLE.put("corpleader", "设计单位项目负责人");
        SJDW_TABLE.put("corpleadercardtype", "项目负责人证件类型");
        SJDW_TABLE.put("corpleadercardnum", "项目负责人证件号码");
        SJDW_TABLE.put("corpleaderphone", "项目负责人联系电话");

        SGDW_TABLE.put("corpname", "施工单位名称");
        SGDW_TABLE.put("corpcode", "施工单位统一社会信用代码");
        SGDW_TABLE.put("corpleader", "施工单位项目负责人");
        SGDW_TABLE.put("corpleadercardtype", "项目负责人证件类型");
        SGDW_TABLE.put("corpleadercardnum", "项目负责人证件号码");
        SGDW_TABLE.put("corpleaderphone", "项目负责人联系电话");

        JLDW_TABLE.put("corpname", "监理单位名称");
        JLDW_TABLE.put("corpcode", "监理单位统一社会信用代码");
        JLDW_TABLE.put("corpleader", "监理单位项目负责人");
        JLDW_TABLE.put("corpleadercardtype", "项目负责人证件类型");
        JLDW_TABLE.put("corpleadercardnum", "项目负责人证件号码");
        JLDW_TABLE.put("corpleaderphone", "项目负责人联系电话");

        ZCBDW_TABLE.put("corpname", "总承包单位名称");
        ZCBDW_TABLE.put("corpcode", "总承包单位统一社会信用代码");
        ZCBDW_TABLE.put("corpleader", "总承包单位项目负责人");
        ZCBDW_TABLE.put("corpleadercardtype", "项目负责人证件类型");
        ZCBDW_TABLE.put("corpleadercardnum", "项目负责人证件号码");
        ZCBDW_TABLE.put("corpleaderphone", "项目负责人联系电话");

        XFRF_TABLE.put("setaddress", "储罐设置位置");
        XFRF_TABLE.put("capacity", "储罐总容量(㎡)");
        XFRF_TABLE.put("settype", "储罐设置类型");
        XFRF_TABLE.put("cctype", "储罐储存性质");
        XFRF_TABLE.put("gccsubname", "储罐储存物质名称");
        XFRF_TABLE.put("reserves", "堆场储量");
        XFRF_TABLE.put("dccsubname", "堆场储存物质名称");
        XFRF_TABLE.put("bwcltype", "建筑保温材料类别");
        XFRF_TABLE.put("bwfloorcount", "建筑保温保温层数");
        XFRF_TABLE.put("bwnatureuse", "建筑保温使用性质");
        XFRF_TABLE.put("bwolduse", "建筑保温原有用途");
        XFRF_TABLE.put("zxplace", "装修工程装修部位");
        XFRF_TABLE.put("zxarea", "装修工程装修面积(㎡)");
        XFRF_TABLE.put("zxffloorcount", "装修工程装修层数");
        XFRF_TABLE.put("zxnatureuse", "装修工程使用性质");
        XFRF_TABLE.put("zxolduse", "装修工程原有用途");
        XFRF_TABLE.put("bigintsites", "大型的人员密集场所");
        XFRF_TABLE.put("qtspecproject", "其他特殊工程");
        XFRF_TABLE.put("xffacilities", "消防设施");

        ALL_TABLE.put("tbbuilderlicencemanage_New",SGXK_TABLE);
        ALL_TABLE.put("tbprojectfinishunitinfo_new",DT_TABLE);
        ALL_TABLE.put("tbeconcorpinfo_new",KCDW_TABLE);
        ALL_TABLE.put("tbdesigncorpinfo_new",SJDW_TABLE);
        ALL_TABLE.put("tbconscorpinfo_new",SGDW_TABLE);
        ALL_TABLE.put("tbsupercorpinfo_new",JLDW_TABLE);
        ALL_TABLE.put("tbzcbcorpinfo_new",ZCBDW_TABLE);
        ALL_TABLE.put("tbrecordinfo_new",XFRF_TABLE);
    }

}
