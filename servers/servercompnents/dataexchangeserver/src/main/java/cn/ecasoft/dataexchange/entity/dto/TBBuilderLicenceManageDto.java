package cn.ecasoft.dataexchange.entity.dto;

import cn.ecasoft.dataexchange.entity.db.TBBuilderLicenceManage;
import lombok.Getter;
import lombok.Setter;

/**
 * @author XuLiuKai
 */
@Setter
@Getter
public class TBBuilderLicenceManageDto extends TBBuilderLicenceManage {
    private String rowguid;
    private String guid;
    private String prjtypenum;
    private String prjnum;
    private String provincenum;
    private String citynum;
    private String countynum;
    private String address;
    private String aqjdman;
    private String aqjdtel;
    private String aqjdphone;
    private String aqjdidcard;
    private String aqjdman1;
    private String aqjdtel1;
    private String aqjdphone1;
    private String aqjdidcard1;
    private String buildcorpname;
    private String buildcorpcode;
    private String buildcorpaddress;
    private String econtypenum;
    private String buildcorppersonphone;
    private String legalname;
    private String legalmanidcard;
    private String buildercorpleader;
    private String buildercorpleaderidcard;
    private String oldbuildename;
    private String buildercorpleaderphone;
    private String buildcorpperson;
    private String planbdate;
    private String planedate;
    private String econcorpname;
    private String econcorpcode;
    private String econcorpleader;
    private String econcorpleadercardnum;
    private String econcorpleaderphone;
    private String designcorpname;
    private String designcorpcode;
    private String designcorpleader;
    private String designcorpleadercardnum;
    private String designcorpleaderphone;
    private String conscorpname;
    private String conscorpcode;
    private String conscorpleader;
    private String conscorpleadercardnum;
    private String conscorpleaderphone;
    private String supercorpname;
    private String supercorpleader;
    private String supercorpleadercardnum;
    private String supercorpleaderphone;
    private String zcbcorpname;
    private String zcbcorpcode;
    private String zcbcorpleader;
    private String zcbcorpleadercardnum;
    private String zcbcorpleaderphone;
    private String zlpgdate;
    private String superCorpCode;
}
