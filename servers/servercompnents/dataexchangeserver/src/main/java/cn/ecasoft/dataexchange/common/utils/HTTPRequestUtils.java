package cn.ecasoft.dataexchange.common.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

/**
 * @author xuliukai
 * @since 2023/9/19 10:25
 */

public class HTTPRequestUtils {


    private static HttpHeaders getHttpHeaders() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        String appKey = "c9406731-50bc-4e23-b3f5-b7e7d3093875";
        String appSecret = "645ffda7-fc12-4415-a237-314d87d66716";
        long time = System.currentTimeMillis();
        String sign = SignUtil.getSign(appKey + appSecret + time);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.add("appKey", appKey);
        headers.add("time", Long.toString(time));
        headers.add("sign", sign);
        return headers;
    }

    public static String requestToCXPT(String url, MultiValueMap<String, Object> formDataParam) throws Exception {
        HttpHeaders headers = getHttpHeaders();
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(formDataParam, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.postForEntity(url, httpEntity, String.class);
        return response.getBody();
    }

    public static String requestToCXPTByHashMap(String url, HashMap<String, Object> param) throws Exception {
        MultiValueMap<String, Object> formDataParam = new LinkedMultiValueMap<>();
        param.forEach(formDataParam::add);
        return requestToCXPT(url, formDataParam);
    }

    public static String requestToCXPTByBody(String url, String body) throws Exception {
        HttpHeaders headers = getHttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> httpEntity = new HttpEntity<>(body, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.postForEntity(url, httpEntity, String.class);
        return response.getBody();
    }
}
