package cn.ecasoft.dataexchange.mapper;

import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.common.utils.Constants;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;

/**
 * @author xuliukai
 * @since 2023/10/25 17:23
 */
@Service
public class PushSgxkToSjcInfoMapper extends CommonMapperByDBHelp {

    //这里是删除数据仓的数据，所以这里的数据源是数据仓的
    public Integer deleteByRowGuids(String tableName, String row_guids) {
        String sql = "delete from " + tableName + " where rowguid in (@row_guids) ";
        HashMap<String, Object> params = new HashMap<>();
        params.put("row_guids", row_guids);
        return super.executeSqlForCount(Constants.DM_SJC_GUID, sql, "dafOpyemenaJHuYv", params);
    }

    //施工许可表
    public DataTable SGXK(String row_guid) {
        String sql = "select  b.Row_Guid as guid ,b.RecordGuid as rowguid,BuilderLicenceNum,PrjCode,PrjCode1 PrjCode2,p.PRJTYPENAME PrjTypeNum,PrjName,ContractMoney, " +
                "ProvinceNum,CityNum,CountyNum,Address,'' PrjPropertyNum,PrjSize,Area,Length,Span,Mark,BARGAINBDATE,bsrgaindays as BARGAINDAYS, " +
                "BARGAINEDATE,BuildCorpName,BuildCorpCode,BUILDCORPADDRESS,legalName,LEGALMANIDCARD,BUILDERCORPLEADER, " +
                "BUILDERCORPLEADERIDCARD,BUILDERCORPLEADERPHONE,BUILDCORPPERSON,BUILDCORPPERSONPHONE,ECONCORPMark,DESIGNCORPMark, " +
                "SUPERCORPMark,ReleaseDate,ReleaseDeptName,Optiontype,BARCODE,e.EconTypeName econtypeNum,PrjNum,PLANBDATE as pstartDate,PLANEDATE as pcompDate " +
                "from tbbuilderlicencemanage_new b " +
                "left join tbprjtypedic p on b.PrjTypeNum=p.PRJTYPENUM " +
                "left join tbecontypedic e on e.EconTypeNum=b.econtypeNum  " +
                "where RecordGuid = @row_guid";
        HashMap<String, Object> params = new HashMap<>();
        params.put("row_guid", row_guid);
        DataTable dataTable = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, "ewOiyK5YA", params);
        dataTable.setName("biz_031_TBBuilderLicenceManage_DHY");
        return dataTable;
    }

    //单体表
    public DataTable DT(String row_guid) {
        String sql = "select p.Row_Guid as guid ,p.RecordGuid as rowguid ,s.`Name` StructureType,NHLEVEL,BuildArea,ALLFloorCount,FLOORBUILDAREA, " +
                "BOTTOMFLOORBUILDAREA,FloorCount,BottomFloorCount,PjrSize,SubPrjName " +
                "from tbprojectfinishunitinfo_new p " +
                "left join tbstructuretypedic s on p.StructureType=s.TBBM " +
                "where RecordGuid = @row_guid";

        HashMap<String, Object> params = new HashMap<>();
        params.put("row_guid", row_guid);
        DataTable dataTable = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, "DVVnAM5Ft", params);
        dataTable.setName("biz_031_tbprojectfinishunitinfo_DHY");
        return dataTable;
    }

    //四方主体表
    public DataTable SF(String row_guid) {
        String sql = "select b.Row_Guid as guid ,b.RecordGuid as rowguid,s.CorpTypeName CorpTypeNum,CorpName,CorpCode,CorpLeader," +
                "'身份证' CorpLeaderCardType,CorpLeaderCardNum,CorpLeaderPhone " +
                " from tbsfcorpinfo_new b " +
                "left join tbsfcorptypedic_new s on b.CorpTypeNum=s.CorpTypeNum " +
                "where RecordGuid = @row_guid";

        HashMap<String, Object> params = new HashMap<>();
        params.put("row_guid", row_guid);
        DataTable dataTable = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, "H7J1KSu0a14", params);
        dataTable.setName("biz_031_TBSFCorpInfo_DHY");
        return dataTable;
    }

    //人防表
    public DataTable RF(String row_guid) {
        String sql = "select Row_Guid as guid ,RecordGuid as rowguid,FLOORBUILDAREA,BOTTOMFLOORBUILDAREA,DWELLBUILDAREA,QTAREA,YJBUILDAREA," +
                "SJBUILDAREA,RFZYBUILDAREA,PEACEUSE,WARUSE,KLGRADE,FHGRADE,FHUNITNUM " +
                "from TBRFManageInfo where RecordGuid = @row_guid";

        HashMap<String, Object> params = new HashMap<>();
        params.put("row_guid", row_guid);
        DataTable dataTable = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, "eUpymK2OvwI", params);
        dataTable.setName("biz_031_TBRFManageInfo_DHY");
        return dataTable;
    }

    //消防表
    public DataTable XF(String row_guid) {
        String sql = "select Row_Guid as guid ,RecordGuid as rowguid ,SETADDRESS,CAPACITY,TBSETTYPEDIC.SETTYPENAME SETTYPE,TBCCTYPEDIC.SETTYPENAME CCTYPE,GCCSUBNAME,RESERVES,DCCSUBNAME, " +
                "(select GROUP_CONCAT(BWCLTYPENAME SEPARATOR ',') from TBBWCLTYPEDIC b where  FIND_IN_SET(b.BWCLTYPENUM,TBXFManageInfo.BWCLTYPE)) as BWCLTYPE, " +
                "BWFLOORCOUNT,BWNATUREUSE,BWOLDUSE, " +
                "(select GROUP_CONCAT(ZXPLACENAME SEPARATOR ',') from TBZXPLACEDIC b where  FIND_IN_SET(b.ZXPLACENUM,TBXFManageInfo.ZXPLACE))ZXPLACE, " +
                "ZXAREA,ZXFFLOORCOUNT,ZXNATUREUSE,ZXOLDUSE, " +
                "(select GROUP_CONCAT(USETYPENAME SEPARATOR ',') from TBUSETYPEDIC b where  FIND_IN_SET(b.USETYPENUM,TBXFManageInfo.BIGINTSITES))BIGINTSITES, " +
                "(select GROUP_CONCAT(USETYPENAME SEPARATOR ',') from TBUSETYPEDIC b where  FIND_IN_SET(b.USETYPENUM,TBXFManageInfo.QTSPECPROJECT))QTSPECPROJECT, " +
                "(select GROUP_CONCAT(XFFACILITIESNAME SEPARATOR ',') from TBXFFACILITIESDIC b where  FIND_IN_SET(b.XFFACILITIESNUM,TBXFManageInfo.XFFACILITIES))XFFACILITIES,QTMARK " +
                " from TBXFManageInfo " +
                "left join TBSETTYPEDIC on TBSETTYPEDIC.SETTYPENUM=TBXFManageInfo.SETTYPE " +
                "left join TBCCTYPEDIC on TBCCTYPEDIC.SETTYPENUM=TBXFManageInfo.CCTYPE " +
                "where RecordGuid = @row_guid";

        HashMap<String, Object> params = new HashMap<>();
        params.put("row_guid", row_guid);
        DataTable dataTable = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, "7WNqoV8ZmE0", params);
        dataTable.setName("biz_031_TBXFManageInfo_DHY");
        return dataTable;
    }

    //审核意见表
    public DataTable SHYJ(String row_guid) {
        String sql = "select * from (select RecordGuid as rowguid,'准予受理'`Status`,(select tbadminuser.UserName from tbadminuser where  " +
                "tbadminuser.LoginName=tbrecordopinioninfo_New.OpinionUser)OpinionUser,DATE_FORMAT(OpinionDateTime,'%Y-%m-%d %H:%i:%S') OpinionDateTime,Opinion  " +
                "from tbrecordopinioninfo_New where RecordGuid ='{0}' and `Status`=106 order by OpinionDateTime desc limit 1) a " +
                "union all " +
                "select * from (select RecordGuid as rowguid,'审批通过'`Status`,(select tbadminuser.UserName from tbadminuser where  " +
                "tbadminuser.LoginName=tbrecordopinioninfo_New.OpinionUser)OpinionUser,DATE_FORMAT(OpinionDateTime,'%Y-%m-%d %H:%i:%S') OpinionDateTime,Opinion  " +
                "from tbrecordopinioninfo_New where RecordGuid ='{0}' and `Status`=104 order by OpinionDateTime desc limit 1) b; ";

        HashMap<String, Object> params = new HashMap<>();
        params.put("row_guid", row_guid);
        DataTable dataTable = super.executeSqlForDataTable(Constants.SGXK_MYSQL_GUID, sql, "OPBBD6G4mb0", params);
        dataTable.setName("biz_031_tbrecordopinioninfo_DHY");
        return dataTable;
    }

    public int recordError(String row_guid) {
        String sql = "INSERT INTO pushsgxktosjcerror(RecordGuid, push_time) VALUES (@row_guid, @push_time);";
        HashMap<String, Object> params = new HashMap<>();
        params.put("row_guid", row_guid);
        params.put("push_time", new Date());
        return super.executeSqlForCount(Constants.SGXK_MYSQL_GUID, sql, "KUM9lKkZrUXTHLN0zFQ", params);
    }

}
