package cn.ecasoft.dataexchange.entity.db;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Table;

/**
 * @author xuliukai
 * @since 2023/9/11 10:40
 */
@Setter
@Getter
@Table("tbbuilderlicencemanage_new_changedetails")
public class SGXKBGDetails {

    private String ToRowGuid;
    private String FiledName;
    private String OldValue;
    private String NewValue;
    private String DataFilled;

}
