package cn.ecasoft.dataexchange.common.utils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author xuliukai
 */
public class CaseInsensitiveBeanUtils {

    /**
     * 将 source 内的字段赋值给target，忽略大小写
     *
     * @param source 数据源对象
     * @param target 目标对象
     */
    public static void copyProperties(Object source, Object target) throws IllegalAccessException {
        Field[] destFields = target.getClass().getDeclaredFields();
        //获取父类的，这边只获取一级，需要获取多级的话，需要重写方法
        Field[] superFields = source.getClass().getSuperclass().getDeclaredFields();
        Field[] declaredFields = source.getClass().getDeclaredFields();
        Map<String, Field> sourceFieldMap = Stream.concat(Arrays.stream(superFields), Arrays.stream(declaredFields))
                .collect(Collectors.toMap(f -> f.getName().toLowerCase(), f -> f));

        for (Field destField : destFields) {
            Field sourceField = sourceFieldMap.get(destField.getName().toLowerCase());
            if (sourceField != null && sourceField.getType() == destField.getType()) {
                sourceField.setAccessible(true);
                destField.setAccessible(true);
                destField.set(target, sourceField.get(source));
            }
        }
    }

}
