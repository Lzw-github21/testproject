package cn.ecasoft.dataexchange.common.utils;

import cn.ecasoft.basic.datatable.DataColumn;
import cn.ecasoft.basic.datatable.DataRow;
import cn.ecasoft.basic.datatable.DataSet;
import cn.ecasoft.basic.datatable.DataTable;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;

/**
 * @author xuliukai
 * @since 2023/9/15 10:44
 */
public class ChangeJson {

    //将json字符串转化为DataSet,但是这个方法很局限，如果这个json即存在jsonobejct有存在jsonarray，jsonarray就会被转化为jsonobejct
    public static DataSet changeVoToDataSet(String voStr) {
        JSONObject jo = JSONObject.parseObject(voStr);
        DataSet dataSet = new DataSet();
        jo.forEach((k, v) -> {
            DataTable dataTable = new DataTable();
            dataTable.setName(k);
            //如果将str解析成jsonArray出现问题的话，就说明不是一个json，可能是一个object
            try {
                JSONArray array = JSON.parseArray(v.toString());
                for (Object o : array) {
                    DataRow dataRow = JSONObject.parseObject(JSONObject.toJSONString(o), DataRow.class);
                    dataTable.add(dataRow);
                }
            } catch (Exception e) {
                DataRow dataRow = JSONObject.parseObject(v.toString(), DataRow.class);
                dataTable.add(dataRow);
            }

            //列的相关信息
            HashMap<String, DataColumn> columns = new HashMap<>();
            if (!dataTable.isEmpty()) {
                //这个不能用getFirst()，档dataTable为空的时候会出现问题
                DataRow first = dataTable.get(0);
                if (first != null && !first.isEmpty()) {
                    first.forEach((coK, coV) -> {
                        DataColumn c = new DataColumn();
                        c.setColumnName(coK);
                        columns.put(coK, c);
                    });
                }
            }

            dataTable.setColumns(columns);
            dataSet.put(k, dataTable);
        });
        return dataSet;
    }
}
