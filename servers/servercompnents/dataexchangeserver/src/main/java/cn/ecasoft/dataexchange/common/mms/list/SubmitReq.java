package cn.ecasoft.dataexchange.common.mms.list;

import java.util.List;

/**
 * @description: 移动短信
 * @author: 杨攀  2020/8/12 16:51
 */
public class SubmitReq {
    private String apId;
    private String apReqId;
    private List<String> mobiles;
    private String content;
    private String srcId;
    private String serviceId;
    private boolean regReport;
    private String templateId;
    private String ecName;
    private String mac;

    public SubmitReq() {
    }

    public String getApId() {
        return this.apId;
    }

    public void setApId(String apId) {
        this.apId = apId;
    }

    public String getApReqId() {
        return this.apReqId;
    }

    public void setApReqId(String apReqId) {
        this.apReqId = apReqId;
    }

    public List<String> getMobiles() {
        return this.mobiles;
    }

    public void setMobiles(List<String> mobiles) {
        this.mobiles = mobiles;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSrcId() {
        return this.srcId;
    }

    public void setSrcId(String srcId) {
        this.srcId = srcId;
    }

    public String getServiceId() {
        return this.serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public boolean isRegReport() {
        return this.regReport;
    }

    public void setRegReport(boolean regReport) {
        this.regReport = regReport;
    }

    public String getTemplateId() {
        return this.templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getEcName() {
        return this.ecName;
    }

    public void setEcName(String ecName) {
        this.ecName = ecName;
    }

    public String getMac() {
        return this.mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}
