package cn.ecasoft.dataexchange.services.impl;

import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.common.utils.FileUtils;
import cn.ecasoft.dataexchange.common.utils.OssFileStorage;
import cn.ecasoft.dataexchange.entity.db.SysEcafiles;
import cn.ecasoft.dataexchange.entity.model.FileInfo;
import cn.ecasoft.dataexchange.mapper.OFDWebMapper;
import cn.ecasoft.dataexchange.mapper.OSSMapper;
import cn.ecasoft.dataexchange.services.OFDWebService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import static cn.ecasoft.dataexchange.common.utils.Constants.*;

/**
 * @author XuLiuKai
 * @Desc 从建设厅那里提供 生成施工许可电子签章
 */
@Service
public class OFDWebServiceImpl implements OFDWebService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final OssFileStorage OUR_OSS = new OssFileStorage(OSS_ACCESS_KEY, OSS_SECRET_KEY, OSS_END_POINT);

    @Resource
    private OSSMapper ossMapper;
    @Resource
    private OFDWebMapper ofdWebMapper;


    public RetMsgUtil<Object> getOFDFile(String zb, String fb, String qrCodeBase64Content,
                                         String sealCode, String row_guid) throws Exception {
        String result = this.PostFormData(zb, fb, qrCodeBase64Content, sealCode);
        logger.error("result--->" + result);
        // 将字符串转换为 JSONObject
        JSONObject root = JSON.parseObject(result);
        if (root.getString("code").equals("00")) {
            RetMsgUtil<Object> re = RetMsgUtil.ok();
            HashMap<String, Object> data = new HashMap<>();
            data.put("filebase", this.uploadOFD(root.getString("msg"), row_guid));
            re.setData(data);
            return re;
        } else {
            return RetMsgUtil.fail("从数据源获取数据失败");
        }
    }

    @Override
    public RetMsgUtil<Object> getOSSParams() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("getEndPoint", OUR_OSS.getEndPoint());
        hashMap.put("getAccessKey", OUR_OSS.getAccessKey());
        hashMap.put("getSecretKey", OUR_OSS.getSecretKey());
        return RetMsgUtil.ok(hashMap);
    }

    /**
     * 加载OFD文件
     * 在c# 代码中，这个方法会保存文件到本地
     * 现在不保存在本地了，都保存在oss上了。
     *
     * @param Url 路径
     * @return
     */
    private String uploadOFD(String Url, String RowGuid) throws Exception {
        FileInfo fileInfo = FileUtils.getFileInfo(Url, true);
        DataTable dataTable = ofdWebMapper.getInfoByRecordGuid(RowGuid);
        if (dataTable == null || dataTable.isEmpty()) {
            return "通过备案表主键获取项目代码出现错误";
        }
        String PrjCode = dataTable.get(0).getString("PrjCode");
        String ECertID = dataTable.get(0).getString("ECertID");
        String unique_num = dataTable.get(0).getString("UniqueNum");
        String sgxkguid = dataTable.get(0).getString("sgxkguid");
        String virtualPath;
        String actualName;

        virtualPath = "oss/upload/ScanImageList/SGXKDZZS/" + PrjCode + "/" + unique_num;
        actualName = ECertID + ".OFD";

        //1的话代表 新申请，2的话代表 变更 c#原文逻辑中，新申请和变更的推送文件不是同一个，
        //但是我在翻新的时候，发现变更推送的时候推送的是pdf文件，但是新申请的时候确实ofd
        //变更生成文件的规则也是不一样，这也导致了，推送电子证照库的时候，没有将变更之后的文件正确的推送给电子证照库
        //这里原本要做区分的，但是后来还是没有作区分，因为一个项目只有一本施工许可证书
//        if ("1".equals(type)) {
//            virtualPath = "oss/upload/ScanImageList/SGXKDZZS/" + PrjCode + "/" + unique_num;
//            actualName = ECertID + ".OFD";
//        } else {
//            virtualPath = "oss/upload/ScanImageList/SGXKDZZS/" + PrjCode + "/" + unique_num;
//            actualName = sgxkguid + "_SGXKDZZS.pdf";
//        }

        String objectName = virtualPath + "/" + actualName;
        if (!OUR_OSS.upload(OSS_BUCKET_NAME, objectName, fileInfo.getFileByte())) {
            return "上传失败";
        }
        //上传成功之后，我们需要保存到数据库中
        SysEcafiles sysEcafiles = new SysEcafiles();
        sysEcafiles.setCREATEDATE(new Date());
        sysEcafiles.setFILENAME(fileInfo.getFileName());
        sysEcafiles.setFILETYPE(fileInfo.getFileExtension());
        sysEcafiles.setFILESIZE(fileInfo.getSize());
        sysEcafiles.setRow_Guid(UUID.randomUUID().toString());
        sysEcafiles.setBUCKETNAME(OSS_BUCKET_NAME);
        sysEcafiles.setGROUPGUID("25dbcecf-d3e5-4f0c-a75a-f614c23aa10f");
        sysEcafiles.setTO_ROW_GUID(RowGuid);
        sysEcafiles.setCREATEUSERID(-1);
        sysEcafiles.setVIRTUALPATH(virtualPath);
        sysEcafiles.setACTUALNAME(actualName);
        ossMapper.insertOss(sysEcafiles);

        return objectName;
    }


    //从建设厅获取电子证照库，同一组数据可以获取多次，并且建设厅不会对此进行校验
    private String PostFormData(String zb, String fb, String qrCodeBase64Content, String sealCode) throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://10.142.4.170:8096/license/getJzsg");
        httpPost.addHeader("Content-Type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");

        String body = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"zb\"\r\n\r\n" + zb +
                "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"fb\"\r\n\r\n" + fb +
                "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"sealCode\"\r\n\r\n" + sealCode +
                "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"qrCodeBase64Content\"\r\n\r\n" + qrCodeBase64Content +
                "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--";

        HttpEntity entity = new StringEntity(body, StandardCharsets.UTF_8);
        httpPost.setEntity(entity);

        try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
            HttpEntity responseEntity = response.getEntity();
            return EntityUtils.toString(responseEntity);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
