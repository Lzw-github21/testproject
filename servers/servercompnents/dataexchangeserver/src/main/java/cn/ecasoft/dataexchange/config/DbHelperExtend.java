package cn.ecasoft.dataexchange.config;

import cn.ecasoft.configproperties.EcaSqlsConfig;
import cn.ecasoft.dataexchange.entity.enums.SqlRepos;
import cn.ecasoft.utils.Impls.DBhelperImpl;
import org.apache.commons.lang3.ObjectUtils;
import org.nutz.dao.Dao;
import org.nutz.dao.QueryResult;
import org.nutz.dao.Sqls;
import org.nutz.dao.entity.Record;
import org.nutz.dao.pager.Pager;
import org.nutz.dao.sql.Sql;
import org.nutz.dao.sql.SqlCallback;
import org.nutz.dao.util.Daos;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Dee
 * @date 2023/7/10
 * <p>Description:
 */
@Service
public class DbHelperExtend {
    @Resource
    private EcaSqlsConfig ecaSqlsConfig;

    @Resource
    private DBhelperImpl dBhelper;
    @PostConstruct
    public void loadingSqlMap() {
        Map<String, String> ecaSqlExtend = Arrays
                .stream(SqlRepos.values())
                .distinct()
                .collect(Collectors.toMap(Enum::name, en -> en.sql));
        ecaSqlsConfig.getMap().putAll(ecaSqlExtend);
    }

    public Dao getDao(String datasourceGuid) {
        try {
            return dBhelper.getDao(datasourceGuid, false);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public QueryResult pageList(String datasourceGuid, Sql sql) {
        return this.pageList(datasourceGuid, sql, 1, 20, Sqls.callback.records(), Record.class);
    }

    public QueryResult pageList(String datasourceGuid, Sql sql, int current, int size) {
        return this.pageList(datasourceGuid, sql, current, size, Sqls.callback.records(), Record.class);
    }

    public <T> QueryResult pageList(String datasourceGuid, Sql sql, SqlCallback callback, Class<T> clazz) {
        return this.pageList(datasourceGuid, sql, 1, 20, callback, clazz);
    }

    public <T> QueryResult pageList(String datasourceGuid, Sql sql, int current, int size, SqlCallback callback, Class<T> clazz) {
        //初始化数据源
        Dao dao = this.getDao(datasourceGuid);
        //设置分页
        Pager pager = dao.createPager(current, size);
        sql.setPager(pager);
        //回调类型
        sql.setCallback(ObjectUtils.isNotEmpty(callback)?callback:Sqls.callback.records());
        //实体设置
        sql.setEntity(dao.getEntity(clazz));
        //执行sql
        try {
            dao.execute(sql);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        List<T> list = sql.getList(clazz);

        long count = Daos.queryCount(dao, sql);
        pager.setRecordCount((int) count);
        return new QueryResult(list, pager);
    }

    public int queryInt(String datasourceGuid, Sql sql) {
        Dao dao = this.getDao(datasourceGuid);
        sql.setCallback(Sqls.callback.integer());
        try {
            dao.execute(sql);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return sql.getUpdateCount();
    }

    public Record queryOne(String datasourceGuid, Sql sql) {
        return this.queryOne(datasourceGuid, sql, Sqls.callback.record(), Record.class);
    }

    public <T> T queryOne(String datasourceGuid, Sql sql, SqlCallback callback, Class<T> clazz) {
        //初始化数据源
        Dao dao = this.getDao(datasourceGuid);
        //回调类型
        sql.setCallback(ObjectUtils.isNotEmpty(callback)?callback:Sqls.callback.record());
        //实体设置
        sql.setEntity(dao.getEntity(clazz));
        //执行sql
        try {
            dao.execute(sql);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return sql.getObject(clazz);
    }

    public List<Record> queryList(String datasourceGuid, Sql sql) {
        return this.queryList(datasourceGuid, sql, Sqls.callback.records(), Record.class);
    }

    public <T> List<T> queryList(String datasourceGuid, Sql sql, SqlCallback callback, Class<T> clazz) {
        //初始化数据源
        Dao dao = this.getDao(datasourceGuid);
        //回调类型
        sql.setCallback(ObjectUtils.isNotEmpty(callback)?callback:Sqls.callback.records());
        //实体设置
        sql.setEntity(dao.getEntity(clazz));
        //执行sql
        try {
            dao.execute(sql);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return sql.getList(clazz);
    }

    public int queryCount(String datasourceGuid, Sql sql) {
        //初始化数据源
        Dao dao = this.getDao(datasourceGuid);
        //回调类型
        sql.setCallback(Sqls.callback.integer());
        try {
            dao.execute(sql);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return sql.getInt();
    }

    public QueryResult queryResult(String datasourceGuid, Sql sql, Sql countSql, int current, int size) {
        List<Record> records = this.queryList(datasourceGuid, sql);
        int count = this.queryCount(datasourceGuid, countSql);
        Pager pager = new Pager(current, size);
        pager.setRecordCount(count);
        return new QueryResult(records,pager);
    }

    public Integer GetPkValue(String datasourceguid, String tableName, String fieldName) {
        Sql sql = Sqls.create("call GetPKValue (@tablename,@fieldname,@OUTpkvalue,@ds_id)");
        sql.params().set("tablename",tableName).set("fieldname",fieldName).set("OUTpkvalue",0).set("ds_id","1");
        Dao dao = this.getDao(datasourceguid);
        try {
            dao.execute(sql);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        Record outParams = sql.getOutParams();
        return outParams.getInt("pkvalue");
    }
}
