package cn.ecasoft.dataexchange.services.impl;

import cn.ecasoft.basic.datatable.DataRow;
import cn.ecasoft.basic.datatable.DataSet;
import cn.ecasoft.dataexchange.common.Ret.RetMsgUtil;
import cn.ecasoft.dataexchange.common.Ret.State;
import cn.ecasoft.dataexchange.common.utils.CaseInsensitiveBeanUtils;
import cn.ecasoft.dataexchange.common.utils.ChangeJson;
import cn.ecasoft.dataexchange.entity.db.*;
import cn.ecasoft.dataexchange.entity.dto.*;
import cn.ecasoft.dataexchange.entity.enums.AnnexMapping;
import cn.ecasoft.dataexchange.entity.model.LockInfoModel;
import cn.ecasoft.dataexchange.mapper.SGXKMapper;
import cn.ecasoft.dataexchange.services.LockHelper;
import cn.ecasoft.dataexchange.services.PushToCXPTService;
import org.apache.commons.lang3.StringUtils;
import org.nutz.dao.entity.Record;
import org.nutz.trans.Trans;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author XuLiuKai
 */
@Service
public class PushToCXPTServiceImpl implements PushToCXPTService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private SGXKMapper sgxkMapper;
    @Resource
    private LockHelper lockHelper;

    /**
     * 读取人员的状态
     *
     * @param CorpCode
     * @param strIDCard
     * @param strErrMsg
     * @return 1:已调离 0:在职 -1:不存在 -2:失败 11:锁定
     */
    public RetMsgUtil<Object> ReadPersonnelLeaveStatus(String CorpCode, String strIDCard, String strErrMsg) {
        Map<String, Integer> data = new HashMap<>();
        RetMsgUtil<Object> result = new RetMsgUtil<>();
        String CorpCode10 = "";
        int userStatus = 0;
        String isOutCase;

        if (StringUtils.isNotBlank(CorpCode)) {
            //91330500737796891U   73779689-1
            if (CorpCode.length() == 18) {
                CorpCode10 = CorpCode.substring(8, 16) + "-" + CorpCode.substring(16, 17);
            } else {
                CorpCode10 = CorpCode;
            }
        }
        //查询省内省外结果，有数据就是省内，没数据就是省外
        List<Record> records = sgxkMapper.getTbRecordInfoByCorpCode(CorpCode, CorpCode10);
        if (records.size() > 0) {
            //省内
            isOutCase = "0";
        } else {
            //省外
            isOutCase = "1";
        }
        //读取人员的状态
        try {
            if ("0".equals(isOutCase)) {
                //在省内人员
                List<Record> tbUserInfoRowGuidByIdCard = sgxkMapper.getTbUserInfoRowGuidByIdCard(strIDCard, CorpCode, CorpCode10);
                if (tbUserInfoRowGuidByIdCard != null && tbUserInfoRowGuidByIdCard.size() > 0) {
                    List<Record> status = sgxkMapper.getTbUserInfoStatusByIdCard(strIDCard, CorpCode, CorpCode10);
                    if (status != null && status.size() > 0) {
                        userStatus = Integer.parseInt(status.get(0).get("isexist").toString());
                        userStatus = (userStatus == 1) ? 0 : 1;
                    }
                } else {
                    userStatus = 0;
                }
            } else {
                List<Record> sw = sgxkMapper.getTbUserInfoRowGuidByIdCardSW(strIDCard, CorpCode, CorpCode10);
                if (sw != null && sw.size() > 0) {
                    List<Record> status = sgxkMapper.getTbUserInfoStatusByIdCardSW(strIDCard, CorpCode, CorpCode10);
                    if (status != null && status.size() > 0) {
                        userStatus = Integer.parseInt(status.get(0).get("isexist").toString());
                        userStatus = (userStatus == 1) ? 0 : 1;
                    } else {
                        userStatus = 0;
                    }
                } else {
                    userStatus = -1;
                }
            }

            if (userStatus == 0) {
                //判断是否锁定
                List<Record> isLock = sgxkMapper.getTbUserInfoIsLock(strIDCard);
                if (isLock != null && isLock.size() > 0) {
                    result.setMsg("账号被锁定");
                    data.put("userStatus", 11);
                    result.setData(data);
                    return result;
                }
            }
        } catch (Exception e) {
            result.setMsg("失败");
            data.put("userStatus", -2);
            result.setData(data);
            return result;
        }
        data.put("userStatus", userStatus);
        result.setData(data);
        return result;
    }

    /**
     * 衢州获取施工许可证编号/项目编号
     * <param name="xzqhnum">行政区划代码</param>
     * <param name="getdate">当前的日期 格式是yyyy-MM-dd (必须为今天的日期。与服务器当前日期做比较)</param>
     * <param name="type">类型（1为获取项目编号，2为获取施工许可编号）</param>
     * <param name="xmfl">项目分类（参考项目分类字典表:01,02,03）</param>
     * <param name="strErrMsg"></param>
     * <returns></returns>
     */
    public RetMsgUtil<Object> getxmnum(String xzqhnum, String getdate, String type, String xmfl) {
        RetMsgUtil<Object> result = new RetMsgUtil<>();
        String num;
        String BH = "";

        if (xzqhnum.contains("3308")) {
            List<Record> record1 = sgxkMapper.getTbxzqdmdicInfoByClass3ID("330800");
            String s = record1.get(0).get("adminareaclassid").toString();
            if (s == null || "".equals(s) || !xzqhnum.equals(s)) {
                xzqhnum = "330801";
            }
        } else if (xzqhnum.contains("3309")) {
            List<Record> record1 = sgxkMapper.getTbxzqdmdicInfoByClass3ID("330900");
            String s = record1.get(0).get("adminareaclassid").toString();
            if (s == null || "".equals(s) || !xzqhnum.equals(s)) {
                xzqhnum = "330901";
            }
        } else if (xzqhnum.contains("3301")) {
            List<Record> record1 = sgxkMapper.getTbxzqdmdicInfoByClass3ID("330100");
            String s = record1.get(0).get("adminareaclassid").toString();
            if (s == null || "".equals(s) || !xzqhnum.equals(s)) {
                xzqhnum = "330101";
            }
        } else if (xzqhnum.contains("3302")) {
            List<Record> record1 = sgxkMapper.getTbxzqdmdicInfoByClass3ID("330200");
            String s = record1.get(0).get("adminareaclassid").toString();
            if (s == null || "".equals(s) || !xzqhnum.equals(s)) {
                xzqhnum = "330201";
            }
        } else if (xzqhnum.contains("3303")) {
            List<Record> record1 = sgxkMapper.getTbxzqdmdicInfoByClass3ID("330300");
            String s = record1.get(0).get("adminareaclassid").toString();
            if (s == null || "".equals(s) || !xzqhnum.equals(s)) {
                xzqhnum = "330301";
            }
        } else if (xzqhnum.contains("3304")) {
            List<Record> record1 = sgxkMapper.getTbxzqdmdicInfoByClass3ID("330400");
            String s = record1.get(0).get("adminareaclassid").toString();
            if (s == null || "".equals(s) || !xzqhnum.equals(s)) {
                xzqhnum = "330401";
            }

        } else if (xzqhnum.contains("3305")) {
            List<Record> record1 = sgxkMapper.getTbxzqdmdicInfoByClass3ID("330500");
            String s = record1.get(0).get("adminareaclassid").toString();
            if (s == null || "".equals(s) || !xzqhnum.equals(s)) {
                xzqhnum = "330501";
            }
        } else if (xzqhnum.contains("3306")) {
            List<Record> record1 = sgxkMapper.getTbxzqdmdicInfoByClass3ID("330600");
            String s = record1.get(0).get("adminareaclassid").toString();
            if (s == null || "".equals(s) || !xzqhnum.equals(s)) {
                xzqhnum = "330601";
            }
        } else if (xzqhnum.contains("3307")) {
            List<Record> record1 = sgxkMapper.getTbxzqdmdicInfoByClass3ID("330700");
            String s = record1.get(0).get("adminareaclassid").toString();
            if (s == null || "".equals(s) || !xzqhnum.equals(s)) {
                xzqhnum = "330701";
            }
        } else if (xzqhnum.contains("3310")) {
            List<Record> record1 = sgxkMapper.getTbxzqdmdicInfoByClass3ID("331000");
            String s = record1.get(0).get("adminareaclassid").toString();
            if (s == null || "".equals(s) || !xzqhnum.equals(s)) {
                xzqhnum = "331001";
            }
        } else if (xzqhnum.contains("3311")) {
            List<Record> record1 = sgxkMapper.getTbxzqdmdicInfoByClass3ID("331100");
            String s = record1.get(0).get("adminareaclassid").toString();
            if (s == null || "".equals(s) || !xzqhnum.equals(s)) {
                xzqhnum = "331101";
            }
        } else {
            if (!xzqhnum.equals("330190")) {
                String num1 = xzqhnum.substring(0, 4) + "00";
                List<Record> record1 = sgxkMapper.getTbxzqdmdicInfoByClass3ID(num1);
                String s = record1.get(0).get("adminareaclassid").toString();
                if (s == null || "".equals(s) || !xzqhnum.equals(s)) {
                    result.setMsg("参数有误");
                    return result;
                }
            }
        }
        if ("2".equals(type)) {
            List<Record> nums = sgxkMapper.getNum(xzqhnum + getdate.replace(" - ", ""));
            if (nums.size() > 0) {
                int i = Integer.parseInt(nums.get(0).get("num").toString()) + 1;
                num = String.format("%02d", i);
            } else {
                num = "01";
            }
            BH = xzqhnum + getdate.replace("-", "") + num + xmfl;
            sgxkMapper.insertTbbuilderlicencemanagenum_nb(BH);
        } else if ("1".equals(type)) {
            List<Record> dt = sgxkMapper.getPrjnum(xzqhnum + getdate.replace("-", "").substring(2) + xmfl);
            if (dt.size() > 0) {
                int i = Integer.parseInt(dt.get(0).get("prjnum").toString()) + 1;
                num = String.format("%02d", i);
            } else {
                num = "01";
            }
            BH = xzqhnum + getdate.replace("-", "").substring(2) + xmfl + num;
            sgxkMapper.insertTbprojectnum_nb(BH);
        }

        result.setData(BH);
        return result;
    }

    public RetMsgUtil<Object> CancelSgxk_QZ(CancelOptionDto dto) {
        String strErrMsg = "";

        try {//操作类型 1：注销 2：延期  3：中止  4：恢复 5:中止
            TBBuilderLicenceManageOption table = dto.getTbbuilderlicencemanage_option().get(0);
            String cityNum = dto.getCityNum();
            String guid = table.getSgxkGuid();//施工许可主键
            Trans.exec(() -> {

                String prjName;
                String corpcode = "";

                String optionType = table.getOptionType();
                if (StringUtils.isBlank(optionType) || "0".equals(optionType)) {
                    throw new RuntimeException("参数错误");
                }
                String OptionCase = table.getOptionCase();//原因
                if (StringUtils.isAnyBlank(OptionCase, guid)) {
                    throw new RuntimeException("参数错误");
                }

                boolean jl2, zj1, jl_lock2 = true, zj_lock1 = true;//锁定
                List<Record> dt = sgxkMapper.getDataFromTBBUILDERLICENCEMANAGEByFilter(guid, optionType);

                //判断是否存在数据
                if (dt == null || dt.size() < 1) {
                    throw new RuntimeException("未找到相应的未竣工施工许可数据信息");
                }

                String sgxkGuid = dt.get(0).getString("row_guid");//施工许可guid
                prjName = dt.get(0).getString("PRJNAME");//tbcontractrecordmanag
                StringBuilder strConsCorpCode = new StringBuilder();
                int CancelType = 1;

                //四方主体 特殊处理
                List<Record> dtsf = sgxkMapper.getDataFromTBBUILDERLICENCEMANAGEByFilter(sgxkGuid);
                //注销通过的,延期，终止，解锁相关人员
                if ("1".equals(optionType) || "2".equals(optionType) || "3".equals(optionType)) {
                    String yy;
                    int TypeState = 4;//延期注销中止
                    if ("1".equals(optionType)) {
                        yy = "施工许可注销";
                    } else if ("2".equals(optionType)) {
                        yy = "施工许可延期";
                    } else {
                        yy = "施工许可中止";
                    }
                    LockInfoModel model = new LockInfoModel();
                    model.ManageAdminAreaNum = dt.get(0).getString("ManageAdminAreaNum");
                    model.ManageCityNum = cityNum;//登录人所在市
                    model.ManageCountyNum = "-1";//登录人所在区县

                    for (Record item : dtsf) {
                        //施工 经理
                        if ("3".equals(item.getString("CorpTypeNum"))) {
                            strConsCorpCode.append(";").append(item.getString("CorpCode"));
                            model.CorpName = item.getString("CorpName");//企业名称
                            model.PersonName = item.getString("CorpLeader");
                            jl2 = lockHelper.Un_LockInfo(item.getString("CorpLeaderCardNum"), sgxkGuid, cityNum, cityNum, yy, "1", prjName, item.getString("CorpCode"), TypeState, model, strErrMsg).getResult();//解锁旧的
                            if (!jl2) {
                                throw new RuntimeException("操作失败,optionType：1、2、3，CorpTypeNum：3，解锁失败");
                            }
                        }
                        //监理 总监
                        if ("4".equals(item.getString("CorpTypeNum"))) {
                            model.CorpName = item.getString("CorpName");//企业名称
                            model.PersonName = item.getString("CorpLeader");
                            //人员姓名
                            //解锁旧的
                            zj1 = lockHelper.Un_LockInfo(item.getString("CorpLeaderCardNum"), sgxkGuid, cityNum, cityNum, yy, "2", prjName, corpcode, TypeState, model, strErrMsg).getResult();
                            if (!zj1) {
                                throw new RuntimeException("操作失败,optionType：1、2、3，CorpTypeNum：4，解锁失败");
                            }
                        }
                    }
                } else if ("4".equals(optionType)) {
                    //恢复施工许可证时锁定相关人员。
                    logger.info("3.0注销延期废止,施工许可恢复" + guid);
                    //判断合同是否已经注销
                    List<Record> dtht = sgxkMapper.determineContractIsExpired(dt.get(0).getString("HTGuid"));
                    if (dtht != null && dtht.size() > 0) {
                        throw new RuntimeException("该施工许可对应的合同信息已经注销/废止！");
                    }
                    CancelType = 1;
                    int TypeState = 8;//恢复施工
                    String LockMark = "恢复施工";
                    LockInfoModel model = new LockInfoModel();
                    model.ManageAdminAreaNum = dt.get(0).getString("ManageAdminAreaNum");
                    model.CityNum = dt.get(0).getString("CityNum");
                    model.CountyNum = dt.get(0).getString("CountyNum");
                    model.ManageCityNum = cityNum;//登录人所在市
                    model.ManageCountyNum = "-1";//登录人所在区县
                    logger.info("3.0注销延期废止,施工许可恢复" + guid);
                    for (Record item : dtsf) {
                        //施工 经理
                        if ("3".equals(item.getString("CorpTypeNum"))) {
                            //先检测变更申请中所选的项目经理是否已经被锁定(宁波之前未判断（施工负责人1，其他的未做判断）)
                            boolean b = lockHelper.HaveNumProject("1", item.getString("CorpLeaderCardNum"), item.getString("CorpLeader"), strErrMsg).getResult();
                            if (!b) {
                                throw new RuntimeException("变更申请中所选的项目经理被锁定（施工经理）");
                            }
                            strConsCorpCode.append(";").append(item.getString("CorpCode"));
                            model.CorpName = item.getString("CorpName");//企业名称
                            //  model.PersonName = changeTable.get(0).getString("CONSCORPLEADER1");//人员姓名
                            model.PersonName = item.getString("CorpLeader");
                            //锁定
                            jl_lock2 = lockHelper.LockInfo(item.getString("CorpLeaderCardNum"), sgxkGuid, cityNum, cityNum, LockMark, "1", prjName, item.getString("CorpCode"), TypeState, model, strErrMsg).getResult();
                            if (!jl_lock2) {
                                throw new RuntimeException("变更申请中所选的项目经理解锁失败");
                            }
                        }
                        //监理 总监
                        if ("4".equals(item.getString("CorpTypeNum"))) {
                            //首先检测项目总监是否已被锁定
                            boolean b = lockHelper.HaveNumProject("2", item.getString("CorpLeaderCardNum"), item.getString("CorpLeader"), strErrMsg).getResult();
                            if (!b) {
                                throw new RuntimeException("首先检测项目总监是否已被锁定");
                            }
                            model.CorpName = item.getString("CorpName");//企业名称
                            model.PersonName = item.getString("CorpLeader");//人员姓名
                            zj_lock1 = lockHelper.LockInfo(item.getString("CorpLeaderCardNum"), sgxkGuid, cityNum, cityNum, LockMark, "2", prjName, item.getString("CorpCode"), TypeState, model, strErrMsg).getResult();//锁定新的
                            if (!zj_lock1) {
                                throw new RuntimeException("变更申请中所选的项目经理解锁失败（监理总监）");
                            }
                        }
                    }
                }
                //更新数据
                sgxkMapper.updateOptionTypeByRowGuid(optionType, sgxkGuid);
                //删除数据
                sgxkMapper.deleteOptionBySgxkGuidAndAuditDate(table.getAuditDate().toString(), sgxkGuid);
                logger.info("3.0注销延期废止" + guid + "删除成功");
                String rowGuid = UUID.randomUUID().toString().replace("-", "");
                //插入数据
                table.setAuditStatus(3);
                table.setRow_Guid(rowGuid);
                table.setConsCorpCode(strConsCorpCode.toString().replaceAll("^;", ""));
                sgxkMapper.insertTable(table, "tbbuilderlicencemanage_option");
                logger.info("3.0注销延期废止" + guid + "执行完成");
            });
        } catch (Exception e) {
            e.printStackTrace();
            return RetMsgUtil.fail(State.OPERATION_FAILED.getCode(), e.getMessage());
        }
        return RetMsgUtil.ok(State.OPERATE_SUCCESS.getCode(), State.OPERATE_SUCCESS.getMsg());
    }

    @Override
    public RetMsgUtil<Object> InsertTSSKBGInfo_New(TSSGXKBGDto dto) {
        logger.info("----->service1");
        try {
            Trans.exec(() -> {
                String CityType = dto.getCityType();
                String[] strErrMsg = {""};

                String oldXMZJcorpcode = "";//原项目总监所在企业组织机构代码
                int TypeState = 12;//宁波施工许可变更接口
                AtomicBoolean[] jl2 = {new AtomicBoolean(true)};
                boolean[] zj1 = {true};
                boolean[] jl_lock2 = {true};
                boolean[] zj_lock1 = {true};
                String prjname = "";//项目名称

                TBBuilderLicenceManageChangeDto changeTable = dto.getTbbuilderlicencemanage_change().get(0);

                String sgxkGuid = changeTable.getGuid();
                String RowGuid = changeTable.getRowGuid();
                String PrjNum = changeTable.getPrjNum();//项目编号
                //项目guid
                String PrjGuid = changeTable.getPrjGuid();
                String changeGuid = UUID.randomUUID().toString();

                Integer c = sgxkMapper.getTbbuilderlicencemanage_changeCountByRowGuid(RowGuid);
                if (c > 0) {
                    throw new RuntimeException(changeTable.getRowGuid() + "施工许可已经推送！");
                }
                logger.info("----->service2");
                for (TBBuilderLicenceManageChangeDto item : dto.getTbbuilderlicencemanage_change()) {
                    BuilderLicenceManageChange entity = new BuilderLicenceManageChange();
                    try {
                        CaseInsensitiveBeanUtils.copyProperties(item, entity);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    entity.setID(sgxkMapper.getPKValue("TBBuilderLicenceManage_Change", "ID"));
                    entity.setROW_GUID(item.getRowGuid());
                    entity.setUpDateTime(new Date());
                    entity.setCreateTime(new Date());
                    entity.setUploadDate(new Date());
                    entity.setCreateDate(new Date());
                    String manageAdminAreaNum = item.getManageAdminAreaNum();
                    if (StringUtils.isBlank(manageAdminAreaNum)) {
                        entity.setManageAdminAreaNum(null);
                    } else if (manageAdminAreaNum.length() > 6) {
                        entity.setManageAdminAreaNum(manageAdminAreaNum.substring(0, 6));
                    }
                    entity.setStatusNum(3);

                    Integer a = sgxkMapper.insertDataToTBBuilderLicenceManage_Change(entity);
                    //受影响行数小于等于零
                    if (a <= 0) {
                        throw new RuntimeException("施工许可变更记录表插入数据失败");
                    }
                }
                logger.info("----->service3");
                for (ChangeDetail item : dto.getTbbuilderlicencemanage_changedetails()) {
                    ChangeDetailsEntity entity = new ChangeDetailsEntity();
                    try {
                        CaseInsensitiveBeanUtils.copyProperties(item, entity);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    entity.setId(sgxkMapper.getPKValue("tbbuilderlicencemanage_changedetails", "ID"));
                    entity.setRow_Guid(UUID.randomUUID().toString());
                    Integer a = sgxkMapper.insertDataTotbbuilderlicencemanage_changedetails(entity);
                    if (a <= 0) {
                        throw new RuntimeException("施工许可变更详细记录表插入数据失败");
                    }
                }
                logger.info("----->service4");
                for (TBProjectInfoChangeDto item : dto.getTbprojectinfo_change()) {
                    TBProjectInfoChange entity = new TBProjectInfoChange();
                    try {
                        CaseInsensitiveBeanUtils.copyProperties(item, entity);
                    } catch (Exception e) {

                    }
                    entity.setID(sgxkMapper.getPKValue("TBProjectInfo_Change", "ID"));
                    entity.setROW_GUID(PrjGuid);
                    entity.setChangeGuid(changeGuid);
                    Integer a = sgxkMapper.insertDataToTBProjectInfo_Change(entity);
                    if (a <= 0) {
                        throw new RuntimeException("tbprojectinfo_change插入数据失败");
                    }
                }
                logger.info("----->service5");
                for (TBBuilderLicenceUnitInfoChangeDto item : dto.getTbbuilderlicenceunitinfo_change()) {
                    TBBuilderLicenceUnitInfoChange entity = new TBBuilderLicenceUnitInfoChange();
                    try {
                        CaseInsensitiveBeanUtils.copyProperties(item, entity);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    entity.setID(sgxkMapper.getPKValue("TBBuilderLicenceUnitInfo_Change", "ID"));
                    entity.setRow_Guid(item.getGuid());
                    entity.setToRowGuid(item.getRowguid());
                    entity.setUnitCode(item.getUnitCode() == null ? null : PrjNum + item.getUnitCode());
                    Integer a = sgxkMapper.insertDataToTBBuilderLicenceUnitInfo_Change(entity);
                    if (a <= 0) {
                        throw new RuntimeException("TBBuilderLicenceUnitInfo_Change 插入数据失败");
                    }
                }
                logger.info("----->service6");
                for (TBSFCorpInfoChange item : dto.getTbsfcorpinfo_change()) {
                    TBSFCorpInfoChange entity = new TBSFCorpInfoChange();
                    try {
                        CaseInsensitiveBeanUtils.copyProperties(item, entity);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    entity.setId(sgxkMapper.getPKValue("tbsfcorpinfo_change", "ID"));
                    entity.setToRowGuid(item.getRow_Guid());
                    entity.setChangeGuid(changeGuid);
                    Integer a = sgxkMapper.insertDataToTBBuilderLicenceUnitInfo_Change(entity);
                    if (a <= 0) {
                        throw new RuntimeException("tbsfcorpinfo_change 插入数据失败");
                    }
                }
                logger.info("----->service7");
                for (TBBuilderLicenceManageChangeDto item : dto.getTbbuilderlicencemanage_change()) {
                    TBBuilderLicenceManage_New entity = new TBBuilderLicenceManage_New();
                    try {
                        CaseInsensitiveBeanUtils.copyProperties(item, entity);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    entity.setRow_Guid(sgxkGuid);
                    //区县代码，截取前面六位
                    if (StringUtils.isNotBlank(entity.getManageAdminAreaNum()) && entity.getManageAdminAreaNum().length() > 6) {
                        entity.setManageAdminAreaNum(entity.getManageAdminAreaNum().substring(0, 6));
                    }
                    Integer a = sgxkMapper.updateTBBuilderLicenceManage_ChangeByCondition(entity, "row_Guid");
                    if (a <= 0) {
                        throw new RuntimeException("TBBuilderLicenceManage_Change 修改数据失败");
                    }
                }
                logger.info("----->service8");
                for (TBProjectInfoChangeDto item : dto.getTbprojectinfo_change()) {
                    TBProjectInfoChange entity = new TBProjectInfoChange();
                    try {
                        CaseInsensitiveBeanUtils.copyProperties(item, entity);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    entity.setROW_GUID(PrjGuid);
                    Integer a = sgxkMapper.updateTBPROJECTINFO_NEWByCondition(entity, "ROW_GUID");
                    if (a <= 0) {
                        throw new RuntimeException("TBPROJECTINFO_NEW 修改数据失败");
                    }
                }
                logger.info("----->service9");
                List<Record> dt = sgxkMapper.selectDataFromTBBuilderLicenceUnitInfo_NewBysgxkGuid(sgxkGuid);
                if (dt.size() > 0) {
                    List<String> UnitCode = new ArrayList<>();
                    List<String> ROW_GUID = new ArrayList<>();

                    for (TBBuilderLicenceUnitInfoChangeDto item : dto.getTbbuilderlicenceunitinfo_change()) {
                        Integer selnum = sgxkMapper.getCountTBBuilderLicenceUnitInfo_NewByRowGuid(item.getGuid());
                        UnitCode.add(item.getUnitCode());
                        ROW_GUID.add(item.getGuid());
                        if (selnum > 0) {
                            TBBuilderLicenceUnitInfo_New entity = new TBBuilderLicenceUnitInfo_New();
                            try {
                                CaseInsensitiveBeanUtils.copyProperties(item, entity);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            entity.row_Guid = item.getGuid();
                            Integer a = sgxkMapper.updateTBBuilderLicenceUnitInfo_NewByRowGuid(entity, "row_Guid");
                            if (a <= 0) {
                                throw new RuntimeException("TBBuilderLicenceUnitInfo_New 修改数据失败");
                            }
                        } else {
                            TBBuilderLicenceUnitInfo_New entity = new TBBuilderLicenceUnitInfo_New();
                            entity.unitCode = item.getUnitCode() == null ? null : PrjNum + item.getUnitCode();
                            try {
                                CaseInsensitiveBeanUtils.copyProperties(item, entity);
                            } catch (Exception e) {

                            }
                            Integer a = sgxkMapper.insertTBBuilderLicenceUnitInfo_New(entity);
                            if (a <= 0) {
                                throw new RuntimeException("TBBuilderLicenceUnitInfo_New 插入数据失败");
                            }
                        }
                    }
                    logger.info("----->service10");
                    if (!ROW_GUID.isEmpty()) {
                        StringBuilder parms = new StringBuilder();
                        for (String s : ROW_GUID) {
                            parms.append("'").append(s).append("',");
                        }
                        //移除掉最后一个逗号
                        parms.setLength(parms.length() - 1);
                        List<Record> records = sgxkMapper.selectDataFromTBBuilderLicenceUnitInfo_New(sgxkGuid, parms.toString());
                        for (Record item : records) {
                            sgxkMapper.deleteTBBuilderLicenceUnitInfo_NewByRowGuid(item.getString("ROW_GUID"));
                        }
                    }
                }
                logger.info("3.0变更 TBBuilderLicenceManage——语句前");
                for (TBBuilderLicenceManageChangeDto item : dto.getTbbuilderlicencemanage_change()) {
                    TBBuilderLicenceManage entity = new TBBuilderLicenceManage();
                    try {
                        CaseInsensitiveBeanUtils.copyProperties(item, entity);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String tempStr = item.getManageAdminAreaNum();
                    if (StringUtils.isNotBlank(tempStr) && tempStr.length() > 6) {
                        entity.setManageAdminAreaNum(tempStr.substring(0, 6));
                    }
                    entity.setRow_Guid(sgxkGuid);
                    Integer a = sgxkMapper.updateTBBUILDERLICENCEMANAGEByRowGuid(entity, "row_Guid");
                    if (a <= 0) {
                        throw new RuntimeException("TBBuilderLicenceManage 修改数据失败");
                    }
                }

                for (TBProjectInfoChangeDto item : dto.getTbprojectinfo_change()) {
                    TBProjectInfo entity = new TBProjectInfo();
                    try {
                        CaseInsensitiveBeanUtils.copyProperties(item, entity);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    entity.setRow_Guid(PrjGuid);
                    Integer a = sgxkMapper.updateTableByCondition(entity, "row_Guid", "DMSTAND.tbprojectinfo_change");
                    if (a <= 0) {
                        throw new RuntimeException("tbprojectinfo_change 修改数据失败");
                    }
                }

                dt = sgxkMapper.getDataFromTBBuilderLicenceUnitInfoBySgxkGuid(sgxkGuid);

                if (dt.size() > 0) {
                    List<String> UnitCode = new ArrayList<>();
                    List<String> ROW_GUID = new ArrayList<>();

                    for (TBBuilderLicenceUnitInfoChangeDto item : dto.getTbbuilderlicenceunitinfo_change()) {
                        Integer selnum = sgxkMapper.getCountTBBuilderLicenceUnitInfoByRowGuid(item.getGuid());
                        UnitCode.add(item.getUnitCode());
                        ROW_GUID.add(item.getGuid());

                        if (selnum > 0) {
                            TBBuilderLicenceUnitInfo entity = new TBBuilderLicenceUnitInfo();
                            try {
                                CaseInsensitiveBeanUtils.copyProperties(item, entity);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            entity.setRow_Guid(sgxkGuid);
                            Integer a = sgxkMapper.updateTableByCondition(entity, "ROW_GUID", "DMSTAND.TBBuilderLicenceUnitInfo");
                            if (a <= 0) {
                                throw new RuntimeException("TBBuilderLicenceUnitInfo 修改数据失败");
                            }
                        } else {
                            TBBuilderLicenceUnitInfo entity = new TBBuilderLicenceUnitInfo();
                            entity.setId(sgxkMapper.getPKValue("TBBuilderLicenceUnitInfo", "ID"));
                            try {
                                CaseInsensitiveBeanUtils.copyProperties(item, entity);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            entity.setRow_Guid(item.getGuid());
                            entity.setUnitCode(item.getUnitCode() == null ? null : PrjNum + item.getUnitCode());
                            entity.setSgxkGuid(item.getSgxkGuid());
                            Integer a = sgxkMapper.insertTable(entity, "DMSTAND.TBBuilderLicenceUnitInfo");
                            if (a <= 0) {
                                throw new RuntimeException("TBBuilderLicenceUnitInfo 插入数据失败");
                            }
                        }
                    }
                    if (!ROW_GUID.isEmpty()) {
                        StringBuilder parms = new StringBuilder();
                        for (String s : ROW_GUID) {
                            parms.append("'").append(s).append("',");
                        }
                        parms.setLength(parms.length() - 1);
                        dt = sgxkMapper.getDataFromTBBuilderLicenceUnitInfoBySgxkGuid(sgxkGuid, parms.toString());
                        if (dt.size() > 0) {
                            for (Record item : dt) {
                                sgxkMapper.deleteTBBuilderLicenceUnitInfoByRowGuid(item.getString("ROW_GUID"));
                            }
                        }
                    }
                }
                logger.info("3.0变更,执行四方主体前");
                //拼接model
                LockInfoModel model = new LockInfoModel();

                model.CityNum = CityType;
                model.CountyNum = dto.getTbprojectinfo_change().get(0).getCountyNum();
                model.ManageCityNum = CityType;//主管部门所属地市
                model.ManageCountyNum = dto.getTbprojectinfo_change().get(0).getCountyNum();//主管部门所属区县
                dt = sgxkMapper.getDataFromTBSFCorpInfo_NewBySgxk(sgxkGuid);
                for (Record item : dt) {
                    //施工 经理
                    if ("3".equals(item.getString("CorpTypeNum"))) {
                        model.CorpName = item.getString("CorpName");//企业名称
                        model.PersonName = item.getString("CorpLeader");
                        jl2[0].set(lockHelper.Un_LockInfo(item.getString("CorpLeaderCardNum"), sgxkGuid, CityType, CityType, "", "1", prjname, item.getString("CorpCode"),
                                TypeState, model, strErrMsg[0]).getResult());//解锁旧的
                        if (!jl2[0].get()) {
                            throw new RuntimeException("TBBuilderLicenceUnitInfo 插入数据失败");
                        }
                    }

                    //监理 总监
                    if ("4".equals(item.getString("CorpTypeNum"))) {
                        model.CorpName = item.getString("CorpName");//企业名称
                        model.PersonName = item.getString("CorpLeader");
                        //人员姓名
                        zj1[0] = lockHelper.Un_LockInfo(item.getString("CorpLeaderCardNum"), sgxkGuid, CityType, CityType, "项目总监变更", "2", prjname, oldXMZJcorpcode, TypeState, model, strErrMsg[0]).getResult();//解锁旧的
                        logger.info("3.0变更,监理总监" + strErrMsg[0]);
                        if (!zj1[0]) {
                            strErrMsg[0] += strErrMsg[0];
                            throw new RuntimeException("监理 总监 关锁失败");
                        }

                    }
                }

                //根据 施工许可guid的 _new 的四方主体 条数>0 先删除后插入
                if (dt.size() > 0) {
                    sgxkMapper.deleteTBSFCorpInfo_NewBySgxkGuid(sgxkGuid);
                    for (TBSFCorpInfoChange item : dto.getTbsfcorpinfo_change()) {
                        //施工 经理
                        if ("3".equals(item.getCorpTypeNum())) {
                            model.CorpName = item.getCorpName();//企业名称
                            model.PersonName = item.getCorpLeader();
                            jl_lock2[0] = lockHelper.LockInfo(item.getCorpLeaderCardNum(), sgxkGuid, CityType, CityType,
                                    "", "1", prjname, item.getCorpCode(),
                                    TypeState, model, strErrMsg[0]).getResult();//锁定新的
                            if (!jl_lock2[0]) {
                                strErrMsg[0] += strErrMsg[0];
                                throw new RuntimeException("施工 经理 锁定失败");
                            }
                        }

                        //监理 总监
                        if ("4".equals(item.getCorpTypeNum())) {
                            model.CorpName = item.getCorpName();//企业名称
                            model.PersonName = item.getCorpLeader();//人员姓名
                            zj_lock1[0] = lockHelper.LockInfo(item.getCorpLeaderCardNum(), sgxkGuid, CityType, CityType,
                                    "", "2", prjname, item.getCorpCode(),
                                    TypeState, model, strErrMsg[0]).getResult();//锁定新的
                            if (!zj_lock1[0]) {
                                strErrMsg[0] += strErrMsg[0];
                                throw new RuntimeException("监理 总监 锁定失败");
                            }
                        }

                        TBSFCorpInfoNew entity = new TBSFCorpInfoNew();
                        try {
                            CaseInsensitiveBeanUtils.copyProperties(item, entity);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        entity.id = sgxkMapper.getPKValue("tbsfcorpinfo_new", "ID");
                        Integer a = sgxkMapper.insertTBSFCorpInfo_New(entity);
                        if (a <= 0) {
                            throw new RuntimeException("TBSFCorpInfo_New 数据插入失败");
                        }
                    }
                }

                dt = sgxkMapper.getDataFromTBSFCorpInfoBySgxk(sgxkGuid);
                //根据 施工许可guid的 _new 的四方主体 条数>0 先删除后插入
                if (dt.size() > 0) {
                    sgxkMapper.delTBSFCorpInfoBySgxkGuid(sgxkGuid);
                    for (TBSFCorpInfoChange item : dto.getTbsfcorpinfo_change()) {
                        TBSFCorpInfoChange entity = new TBSFCorpInfoChange();

                        try {
                            CaseInsensitiveBeanUtils.copyProperties(item, entity);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        entity.setId(sgxkMapper.getPKValue("tbsfcorpinfo_change", "ID"));
                        Integer a = sgxkMapper.insertTable(entity, "DMSTAND.tbsfcorpinfo_change");
                        if (a <= 0) {
                            throw new RuntimeException("tbsfcorpinfo_change 数据插入失败");
                        }
                    }
                }
                logger.info("3.0变更 执行完成");
            });
        } catch (Exception e) {
            e.printStackTrace();
            return RetMsgUtil.fail(State.OPERATION_FAILED.getCode(), e.getMessage());
        }
        return RetMsgUtil.ok();
    }

    /**
     * @param dto
     * @return
     */
    @Override
    public RetMsgUtil<Object> InsertSKByQZ_Manager_New(ISKBQZMNDTO dto) {
        try {
            Trans.exec(() -> {
                //字段定义
                String IsTsFG = dto.getIsTsFG();
                String CityType = dto.getCityType();
                String strErrMsg = "";
                boolean jl_lock1 = true; //锁定
                boolean zj_lock1 = true;
                String sgxkguid = "";
                LockInfoModel model = new LockInfoModel();
                String idcard = "";//身份证
                String UnLockMark = "";
                String prjName = "";
                String corpcode = "";
                int TypeState = 17;//衢州系统施工许可信息接口入库
                String htGuid = "";
                String builderlicencenum = "";
                //0项目基本信息 1 合同备案  2 招投标 3 施工许可
                if (!validateNewForJBXX(dto.getTbprojectinfo()) || !validateNewForSGXK(dto.getTbbuilderlicencemanage())) {
                    throw new RuntimeException("3.0施工许可工程项目新项目验证失败");
                }
                //施工许可表
                TBBuilderLicenceManageDto drSgxk = dto.getTbbuilderlicencemanage().get(0);
                List<Record> record = sgxkMapper.getRowGuidFromTbbuilderlicencemanageByliceNum(drSgxk.getBuilderLicenceNum());
                if (record != null && !record.isEmpty()) {
                    throw new RuntimeException("施工许可证存在重复");
                }

                builderlicencenum = drSgxk.getBuilderLicenceNum();

                //锁定相关人员
                String strManageDepUserName = "";
                List<Record> dt_City = sgxkMapper.getTbxzqdmdicInfoByCityType(CityType);
                if (dt_City != null && !dt_City.isEmpty()) {
                    strManageDepUserName = dt_City.get(0).getString("adminareaname");
                }
                sgxkguid = dto.getTbbuilderlicencemanage().get(0).getGuid();
                prjName = dto.getTbbuilderlicencemanage().get(0).getPrjName();
                //判断是否有在建项目 施工
                String[] consCorpNames = drSgxk.getConscorpcode().split("；");
                String[] consCorpCodes = drSgxk.getConscorpcode().split("；");
                String[] consCorpLeaders = drSgxk.getConscorpleader().split("；");
                String[] consCorpLeaderCardNums = drSgxk.getConscorpleadercardnum().split("；");

                String[] superCorpNames = drSgxk.getSupercorpname().split("；");
                String[] superCorpCodes = drSgxk.getSuperCorpCode().split("；");
                String[] superCorpLeaders = drSgxk.getSupercorpleader().split("；");
                String[] superCorpLeaderCardNums = drSgxk.getSupercorpleadercardnum().split("；");


                model.ManageAdminAreaNum = dto.getTbprojectinfo().get(0).getManageAdminAreaNum();

                model.CityNum = CityType;
                model.CountyNum = dto.getTbprojectinfo().get(0).getCountyNum();
                model.ManageCityNum = CityType;
                model.ManageCountyNum = "-1";
                //施工锁定
                for (int i = 0; i < consCorpLeaderCardNums.length; i++) {
                    if ("".equals(consCorpLeaderCardNums[i])) continue;
                    corpcode = consCorpCodes[i];
                    idcard = consCorpLeaderCardNums[i];
                    //锁定项目经理
                    if (!StringUtils.isAnyBlank(idcard)) {
                        //企业名称
                        model.CorpName = consCorpNames[i];
                        //人员姓名
                        model.PersonName = consCorpLeaders[i];
                        jl_lock1 = lockHelper.LockInfo(idcard, sgxkguid, strManageDepUserName, CityType, UnLockMark, "1", prjName, corpcode, TypeState, model, strErrMsg).getResult();
                        logger.info("锁定项目经理:" + idcard + "--" + strErrMsg);
                    }
                }

                //监理锁定
                for (int i = 0; i < superCorpLeaderCardNums.length; i++) {
                    if ("".equals(superCorpLeaderCardNums[i])) continue;
                    corpcode = superCorpCodes[i];//企业组织机构代码
                    idcard = superCorpLeaderCardNums[i];
                    //锁定项目总监
                    if (!StringUtils.isAnyBlank(idcard)) {
                        model.CorpName = superCorpNames[i];//企业名称
                        model.PersonName = superCorpLeaders[i];//人员姓名
                        zj_lock1 = (lockHelper.LockInfo(idcard, sgxkguid, strManageDepUserName, CityType, UnLockMark, "2", prjName, corpcode, TypeState, model, strErrMsg).getResult());
                        logger.info("锁定项目经理:" + idcard + "--" + strErrMsg);
                    }
                }

                if (!(jl_lock1 && zj_lock1)) {
                    logger.error("人员锁定出错:" + idcard);
                    throw new RuntimeException("人员锁定出错:idcard");
                }

                //ds中 四个table 直接入 _new表 // 无TBProjectInfo
                for (TBBuilderLicenceManageDto item : dto.getTbbuilderlicencemanage()) {
                    TBBuilderLicenceManageNew params = new TBBuilderLicenceManageNew();
                    //将item内的值赋值给params 忽略大小写
                    try {
                        CaseInsensitiveBeanUtils.copyProperties(item, params);
                    } catch (Exception e) {
                        throw new RuntimeException("对象复制出现错误，TBBuilderLicenceManageNew");
                    }
                    params.setID(sgxkMapper.getPKValue("TBBuilderLicenceManage_New", "ID"));
                    params.setROW_GUID(item.getGuid());

                    Integer count = sgxkMapper.getTBBuilderLicenceManage_NewCountByRowGuid(item.getGuid());
                    if (count == null || count < 1) {
                        int i = sgxkMapper.insertDataToTBBuilderLicenceManageNew(params);
                        //受影响行数小于等于零
                        if (i <= 0) {
                            throw new RuntimeException("插入数据失败，受影响行数小于等于零->code:592");
                        }
                    }
                }
                //Guid AS ROW_GUID  econtypeNum
                logger.info("3.0新申请" + "tbprojectinfo_New前");
                int jectnum;
                for (TBProjectInfoDto item : dto.getTbprojectinfo()) {
                    TBProjectInfo_New tbProjectInfo_new = new TBProjectInfo_New();
                    try {
                        CaseInsensitiveBeanUtils.copyProperties(item, tbProjectInfo_new);
                    } catch (Exception e) {
                        throw new RuntimeException("对象复制出现错误，TBProjectInfo_New");
                    }
                    tbProjectInfo_new.setId(sgxkMapper.getPKValue("tbprojectinfo_New", "ID"));
                    tbProjectInfo_new.setRow_Guid(item.getGuid());
                    jectnum = sgxkMapper.getTbprojectinfo_NewCountByRowGuid(item.getGuid());
                    if (jectnum < 1) {
                        int a = sgxkMapper.insertDataToTbprojectinfo_New(tbProjectInfo_new);
                        //受影响行数小于等于零
                        if (a <= 0) {
                            throw new RuntimeException("插入数据失败，受影响行数小于等于零->code:592");
                        }
                    }
                }
                logger.info("3.0新申请 ,TBBuilderLicenceUnitInfo_New前");
                int UnitInfonum;
                for (TBBuilderLicenceUnitInfoDto item : dto.getTbbuilderlicenceunitinfo()) {

                    TBBuilderLicenceUnitInfo_New tbBuilderLicenceUnitInfo_new = new TBBuilderLicenceUnitInfo_New();
                    try {
                        CaseInsensitiveBeanUtils.copyProperties(item, tbBuilderLicenceUnitInfo_new);
                    } catch (Exception e) {
                        throw new RuntimeException("对象复制出现错误，TBSFCorpInfoNew");
                    }
                    tbBuilderLicenceUnitInfo_new.id = sgxkMapper.getPKValue("TBBuilderLicenceUnitInfo_New", "ID");
                    UnitInfonum = sgxkMapper.getTBBuilderLicenceUnitInfo_NewCountByRowGuid(item.getGuid());
                    if (UnitInfonum < 1) {
                        Integer a = sgxkMapper.insertTBBuilderLicenceUnitInfo_New(tbBuilderLicenceUnitInfo_new);
                        //受影响行数小于等于零
                        if (a <= 0) {
                            throw new RuntimeException("插入数据失败，受影响行数小于等于零->code:660");
                        }
                    }
                }
                logger.info("3.0新申请" + "TBSFCorpInfo_New前");
                int sfcorpnum;
                sfcorpnum = sgxkMapper.getCountTbsfcorpinfo_newByRowGuid(dto.getTbsfcorpinfo_new().get(0).getSgxkGuid());
                if (sfcorpnum < 1) {
                    for (TBSFCorpInfoNewDto item : dto.getTbsfcorpinfo_new()) {
                        TBSFCorpInfoNew tbsfCorpInfo_new = new TBSFCorpInfoNew();
                        try {
                            CaseInsensitiveBeanUtils.copyProperties(item, tbsfCorpInfo_new);
                        } catch (Exception e) {
                            throw new RuntimeException("对象复制出现错误，TBSFCorpInfoNew");
                        }
                        tbsfCorpInfo_new.id = sgxkMapper.getPKValue("tbsfcorpinfo_new", "ID");
                        tbsfCorpInfo_new.row_Guid = item.getGuid();

                        Integer a = sgxkMapper.insertTBSFCorpInfo_New(tbsfCorpInfo_new);
                        if (a <= 0) {
                            throw new RuntimeException("插入数据失败，受影响行数小于等于零->code:681");
                        }
                    }
                }

                //存储发改委对接进度数据
                TBProjectInfoDto li = dto.getTbprojectinfo().get(0);
                if ("".equals(li.getLegalName()) || li.getBuildCorpCode().length() != 18) {
                    //是否推送发改2.0平台：1推送受理信息，2不推送受理信息，0不推送
                    // 不正常的数据，直接给pre_sgxk_clqk表里的istb字段存4，方便查找哪些数据有问题
                    if (!"0".equals(IsTsFG)) {
                        sgxkMapper.insertPre_sgxk_clqk(sgxkguid, "1", "4");
                        //推送受理信息
                        if ("1".equals(IsTsFG)) {
                            //受理信息
                            sgxkMapper.insertPre_sgxk_clqk(sgxkguid, "0", "4");
                        }
                    }
                } else {
                    //正常完整的数据正常存库
                    if (!"0".equals(IsTsFG)) {
                        //是否推送发改2.0平台：1推送受理信息，2不推送受理信息，0不推送
                        sgxkMapper.insertPre_sgxk_clqk(sgxkguid, "1", "0");
                        //推送受理信息
                        if (IsTsFG.equals("1")) {
                            //受理信息
                            sgxkMapper.insertPre_sgxk_clqk(sgxkguid, "0", "0");
                        }
                    }
                }

                logger.info("3.0新申请插入完成-------" + builderlicencenum);
            });
        } catch (Exception e) {
            e.printStackTrace();
            return RetMsgUtil.fail(State.OPERATION_FAILED.getCode(), e.getMessage());
        }
        return RetMsgUtil.ok(State.OPERATE_SUCCESS.getCode(), State.OPERATE_SUCCESS.getMsg());
    }

    //项目基本信息 type = 0
    private boolean validateNewForJBXX(List<TBProjectInfoDto> list) {

        //项目基本信息验证方法
        if ("1".equals(list.get(0).getSFXW())) {
            String[] yzzd = {"项目名称|PrjName", "项目分类|PrjTypeNum", "项目所在地市|CityNum", "项目所在区县|CountyNum", "项目地点|ADDRESS",
                    "建设单位名称|BuildCorpName", "建设单位地址|BUILDCORPADDRESS", "法定代表人|legalName"};

            for (TBProjectInfoDto dr : list) {
                for (String s : yzzd) {
                    String[] zd = s.split("\\|");
                    if (("02".equals(dr.getPrjTypeNum()) || "03".equals(dr.getPrjTypeNum())) && "AllArea".equals(zd[1])) {
                        continue;
                    }
                    if (StringUtils.isAnyBlank(dr.getPrjTypeNum(), dr.getCityNum(), dr.getCountyNum(), dr.getAddress(), dr.getBuildCorpName(), dr.getBuildCorpAddress(), dr.getLegalName())) {
                        return false;
                    }
                }
            }
        } else {
            String[] yzzd = {"项目代码|PRJCODE", "项目名称|PrjName", "项目分类|PrjTypeNum", "项目所在地市|CityNum", "项目所在区县|CountyNum",
                    "项目地点|ADDRESS", "建设单位名称|BuildCorpName", "建设单位地址|BUILDCORPADDRESS", "法定代表人|legalName"};

            for (TBProjectInfoDto dr : list) {
                for (String s : yzzd) {
                    String[] zd = s.split("\\|");
                    if (("02".equals(dr.getPrjTypeNum()) || "03".equals(dr.getPrjTypeNum())) && "AllArea".equals(zd[1])) {
                        continue;
                    }

                    if (StringUtils.isAnyBlank(dr.getPrjCode(), dr.getPrjTypeNum(), dr.getCityNum(), dr.getCountyNum(), dr.getAddress(), dr.getBuildCorpName(), dr.getBuildCorpAddress(), dr.getLegalName())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    //type = 3 施工许可
    private boolean validateNewForSGXK(List<TBBuilderLicenceManageDto> list) {
        String[] yzzd = {"工程名称|PrjName",
                "施工许可证编号|BuilderLicenceNum",
                "合同金额（万元）|ContractMoney",
                "建设规模|PrjSize",
                "合同工期（天）|BARGAINDAYS",
                "施工单位|ConsCorpName",
                "项目经理|CONSCORPLEADER",
                "项目经理身份证号码|CONSCORPLEADERCARDNUM"
        };
        for (TBBuilderLicenceManageDto dr : list) {
            for (String s : yzzd) {
                String[] zd = s.split("\\|");
                if (StringUtils.isAnyBlank(dr.getPrjName(), dr.getBuilderLicenceNum(), dr.getPrjSize(), dr.getBargainDays(),
                        dr.getConscorpname(), dr.getConscorpleader(), dr.getConscorpleadercardnum()) || dr.getContractMoney() == null) {
                    return false;
                }
            }
        }
        return true;
    }


    public RetMsgUtil<Object> UploadImageInfo_QZ(String strYeWuID, String dto) {
        DataSet ds = ChangeJson.changeVoToDataSet(dto);

        if (ds.isEmpty()) {
            return RetMsgUtil.fail("二进制转化为数据表失败");
        }
        try {
            if (sgxkMapper.getCountFrom(strYeWuID) < 1) {
                return RetMsgUtil.fail("没有找到对应的施工许可信息");
            }
            //替换附件类型
            for (DataRow item : ds.getTable("tbHuanJieMaterialInfo")) {
                String title = item.getString("title");
                if (AnnexMapping.titleMap.containsKey(title)) {
                    item.put("title", AnnexMapping.titleMap.get(title));
                }
            }

            //扫描件，明细表
            for (DataRow dr : ds.getTable("tbHuanJieMaterialInfo")) {
                String groupguid = dr.getString("title");
                sgxkMapper.deleteSYS_ECAFILESByTO_ROW_GUID(strYeWuID);

                HashMap<String, Object> paramsMap = new HashMap<>();
                paramsMap.put("AFFIXID", sgxkMapper.getPKValue("SYS_ECAFILES", "AFFIXID"));
                paramsMap.put("CREATEUSERID", "-1");
                paramsMap.put("FILENAME", dr.getString("filezname"));
                paramsMap.put("FILETYPE", dr.getString("fileext"));
                paramsMap.put("FILESIZE", dr.getString("filesize"));
                paramsMap.put("VIRTUALPATH", dr.getString("serverfilename"));
                paramsMap.put("ACTUALNAME", dr.getString("filename"));
                paramsMap.put("ROW_GUID", dr.getString("ID"));
                paramsMap.put("GROUPGUID", groupguid);
                paramsMap.put("REMARK", null);
                paramsMap.put("ISCRYPTED", "0");
                paramsMap.put("ISDELETED", null);
                paramsMap.put("UPLOADUSERID", null);
                paramsMap.put("UPLOADUSERNAME", null);
                paramsMap.put("FILEGUID", null);
                paramsMap.put("TO_ROW_GUID", strYeWuID);

                LocalDateTime now = LocalDateTime.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                paramsMap.put("CREATEDATE", now.format(formatter));
                sgxkMapper.insertSYS_ECAFILES(paramsMap);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return RetMsgUtil.fail(State.OPERATION_FAILED.getCode(), e.getMessage());
        }
        return RetMsgUtil.ok();
    }
}
