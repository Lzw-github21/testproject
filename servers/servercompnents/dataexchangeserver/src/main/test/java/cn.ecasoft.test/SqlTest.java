package cn.ecasoft.test;

import cn.ecasoft.App_DataexchangeServer;
import cn.ecasoft.basic.datatable.DataTable;
import cn.ecasoft.dataexchange.config.DbHelperExtend;
import cn.ecasoft.dataexchange.entity.enums.SqlRepos;
import cn.ecasoft.utils.DBhelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nutz.dao.Sqls;
import org.nutz.dao.entity.Record;
import org.nutz.dao.sql.Sql;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Dee
 * @date 2023/7/10
 * <p>Description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = App_DataexchangeServer.class)
public class SqlTest {
    @Resource
    private DBhelper dBhelper;
    @Resource
    private DbHelperExtend dbHelperExtend;

    @Test
    public void test01() throws Exception {
        DataTable dataRows = dBhelper.QueryDataTable("", SqlRepos.TEST_SQL.name(), null);
        System.out.println(dataRows);
    }

    @Test
    public void test02() {
        Sql sql = Sqls.create(SqlRepos.TEST_SQL.sql);
        List<Record> records = dbHelperExtend.queryList("", sql);
        System.out.println(records);
    }
}
