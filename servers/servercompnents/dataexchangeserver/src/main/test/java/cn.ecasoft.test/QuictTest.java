package cn.ecasoft.test;

import cn.ecasoft.dataexchange.entity.enums.SqlRepos;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Dee
 * @date 2023/7/10
 * <p>Description:
 */
@RunWith(JUnit4.class)
public class QuictTest {

    @Test
    public void test02() {
        String abc = "aaa;bbb;ccc;";
        String[] split = abc.split(";", -1);
        Arrays.stream(split).forEach(System.out::println);
    }
    @Test
    public void test01() {
        Map<String, String> collect = Arrays
                .stream(SqlRepos.values())
                .distinct()
                .collect(Collectors.toMap(Enum::name, en -> en.sql));
        System.out.println(collect);

    }
}
